function notification_success(msg) {
	$.Toast(msg, "", "success", {
        has_icon:true,
        has_close_btn:true,
		stack: true,
        fullscreen:false,
        timeout:3000,
        sticky:false,
        has_progress:true,
        rtl:false,
    });
}

function notification_error(msg) {
	$.Toast(msg, "", "error", {
        has_icon:true,
        has_close_btn:true,
		stack: true,
        fullscreen:false,
        timeout:3000,
        sticky:false,
        has_progress:true,
        rtl:false,
    });
}