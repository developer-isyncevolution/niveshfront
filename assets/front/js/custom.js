
    AOS.init();
    $(document).ready(function () {
      $(".owl-carousel").owlCarousel();
    });

    
    $(window).scroll(function(){
      if ($(window).scrollTop() >= 80) {
          $('header').addClass('fixed-header');
          $('.navbar-brand').addClass('logo');
      }
      else {
          $('header').removeClass('fixed-header');
          $('.navbar-brand').removeClass('logo');
      }
    });
    
    
    
    
    $(document).ready(function(){
        jQuery(window).scroll(function(){
          if($(this).scrollTop() > 100){
            $('#back-to-top').fadeIn();
          }else{
            $('#back-to-top').fadeOut();
          }
        });
        $('#back-to-top').click(function(){
          $("html, body").animate({ scrollTop: 0 }, 600);
          return false;
        });
      });
      
      $('.quality_product').owlCarousel({
        loop:true,
        autoplay:true,
        // autoplayTimeout:3000,
        // autoplayHoverPause:true,
        margin:35,
        // dots:Boolean,
        responsiveClass:true,
        responsive:{
          0:{
            items:1,
          },
          576:{
            items:2,
          },
          993:{
            items:3,
          },
          1200:{
            items:3,
          },
          1400:{
            items:4,
            autoplay:false,
          },
          1920:{
            items:4,
            autoplay:false,
          }
        }
      })