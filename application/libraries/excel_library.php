<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once BASEPATH.'../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class Excel_library
{
    function __construct($config = array())
    {
    }

    function create($criteria = array())
    {
    	$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($criteria["data"],NULL,'A1');
        $spreadsheet->getActiveSheet()->setTitle($criteria["title"]);
        $writer = new Xlsx($spreadsheet);
        if(file_exists($criteria["path"]))
        {
            unlink($criteria["path"]);
        }
        $writer->save($criteria["path"], 'F');
    }
}
