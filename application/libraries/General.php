<?php defined('BASEPATH') OR exit('No direct script access allowed');

class General
{
    function __construct($config = array())
    {
    }

    function authentication()
    {
        $CI =& get_instance();
        $iUserId = $CI->session->userdata('iUserId');
        if(empty($iUserId))
        {
            redirect(base_url('admin/login'));
        }
    }

    function company_info()
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('setting/setting_model');
        return $CI->setting_model->get_setting('Company');
    }

    function general_info()
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('setting/setting_model');
        return $CI->setting_model->get_setting('Appearance');
    }

    function social_info()
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('setting/setting_model');
        return $CI->setting_model->get_setting('Social');
    }

    function email_info()
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('setting/setting_model');
        return $CI->setting_model->get_setting('Email');
    }

    function payment_info()
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('setting/setting_model');
        return $CI->setting_model->get_setting('Payment');
    }

    function get_state_name($iStateId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('state/state_model');
        $data = $CI->state_model->get_by_id($iStateId);
        return $data->vState;
    }

    function get_state_code($iStateId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('state/state_model');
        $data = $CI->state_model->get_by_id($iStateId);
        return $data->vStateCode;
    }

    function get_state_id($vState = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('state/state_model');
        $data = $CI->state_model->get_by_ids($vState);
        return $data->iStateId;
    }

    function get_country_id($vCountry = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('country/country_model');
        $data = $CI->country_model->get_by_ids($vCountry);
        return $data->iCountryId;
    }

    function get_occupation_name($iOccupationId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('occupation/occupation_model');
        $criteria                   = array();
        $criteria["iOccupationId"]  = $iOccupationId;
        $data                       = $CI->occupation_model->get_by_id($criteria);
        return $data->vOccupation;
    }

    function get_occupation_code($iOccupationId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('occupation/occupation_model');
        $criteria                   = array();
        $criteria["iOccupationId"]  = $iOccupationId;
        $data                       = $CI->occupation_model->get_by_id($criteria);
        return $data->vOccupationCode;
    }

    function get_occupation_id($vOccupation = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('occupation/occupation_model');
        $criteria                   = array();
        $criteria["vOccupation"]    = $vOccupation;
        $data                       = $CI->occupation_model->get_by_id($criteria);
        return $data->iOccupationId;
    }

    function get_client_name($iClientId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('client/client_model');
        $criteria               = array();
        $criteria["iClientId"]  = $iClientId;
        $data                   = $CI->client_model->get_by_id($criteria);
        return $data->vFirstApplicant;
    }

    function get_TaxStatus($iTaxStatusId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('tax_status/tax_status_model');
        $criteria                   = array();
        $criteria["iTaxStatusId"]   = $iTaxStatusId;
        $data                       = $CI->tax_status_model->get_by_id($criteria);
        return $data->vTaxStatus;
    }

    function get_tax_status_code($iTaxStatusId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('tax_status/tax_status_model');
        $criteria                   = array();
        $criteria["iTaxStatusId"]   = $iTaxStatusId;
        $data                       = $CI->tax_status_model->get_by_id($criteria);
        return $data->vTaxStatusCode;
    }

    function get_tax_status_id($vTaxStatus = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('tax_status/tax_status_model');
        $criteria                   = array();
        $criteria["vTaxStatus"]     = $vTaxStatus;
        $data                       = $CI->tax_status_model->get_by_id($criteria);
        return $data->iTaxStatusId;
    }

    function get_incomeSlab_name($iIncomeSlabId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('income_slab/income_slab_model');
        $criteria                   = array();
        $criteria["iIncomeSlabId"]  = $iIncomeSlabId;
        $data                       = $CI->income_slab_model->get_by_id($criteria);
        return $data->vIncomeSlab;
    }

    function get_income_slab_code($iIncomeSlabId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('income_slab/income_slab_model');
        $criteria                   = array();
        $criteria["iIncomeSlabId"]  = $iIncomeSlabId;
        $data                       = $CI->income_slab_model->get_by_id($criteria);
        return $data->vIncomeSlabCode;
    }

    function get_amc_id($vAMCNumber = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('amc/amc_model');
        $criteria                   = array();
        $criteria["vAMCNumber"]     = $vAMCNumber;
        $data                       = $CI->amc_model->get_by_id($criteria);
        if(!empty($data))
        {
            return $data->iAMCId;    
        }
        else
        {
            return NULL;
        }    
    }

    function get_scheme_isin($vProductCode = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('scheme/scheme_model');
        $criteria                   = array();
        $criteria["vProductCode"]   = $vProductCode;
        $data                       = $CI->scheme_model->get_by_id($criteria);
        if(!empty($data))
        {
            return $data->vISIN;    
        }
        else
        {
            return NULL;
        }    
    }

    function get_scheme_id($vProductCode = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('scheme/scheme_model');
        $criteria                   = array();
        $criteria["vProductCode"]   = $vProductCode;
        $data                       = $CI->scheme_model->get_by_id($criteria);
        if(!empty($data))
        {
            return $data->iSchemeId;    
        }
        else
        {
            return NULL;
        }    
    }

    function send_notifiction($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('setting/setting_model');
        $CI->load->helper(array('email')); 
        $CI->load->library('email');

        $myArray                    = array();
        $myArray["eConfigType"]     = "Company";
        $company_setting            = $CI->setting_model->get_all_data($myArray);

        $myArray                    = array();
        $myArray["eConfigType"]     = "Email";
        $email_setting              = $CI->setting_model->get_all_data($myArray);

        $rights         = $company_setting["COPYRIGHTED_TEXT"]["vValue"];
        $rights         = str_replace("#CURRENT_YEAR#", date("Y"), $rights);
        if($criteria['form_type'] == 'invite')
        {
            $constant       = array('#SITE_NAME#', '#SITE_URL#','#COPYRIGHTS#');
            $value          = array($criteria['vname'],$criteria['tWebsite_name'], $rights);
        }
        else if($criteria['form_type'] == 'Niveshlife')
        {
            $constant       = array('#SITE_NAME#', '#SITE_URL#','#COPYRIGHTS#');
            $value          = array("Niveshlife","https://niveshlife.in", $rights);
        }
        else
        {
            $constant       = array('#SITE_NAME#', '#SITE_URL#','#COPYRIGHTS#');
            $value          = array($company_setting["COMPANY_NAME"]["vValue"], base_url(), $rights);
        }
        
        $stream_opts    = ["ssl" => ["verify_peer"=>false,"verify_peer_name"=>false,]];  

        if($criteria['form_type'] == 'apnanivesh_form')
        {

            $header_text    = file_get_contents(base_url('assets/uploads/email/header.html'),false, stream_context_create($stream_opts));
            $header         = str_replace("#LOGO#", base_url('assets/mf_panel/media/logos/logo.jpg'), $header_text);
            $header         = str_replace("#LOGO_EMAIL#", base_url('assets/mf_panel/media/logos/logo.jpg'), $header);
            $header         = str_replace("#BG#", base_url('assets/front/images/logo_bg.png'), $header);
            $header         = str_replace("#SITE_NAME#", $company_setting["COMPANY_NAME"]["vValue"], $header);

            $footer_text    = file_get_contents(base_url('assets/uploads/email/footer.html'),false, stream_context_create($stream_opts));
            $footer         = str_replace($constant, $value, $footer_text);
            $footer         = str_replace("#LOCATION#", base_url('assets/uploads/logo/location.png'), $footer);
            $footer         = str_replace("#CALL#", base_url('assets/uploads/logo/call.png'), $footer);
            $footer         = str_replace("#MESSAGE#", base_url('assets/uploads/logo/message.png'), $footer);
        }
        else if($criteria['form_type'] == 'invite')
        {

            $header_text    = file_get_contents(base_url('assets/uploads/email/header.html'),false, stream_context_create($stream_opts));
            $header         = str_replace("#LOGO#", base_url('assets/mf_panel/media/logos/logo.jpg'), $header_text);
            $header         = str_replace("#LOGO_EMAIL#", base_url('assets/uploads/logo/logo.png'), $header);
            $header         = str_replace("#BG#", base_url('assets/front/images/logo_bg.png'), $header);
            $header         = str_replace("#SITE_NAME#", $criteria['vname'], $header);

            $footer_text    = file_get_contents(base_url('assets/uploads/email/footer.html'),false, stream_context_create($stream_opts));
            $footer         = str_replace($constant, $value, $footer_text);
            $footer         = str_replace("#LOCATION#", base_url('assets/uploads/logo/location.png'), $footer);
            $footer         = str_replace("#CALL#", base_url('assets/uploads/logo/call.png'), $footer);
            $footer         = str_replace("#MESSAGE#", base_url('assets/uploads/logo/message.png'), $footer);
        }
        else if($criteria['form_type'] == 'Niveshlife')
        {

            $header_text    = file_get_contents(base_url('assets/uploads/email/header.html'),false, stream_context_create($stream_opts));
            $header         = str_replace("#LOGO#", base_url('assets/mf_panelmedia/logos/niveh-logo.png'), $header_text);
            $header         = str_replace("#LOGO_EMAIL#", base_url('assets/mf_panel/media/logos/niveh-logo.png'), $header);
            $header         = str_replace("#BG#", base_url('assets/front/images/logo_bg.png'), $header);
            $header         = str_replace("#SITE_NAME#", $company_setting["COMPANY_NAME"]["vValue"], $header);

            $footer_text    = file_get_contents(base_url('assets/uploads/email/footer.html'),false, stream_context_create($stream_opts));
            $footer         = str_replace($constant, $value, $footer_text);
            $footer         = str_replace("#LOCATION#", base_url('assets/uploads/logo/location.png'), $footer);
            $footer         = str_replace("#CALL#", base_url('assets/uploads/logo/call.png'), $footer);
            $footer         = str_replace("#MESSAGE#", base_url('assets/uploads/logo/message.png'), $footer);
        }
        else
        {

            $header_text    = file_get_contents(base_url('assets/uploads/email/header.html'),false, stream_context_create($stream_opts));
            $header         = str_replace("#LOGO#", base_url('assets/mf_panel/media/logos/niveh-logo.png'), $header_text);
            $header         = str_replace("#LOGO_EMAIL#", base_url('assets/mf_panel/media/logos/niveh-logo.png'), $header);
            $header         = str_replace("#BG#", base_url('assets/front/images/logo_bg.png'), $header);
            $header         = str_replace("#SITE_NAME#", $company_setting["COMPANY_NAME"]["vValue"], $header);

            $footer_text    = file_get_contents(base_url('assets/uploads/email/footer.html'),false, stream_context_create($stream_opts));
            $footer         = str_replace($constant, $value, $footer_text);
            $footer         = str_replace("#LOCATION#", base_url('assets/uploads/logo/location.png'), $footer);
            $footer         = str_replace("#CALL#", base_url('assets/uploads/logo/call.png'), $footer);
            $footer         = str_replace("#MESSAGE#", base_url('assets/uploads/logo/message.png'), $footer);
        }
        
        if($criteria["vCode"] == "ACCOUNT_INFORMATION_CLIENT")
        {
            $vFinalMessage  = $criteria['vData']["message"];
        }
        else
        {
            $vFinalMessage  = $header.$criteria['vData']["message"].$footer;
        }
        
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = $email_setting["SMTP_HOST"]["vValue"];
        $config['smtp_crypto']  = 'tls';
        $config['smtp_port']    = $email_setting["SMTP_PORT"]["vValue"];
        $config['smtp_user']    = $email_setting["SMTP_USERNAME"]["vValue"];
        $config['smtp_pass']    = $email_setting["SMTP_PASS"]["vValue"];

        $CI->email->initialize($config);
        $CI->email->set_newline("\r\n");
        $CI->email->set_mailtype("html");
        $CI->email->reply_to($criteria["from_email"]);
        if($criteria['form_type'] == 'apnanivesh_form')
        {
            $CI->email->from($email_setting["SMTP_USERNAME"]["vValue"], "ApnaNivesh");
        }
        else if($criteria['form_type'] == 'invite'){
            $CI->email->from($criteria["from_email"], $criteria['vname']);
        }
        else if($criteria['form_type'] == 'Niveshlife'){
            $CI->email->from($criteria["from_email"], $criteria['vname']);
        }
        else
        {
            $CI->email->from($email_setting["SMTP_USERNAME"]["vValue"], "Niveshlife");            
        }
        $CI->email->to($criteria['vData']["to"]);
        // $CI->email->to("prashant.punani@isyncevolution.com");
        if($criteria['vData']['bcc'] != "")
        {
            $CI->email->bcc($criteria["vData"]['bcc']);
        }
        $CI->email->subject($criteria['vData']["subject"]);
        $CI->email->message($vFinalMessage);
        $CI->email->send();
        // echo "<pre>"; print_r($CI->email->print_debugger()); exit();
    }
    // function send_notifiction($criteria = array())
    // {
    //     $CI =& get_instance();
    //     $CI->load->database();
    //     $CI->load->model('setting/setting_model');
    //     $CI->load->helper(array('email')); 
    //     $CI->load->library('email');

    //     $myArray                    = array();
    //     $myArray["eConfigType"]     = "Company";
    //     $company_setting            = $CI->setting_model->get_all_data($myArray);

    //     $myArray                    = array();
    //     $myArray["eConfigType"]     = "Email";
    //     $email_setting              = $CI->setting_model->get_all_data($myArray);

    //     $rights         = $company_setting["COPYRIGHTED_TEXT"]["vValue"];
    //     $rights         = str_replace("#CURRENT_YEAR#", date("Y"), $rights);
    //     $constant       = array('#SITE_NAME#', '#SITE_URL#','#COPYRIGHTS#');
    //     $value          = array($company_setting["COMPANY_NAME"]["vValue"], base_url(), $rights);
    //     $stream_opts    = ["ssl" => ["verify_peer"=>false,"verify_peer_name"=>false,]];  

    //     $header_text    = file_get_contents(base_url('assets/uploads/email/header.html'),false, stream_context_create($stream_opts));
    //     $header         = str_replace("#LOGO#", base_url('assets/mf_panel/media/logos/niveh-logo.png'), $header_text);
    //     $header         = str_replace("#LOGO_EMAIL#", base_url('assets/mf_panel/media/logos/niveh-logo.png'), $header);
    //     $header         = str_replace("#BG#", base_url('assets/front/images/logo_bg.png'), $header);
    //     $header         = str_replace("#SITE_NAME#", $company_setting["COMPANY_NAME"]["vValue"], $header);

    //     $footer_text    = file_get_contents(base_url('assets/uploads/email/footer.html'),false, stream_context_create($stream_opts));
    //     $footer         = str_replace($constant, $value, $footer_text);
    //     $footer         = str_replace("#LOCATION#", base_url('assets/uploads/logo/location.png'), $footer);
    //     $footer         = str_replace("#CALL#", base_url('assets/uploads/logo/call.png'), $footer);
    //     $footer         = str_replace("#MESSAGE#", base_url('assets/uploads/logo/message.png'), $footer);
    //     $vFinalMessage  = $header.$criteria['vData']["message"].$footer;

    //     $config['protocol']     = 'smtp';
    //     $config['smtp_host']    = $email_setting["SMTP_HOST"]["vValue"];
    //     $config['smtp_crypto']  = 'tls';
    //     $config['smtp_port']    = $email_setting["SMTP_PORT"]["vValue"];
    //     $config['smtp_user']    = $email_setting["SMTP_USERNAME"]["vValue"];
    //     $config['smtp_pass']    = $email_setting["SMTP_PASS"]["vValue"];

    //     $CI->email->initialize($config);
    //     $CI->email->set_newline("\r\n");  
    //     $CI->email->set_mailtype("html");
    //     $CI->email->reply_to($data['reply']);
    //     $CI->email->from($email_setting["SMTP_USERNAME"]["vValue"], "Niveshlife");
    //     $CI->email->to($criteria['vData']["to"]);
    //     $CI->email->subject($criteria['vData']["subject"]);
    //     $CI->email->message($vFinalMessage);
    //     $CI->email->send();
    //     //echo "<pre>"; print_r($CI->email->print_debugger()); exit();
    // }

    function date_format($date)
    {
        return date("Y/m/d",strtotime($date));
    }

    function save_date_format($date)
    {
        $new_date = explode('/', $date);
        $final_date = $new_date[2].'-'.$new_date[1].'-'.$new_date[0];
        return $final_date;
    }

    function save_datetime($datetime)
    {

        $new_date = explode(' ', $datetime);
        $new_datetime = explode('/', $new_date[0]);

         $final_date1 = $new_datetime[2].'-'.$new_datetime[1].'-'.$new_datetime[0];

         $new = $final_date1. ' ' .$new_date[1];

        return $new;
    }
    function show_datetime_format($datetime)
    {
        $new_date = explode(' ', $datetime);
        $new_datetime = explode('-', $new_date[0]);

         $final_date1 = $new_datetime[2].'/'.$new_datetime[1].'/'.$new_datetime[0];

         $new = $final_date1. ' ' .$new_date[1];
         
        return $new;
      
    }

    function show_date_format($date)
    {
        $new_date = explode('-', $date);
        $final_date = $new_date[2].'/'.$new_date[1].'/'.$new_date[0];
        return $final_date;
    }
    function show_date_blog($date)
    {
        $new_date = explode('-', $date);
        $final_date = $new_date[2].'/'.$new_date[1];
        return $final_date;
    }


    
    function get_amc_name($iAMCId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('amc/amc_model');
        $criteria           = array();
        $criteria["iAMCId"] = $iAMCId;
        $data               = $CI->amc_model->get_by_id($criteria);
        return $data->vAMC;
    }

    function get_schemeName($iSchemeId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('scheme/scheme_model');
        $criteria               = array();
        $criteria["iSchemeId"]  = $iSchemeId;
        $data                   = $CI->scheme_model->get_by_id($criteria);
        return $data->vScheme;
    }

    function get_sip_scheme_name($iSchemeId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('sip_scheme/sip_scheme_model');
        $criteria               = array();
        $criteria["iSIPSchemeId"]  = $iSchemeId;
        $data                   = $CI->sip_scheme_model->get_by_id($criteria);
        return $data->tSchemeName;
    }

    function get_stp_scheme_name($iSchemeId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('stp_scheme/stp_scheme_model');
        $criteria                   = array();
        $criteria["iSTPSchemeId"]   = $iSchemeId;
        $data                       = $CI->stp_scheme_model->get_by_id($criteria);
        return $data->tSchemeName;
    }

    function get_ChannelPartner($iChannelPartnerId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('channl_partner/channel_partner_model');
        $criteria                       = array();
        $criteria["iChannelPartnerId"]  = $iChannelPartnerId;
        $data                           = $CI->channel_partner_model->get_by_id($criteria);
        return $data->vContactName;
    }

    function get_instrument_name($iInstrumentId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('instrument/instrument_model');
        $criteria                       = array();
        $criteria["iInstrumentId"]      = $iInstrumentId;
        $data                           = $CI->instrument_model->get_by_id($criteria);
        return $data->vInstrument;
    }

    function get_Transaction_name($iTransactionId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('transaction/transaction_model');
        $criteria                       = array();
        $criteria["iTransactionId"]     = $iTransactionId;
        $data                           = $CI->transaction_model->get_by_id($criteria);
        return $data->vTransaction;
    }

    function date_time($date)
    {
        return date("d/m/Y h:i A",strtotime($date));
    }
    function show_time($date)
    {
        return date("h:i A",strtotime($date));
    }
 
 
    function date_format2($date)
    {
        return date("d/m/Y",strtotime($date));
    }

    function get_notification($iNotificationId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('notification/notification_model');
        $eType = $CI->session->userdata('eType');
        if($eType == 'SuperPartner'){
            $criteria                  = array();

            $criteria['eNotification'] = 'BDM';
            $criteria['eStatus']       = 'Active';
        }
        else 
        {
            $criteria                  = array();
            
            $criteria['eNotification'] = $eType;
            $criteria['eStatus']       = 'Active';
        }
        
        $data     = $CI->notification_model->get_all_notification($criteria);
        return $data;
    }

    function get_notification_count($iNotificationId = NULL)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('notification/notification_model');

        $eType = $CI->session->userdata('eType');

        if($eType == 'SuperPartner'){
            $criteria                  = array();
            $criteria['eNotification'] = 'BDM';
            $criteria['eRead']         = 'No';
            $criteria['eStatus']       = 'Active';            
        }
        else 
        {
            $criteria                  = array();
            $criteria['eNotification'] = $eType;
            $criteria['eRead']         = 'No';
            $criteria['eStatus']       = 'Active'; 
        }
           
        $data     = $CI->notification_model->notification_count($criteria);
        return $data;
    }

    function setting_info($config)
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('setting/setting_model');
        return $CI->setting_model->get_setting($config);
    }

    function replace_content($vTitle){
        $rs_catname = trim(strtolower(($vTitle)));
        $rs_catname = str_replace("/","",$rs_catname);
        $rs_catname = str_replace("G��","",$rs_catname);
        $rs_catname = str_replace("(","",$rs_catname);
       
        $rs_catname = trim(strtolower(($vTitle)));
        $rs_catname = str_replace("/","",$rs_catname);
        $rs_catname = str_replace("G��","",$rs_catname);
        $rs_catname = str_replace("(","",$rs_catname);
        $rs_catname = str_replace(")","",$rs_catname);
        $rs_catname = str_replace("?","",$rs_catname);
        $rs_catname = str_replace("-","-",$rs_catname);
        $rs_catname = str_replace("#","",$rs_catname);
        $rs_catname = str_replace(",","",$rs_catname);
        $rs_catname = str_replace(";","",$rs_catname);
        $rs_catname = str_replace(":","",$rs_catname);
        $rs_catname = str_replace("'","",$rs_catname);
        $rs_catname = str_replace("\"","",$rs_catname);
        $rs_catname = str_replace("++","-",$rs_catname);
        $rs_catname = str_replace("+","-",$rs_catname);
        $rs_catname = str_replace("+","-",$rs_catname);
        $rs_catname = str_replace("+�","-",$rs_catname);
        //$rs_catname = str_replace("s","_",$rs_catname);

        $rs_catname = str_replace(" ","-",str_replace("&","and",$rs_catname));
        return $rs_catname;
    }

    function meta_info()
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('meta/meta_model');
        $criteria = array();
        $criteria['eStatus'] = "Active";
        $data = $CI->meta_model->get_all_data($criteria);
        return $data;
    }
    
    
}

