<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Orderentry
{
    function __construct($config = array())
    {
      date_default_timezone_set('Asia/Kolkata');
    }

    function ucc($criteria = array())
    {
      $CI =& get_instance();
      $CI->load->database();
      $CI->load->model('client/client_model');

      if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
      {
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 5000,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>
                <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
            </soap:Header>
           <soap:Body>
              <ns:getPassword>
                 <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                 <ns:MemberId>'.$criteria["iMemberId"].'</ns:MemberId>
                 <ns:Password>'.$criteria["vPassword"].'</ns:Password>
                 <ns:PassKey>?</ns:PassKey>
              </ns:getPassword>
           </soap:Body>
        </soap:Envelope>',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/soap+xml'
          ),
        ));
        $response1 = curl_exec($curl1);
        curl_close($curl1);

        if(!empty($response1))
        {
          $vPasswordString = explode("|", $response1);

          if(!empty($vPasswordString[1]))
          {
              $vPassword = trim(strip_tags($vPasswordString[1]));
              
              $curl2 = curl_init();
              curl_setopt_array($curl2, array(
                CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                 <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                      <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>
                      <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                  </soap:Header>
                 <soap:Body>
                    <ns:MFAPI>
                       <ns:Flag>02</ns:Flag>
                       <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                       <ns:EncryptedPassword>'.$vPassword.'</ns:EncryptedPassword>
                       <ns:param>'.$criteria["vClientCode"].'|'.$criteria["vClientHoldingCode"].'|'.$criteria["vClientTaxStatusCode"].'|'.$criteria["vClientOccupationCode"].'|'.$criteria["vClientName1"].'|'.$criteria["vClientName2"].'|'.$criteria["vClientName3"].'|'.$criteria["vClientBirthDate"].'|'.$criteria["vClientGender"].'|'.$criteria["vClientGuardian"].'|'.$criteria["vClientPAN"].'|'.$criteria["vClientNominee"].'|'.$criteria["vClientNomineeRelation"].'|'.$criteria["vClientGuardianPAN"].'|'.$criteria["vClientType"].'|'.$criteria["vClientDefaultDP"].'|'.$criteria["vClientCDSLDPID"].'|'.$criteria["vClientCDSLCLIENTID"].'|'.$criteria["vClientNSDLDPID"].'|'.$criteria["vClientNSDLCLIENTID"].'|'.$criteria["vClientBankAccountType"].'|'.$criteria["vClientBankAccountNumber"].'|'.$criteria["vClientMICR"].'|'.$criteria["vClientIFSC"].'|'.$criteria["vClientBankDefault"].'|'.$criteria["vClientBankAccountType2"].'|'.$criteria["vClientBankAccountNumber2"].'|'.$criteria["vClientMICR2"].'|'.$criteria["vClientIFSC2"].'|'.$criteria["vClientBankDefault2"].'|'.$criteria["vClientBankAccountType3"].'|'.$criteria["vClientBankAccountNumber3"].'|'.$criteria["vClientMICR3"].'|'.$criteria["vClientIFSC3"].'|'.$criteria["vClientBankDefault3"].'|'.$criteria["vClientBankAccountType4"].'|'.$criteria["vClientBankAccountNumber4"].'|'.$criteria["vClientMICR4"].'|'.$criteria["vClientIFSC4"].'|'.$criteria["vClientBankDefault4"].'|'.$criteria["vClientBankAccountType5"].'|'.$criteria["vClientBankAccountNumber5"].'|'.$criteria["vClientMICR5"].'|'.$criteria["vClientIFSC5"].'|'.$criteria["vClientBankDefault5"].'|'.$criteria["vClientChequeName"].'|'.$criteria["vClientAddress1"].'|'.$criteria["vClientAddress2"].'|'.$criteria["vClientAddress3"].'|'.$criteria["vClientCity"].'|'.$criteria["vClientStateCode"].'|'.$criteria["vClientPincode"].'|'.$criteria["vClientCountry"].'|'.$criteria["vClientResidentPhone"].'|'.$criteria["vClientResidentFax"].'|'.$criteria["vClientOfficePhone"].'|'.$criteria["vClientOfficeFax"].'|'.$criteria["vClientEmail"].'|'.$criteria["vClientCommunication"].'|'.$criteria["vClientDivPayMode"].'|'.$criteria["vClientSecondApplicantPAN"].'|'.$criteria["vClientThirdApplicantPAN"].'|'.$criteria["vClientMAPIN"].'|'.$criteria["vClientForeignAddress1"].'|'.$criteria["vClientForeignAddress2"].'|'.$criteria["vClientForeignAddress3"].'|'.$criteria["vClientForeignCity"].'|'.$criteria["vClientForeignPincode"].'|'.$criteria["vClientForeignStateCode"].'|'.$criteria["vClientForeignCountry"].'|'.$criteria["vClientForeignResidentPhone"].'|'.$criteria["vClientForeignResidentFax"].'|'.$criteria["vClientForeignOfficePhone"].'|'.$criteria["vClientForeignOfficeFax"].'|'.$criteria["vClientMobile"].'</ns:param>
                    </ns:MFAPI>
                 </soap:Body>
              </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/soap+xml'
                ),
              ));

              $response2 = curl_exec($curl2);
              curl_close($curl2);
              
              $ucc_response = strip_tags($response2);
              if(strpos($ucc_response, "RECORD INSERTED") !== false)
              {
                  $where                      = array();
                  $where["iClientId"]         = $criteria["iClientId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["eBSEStatus"]         = "Complete";
                  $CI->client_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "200";
                  $mydata["notification"]     = "UCC Registered Successfully.";
                  return $mydata;
              }
              else
              {
                if(strpos($ucc_response, "RECORD UPDATED") !== false)
                {
                    $where                      = array();
                    $where["iClientId"]         = $criteria["iClientId"];
                    $data                       = array();
                    $data["tResponse"]          = $response2;
                    $data["eBSEStatus"]         = "Complete";
                    $CI->client_model->update($where, $data);

                    $mydata                     = array();
                    $mydata["status"]           = "200";
                    $mydata["notification"]     = "UCC Updated Successfully.";
                    return $mydata;
                }
                else
                {
                  $where                      = array();
                  $where["iClientId"]         = $criteria["iClientId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["eBSEStatus"]         = "Failed";
                  $CI->client_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "400";
                  $mydata["notification"]     = "UCC Failed, Please Check Error Response.";
                  return $mydata;
                }
              }
          }
          else
          {
            $where                      = array();
            $where["iClientId"]         = $criteria["iClientId"];
            $data                       = array();
            $data["tResponse"]          = $response1;
            $data["eBSEStatus"]         = "Failed";
            $CI->client_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "Password Not Available";
            return $mydata;
          }
        }
        else
        {
          $where                      = array();
          $where["iClientId"]         = $criteria["iClientId"];
          $data                       = array();
          $data["tResponse"]          = $response1;
          $data["eBSEStatus"]         = "Failed";
          $CI->client_model->update($where, $data);

          $mydata                     = array();
          $mydata["status"]           = "400";
          $mydata["notification"]     = "Password Generation Failed";
          return $mydata;
        }
      }
      else
      { 
        $where                      = array();
        $where["iClientId"]         = $criteria["iClientId"];
        $data                       = array();
        $data["eBSEStatus"]         = "Failed";
        $CI->client_model->update($where, $data);

        $mydata                     = array();
        $mydata["status"]           = "400";
        $mydata["notification"]     = "BSE Credentials Not Available";
        return $mydata;
      }
    }

    function mandate($criteria = array())
    {
      $CI =& get_instance();
      $CI->load->database();
      $CI->load->model('mandate/mandate_model');

      if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
      {
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 5000,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>
                <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
            </soap:Header>
           <soap:Body>
              <ns:getPassword>
                 <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                 <ns:MemberId>'.$criteria["iMemberId"].'</ns:MemberId>
                 <ns:Password>'.$criteria["vPassword"].'</ns:Password>
                 <ns:PassKey>?</ns:PassKey>
              </ns:getPassword>
           </soap:Body>
        </soap:Envelope>',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/soap+xml'
          ),
        ));
        $response1 = curl_exec($curl1);
        curl_close($curl1);

        if(!empty($response1))
        {
          $vPasswordString = explode("|", $response1);

          if(!empty($vPasswordString[1]))
          {
              $vPassword = trim(strip_tags($vPasswordString[1]));
              
              $curl2 = curl_init();
              curl_setopt_array($curl2, array(
                CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                 <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                      <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>
                      <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                  </soap:Header>
                 <soap:Body>
                    <ns:MFAPI>
                       <ns:Flag>06</ns:Flag>
                       <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                       <ns:EncryptedPassword>'.$vPassword.'</ns:EncryptedPassword>
                       <ns:param>'.$criteria["vClientCode"].'|'.$criteria["vAmount"].'|'.$criteria["eType"].'|'.$criteria["vAccountNo"].'|'.$criteria["vAccountType"].'|'.$criteria["vIFSCCode"].'|'.$criteria["vMICRCode"].'|'.$criteria["dStartDate"].'|'.$criteria["dEndDate"].'</ns:param>
                    </ns:MFAPI>
                 </soap:Body>
              </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/soap+xml'
                ),
              ));

              $response2 = curl_exec($curl2);
              curl_close($curl2);
              $mandate_response = strip_tags($response2);
              if(strpos($mandate_response, "MANDATE REGISTRATION DONE") !== false)
              {
                  $result                     = explode("|", $mandate_response);
                  
                  $where                      = array();
                  $where["iManDateId"]        = $criteria["iMandateId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["eMandateStatus"]     = "Complete";
                  $data["vMandateCode"]       = $result[2];
                  $CI->mandate_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "200";
                  $mydata["notification"]     = "Mandate Registered Successfully.";
                  return $mydata;
              }
              else
              {
                  $where                      = array();
                  $where["iManDateId"]        = $criteria["iMandateId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["eMandateStatus"]     = "Failed";
                  $CI->mandate_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "400";
                  $mydata["notification"]     = "Mandate Failed, Please Check Error Response.";
                  return $mydata;
              }
          }
          else
          {
            $where                      = array();
            $where["iManDateId"]        = $criteria["iMandateId"];
            $data                       = array();
            $data["tResponse"]          = $response1;
            $data["eMandateStatus"]     = "Failed";
            $CI->mandate_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "Password Not Available";
            return $mydata;
          }
        }
        else
        {
          $where                      = array();
          $where["iManDateId"]        = $criteria["iMandateId"];
          $data                       = array();
          $data["tResponse"]          = $response1;
          $data["eMandateStatus"]     = "Failed";
          $CI->mandate_model->update($where, $data);

          $mydata                     = array();
          $mydata["status"]           = "400";
          $mydata["notification"]     = "Password Generation Failed";
          return $mydata;
        }
      }
      else
      {
        $where                      = array();
        $where["iManDateId"]        = $criteria["iMandateId"];
        $data                       = array();
        $data["eMandateStatus"]     = "Failed";
        $CI->mandate_model->update($where, $data);

        $mydata                     = array();
        $mydata["status"]           = "400";
        $mydata["notification"]     = "BSE Credentials Not Available";
        return $mydata;
      }
    }

    function purchase($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('purchase/purchase_model');

        if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
        {
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
                CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
                        <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                    </soap:Header>
                   <soap:Body>
                      <bses:getPassword>
                         <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                         <bses:Password>'.$criteria["vPassword"].'</bses:Password>
                         <bses:PassKey>?</bses:PassKey>
                      </bses:getPassword>
                   </soap:Body>
                </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/soap+xml'
                ),
            ));
            $response1 = curl_exec($curl1);
            curl_close($curl1);

            if(!empty($response1))
            {
                $vPasswordString = explode("|", $response1);

                if(!empty($vPasswordString[1]))
                {
                    $vPassword = trim(strip_tags($vPasswordString[1]));

                    $curl2 = curl_init();
                    curl_setopt_array($curl2, array(
                      CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 5000,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_POSTFIELDS => '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                            <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
                            <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                        </soap:Header>
                        <soap:Body>
                            <bses:orderEntryParam>
                                <bses:TransCode>NEW</bses:TransCode>
                                <bses:TransNo>'.$criteria["vTransactionNo"].'</bses:TransNo>
                                <bses:OrderId></bses:OrderId>
                                <bses:UserID>'.$criteria["iUserId"].'</bses:UserID>
                                <bses:MemberId>'.$criteria["iMemberId"].'</bses:MemberId>
                                <bses:ClientCode>'.$criteria["vClientCode"].'</bses:ClientCode>
                                <bses:SchemeCd>'.$criteria["vSchemeCode"].'</bses:SchemeCd>
                                <bses:BuySell>P</bses:BuySell>
                                <bses:BuySellType>'.$criteria["eType"].'</bses:BuySellType>
                                <bses:DPTxn>'.$criteria["vDPTransactionMode"].'</bses:DPTxn>
                                <bses:OrderVal>'.$criteria["vAmount"].'</bses:OrderVal>
                                <bses:Qty></bses:Qty>
                                <bses:AllRedeem>N</bses:AllRedeem>
                                <bses:FolioNo>'.$criteria["iFolioId"].'</bses:FolioNo>
                                <bses:Remarks></bses:Remarks>
                                <bses:KYCStatus>Y</bses:KYCStatus>
                                <bses:RefNo></bses:RefNo>
                                <bses:SubBrCode></bses:SubBrCode>
                                <bses:EUIN>'.$criteria["EUIN"].'</bses:EUIN>
                                <bses:EUINVal>N</bses:EUINVal>
                                <bses:MinRedeem>N</bses:MinRedeem>
                                <bses:DPC>N</bses:DPC>
                                <bses:IPAdd></bses:IPAdd>
                                <bses:Password>'.$vPassword.'</bses:Password>
                                <bses:PassKey>?</bses:PassKey>
                                <bses:Parma1></bses:Parma1>
                                <bses:Param2></bses:Param2>
                                <bses:Param3></bses:Param3>
                            </bses:orderEntryParam>
                        </soap:Body>
                    </soap:Envelope>',
                      CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/soap+xml'
                      ),
                    ));
                    $response2 = curl_exec($curl2);
                    curl_close($curl2);

                    $purchase_response = strip_tags($response2);
                    if(strpos($purchase_response, "ORD CONF:") !== false)
                    {
                        $result = explode("|", $purchase_response);
                        
                        $where                      = array();
                        $where["iPurchaseId"]       = $criteria["iPurchaseId"];
                        $data                       = array();
                        $data["tResponse"]          = $response2;
                        $data["vOrderNumber"]       = $result[2];
                        $data["ePurchaseStatus"]    = "Complete";
                        $data['dAddedDate']         = date("Y-m-d");
                        $data['dUpdatedDate']       = date("Y-m-d");
                        $data['eStatus']            = "Active";
                        $CI->purchase_model->update($where, $data);

                        $mydata                     = array();
                        $mydata["status"]           = "200";
                        $mydata["notification"]     = "Purchase Order Placed Successfully.";
                        return $mydata;
                    }
                    else
                    {
                        $result = explode("|", $purchase_response);

                        $where                      = array();
                        $where["iPurchaseId"]       = $criteria["iPurchaseId"];
                        $data                       = array();
                        $data["tResponse"]          = $response2;
                        $data["ePurchaseStatus"]    = "Failed";
                        $data['dAddedDate']         = date("Y-m-d");
                        $data['dUpdatedDate']       = date("Y-m-d");
                        $data['eStatus']            = "Inactive";
                        $CI->purchase_model->update($where, $data);

                        $mydata                 = array();
                        $mydata["status"]       = "400";
                        $mydata["notification"] = $result[6];
                        return $mydata;
                    }
                }
                else
                {
                    $CI->purchase_model->delete($criteria["iPurchaseId"]);
                    $mydata                 = array();
                    $mydata["status"]       = "400";
                    $mydata["notification"] = "Password Not Available";
                    return $mydata;
                }
            }
            else
            {
                $CI->purchase_model->delete($criteria["iPurchaseId"]);
                $mydata                 = array();
                $mydata["status"]       = "400";
                $mydata["notification"] = "Password Generation Failed";
                return $mydata;
            }
        }
        else
        {
            $CI->purchase_model->delete($criteria["iPurchaseId"]);
            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "BSE Credentials Not Available";
            return $mydata;
        }
    }

    function xsip($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('sip/sip_model');

        if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
        {
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
                CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
                        <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                    </soap:Header>
                   <soap:Body>
                      <bses:getPassword>
                         <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                         <bses:Password>'.$criteria["vPassword"].'</bses:Password>
                         <bses:PassKey>?</bses:PassKey>
                      </bses:getPassword>
                   </soap:Body>
                </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/soap+xml'
                ),
            ));
            $response1 = curl_exec($curl1);
            curl_close($curl1);

            if(!empty($response1))
            {
                $vPasswordString = explode("|", $response1);

                if(!empty($vPasswordString[1]))
                {
                    $vPassword = trim(strip_tags($vPasswordString[1]));

                    $curl2 = curl_init();
                    curl_setopt_array($curl2, array(
                      CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 5000,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                            <wsa:Action>http://bsestarmf.in/MFOrderEntry/xsipOrderEntryParam</wsa:Action>
                            <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                        </soap:Header>
                       <soap:Body>
                          <bses:xsipOrderEntryParam>
                             <bses:TransactionCode>NEW</bses:TransactionCode>
                             <bses:UniqueRefNo>'.$criteria["vUniqueNo"].'</bses:UniqueRefNo>
                             <bses:SchemeCode>'.$criteria["vSchemeCode"].'</bses:SchemeCode>
                             <bses:MemberCode>'.$criteria["iMemberId"].'</bses:MemberCode>
                             <bses:ClientCode>'.$criteria["vClientCode"].'</bses:ClientCode>
                             <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                             <bses:InternalRefNo></bses:InternalRefNo>
                             <bses:TransMode>'.$criteria["vTransactionMode"].'</bses:TransMode>
                             <bses:DpTxnMode>'.$criteria["vDPTransactionMode"].'</bses:DpTxnMode>
                             <bses:StartDate>'.$criteria["dStartDate"].'</bses:StartDate>
                             <bses:FrequencyType>'.$criteria["eFrequencyType"].'</bses:FrequencyType>
                             <bses:FrequencyAllowed>1</bses:FrequencyAllowed>
                             <bses:InstallmentAmount>'.$criteria["vAmount"].'</bses:InstallmentAmount>
                             <bses:NoOfInstallment>'.$criteria["vInstallment"].'</bses:NoOfInstallment>
                             <bses:Remarks></bses:Remarks>
                             <bses:FolioNo></bses:FolioNo>
                             <bses:FirstOrderFlag>'.$criteria["eFirstOrder"].'</bses:FirstOrderFlag>
                             <bses:Brokerage></bses:Brokerage>
                             <bses:MandateID>'.$criteria["vMandateCode"].'</bses:MandateID>
                             <bses:SubberCode></bses:SubberCode>
                             <bses:Euin>'.$criteria["EUIN"].'</bses:Euin>
                             <bses:EuinVal>Y</bses:EuinVal>
                             <bses:DPC>Y</bses:DPC>
                             <bses:XsipRegID></bses:XsipRegID>
                             <bses:IPAdd></bses:IPAdd>
                             <bses:Password>'.$vPassword.'</bses:Password>
                             <bses:PassKey>?</bses:PassKey>
                             <bses:Param1></bses:Param1>
                             <bses:Param2></bses:Param2>
                             <bses:Param3></bses:Param3>
                          </bses:xsipOrderEntryParam>
                       </soap:Body>
                    </soap:Envelope>',
                      CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/soap+xml'
                      ),
                    ));
                    $response2 = curl_exec($curl2);
                    curl_close($curl2);

                    $xsip_response = strip_tags($response2);
                    if(strpos($xsip_response, "X-SIP HAS BEEN REGISTERED") !== false)
                    {
                        $result = explode("|", $xsip_response);

                        $where                      = array();
                        $where["iSIPId"]            = $criteria["iSIPId"];
                        $data                       = array();
                        $data["tResponse"]          = $response2;
                        $data["vRegisterNumber"]    = $result[5];
                        $data["ePurchaseStatus"]    = "Complete";
                        $CI->sip_model->update($where, $data);

                        $mydata                     = array();
                        $mydata["status"]           = "200";
                        $mydata["notification"]     = "XSIP Registered Successfully";
                        return $mydata;
                    }
                    else
                    {
                        $result = explode("|", $xsip_response);

                        $where                      = array();
                        $where["iSIPId"]            = $criteria["iSIPId"];
                        $data                       = array();
                        $data["tResponse"]          = $response2;
                        $data["ePurchaseStatus"]    = "Failed";
                        $CI->sip_model->update($where, $data);

                        $mydata                     = array();
                        $mydata["status"]           = "400";
                        $mydata["notification"]     = $result[6];
                        return $mydata;
                    }
                }
                else
                {
                    $CI->sip_model->delete($criteria["iSIPId"]);
                    $mydata                     = array();
                    $mydata["status"]           = "400";
                    $mydata["notification"]     = "Password Not Available";
                    return $mydata;
                }
            }
            else
            {
                $CI->sip_model->delete($criteria["iSIPId"]);
                $mydata                     = array();
                $mydata["status"]           = "400";
                $mydata["notification"]     = "Password Generation Failed";
                return $mydata;
            }
        }
        else
        {
            $CI->sip_model->delete($criteria["iSIPId"]);
            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "BSE Credentials Not Available";
            return $mydata;
        }
    }

    function xsip_cancellation($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('sip/sip_model');

        if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
        {
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
                CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
                        <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                    </soap:Header>
                   <soap:Body>
                      <bses:getPassword>
                         <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                         <bses:Password>'.$criteria["vPassword"].'</bses:Password>
                         <bses:PassKey>?</bses:PassKey>
                      </bses:getPassword>
                   </soap:Body>
                </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/soap+xml'
                ),
            ));
            $response1 = curl_exec($curl1);
            curl_close($curl1);

            if(!empty($response1))
            {
                $vPasswordString = explode("|", $response1);

                if(!empty($vPasswordString[1]))
                {
                    $vPassword = trim(strip_tags($vPasswordString[1]));

                    $curl2 = curl_init();
                    curl_setopt_array($curl2, array(
                      CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 5000,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                            <wsa:Action>http://bsestarmf.in/MFOrderEntry/xsipOrderEntryParam</wsa:Action>
                            <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                        </soap:Header>
                       <soap:Body>
                          <bses:xsipOrderEntryParam>
                             <bses:TransactionCode>CXL</bses:TransactionCode>
                             <bses:UniqueRefNo>'.$criteria["vUniqueNo"].'</bses:UniqueRefNo>
                             <bses:SchemeCode></bses:SchemeCode>
                             <bses:MemberCode>'.$criteria["iMemberId"].'</bses:MemberCode>
                             <bses:ClientCode>'.$criteria["vClientCode"].'</bses:ClientCode>
                             <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                             <bses:InternalRefNo></bses:InternalRefNo>
                             <bses:TransMode></bses:TransMode>
                             <bses:DpTxnMode></bses:DpTxnMode>
                             <bses:StartDate></bses:StartDate>
                             <bses:FrequencyType></bses:FrequencyType>
                             <bses:FrequencyAllowed></bses:FrequencyAllowed>
                             <bses:InstallmentAmount></bses:InstallmentAmount>
                             <bses:NoOfInstallment></bses:NoOfInstallment>
                             <bses:Remarks></bses:Remarks>
                             <bses:FolioNo></bses:FolioNo>
                             <bses:FirstOrderFlag></bses:FirstOrderFlag>
                             <bses:Brokerage></bses:Brokerage>
                             <bses:MandateID></bses:MandateID>
                             <bses:SubberCode></bses:SubberCode>
                             <bses:Euin></bses:Euin>
                             <bses:EuinVal>Y</bses:EuinVal>
                             <bses:DPC>Y</bses:DPC>
                             <bses:XsipRegID>'.$criteria["vRegisterNumber"].'</bses:XsipRegID>
                             <bses:IPAdd></bses:IPAdd>
                             <bses:Password>'.$vPassword.'</bses:Password>
                             <bses:PassKey>?</bses:PassKey>
                             <bses:Param1></bses:Param1>
                             <bses:Param2></bses:Param2>
                             <bses:Param3></bses:Param3>
                          </bses:xsipOrderEntryParam>
                       </soap:Body>
                    </soap:Envelope>',
                      CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/soap+xml'
                      ),
                    ));
                    $response2 = curl_exec($curl2);
                    curl_close($curl2);

                    $xsip_response = strip_tags($response2);
                    if(strpos($xsip_response, "IS CANCELLED SUCCESSFULLY") !== false)
                    {
                        $where                      = array();
                        $where["iSIPId"]            = $criteria["iSIPId"];
                        $data                       = array();
                        $data["tResponse"]          = $response2;
                        $data["ePurchaseStatus"]    = "Cancel";
                        $data["dtCancel"]           = date("Y-m-d H:i:s");
                        $CI->sip_model->update($where, $data);

                        $mydata                     = array();
                        $mydata["status"]           = "200";
                        $mydata["notification"]     = "XSIP Cancelled Successfully";
                        return $mydata;
                    }
                    else
                    {
                        $where                      = array();
                        $where["iSIPId"]            = $criteria["iSIPId"];
                        $data                       = array();
                        $data["tResponse"]          = $response2;
                        $data["ePurchaseStatus"]    = "Complete";
                        $CI->sip_model->update($where, $data);

                        $mydata                     = array();
                        $mydata["status"]           = "400";
                        $mydata["notification"]     = $xsip_response;
                        return $mydata;
                    }
                }
                else
                {
                    $where                      = array();
                    $where["iSIPId"]            = $criteria["iSIPId"];
                    $data                       = array();
                    $data["tResponse"]          = $response1;
                    $data["ePurchaseStatus"]    = "Complete";
                    $CI->sip_model->update($where, $data);

                    $mydata                     = array();
                    $mydata["status"]           = "400";
                    $mydata["notification"]     = "Password Not Available";
                    return $mydata;
                }
            }
            else
            {
                $where                      = array();
                $where["iSIPId"]            = $criteria["iSIPId"];
                $data                       = array();
                $data["tResponse"]          = $response1;
                $data["ePurchaseStatus"]    = "Complete";
                $CI->sip_model->update($where, $data);

                $mydata                     = array();
                $mydata["status"]           = "400";
                $mydata["notification"]     = "Password Generation Failed";
                return $mydata;
            }
        }
        else
        {
            $where                      = array();
            $where["iSIPId"]            = $criteria["iSIPId"];
            $data                       = array();
            $data["tResponse"]          = $response1;
            $data["ePurchaseStatus"]    = "Complete";
            $CI->sip_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "BSE Credentials Not Available";
            return $mydata;
        }
    }

    function switch($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('switch/switch_model');

        if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
        {
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
                CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
                        <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                    </soap:Header>
                   <soap:Body>
                      <bses:getPassword>
                         <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                         <bses:Password>'.$criteria["vPassword"].'</bses:Password>
                         <bses:PassKey>?</bses:PassKey>
                      </bses:getPassword>
                   </soap:Body>
                </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/soap+xml'
                ),
            ));
            $response1 = curl_exec($curl1);
            curl_close($curl1);

            if(!empty($response1))
            {
              $vPasswordString = explode("|", $response1);

              if(!empty($vPasswordString[1]))
              {
                  $vPassword = trim(strip_tags($vPasswordString[1]));

                  $curl2 = curl_init();
                  curl_setopt_array($curl2, array(
                    CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 5000,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                     <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                          <wsa:Action>http://bsestarmf.in/MFOrderEntry/switchOrderEntryParam</wsa:Action>
                          <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                      </soap:Header>
                     <soap:Body>
                          <bses:switchOrderEntryParam>
                           <bses:TransCode>NEW</bses:TransCode>
                           <bses:TransNo>'.$criteria["vTransactionNo"].'</bses:TransNo>
                           <bses:OrderId></bses:OrderId>
                           <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                           <bses:MemberId>'.$criteria["iMemberId"].'</bses:MemberId>
                           <bses:ClientCode>'.$criteria["vClientCode"].'</bses:ClientCode>
                           <bses:FromSchemeCd>'.$criteria["vFromSchemeCode"].'</bses:FromSchemeCd>
                           <bses:ToSchemeCd>'.$criteria["vToSchemeCode"].'</bses:ToSchemeCd>
                           <bses:BuySell>SO</bses:BuySell>
                           <bses:BuySellType>'.$criteria["eType"].'</bses:BuySellType>
                           <bses:DPTxn>'.$criteria["vTransactionMode"].'</bses:DPTxn>
                           <bses:OrderVal>'.$criteria["vAmount"].'</bses:OrderVal>
                           <bses:SwitchUnits>'.$criteria["SwitchUnits"].'</bses:SwitchUnits>
                           <bses:AllUnitsFlag>'.$criteria["AllUnit"].'</bses:AllUnitsFlag>
                           <bses:FolioNo>'.$criteria["iFolioId"].'</bses:FolioNo>
                           <bses:Remarks></bses:Remarks>
                           <bses:KYCStatus>'.$criteria["KYCStatus"].'</bses:KYCStatus>
                           <bses:SubBrCode></bses:SubBrCode>
                           <bses:EUIN>'.$criteria["EUIN"].'</bses:EUIN>
                           <bses:EUINVal>Y</bses:EUINVal>
                           <bses:MinRedeem>Y</bses:MinRedeem>
                           <bses:IPAdd></bses:IPAdd>
                           <bses:Password>'.$vPassword.'</bses:Password>
                           <bses:PassKey>?</bses:PassKey>
                           <bses:Parma1></bses:Parma1>
                           <bses:Param2></bses:Param2>
                           <bses:Param3></bses:Param3>
                        </bses:switchOrderEntryParam>
                     </soap:Body>
                  </soap:Envelope>',
                    CURLOPT_HTTPHEADER => array(
                      'Content-Type: application/soap+xml'
                    ),
                  ));
                  $response2 = curl_exec($curl2);
                  curl_close($curl2);

                  $switch_response = strip_tags($response2);
                  if(strpos($switch_response, "SWITCH HAS BEEN") !== false)
                  {
                      $result = explode("|", $switch_response);

                      $where                      = array();
                      $where["iSwitchId"]         = $criteria["iSwitchId"];
                      $data                       = array();
                      $data["tResponse"]          = $response2;
                      $data["vOrderNumber"]       = $result[5];
                      $data["ePurchaseStatus"]   = "Complete";
                      $CI->switch_model->update($where, $data);

                      $mydata                     = array();
                      $mydata["status"]           = "200";
                      $mydata["notification"]     = "Switch Registered Successfully";
                      return $mydata;
                  }
                  else
                  {
                      $where                      = array();
                      $where["iSwitchId"]         = $criteria["iSwitchId"];
                      $data                       = array();
                      $data["tResponse"]          = $response2;
                      $data["ePurchaseStatus"]    = "Failed";
                      $CI->switch_model->update($where, $data);

                      $mydata                     = array();
                      $mydata["status"]           = "400";
                      $mydata["notification"]     = "Switch Failed. Please See Error Response";
                      return $mydata;
                  }
              }
            }
        }
        else
        {
            $CI->switch_model->delete($criteria["iSwitchId"]);
            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "BSE Credentials Not Available";
            return $mydata;
        }
    }

    function stp($criteria = array())
    {
      $CI =& get_instance();
      $CI->load->database();
      $CI->load->model('stp/stp_model');

      if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
      {
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 5000,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>
                <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
            </soap:Header>
           <soap:Body>
              <ns:getPassword>
                 <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                 <ns:MemberId>'.$criteria["iMemberId"].'</ns:MemberId>
                 <ns:Password>'.$criteria["vPassword"].'</ns:Password>
                 <ns:PassKey>?</ns:PassKey>
              </ns:getPassword>
           </soap:Body>
        </soap:Envelope>',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/soap+xml'
          ),
        ));
        $response1 = curl_exec($curl1);
        curl_close($curl1);

        if(!empty($response1))
        {
          $vPasswordString = explode("|", $response1);

          if(!empty($vPasswordString[1]))
          {
              $vPassword = trim(strip_tags($vPasswordString[1]));
              
              $curl2 = curl_init();
              curl_setopt_array($curl2, array(
                CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                 <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                      <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>
                      <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                  </soap:Header>
                 <soap:Body>
                    <ns:MFAPI>
                       <ns:Flag>07</ns:Flag>
                       <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                       <ns:EncryptedPassword>'.$vPassword.'</ns:EncryptedPassword>
                       <ns:param>'.$criteria["vClientCode"].'|'.$criteria["vFromSchemeCode"].'|'.$criteria["vToSchemeCode"].'|'.$criteria["eType"].'|'.$criteria["vTransactionMode"].'|'.$criteria["iFolioId"].'||'.$criteria["dStartDate"].'|'.$criteria["eFrequencyType"].'|'.$criteria["vInstallment"].'|'.$criteria["vAmount"].'|'.$criteria["eFirstOrder"].'||Y|'.$criteria["EUIN"].'||</ns:param>
                    </ns:MFAPI>
                 </soap:Body>
              </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/soap+xml'
                ),
              ));
              $response2 = curl_exec($curl2);
              curl_close($curl2);
              
              $stp_response = strip_tags($response2);
              if(strpos($stp_response, "STP REGISTRATION DONE") !== false)
              {
                  $result = explode("|", $stp_response);

                  $where                      = array();
                  $where["iSTPId"]            = $criteria["iSTPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["vRegisterNumber"]    = $result[2];
                  $data["ePurchaseStatus"]    = "Complete";
                  $CI->stp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "200";
                  $mydata["notification"]     = "STP Registered Successfully";
                  return $mydata;
              }
              else
              {
                  $where                      = array();
                  $where["iSTPId"]            = $criteria["iSTPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["ePurchaseStatus"]    = "Failed";
                  $CI->stp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "400";
                  $mydata["notification"]     = "STP Failed. Please See Error Response";
                  return $mydata;
              }
          }
          else
          {
            $where                      = array();
            $where["iSTPId"]            = $criteria["iSTPId"];
            $data                       = array();
            $data["tResponse"]          = $response1;
            $data["ePurchaseStatus"]    = "Failed";
            $CI->stp_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "Password Not Available";
            return $mydata;
          }
        }
        else
        {
          $where                      = array();
          $where["iSTPId"]            = $criteria["iSTPId"];
          $data                       = array();
          $data["tResponse"]          = $response1;
          $data["ePurchaseStatus"]    = "Failed";
          $CI->stp_model->update($where, $data);

          $mydata                     = array();
          $mydata["status"]           = "400";
          $mydata["notification"]     = "Password Generation Failed";
          return $mydata;
        }
      }
      else
      { 
        $where                      = array();
        $where["iSTPId"]            = $criteria["iSTPId"];
        $data                       = array();
        $data["tResponse"]          = "BSE Credentials Not Available";
        $data["ePurchaseStatus"]    = "Failed";
        $CI->stp_model->update($where, $data);

        $mydata                     = array();
        $mydata["status"]           = "400";
        $mydata["notification"]     = "BSE Credentials Not Available";
        return $mydata;
      }
    }

    function stp_cancellation($criteria = array())
    {
      $CI =& get_instance();
      $CI->load->database();
      $CI->load->model('stp/stp_model');

      if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
      {
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 5000,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>
                <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
            </soap:Header>
           <soap:Body>
              <ns:getPassword>
                 <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                 <ns:MemberId>'.$criteria["iMemberId"].'</ns:MemberId>
                 <ns:Password>'.$criteria["vPassword"].'</ns:Password>
                 <ns:PassKey>?</ns:PassKey>
              </ns:getPassword>
           </soap:Body>
        </soap:Envelope>',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/soap+xml'
          ),
        ));
        $response1 = curl_exec($curl1);
        curl_close($curl1);

        if(!empty($response1))
        {
          $vPasswordString = explode("|", $response1);

          if(!empty($vPasswordString[1]))
          {
              $vPassword = trim(strip_tags($vPasswordString[1]));
              
              $curl2 = curl_init();
              curl_setopt_array($curl2, array(
                CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                 <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                      <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>
                      <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                  </soap:Header>
                 <soap:Body>
                    <ns:MFAPI>
                       <ns:Flag>09</ns:Flag>
                       <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                       <ns:EncryptedPassword>'.$vPassword.'</ns:EncryptedPassword>
                       <ns:param>'.$criteria["vRegisterNumber"].'|'.$criteria["vClientCode"].'|</ns:param>
                    </ns:MFAPI>
                 </soap:Body>
              </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/soap+xml'
                ),
              ));
              $response2 = curl_exec($curl2);
              curl_close($curl2);
              
              $stp_response = strip_tags($response2);
              if(strpos($stp_response, "STP CANCELATION DONE") !== false)
              {
                  $where                      = array();
                  $where["iSTPId"]            = $criteria["iSTPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["ePurchaseStatus"]    = "Cancel";
                  $data["dtCancel"]           = date("Y-m-d H:i:s");
                  $CI->stp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "200";
                  $mydata["notification"]     = "STP Cancelled Successfully.";
                  return $mydata;
              }
              else
              {
                  $where                      = array();
                  $where["iSTPId"]            = $criteria["iSTPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["ePurchaseStatus"]    = "Complete";
                  $CI->stp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "400";
                  $mydata["notification"]     = $stp_response;
                  return $mydata;
              }
          }
          else
          {
            $where                      = array();
            $where["iSTPId"]            = $criteria["iSTPId"];
            $data                       = array();
            $data["tResponse"]          = $response1;
            $data["ePurchaseStatus"]    = "Complete";
            $CI->stp_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "Password Not Available";
            return $mydata;
          }
        }
        else
        {
          $where                      = array();
          $where["iSTPId"]            = $criteria["iSTPId"];
          $data                       = array();
          $data["tResponse"]          = $response1;
          $data["ePurchaseStatus"]    = "Complete";
          $CI->stp_model->update($where, $data);

          $mydata                     = array();
          $mydata["status"]           = "400";
          $mydata["notification"]     = "Password Generation Failed";
          return $mydata;
        }
      }
      else
      { 
        $where                      = array();
        $where["iSTPId"]            = $criteria["iSTPId"];
        $data                       = array();
        $data["tResponse"]          = "BSE Credentials Not Available";
        $data["ePurchaseStatus"]    = "Complete";
        $CI->stp_model->update($where, $data);

        $mydata                     = array();
        $mydata["status"]           = "400";
        $mydata["notification"]     = "BSE Credentials Not Available";
        return $mydata;
      }
    }

    function swp($criteria = array())
    {
      $CI =& get_instance();
      $CI->load->database();
      $CI->load->model('swp/swp_model');

      if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
      {
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 5000,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>
                <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
            </soap:Header>
           <soap:Body>
              <ns:getPassword>
                 <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                 <ns:MemberId>'.$criteria["iMemberId"].'</ns:MemberId>
                 <ns:Password>'.$criteria["vPassword"].'</ns:Password>
                 <ns:PassKey>?</ns:PassKey>
              </ns:getPassword>
           </soap:Body>
        </soap:Envelope>',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/soap+xml'
          ),
        ));
        $response1 = curl_exec($curl1);
        curl_close($curl1);

        if(!empty($response1))
        {
          $vPasswordString = explode("|", $response1);

          if(!empty($vPasswordString[1]))
          {
              $vPassword = trim(strip_tags($vPasswordString[1]));
              
              $curl2 = curl_init();
              curl_setopt_array($curl2, array(
                CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                 <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                      <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>
                      <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                  </soap:Header>
                 <soap:Body>
                    <ns:MFAPI>
                       <ns:Flag>08</ns:Flag>
                       <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                       <ns:EncryptedPassword>'.$vPassword.'</ns:EncryptedPassword>
                       <ns:param>'.$criteria["vClientCode"].'|'.$criteria["vSchemeCode"].'|'.$criteria["vTransactionMode"].'|'.$criteria["iFolioId"].'||'.$criteria["dStartDate"].'|'.$criteria["vInstallment"].'|'.$criteria["eFrequencyType"].'|'.$criteria["vAmount"].'|'.$criteria["vQuantity"].'|'.$criteria["eFirstOrder"].'||Y|'.$criteria["EUIN"].'||</ns:param>
                    </ns:MFAPI>
                 </soap:Body>
              </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/soap+xml'
                ),
              ));
              $response2 = curl_exec($curl2);
              curl_close($curl2);
              
              $swp_response = strip_tags($response2);
              if(strpos($swp_response, "SWP REGISTRATION DONE") !== false)
              {
                  $result = explode("|", $swp_response);

                  $where                      = array();
                  $where["iSWPId"]            = $criteria["iSWPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["vRegisterNumber"]    = $result[2];
                  $data["ePurchaseStatus"]    = "Complete";
                  $CI->swp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "200";
                  $mydata["notification"]     = "SWP Registered Successfully";
                  return $mydata;
              }
              else
              {
                  $where                      = array();
                  $where["iSWPId"]            = $criteria["iSWPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["ePurchaseStatus"]    = "Failed";
                  $CI->swp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "400";
                  $mydata["notification"]     = "SWP Failed. Please See Error Response";
                  return $mydata;
              }
          }
          else
          {
            $where                      = array();
            $where["iSWPId"]            = $criteria["iSWPId"];
            $data                       = array();
            $data["tResponse"]          = $response1;
            $data["ePurchaseStatus"]    = "Failed";
            $CI->swp_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "Password Not Available";
            return $mydata;
          }
        }
        else
        {
          $where                      = array();
          $where["iSWPId"]            = $criteria["iSWPId"];
          $data                       = array();
          $data["tResponse"]          = $response1;
          $data["ePurchaseStatus"]    = "Failed";
          $CI->swp_model->update($where, $data);

          $mydata                     = array();
          $mydata["status"]           = "400";
          $mydata["notification"]     = "Password Generation Failed";
          return $mydata;
        }
      }
      else
      { 
        $where                      = array();
        $where["iSWPId"]            = $criteria["iSWPId"];
        $data                       = array();
        $data["tResponse"]          = "BSE Credentials Not Available";
        $data["ePurchaseStatus"]    = "Failed";
        $CI->swp_model->update($where, $data);

        $mydata                     = array();
        $mydata["status"]           = "400";
        $mydata["notification"]     = "BSE Credentials Not Available";
        return $mydata;
      }
    }

    function swp_cancellation($criteria = array())
    {
      $CI =& get_instance();
      $CI->load->database();
      $CI->load->model('swp/swp_model');

      if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
      {
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 5000,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>
                <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
            </soap:Header>
           <soap:Body>
              <ns:getPassword>
                 <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                 <ns:MemberId>'.$criteria["iMemberId"].'</ns:MemberId>
                 <ns:Password>'.$criteria["vPassword"].'</ns:Password>
                 <ns:PassKey>?</ns:PassKey>
              </ns:getPassword>
           </soap:Body>
        </soap:Envelope>',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/soap+xml'
          ),
        ));
        $response1 = curl_exec($curl1);
        curl_close($curl1);

        if(!empty($response1))
        {
          $vPasswordString = explode("|", $response1);

          if(!empty($vPasswordString[1]))
          {
              $vPassword = trim(strip_tags($vPasswordString[1]));
              
              $curl2 = curl_init();
              curl_setopt_array($curl2, array(
                CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                 <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                      <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>
                      <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                  </soap:Header>
                 <soap:Body>
                    <ns:MFAPI>
                       <ns:Flag>10</ns:Flag>
                       <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                       <ns:EncryptedPassword>'.$vPassword.'</ns:EncryptedPassword>
                       <ns:param>'.$criteria["vRegisterNumber"].'|'.$criteria["vClientCode"].'|</ns:param>
                    </ns:MFAPI>
                 </soap:Body>
              </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/soap+xml'
                ),
              ));
              $response2 = curl_exec($curl2);
              curl_close($curl2);
              
              $swp_response = strip_tags($response2);
              if(strpos($swp_response, "SWP CANCELLATION INITIATED") !== false)
              {
                  $where                      = array();
                  $where["iSWPId"]            = $criteria["iSWPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["ePurchaseStatus"]    = "Cancel";
                  $data["dtCancel"]           = date("Y-m-d H:i:s");
                  $CI->swp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "200";
                  $mydata["notification"]     = "SWP Cancelled Successfully";
                  return $mydata;
              }
              else
              {
                  $where                      = array();
                  $where["iSWPId"]            = $criteria["iSWPId"];
                  $data                       = array();
                  $data["tResponse"]          = $response2;
                  $data["ePurchaseStatus"]    = "Complete";
                  $CI->swp_model->update($where, $data);

                  $mydata                     = array();
                  $mydata["status"]           = "400";
                  $mydata["notification"]     = $swp_response;
                  return $mydata;
              }
          }
          else
          {
            $where                      = array();
            $where["iSWPId"]            = $criteria["iSWPId"];
            $data                       = array();
            $data["tResponse"]          = $response1;
            $data["ePurchaseStatus"]    = "Complete";
            $CI->swp_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "Password Not Available";
            return $mydata;
          }
        }
        else
        {
          $where                      = array();
          $where["iSWPId"]            = $criteria["iSWPId"];
          $data                       = array();
          $data["tResponse"]          = $response1;
          $data["ePurchaseStatus"]    = "Complete";
          $CI->swp_model->update($where, $data);

          $mydata                     = array();
          $mydata["status"]           = "400";
          $mydata["notification"]     = "Password Generation Failed";
          return $mydata;
        }
      }
      else
      { 
        $where                      = array();
        $where["iSWPId"]            = $criteria["iSWPId"];
        $data                       = array();
        $data["tResponse"]          = "BSE Credentials Not Available";
        $data["ePurchaseStatus"]    = "Complete";
        $CI->swp_model->update($where, $data);

        $mydata                     = array();
        $mydata["status"]           = "400";
        $mydata["notification"]     = "BSE Credentials Not Available";
        return $mydata;
      }
    }

    function redemption($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('redemption/redemption_model');

        if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
        {
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
                CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
                        <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                    </soap:Header>
                   <soap:Body>
                      <bses:getPassword>
                         <bses:UserId>'.$criteria["iUserId"].'</bses:UserId>
                         <bses:Password>'.$criteria["vPassword"].'</bses:Password>
                         <bses:PassKey>?</bses:PassKey>
                      </bses:getPassword>
                   </soap:Body>
                </soap:Envelope>',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/soap+xml'
                ),
            ));
            $response1 = curl_exec($curl1);
            curl_close($curl1);

            if(!empty($response1))
            {
                $vPasswordString = explode("|", $response1);

                if(!empty($vPasswordString[1]))
                {
                    $vPassword = trim(strip_tags($vPasswordString[1]));

                    $curl2 = curl_init();
                    curl_setopt_array($curl2, array(
                      CURLOPT_URL => 'https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 5000,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_POSTFIELDS => '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                            <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
                            <wsa:To>https://bsestarmf.in/MFOrderEntry/MFOrder.svc/Secure</wsa:To>
                        </soap:Header>
                        <soap:Body>
                            <bses:orderEntryParam>
                                <bses:TransCode>NEW</bses:TransCode>
                                <bses:TransNo>'.$criteria["vTransactionNo"].'</bses:TransNo>
                                <bses:OrderId></bses:OrderId>
                                <bses:UserID>'.$criteria["iUserId"].'</bses:UserID>
                                <bses:MemberId>'.$criteria["iMemberId"].'</bses:MemberId>
                                <bses:ClientCode>'.$criteria["vClientCode"].'</bses:ClientCode>
                                <bses:SchemeCd>'.$criteria["vSchemeCode"].'</bses:SchemeCd>
                                <bses:BuySell>R</bses:BuySell>
                                <bses:BuySellType>FRESH</bses:BuySellType>
                                <bses:DPTxn>'.$criteria["vDPTransactionMode"].'</bses:DPTxn>
                                <bses:OrderVal>'.$criteria["vAmount"].'</bses:OrderVal>
                                <bses:Qty></bses:Qty>
                                <bses:AllRedeem>'.$criteria["eAllUnit"].'</bses:AllRedeem>
                                <bses:FolioNo>'.$criteria["iFolioId"].'</bses:FolioNo>
                                <bses:Remarks></bses:Remarks>
                                <bses:KYCStatus>Y</bses:KYCStatus>
                                <bses:RefNo></bses:RefNo>
                                <bses:SubBrCode></bses:SubBrCode>
                                <bses:EUIN>'.$criteria["EUIN"].'</bses:EUIN>
                                <bses:EUINVal>N</bses:EUINVal>
                                <bses:MinRedeem>N</bses:MinRedeem>
                                <bses:DPC>N</bses:DPC>
                                <bses:IPAdd></bses:IPAdd>
                                <bses:Password>'.$vPassword.'</bses:Password>
                                <bses:PassKey>?</bses:PassKey>
                                <bses:Parma1></bses:Parma1>
                                <bses:Param2></bses:Param2>
                                <bses:Param3></bses:Param3>
                            </bses:orderEntryParam>
                        </soap:Body>
                    </soap:Envelope>',
                      CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/soap+xml'
                      ),
                    ));
                    $response2 = curl_exec($curl2);
                    curl_close($curl2);

                    $redemption_response = strip_tags($response2);
                    if(strpos($redemption_response, "ORD CONF:") !== false)
                    {   
                        $where                  = array();
                        $where["iRedemptionId"] = $criteria["iRedemptionId"];
                        $data                   = array();
                        $data["tResponse"]      = $response2;
                        $data["ePurchaseStatus"]= "Complete";
                        $CI->redemption_model->update($where, $data);

                        $mydata                 = array();
                        $mydata["status"]       = "200";
                        $mydata["notification"] = "Redemption Requested Successfully.";
                        return $mydata;
                    }
                    else
                    {
                        $where                  = array();
                        $where["iRedemptionId"] = $criteria["iRedemptionId"];
                        $data                   = array();
                        $data["tResponse"]      = $redemption_response;
                        $data["ePurchaseStatus"]= "Failed";
                        $CI->redemption_model->update($where, $data);

                        $mydata                 = array();
                        $mydata["status"]       = "400";
                        $mydata["notification"] = $redemption_response;
                        return $mydata;
                    }
                }
                else
                {
                    $where                  = array();
                    $where["iRedemptionId"] = $criteria["iRedemptionId"];
                    $data                   = array();
                    $data["tResponse"]      = $response1;
                    $CI->redemption_model->update($where, $data);

                    $mydata                 = array();
                    $mydata["status"]       = "400";
                    $mydata["notification"] = "Password Not Available";
                    return $mydata;
                }
            }
            else
            {
                $where                  = array();
                $where["iRedemptionId"] = $criteria["iRedemptionId"];
                $data                   = array();
                $data["tResponse"]      = $response1;
                $CI->redemption_model->update($where, $data);

                $mydata                 = array();
                $mydata["status"]       = "400";
                $mydata["notification"] = "Password Generation Failed";
                return $mydata;
            }
        }
        else
        {
            $where                  = array();
            $where["iRedemptionId"] = $criteria["iRedemptionId"];
            $data                   = array();
            $data["tResponse"]      = $response1;
            $CI->redemption_model->update($where, $data);

            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["notification"]     = "BSE Credentials Not Available";
            return $mydata;
        }
    }

    public function mandate_status($criteria = array())
    {
        ini_set('max_execution_time','0');
        ini_set('memory_limit', '4096M');

        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('mandate/mandate_model');
        $CI->load->model('setting/setting_model');

        $myArray                    = array();
        $myArray["eConfigType"]     = "Config";
        $config_setting             = $CI->setting_model->get_all_data($myArray);

        $iUserId      = $config_setting["BSE_USERID"]["vValue"];
        $iMemberId    = $config_setting["BSE_MEMBERID"]["vValue"];
        $vPassword    = $config_setting["BSE_PASSWORD"]["vValue"];

        if(!empty($iUserId) && !empty($iMemberId) && !empty($vPassword))
        {
            $curl1 = curl_init();
                curl_setopt_array($curl1, array(
                    CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 5000,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService">
                      <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                          <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/GetAccessToken</wsa:Action>
                          <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                      </soap:Header>
                      <soap:Body>
                          <ns:GetAccessToken>
                              <ns:Param>
                                  <star:MemberId>'.$iMemberId.'</star:MemberId>
                                  <star:PassKey>?</star:PassKey>
                                  <star:Password>'.$vPassword.'</star:Password>
                                  <star:RequestType>Mandate</star:RequestType>
                                  <star:UserId>'.$iUserId.'</star:UserId>
                              </ns:Param>
                          </ns:GetAccessToken>
                     </soap:Body>
                  </soap:Envelope>',
                    CURLOPT_HTTPHEADER => array(
                      'Content-Type: application/soap+xml'
                    ),
                ));

                $response1 = curl_exec($curl1);
                curl_close($curl1);

                if(!empty($response1))
                {
                    $p = xml_parser_create();
                    xml_parse_into_struct($p, $response1, $vals, $index);
                    xml_parser_free($p);

                    $array = array();
                    foreach ($vals as $key => $value) 
                    {
                        $array[$value["tag"]]['vValue'] = $value["value"];
                    }

                    if(!empty($array["B:RESPONSESTRING"]["vValue"]))
                    {
                        $vPassword = trim($array["B:RESPONSESTRING"]["vValue"]);
                        
                        $curl2 = curl_init();
                        curl_setopt_array($curl2, array(
                          CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => '',
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 5000,
                          CURLOPT_FOLLOWLOCATION => true,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => 'POST',
                          CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService">
                            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                                <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MandateDetails</wsa:Action>
                                <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                            </soap:Header>
                           <soap:Body>
                              <ns:MandateDetails>
                                 <ns:Param>
                                    <star:ClientCode>'.$criteria["vClientCode"].'</star:ClientCode>
                                    <star:EncryptedPassword>'.trim($array["B:RESPONSESTRING"]["vValue"]).'</star:EncryptedPassword>
                                    <star:FromDate>01/01/1990</star:FromDate>
                                    <star:MandateId>'.$criteria["vMandateCode"].'</star:MandateId>
                                    <star:MemberCode>'.$iMemberId.'</star:MemberCode>
                                    <star:ToDate>31/12/2100</star:ToDate>
                                 </ns:Param>
                              </ns:MandateDetails>
                           </soap:Body>
                        </soap:Envelope>',
                          CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/soap+xml'
                          ),
                        ));

                        $response2 = curl_exec($curl2);
                        curl_close($curl2);

                        if(!empty($response1))
                        {
                            $p = xml_parser_create();
                            xml_parse_into_struct($p, $response2, $vals, $index);
                            xml_parser_free($p);

                            $array = array();
                            foreach ($vals as $key => $value) 
                            {
                                if(!array_key_exists("B:STATUS", $array))
                                {
                                    $array[$value["tag"]]['vValue'] = $value["value"];    
                                }
                            }

                            $where                = array();
                            $where["iManDateId"]  = $criteria["iManDateId"];
                            $data                 = array();
                            $data["eStatus"]      = $array["B:STATUS"]["vValue"];
                            $CI->mandate_model->update($where, $data);
                        }
                        else
                        {
                            $mydata                 = array();
                            $mydata["status"]       = "400";
                            $mydata["notification"] = "No Mandate Information Found";
                            echo json_encode($mydata);
                        }
                    }
                    else
                    {
                        $mydata                 = array();
                        $mydata["status"]       = "400";
                        $mydata["notification"] = "Failed to Generate Password";
                        echo json_encode($mydata);
                    }
                }
        }
        else
        {
            $mydata                 = array();
            $mydata["status"]       = "400";
            $mydata["notification"] = "BSE Credentials Not Found";
            echo json_encode($mydata);
        }
    }

    public function fatca($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('client/client_model');

        if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
        {
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
              CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 5000,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
               <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                    <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>
                    <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                </soap:Header>
               <soap:Body>
                  <ns:getPassword>
                     <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                     <ns:MemberId>'.$criteria["iMemberId"].'</ns:MemberId>
                     <ns:Password>'.$criteria["vPassword"].'</ns:Password>
                     <ns:PassKey>?</ns:PassKey>
                  </ns:getPassword>
               </soap:Body>
            </soap:Envelope>',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/soap+xml'
              ),
            ));
            $response1 = curl_exec($curl1);
            curl_close($curl1);

            if(!empty($response1))
            {
              $vPasswordString = explode("|", $response1);

              if(!empty($vPasswordString[1]))
              {
                  $vPassword = trim(strip_tags($vPasswordString[1]));

                  $curl2 = curl_init();
                  curl_setopt_array($curl2, array(
                    CURLOPT_URL => 'https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 5000,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                     <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                          <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>
                          <wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>
                      </soap:Header>
                     <soap:Body>
                        <ns:MFAPI>
                           <ns:Flag>01</ns:Flag>
                           <ns:UserId>'.$criteria["iUserId"].'</ns:UserId>
                           <ns:EncryptedPassword>'.$vPassword.'</ns:EncryptedPassword>
                           <ns:param>'.$criteria["PAN_RP"].'|'.$criteria["PEKRN"].'|'.$criteria["INV_NAME"].'|'.$criteria["DOB"].'|'.$criteria["FR_NAME"].'|'.$criteria["SP_NAME"].'|'.$criteria["TAX_STATUS"].'|'.$criteria["DATA_SRC"].'|'.$criteria["ADDR_TYPE"].'|'.$criteria["PO_BIR_INC"].'|'.$criteria["CO_BIR_INC"].'|'.$criteria["TAX_RES1"].'|'.$criteria["TPIN1"].'|'.$criteria["ID1_TYPE"].'|'.$criteria["TAX_RES2"].'|'.$criteria["TPIN2"].'|'.$criteria["ID2_TYPE"].'|'.$criteria["TAX_RES3"].'|'.$criteria["TPIN3"].'|'.$criteria["ID3_TYPE"].'|'.$criteria["TAX_RES4"].'|'.$criteria["TPIN4"].'|'.$criteria["ID4_TYPE"].'|'.$criteria["SRCE_WEALT"].'|'.$criteria["CORP_SERVS"].'|'.$criteria["INC_SLAB"].'|'.$criteria["NET_WORTH"].'|'.$criteria["NW_DATE"].'|'.$criteria["PEP_FLAG"].'|'.$criteria["OCC_CODE"].'|'.$criteria["OCC_TYPE"].'|'.$criteria["EXEMP_CODE"].'|'.$criteria["FFI_DRNFE"].'|'.$criteria["GIIN_NO"].'|'.$criteria["SPR_ENTITY"].'|'.$criteria["GIIN_NA"].'|'.$criteria["GIIN_EXEMC"].'|'.$criteria["NFFE_CATG"].'|'.$criteria["ACT_NFE_SC"].'|'.$criteria["NATURE_BUS"].'|'.$criteria["REL_LISTED"].'|'.$criteria["EXCH_NAME"].'|'.$criteria["UBO_APPL"].'|'.$criteria["UBO_COUNT"].'|'.$criteria["UBO_NAME"].'|'.$criteria["UBO_PAN"].'|'.$criteria["UBO_NATION"].'|'.$criteria["UBO_ADD1"].'|'.$criteria["UBO_ADD2"].'|'.$criteria["UBO_ADD3"].'|'.$criteria["UBO_CITY"].'|'.$criteria["UBO_PIN"].'|'.$criteria["UBO_STATE"].'|'.$criteria["UBO_CNTRY"].'|'.$criteria["UBO_ADD_TY"].'|'.$criteria["UBO_CTR"].'|'.$criteria["UBO_TIN"].'|'.$criteria["UBO_ID_TY"].'|'.$criteria["UBO_COB"].'|'.$criteria["UBO_DOB"].'|'.$criteria["UBO_GENDER"].'|'.$criteria["UBO_FR_NAM"].'|'.$criteria["UBO_OCC"].'|'.$criteria["UBO_OCC_TY"].'|'.$criteria["UBO_TEL"].'|'.$criteria["UBO_MOBILE"].'|'.$criteria["UBO_CODE"].'|'.$criteria["UBO_HOL_PC"].'|'.$criteria["SDF_FLAG"].'|'.$criteria["UBO_DF"].'|'.$criteria["AADHAAR_RP"].'|'.$criteria["NEW_CHANGE"].'|'.$criteria["LOG_NAME"].'|'.$criteria["FILLER1"].'|'.$criteria["FILLER2"].'</ns:param>
                        </ns:MFAPI>
                     </soap:Body>
                  </soap:Envelope>',
                    CURLOPT_HTTPHEADER => array(
                      'Content-Type: application/soap+xml'
                    ),
                  ));

                  $response2 = curl_exec($curl2);
                  curl_close($curl2);

                  $fatca_response = strip_tags($response2);
                  if(strpos($fatca_response, "RECORD INSERTED") !== false)
                  {
                    $where                      = array();
                    $where["iClientId"]         = $criteria["iClientId"];
                    $data                       = array();
                    $data["tFATCAResponse"]     = $response2;
                    $data["eFATCA"]             = "Complete";
                    $CI->client_model->update($where, $data);

                    $mydata                     = array();
                    $mydata["status"]           = "200";
                    $mydata["notification"]     = "FATCA Registered Successfully.";
                    return $mydata;
                  }
                  else
                  {
                    if(strpos($fatca_response, "RECORD UPDATED") !== false)
                    {
                      $where                      = array();
                      $where["iClientId"]         = $criteria["iClientId"];
                      $data                       = array();
                      $data["tFATCAResponse"]     = $response2;
                      $data["eFATCA"]             = "Complete";
                      $CI->client_model->update($where, $data);

                      $mydata                     = array();
                      $mydata["status"]           = "200";
                      $mydata["notification"]     = "FATCA Updated Successfully.";
                      return $mydata;
                    }
                    else
                    {
                      $where                      = array();
                      $where["iClientId"]         = $criteria["iClientId"];
                      $data                       = array();
                      $data["tFATCAResponse"]     = $response2;
                      $data["eFATCA"]             = "Failed";
                      $CI->client_model->update($where, $data);

                      $mydata                     = array();
                      $mydata["status"]           = "400";
                      $mydata["notification"]     = "FATCA Failed, Please Check Error Response.";
                      return $mydata;
                    }
                  }
                }
              else
              {
                $where                      = array();
                $where["iClientId"]         = $criteria["iClientId"];
                $data                       = array();
                $data["tFATCAResponse"]     = $response1;
                $data["eFATCA"]             = "Failed";
                $CI->client_model->update($where, $data);

                $mydata                     = array();
                $mydata["status"]           = "400";
                $mydata["notification"]     = "Password Not Available";
                return $mydata;
              }
            }
            else
            {
              $where                      = array();
              $where["iClientId"]         = $criteria["iClientId"];
              $data                       = array();
              $data["tFATCAResponse"]     = $response1;
              $data["eFATCA"]             = "Failed";
              $CI->client_model->update($where, $data);

              $mydata                     = array();
              $mydata["status"]           = "400";
              $mydata["notification"]     = "Password Generation Failed";
              return $mydata;
            }
        }
        else
        { 
          $where                      = array();
          $where["iClientId"]         = $criteria["iClientId"];
          $data                       = array();
          $data["eFATCA"]             = "Failed";
          $CI->client_model->update($where, $data);

          $mydata                     = array();
          $mydata["status"]           = "400";
          $mydata["notification"]     = "BSE Credentials Not Available";
          return $mydata;
        }
    }

    public function mandate_upload($criteria = array())
    {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->model('mandate/mandate_model');

        if(!empty($criteria["iUserId"]) && !empty($criteria["iMemberId"]) && !empty($criteria["vPassword"]))
        {
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
              CURLOPT_URL => 'https://www.bsestarmf.in/StarMFFileUploadService/StarMFFileUploadService.svc/Secure',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 5000,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFFileUploadService">
               <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                    <wsa:Action>http://tempuri.org/IStarMFFileUploadService/GetPassword</wsa:Action>
                    <wsa:To>https://www.bsestarmf.in/StarMFFileUploadService/StarMFFileUploadService.svc/Secure</wsa:To>
                </soap:Header>
               <soap:Body>
                  <tem:GetPassword>
                     <tem:Param>
                        <star:MemberId>'.$criteria['iMemberId'].'</star:MemberId>
                        <star:Password>'.$criteria['vPassword'].'</star:Password>
                        <star:UserId>'.$criteria['iUserId'].'</star:UserId>
                     </tem:Param>
                  </tem:GetPassword>
               </soap:Body>
            </soap:Envelope>',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/soap+xml'
              ),
            ));
            $response1 = curl_exec($curl1);
            curl_close($curl1);

            if(!empty($response1))
            {
                $p = xml_parser_create();
                xml_parse_into_struct($p, $response1, $vals, $index);
                xml_parser_free($p);

                $array = array();
                foreach ($vals as $key => $value) 
                {
                    $array[$value["tag"]]['vValue'] = $value["value"];
                }

                if(!empty($array["B:RESPONSESTRING"]["vValue"]))
                {
                    $curl2 = curl_init();
                    curl_setopt_array($curl2, array(
                      CURLOPT_URL => 'https://www.bsestarmf.in/StarMFFileUploadService/StarMFFileUploadService.svc/Secure',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 5000,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_POSTFIELDS =>'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFFileUploadService">
                       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                            <wsa:Action>http://tempuri.org/IStarMFFileUploadService/UploadMandateScanFile</wsa:Action>
                            <wsa:To>https://www.bsestarmf.in/StarMFFileUploadService/StarMFFileUploadService.svc/Secure</wsa:To>
                        </soap:Header>
                       <soap:Body>
                          <tem:UploadMandateScanFile>
                             <tem:Data>
                                <star:ClientCode>'.$criteria["vClientCode"].'</star:ClientCode>
                                <star:EncryptedPassword>'.$array["B:RESPONSESTRING"]["vValue"].'</star:EncryptedPassword>
                                <star:Filler1>'.$criteria["vFilter1"].'</star:Filler1>
                                <star:Filler2>'.$criteria["vFilter2"].'</star:Filler2>
                                <star:Flag>'.$criteria["vFlag"].'</star:Flag>
                                <star:ImageName>'.$criteria["vImage"].'</star:ImageName>
                                <star:ImageType>'.$criteria["vImageType"].'</star:ImageType>
                                <star:MandateId>'.$criteria["vMandateCode"].'</star:MandateId>
                                <star:MandateType>'.$criteria["vMandateType"].'</star:MandateType>
                                <star:MemberCode>'.$criteria["iMemberId"].'</star:MemberCode>
                                <star:pFileBytes>'.$criteria["vImageCode"].'</star:pFileBytes>
                             </tem:Data>
                          </tem:UploadMandateScanFile>
                       </soap:Body>
                    </soap:Envelope>',
                      CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/soap+xml'
                      ),
                    ));

                    $response2 = curl_exec($curl2);
                    curl_close($curl2);
                    
                    if(!empty($response2))
                    {
                        $p = xml_parser_create();
                        xml_parse_into_struct($p, $response2, $vals, $index);
                        xml_parser_free($p);

                        $array = array();
                        foreach ($vals as $key => $value) 
                        {
                            $array[$value["tag"]]['vValue'] = $value["value"];
                        }

                        if($array["B:STATUS"]["vValue"] == "200")
                        {
                            $where                      = array();
                            $where["iManDateId"]        = $criteria["iManDateId"];
                            $data                       = array();
                            $data["tMandateResponse"]   = $array["B:RESPONSESTRING"]["vValue"];
                            $CI->mandate_model->update($where, $data);

                            $mydata                     = array();
                            $mydata["status"]           = "200";
                            $mydata["notification"]     = $array["B:RESPONSESTRING"]["vValue"];
                            return $mydata;
                        }
                        else
                        {
                            $where                      = array();
                            $where["iManDateId"]        = $criteria["iManDateId"];
                            $data                       = array();
                            $data["tMandateResponse"]   = $array["B:RESPONSESTRING"]["vValue"];
                            $CI->mandate_model->update($where, $data);

                            $mydata                     = array();
                            $mydata["status"]           = "400";
                            $mydata["notification"]     = $array["B:RESPONSESTRING"]["vValue"];
                            return $mydata;
                        }
                    }
                    else
                    {
                        $mydata                     = array();
                        $mydata["status"]           = "400";
                        $mydata["notification"]     = $response1;
                        return $mydata;
                    }
                }
                else
                {
                    $mydata                     = array();
                    $mydata["status"]           = "200";
                    $mydata["notification"]     = $response1;
                    return $mydata;
                }
            }
        }
        else
        {
            $mydata                     = array();
            $mydata["status"]           = "200";
            $mydata["notification"]     = "BSE Credentials Not Available.";
            return $mydata;
        }
    }
}