<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KYC
{
    function __construct($config = array())
    {

    }

    function check_kyc($criteria = array())
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://recycle.icicipruamc.com/Distributorsvcs/InvestorService.svc/JSON/checkkyc?oauth_consumer_key=Fgjti_b&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1611299253&oauth_nonce=bvwyw&oauth_version=1.0&oauth_signature=9/4O1HwMj4Al0IZkC87Uf/sibL4=',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 50000,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "FirstPan": "'.$criteria['vPan'].'",
            "SecPan": "",
            "ThirdPan": "",
            "TaxStatus": "'.$criteria['vStatus'].'",
            "IsNewVersion": "'.$criteria['vVersion'].'"
        }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($response, true);
        return $result;
    }

    function channel_login()
    {

    }

    function create_captcha()
    {

    }

    function verify_captcha()
    {

    }

    function create_onboard_object()
    {

    }

    function investor_login()
    {
        
    }
}
