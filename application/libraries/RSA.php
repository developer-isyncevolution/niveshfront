<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RSA
{
    function __construct($config = array())
    {

    }

    function encrypt($criteria = array())
    {
        if(openssl_public_encrypt($criteria["vValue"], $encrypted, $criteria["vKey"]))
        {
            $data = base64_encode($encrypted);
        }
        else
        {
            $data = "Error";
        }
        return $data;
    }
}
