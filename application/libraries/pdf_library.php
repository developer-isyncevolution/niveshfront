<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once BASEPATH.'../vendor/autoload.php';

class Pdf_library
{
    function __construct($config = array())
    {
    }

    function create($criteria = array())
    {
        // $mpdf = new \Mpdf\Mpdf(['format' => 'A4'] );
        $mpdf= new \Mpdf\Mpdf(['mode' => 'utf-8','format' => [215, 300] ,'margin_left' => 10,'margin_right' => 10,'margin_top' => 2,'margin_bottom' => 2,'margin_header' => 2,'margin_footer' => 2]); //use this customization
        $mpdf->WriteHTML(file_get_contents(base_url('assets/mf_panel/css/pdf.css')),1);
        $mpdf->WriteHTML($criteria["html"]);
       
        if(file_exists($criteria["path"]))
        {
            unlink($criteria["path"]);
        }
        $mpdf->Output($criteria["path"], 'F');
    }
    function valuation_report($criteria = array())
    {
        // $mpdf = new \Mpdf\Mpdf(['format' => 'A4'] );
        $mpdf= new \Mpdf\Mpdf(['mode' => 'utf-8','format' => [300, 180] ,'margin_left' => 10,'margin_right' => 10,'margin_top' => 2,'margin_bottom' => 10,'margin_header' => 2,'margin_footer' => 10]); //use this customization
        $mpdf->WriteHTML(file_get_contents(base_url('assets/mf_panel/css/pdf.css')),1);
        $mpdf->WriteHTML($criteria["html"]);
       
        if(file_exists($criteria["path"]))
        {
            unlink($criteria["path"]);
        }
        $mpdf->Output($criteria["path"], 'F');
    }
    function mandate($criteria = array())
    {
        // $mpdf = new \Mpdf\Mpdf(['format' => 'A4'] );
        $mpdf= new \Mpdf\Mpdf(['mode' => 'utf-8','format' => [215, 300] ,'margin_left' => 14,'margin_right' => 10,'margin_top' => 14,'margin_bottom' => 10,'margin_header' => 2,'margin_footer' => 10]); //use this customization
        $mpdf->WriteHTML(file_get_contents(base_url('assets/mf_panel/css/pdf.css')),1);
        $mpdf->WriteHTML($criteria["html"]);
       
        if(file_exists($criteria["path"]))
        {
            unlink($criteria["path"]);
        }
        $mpdf->Output($criteria["path"], 'F');
    }
}
