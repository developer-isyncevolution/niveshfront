<?php
class Paginator 
{       
    public $itemsPerPage;
    public $range;
    public $currentPage;
    public $total;
    public $textNav;
    private $_navigation;       
    private $_link;
    private $_pageNumHtml;
    private $_itemHtml;

    public $is_ajax = false;

    public function __construct($page = 1)
    {
        $this->itemsPerPage = 21;
        $this->range        = 3;
        $this->currentPage  = $page;
        $this->total        = 0;
        
        $this->textNav      = true;     


    $this->_navigation  = array(
        'next'=>'NEXT',
        'prev' =>'PREV',
        // 'first' =>'',
        // 'last' =>'',
        // 'ipp' =>'Item per page'
    );

        $this->_link         = filter_var($_SERVER['REDIRECT_URL'], FILTER_SANITIZE_STRING);
        $this->_pageNumHtml  = '';
        $this->_itemHtml     = '';
    }

    public function paginate()
    {
        if(isset($_POST['current'])){
            $this->currentPage  = $_POST['current'];        
        }

        if(isset($_GET['item'])){
            $this->itemsPerPage = $_GET['item'];
        }           

        $this->_pageNumHtml = $this->_getPageNumbers();         
        return $this->_pageNumHtml;
    }


    public function pageNumbers()
    {
        if(empty($this->_pageNumHtml))
        {
            exit('Please call function paginate() first.');
        }
        return $this->_pageNumHtml;
    }


    public function itemsPerPage()
    {          
        if(empty($this->_itemHtml))
        {
            exit('Please call function paginate() first.');
        }
        return $this->_itemHtml;    
    } 


    private function  _getPageNumbers()
    {
        $html  = '<div class="paginations">';
        $html .= '<div class="">';
        $html .= '<div class="d-flex justify-content-between align-items-center flex-wrap">';
        $html .= '<div class="d-flex flex-wrap mr-3">';

        $query_string = $_GET;
        $str = array();

        if(count($query_string) > 0)
        {
            foreach ($query_string as $key => $value) 
            {
                if($key != 'pages'){
                    $str[] = "$key=$value";
                }
            }
        }

        if($this->currentPage > 1)
        {
            $first = $str;
            $first[] = "pages=1";

            if($this->is_ajax == false)
            {
                $string = $this->_link."?".implode("&", $first);
            } else {
                $string = 'javascript:void(0)';
            }
            $page_prev = $this->currentPage - 1; 
           
                               
        }
        // $html .= '<a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 ajax_page" href="'.$string.'" data-pages="1">'.$this->_navigation['first'].'</a>';

        $html .= '<a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 ajax_page" href="'.$string.'" data-pages="'.$page_prev.'">'.$this->_navigation['prev'].'</a>';            

        $html .= '<div class="d-flex align-items-center">';
      

        if(($this->currentPage>1))
        {
            $prev = $str;
            $prev[] = "pages=".($this->currentPage-1);

            if($this->is_ajax == false)
            {
                $string = $this->_link."?".implode("&", $prev);
            } else {
                $string = 'javascript:void(0)';
            }
        }

        $last = ceil($this->total/$this->itemsPerPage);


        if($this->total > $this->range)
        {
            if($this->currentPage <= $this->range)
            {
                $start = 1;
            } else {
                $start = $this->currentPage - $this->range;
            }
        
            if($this->currentPage+$this->range > $last)
            {
                $end = $last;
            } else if ($this->currentPage+$this->range <= $last) {
                $end = $this->currentPage+$this->range;
            }
        } else {
            if($total > $this->itemsPerPage)
            {
                $start = 1;
                $end   = $this->total;
            } else {
                $start = 1;
                $end   = 1;
            }
        }    

      
           for($i = $start; $i <= $end; $i++)
        {
            $p = $str;
            $p[] = "pages=".$i;

            if($this->is_ajax == false)
            {
                $string = $this->_link."?".implode("&", $p);
            } else {
                $string = 'javascript:void(0)';
            }
            

            if($i==$this->currentPage) {
                $html .= '<a href="'.$string.'" data-pages="'.$i.'"';
                $html .= 'class="btn btn-icon btn-sm border-0 btn-hover-primary active mr-2 my-1 page-link active"';
            }else{
                $html .= '<a href="'.$string.'" data-pages="'.$i.'"';
                $html .= 'class=" btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 ajax_page"';
            }
            $html .= '>'.$i.'</a>';
          

        }  
              

        if(($this->currentPage < ($this->total/$this->itemsPerPage)))
        {
            $next = $str;
            $next[] = "pages=".($this->currentPage+1);

            if($this->is_ajax == false)
            {
                $string = $this->_link."?".implode("&", $next);
            } else {
                $string = 'javascript:void(0)';
            }
        }

        if($this->currentPage < $last)
        {
            $end = $str;
            $end[] = "pages=".$last;
            
            if($this->is_ajax == false)
            {
                $string = $this->_link."?".implode("&", $end);
            } else {
                $string = 'javascript:void(0)';
            }

            $page_next = $this->currentPage + 1;
     
               
                      
        }

        // $html .= '<a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 ajax_page" href="'.$string.'" data-pages="'.$page_next.'">'.$this->_navigation['next'].'</a>';

        // $html .= '<a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 ajax_page" href="'.$string.'" data-pages="'.$last.'">'.$this->_navigation['last'].'</a>';    
        
        $html .= '<a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 ajax_page" href="'.$string.'" data-pages="'.$last.'">'.$this->_navigation['next'].'</a>';

        // $html .= '<a class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 ajax_page" href="'.$string.'" data-pages="'.$last.'">'.$this->_navigation['last'].'</a>';   
      
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';


        return $html;
    }
}