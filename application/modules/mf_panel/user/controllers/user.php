<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('paginator');
        $this->load->library('general');
        $this->general->authentication();
    }

    public function index()
    {
        $this->template->build('listing', NULL);
    }

    public function ajax_listing()
    {   
        $vAction = $this->input->post('vAction');
        $iUserId = $this->input->post('iUserId');
        $eStatus = $this->input->post('eStatus');

        if($vAction == "sort") {
            $column = $this->input->post('column');
            $order = $this->input->post('order');
        } else {
            $column = "iUserId";
            $order = "ASC";
        }

        if($vAction == "search") {
            $keyword    = $this->input->post('keyword');
            $filter     = $this->input->post('filter');
        } else {
            $keyword    = NULL;
            $filter     = NULL;
        }

        if($vAction == "delete" & !empty($iUserId))
        {
            $this->user_model->delete($iUserId);
        }       

        $criteria['keyword']    = $keyword;
        $criteria['filter']     = $filter;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eStatus']    = $eStatus;

        $data                   = $this->user_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages = 1;
        if($this->input->post('pages') != "") {
            $pages = $this->input->post('pages');
        }
        $paginator                  = new Paginator($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->user_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $iUserId                = $this->input->post('iUserId');
            $data['vFirstName']     = $this->input->post('vFirstName');
            $data['vLastName']      = $this->input->post('vLastName');
            $data['vEmail']         = $this->input->post('vEmail');
            $data['vMobile']        = $this->input->post('vMobile');
            $data['eStatus']        = $this->input->post('eStatus');
            $data['eType']          = $this->input->post('eType');
            $data['iRoleId']        = $this->input->post('iRoleId');

            if(empty($iUserId))
            {
                $data['vPassword']  = md5($this->input->post('vPassword'));
            }

            if($iUserId)
            {
                $where = array("iUserId" => $iUserId);
                $this->user_model->update($where, $data);
                $this->session->set_flashdata('success', "User Updated Successfully." );
            }
            else
            {
                $id = $this->user_model->add($data);
                $this->session->set_flashdata('success', "User Added Successfully." );
            }
            redirect(base_url('mf_panel/user'));
        }
        else
        {
            $this->template->build('add', $data);
        }
    }

    public function edit($iUserId = NULL)
    {
        $criteria               = array();
        $criteria['iUserId']    = $iUserId;

        if(!empty($criteria['iUserId']))
        {
            $data['data']           = $this->user_model->get_by_id($criteria);
            $this->template->build('add',$data);
        }
        else
        {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/user'));
        }
    }

    public function change_password($iUserId = NULL)
    {
        $criteria               = array();
        $criteria['iUserId']    = $iUserId;
        if(!empty($criteria['iUserId']))
        {
            $data['data']           = $this->user_model->get_by_id($criteria);
            $this->template->build('change_password',$data);
        }
        else
        {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/user'));
        }
    }

    public function change_password_action()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $iUserId                        = $this->input->post('iUserId');
            if(!empty($iUserId))
            {
                $where = array('iUserId' => $iUserId);
                $data['vPassword']          = md5($this->input->post('vPassword'));

                $this->user_model->update($where, $data);

                $this->session->set_flashdata('success', "Password Changed Successfully." );
                redirect(base_url('mf_panel/user'));
            }
            else
            {
                $this->session->set_flashdata('error', "Something Went Wrong." );
                redirect(base_url('mf_panel/user'));
            }
        }
        else
        {
            redirect(base_url('mf_panel/user'));
        }
    }

    public function check_unique_email()
    {
        $criteria['iUserId']    = $this->input->post('iUserId');
        $criteria['vEmail']     = $this->input->post('vEmail');

        $result                 = $this->user_model->check_unique_email($criteria);

        if(!empty($result)) {
            echo "1";
        } else {
            echo '0';
        }
    }

    public function check_unique_mobile()
    {
        $criteria['iUserId']    = $this->input->post('iUserId');
        $criteria['vMobile']    = $this->input->post('vMobile');

        $result                 = $this->user_model->check_unique_mobile($criteria);

        if(!empty($result)) {
            echo "1";
        } else {
            echo '0';
        }
    }
}