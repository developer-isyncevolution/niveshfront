<h1>User Listing</h1>


<script type="text/javascript">
    $(document).ready(function() {
        $("#ajax_loader").show();
        $.ajax({
            url: "<?php echo base_url('mf_panel/user/ajax_listing');?>",
            type: "POST",
            data:  {  }, 
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax_loader").hide();
                console.clear();
            }
        });
    });
</script>