<h1>Change Password</h1>


<script type="text/javascript">
    $(document).on('click', '.submit', function () {
        var vPassword           = $("#vPassword").val();
        var vConfirmPassword    = $("#vConfirmPassword").val();
        var vError               = false;

        if (vPassword.length == 0){
            $("#vPassword_Error").show();
            vError = true;
        } else {
            $("#vPassword_Error").hide();
        } 

        if (vConfirmPassword.length == 0){
            $("#vConfirmPassword_Error").show();
            vError = true; 
        } else {
            $("#vConfirmPassword_Error").hide();
        }

        if (vPassword.length > 0 && vConfirmPassword.length > 0) {
            if (vPassword != vConfirmPassword) {
                $("#vSamePassword_Error").show();
                vError = true;
            } else {
                $("#vSamePassword_Error").hide();
            }
        }

        if(vError == false) {
          $(".submit").hide();
          $("#submit_loader").show();
          setTimeout(function(){
            $("#change_password_form").submit();
          }, 500);
        }

    });
</script>