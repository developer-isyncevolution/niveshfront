<h1>User Add</h1>

<script type="text/javascript">
    $(document).on('click', '.submit', function () {
        var iUserId             = $("#iUserId").val();
        var vFirstName          = $("#vFirstName").val();
        var vLastName           = $("#vLastName").val();
        var vEmail              = $("#vEmail").val();
        var vPassword           = $("#vPassword").val();
        var vConfirmPassword    = $("#vConfirmPassword").val();
        var vMobile             = $("#vMobile").val();
        var eStatus             = $("#eStatus").val();
        var eType               = $("#eType").val();
        var iRoleId             = $("#iRoleId").val();
        var vError              = false;

        if(vFirstName.length == 0){
            $("#vFirstName_Error").show();
            vError = true;
        } else {
            $("#vFirstName_Error").hide();
        }

        if(vLastName.length == 0){
            $("#vLastName_Error").show();
            vError = true;
        } else {
            $("#vLastName_Error").hide();
        }

        if(eStatus.length == 0){
          $("#eStatus_Error").show();
          vError = true;
        } else {
          $("#eStatus_Error").hide();
        }

        if(eType.length == 0){
          $("#eType_Error").show();
          vError = true;
        } else {
          $("#eType_Error").hide();
        }

        if(iRoleId.length == 0){
          $("#iRoleId_Error").show();
          vError = true;
        } else {
          $("#iRoleId_Error").hide();
        }

        if (vEmail.length == 0) {
            $("#vEmail_Error").show();
            vError = true;
        } else {
            if (validateEmail(vEmail)) {
                $("#vEmail_Valid_Error").hide();
                $.ajax({
                    url: "<?php echo base_url('user/check_unique_email');?>",
                    type: "POST",
                    data: { iUserId:iUserId, vEmail:vEmail },
                    success: function (response) {
                        if (response == 1) {
                            $("#vEmail_Unique_Error").show();
                            $("#vEmail_Error").hide();
                            vError = true;
                        } else {
                            $("#vEmail_Unique_Error").hide();
                            $("#vEmail_Error").hide();
                        }
                    }
                });
            } else {
                $("#vEmail_Valid_Error").show();
                $("#vEmail_Error").hide();
                $("#vEmail_Unique_Error").hide();
                vError = true;
            }
        }

        if (iUserId.length == 0) {
            if (vPassword.length == 0) {
                $("#vPassword_Error").show();
                vError = true;
            } else {
                $("#vPassword_Error").hide();
            }

            if (vConfirmPassword.length == 0) {
                $("#vConfirmPassword_Error").show();
                vError = true;
            } else {
                $("#vConfirmPassword_Error").hide();
            }

            if (vPassword.length != 0 && vConfirmPassword.length != 0) {
                if (vPassword != vConfirmPassword) {
                    $("#vSamePassword_Error").show();
                    vError = true;
                } else {
                    $("#vSamePassword_Error").hide();
                }
            }
        }

        if (vMobile.length == 0) {
            $("#vMobile_Error").show();
            $("#vMobile_Unique_Error").hide();
            $("#vMobile_Valid_Error").hide();
            vError = true;
        } else {

            if (vMobile.length < 10 || vMobile.length > 10) {
                $("#vMobile_Valid_Error").show();
                $("#vMobile_Unique_Error").hide();
                $("#vMobile_Error").hide();
                vError = true;
            } else {
                $.ajax({
                    url: "<?php echo base_url('user/check_unique_mobile');?>",
                    type: "POST",
                    data: { iUserId: iUserId, vMobile: vMobile },
                    success: function (response) {
                        if (response == 1) {
                            $("#vMobile_Unique_Error").show();
                            $("#vMobile_Valid_Error").hide();
                            $("#vMobile_Error").hide();
                            vError = true;
                        } else {
                            $("#vMobile_Unique_Error").hide();
                            $("#vMobile_Valid_Error").hide();
                            $("#vMobile_Error").hide();
                        }
                    }
                });
            }
        }

      if(vError == false){
        $(".submit").hide();
        $("#submit_loader").show();
        setTimeout(function(){
          $("#user_form").submit();
        }, 500);
      }

    });

    function validateEmail(vEmail)
    {
      var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (filter.test(vEmail)) {
          return true;
      } else {
          return false;
      }
    }
</script>