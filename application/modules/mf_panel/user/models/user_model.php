<?php
defined('BASEPATH') || exit('No direct script access allowed');

class User_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    public $listing_data;
    public $rec_per_page;
    public $message;

    var $table = 'user';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($criteria = array())
    {
        $this->db->from($this->table. ' t');
        if(!empty($criteria['vFirstName']))
        {
            $this->db->where('t.vFirstName', $criteria['vFirstName']);
        }
        if(!empty($criteria['vLastName']))
        {
            $this->db->where('t.vLastName', $criteria['vLastName']);
        }
        if(!empty($criteria['eStatus']))
        {
            $this->db->where('t.eStatus', $criteria['eStatus']);
        }
        if(!empty($criteria['eType']))
        {
            $this->db->where('t.eType', $criteria['eType']);
        }
        if(!empty($criteria['column']) || !empty($criteria['order']))
        {
            $this->db->order_by($criteria['column'],$criteria['order']);
        }
        if(!empty($criteria['limit']) || !empty($criteria['start']))
        {
            $this->db->limit($criteria['limit'], $criteria['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_id($criteria = array())
    {
        $this->db->from($this->table. ' t');
        if(!empty($criteria['iUserId']))
        {
            $this->db->where('t.iUserId', $criteria['iUserId']);
        }
        if(!empty($criteria['vEmail']))
        {
            $this->db->where('t.vEmail', $criteria['vEmail']);
        }
        if(!empty($criteria['vUserName']))
        {
            $this->db->where('t.vUserName', $criteria['vUserName']);
        }
        if(!empty($criteria['vPassword']))
        {
            $this->db->where('t.vPassword', $criteria['vPassword']);
        }
        if(!empty($criteria['eStatus']))
        {
            $this->db->where('t.eStatus', $criteria['eStatus']);
        }
        if(!empty($criteria['eType']))
        {
            $this->db->where('t.eType', $criteria['eType']);
        }
        if(!empty($criteria['vAuthCode']))
        {
            $this->db->where('t.vAuthCode', $criteria['vAuthCode']);
        }
        $query = $this->db->get();
        return $query->row();
    }

    public function add($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete($iUserId)
    {
        $this->db->where('iUserId', $iUserId);
        $this->db->delete($this->table);
    }

    public function check_unique_email($criteria = array())
    {
        $this->db->from($this->table);
        if(!empty($criteria['vEmail']))
        {
            $this->db->where('vEmail', $criteria['vEmail']);
        }
        if(!empty($criteria['iUserId']))
        {
            $this->db->where("iUserId !=",$criteria['iUserId']);
        }
        $query=$this->db->get();
        return $query->row();
    }

    public function check_unique_mobile($criteria = array())
    {
        $this->db->from($this->table);
        if(!empty($criteria['vMobile']))
        {
            $this->db->where('vMobile', $criteria['vMobile']);
        }
        if(!empty($criteria['iUserId']))
        {
            $this->db->where("iUserId !=",$criteria['iUserId']);
        }
        $query=$this->db->get();
        return $query->row();
    }

    public function get_by_email($vEmail)
    {
        $this->db->from($this->table);
        
            $this->db->where('vEmail', $vEmail);
    
        $query=$this->db->get();
        return $query->row();
    }
 
    public function update_emp_user($where, $data1)
    {
        $this->db->update('user', $data1, $where);
        return $this->db->affected_rows();
    }

 
}

