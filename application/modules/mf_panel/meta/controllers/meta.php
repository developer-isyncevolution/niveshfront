<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class meta extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('meta_model');
        $this->load->library('pagination');
        $this->load->library('general');  
        $this->general->authentication();
    }

    public function index()
    {
        $criteria               = array();
        $criteria["eDeleted"]   = "No";
        $data["data"]           = $this->meta_model->get_all_data($criteria);
        $data["eDeleted"]       = "No";

        $this->template->build('listing', $data);
    }

    public function ajax_listing()
    {
        $vAction        = $this->input->post('vAction');
        $iMetaId        = $this->input->post('iMetaId');
        $vKeyword       = $this->input->post('vKeyword');
        $eStatus        = $this->input->post('eStatus');
        $eDeleted       = $this->input->post('eDeleted');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');
        $iMetaIdArray   = $this->input->post('iMetaIdArray');


        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iMetaId";
            $order      = "DESC";
        }

        
        if($vAction == "client_status" & !empty($iClientIdArray))
        {
            foreach ($iMetaIdArray as $key => $value) 
            {
                $myArray                = array();
                $myArray["iMetaid"]     = $value;
                $check                  = $this->meta_model->get_by_id($myArray);

                if($check->eStatus == "Active")
                {
                    $where               = array();
                    $where["iMetaid"]    = $value;

                    $mydata              = array();
                    $mydata["eStatus"]   = "Inactive";

                    $this->meta_model->update($where, $mydata);
                }
                else
                {
                    $where              = array();
                    $where["iMetaid"]   = $value;

                    $mydata             = array();
                    $mydata["eStatus"]  = "Active";

                    $this->meta_model->update($where, $mydata);   
                }
            }
        } 

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(!empty($eStatus)) {
            $criteria['eStatus']  = $eStatus;
        }            
        $criteria['vKeyword']   = $vKeyword;
        $criteria['eDeleted']   = "No";
        $data                   = $this->meta_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->meta_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $base_path                  = $this->config->item("base_path");
            $iMetaId                    = $this->input->post('iMetaId');
      
            $data['vMetaTitle']         = trim($this->input->post('vMetaTitle'));
            $data['tMetaDescription']   = trim($this->input->post('tMetaDescription'));
            $data['eStatus']            = $this->input->post('eStatus');
           
			if($iMetaId){
				$data['dtUpdatedDate'] 	= date("Y-m-d");
			}else{
				$data['dtAddedDate'] 	= date("Y-m-d");
				$data['dtUpdatedDate'] 	= date("Y-m-d");
			}
            if(!empty($iMetaId))
            {
                $where = array("iMetaId" => $iMetaId);
                $this->meta_model->update($where, $data);              

                $this->session->set_flashdata('success', "Meta Updated Successfully." );
                redirect(base_url('mf_panel/meta/meta'));
            }
            else
            {
                $ID = $this->meta_model->add($data);
                
                $this->session->set_flashdata('success', "Meta Added Successfully." );
                redirect(base_url('mf_panel/meta/meta'));
            }
        }
        else
        {
            $this->template->build('add', NULL);
        }
    }

    public function edit($iMetaId ) 
	{
        if(!empty($iMetaId))
        {
            $criteria             = array();
            $criteria["iMetaId"]  = $iMetaId;

            $data['data']      = $this->meta_model->get_by_id($criteria);
            $data['iMetaId']   = $iMetaId;
            $this->template->build('add',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/meta/add'));    
        }
    }
}