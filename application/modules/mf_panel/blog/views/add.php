<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary"><?php echo ($data->iBlogId) ? 'Edit ' : 'Add '; ?> Blog</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 py-md-6 px-md-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post" action="<?php echo base_url('mf_panel/blog/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iBlogId" name="iBlogId" value="<?php echo $iBlogId;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                            
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label> Title <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder=" Enter Title" value="<?php echo $data->vTitle;?>">
                                            <div class="text-danger" id="vTitle_error" style="display: none;">Enter  Title</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6  col-md-6 ">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Image </label>
                                                    <input type="file" class="form-control" name="vImage" id="vblogPicture">        
                                                </div>
                                                <div class="col-md-6 mt-0 mt-2">
                                                    <?php if(!empty($data->vImage)) { ?>
                                                        <img src="<?php echo base_url('assets/uploads/blog/'.$iBlogId.'/'.$data->vImage); ?>" width="60px" id="vblogPicture_Preview">
                                                    <?php } else { ?>  
                                                        <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" id="vblogPicture_Preview">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="text-danger" id="vImage_Error" style="display: none;">Image Required</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-md-12">
                                        <div class="form-group">
                                            <textarea id="tDescription" name="tDescription" value="<?php echo $data->tDescription;?>"><?php echo $data->tDescription;?></textarea>
                                            <div class="text-danger" id="tDescription_error" style="display: none;">Enter Description</div>

                                        </div>
                                    </div>
                                     <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <select name="eStatus" id="eStatus" class="form-control show-tick">
                                        <option value="">Select Status</option>
                                            <option <?php echo ($data->eStatus == 'Active') ? 'selected' : '';?> value="Active">Active</option>
                                            <option <?php echo ($data->eStatus == 'Inactive') ? 'selected' : '';?> value="Inactive">Inactive</option>
                                            
                                        </select>
                                        <div class="text-danger" id="eStatus_error" style="display: none;">Select Status</div>

                                        </div>
                                    </div> 
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                        <label>Featured <span class="text-danger">*</span></label>
                                        <select name="eFeatured" id="eFeatured" class="form-control show-tick">
                                        <option value="">Select Feature</option>
                                            <option <?php echo ($data->eFeatured == 'Yes') ? 'selected' : '';?> value="Yes">Yes</option>
                                            <option <?php echo ($data->eFeatured == 'No') ? 'selected' : '';?> value="No">No</option>
                                            
                                        </select>
                                        <div class="text-danger" id="eFeatured_error" style="display: none;">Select Feature</div>

                                        </div>
                                    </div>  
                              
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader" type="button" disabled style="display: none;">
                                  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                  Loading...
                                </button>
                                &nbsp;
                                <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                </div>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.9.2/tinymce.min.js"></script>


<script type="text/javascript">
  
    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/blog/blog'); ?>";
    });

    $(document).on('click', '.submit', function () {
        var iBlogId            = $("#iBlogId").val();
        var vTitle             = $("#vTitle").val();
        var eStatus            = $("#eStatus").val();
        var eFeatured          = $("#eFeatured").val();
        var vError             = false;

        if(vTitle.length == 0){
            $("#vTitle_error").show();
            vError = true;
        } else {
            $("#vTitle_error").hide();
        }

        if(eStatus.length == 0){
            $("#eStatus_error").show();
            vError = true;
        } else {
            $("#eStatus_error").hide();
        }
        if(eFeatured.length == 0){
            $("#eFeatured_error").show();
            vError = true;
        } else {
            $("#eFeatured_error").hide();
        }
        if(vError == false){
            $(".submit").hide();
            $(".submit_loader").show();
            setTimeout(function(){
              $("#frm").submit();
            }, 1000);
        }
    });


    $(document).on('change','#vblogPicture',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vblogPicture_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
   
    $(document).ready(function() {
    tinymce.init({
        selector: 'textarea#tDescription',
        height: 500,
        width: 905,
        file_picker_types: 'file image media',
        menubar: true,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount', 'image code',
        ],
        toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help | link image ',
                image_title: true,
            automatic_uploads: true,
            file_picker_types: 'image',
            file_picker_callback: function (cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {

                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };
            input.click();
        },
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
});
    // $(document).ready(function() {
    //     tinymce.init({
    //         selector: 'textarea#tDescription',
    //         file_picker_types: 'file image media',
    //         height: 500,
    //         width: 905,
    //         menubar: true,
    //         plugins: [
    //             'advlist autolink lists link image charmap print preview anchor',
    //             'searchreplace visualblocks code fullscreen',
    //             'insertdatetime media table paste code help wordcount',
    //             'image code',
    //         ],
    //         toolbar: 'undo redo | formatselect | ' +
    //         'bold italic backcolor | alignleft aligncenter ' +
    //         'alignright alignjustify | bullist numlist outdent indent | ' +
    //         'removeformat | help | link image ',
    //         image_title: true,
    //         automatic_uploads: true,
    //         file_picker_types: 'image',
    //         file_picker_callback: function (cb, value, meta) {
    //             var input = document.createElement('input');
    //             input.setAttribute('type', 'file');
    //             input.setAttribute('accept', 'image/*');

    //             input.onchange = function () {
    //             var file = this.files[0];

    //             var reader = new FileReader();
    //             reader.onload = function () {

    //                 var id = 'blobid' + (new Date()).getTime();
    //                 var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
    //                 var base64 = reader.result.split(',')[1];
    //                 var blobInfo = blobCache.create(id, file, base64);
    //                 blobCache.add(blobInfo);

    //                 cb(blobInfo.blobUri(), { title: file.name });
    //             };
    //             reader.readAsDataURL(file);
    //         };
    //         input.click();
    //     },
    //         content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
         
    //     });
    // });

</script>