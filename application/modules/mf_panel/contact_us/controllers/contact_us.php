<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact_us_model');
        $this->load->model('user/user_model');
        $this->load->model('system_email/system_email_model');
        $this->load->model('contact_us_email/contact_us_email_model');
        $this->load->library('pagination');
        $this->load->library('general');
        $this->load->library('pdf_library');
        $this->load->library('excel_library');
  
        $this->general->authentication();
    }

    public function index()
    {
        $criteria               = array(); 
        $criteria["eDeleted"]   = "No";
        $data["data"]           = $this->contact_us_model->get_all_data($criteria);
        $data["eDeleted"]       = "No";

        $this->template->build('listing', $data);
    }

    public function ajax_listing()
    {
        $vAction        = $this->input->post('vAction');
        $iContactId     = $this->input->post('iContactId');
        $vKeyword       = $this->input->post('vKeyword');
        $eStatus        = $this->input->post('eStatus');
        $eDeleted       = $this->input->post('eDeleted');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');

        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iContactId";
            $order      = "DESC";
        }

        if($vAction == "delete"){
			$iContactId = $this->input->post('iContactId');
			$this->contact_us_model->delete($iContactId);
		}
        if($vAction == "banner_status" & !empty($iContactId))
        {
            $myArray                = array();
            $myArray["iContactId"]  = $iContactId;
            $check                  = $this->contact_us_model->get_by_id($myArray);

            if($check->eStatus == "Active")
            {
                $where                      = array();
                $where["iContactId"]        = $iContactId;
                $mydata                     = array();
                $mydata["eStatus"]          = "Inactive";
                $this->contact_us_model->update($where, $mydata);
            }
            else
            {
                $where                      = array();
                $where["iContactId"]        = $iContactId;
                $mydata                     = array();
                $mydata["eStatus"]          = "Active";
                $this->contact_us_model->update($where, $mydata);   
            }
        }      

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eStatus']    = $eStatus;
        $criteria['vKeyword']   = $vKeyword;
        $criteria['eDeleted']   = "No";

        $data                   = $this->contact_us_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->contact_us_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function view($iContactId) 
	{

        if(!empty($iContactId))
        {
        $criteria                = array();
        $criteria["iContactId"]  = $iContactId;
        $data['data']         = $this->contact_us_model->get_by_id($criteria);
        $data['iContactId']   = $iContactId;

        $criteria               = array();
        $criteria["iContactId"]= $iContactId;
        $data['data_email']     = $this->contact_us_email_model->get_all_data($criteria);
       $this->template->build('view',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/banner/add'));    
        }
    }

    public function download_listing_pdf()
    {
        $iContactId = $this->input->post("iContactId");
        $base_path = $this->config->item('base_path');
 
        if(!empty($iContactId))
        {
            foreach ($iContactId as $key => $value) 
            {
                $criteria                          = array();
                $criteria["iContactId"]            = $value;
                $data[$key]                        = $this->contact_us_model->get_by_id($criteria);
            }            

            if(!empty($data))
            {
                $html = "";
                $html.= '<table style="border-collapse: collapse;width: 100%;max-width: 1000px;margin: 0 auto;">';
                    $html.= '<tr>';
                        $html.= '<td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">';
                            $html.= '<img src="'.base_url("assets/uploads/company_logo/logo.png").'" style="height: 70px;">';
                        $html.= '</td>';
                    $html.= '</tr>';
                $html.= '</table>';

                $html.= "<table style='width:100%;border-collase:collapse;border-spacing:0px;border-bottom:1px solid #b5b5c3;padding:0;margin:0;font-size:14px;'>";
                
                $html.= "<tr>";
                $html.= "<td colspan='7' style='padding: 10px;background: #cacaca;color: #000;font-size: 14px;font-weight: bold;'><strong>Contact Us Listing</strong></td>";
                $html.= "</tr>";
    
                $html.= "<tr>";
                $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Name</th>";
                $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Email</th>";
                $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Subject</th>";
                $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Phone</th>";
                $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Message</th>";
                $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:70px;height:50px;'>Status</th>";
                $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;border-right:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Address</th>";
                $html.= "</tr>";
               
                foreach ($data as $key => $value) 
                {
                    $html.= "<tr>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->vName."</td>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->vEmail."</td>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->vSubject."</td>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->vPhone."</td>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->tMessage."</td>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:70px;height:50px;'>".$value->eStatus."</td>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;border-right:1px solid #b5b5c3;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->vAddress."</td>";
                    $html.= "</tr>";
                }
 
                $html.= "</table>";

                $criteria               = array();
                $criteria["html"]       = $html;
                $criteria["path"]       = $base_path.'assets/uploads/listing_pdf/contact_us/'.'contact_us_listing_'.date("Y-m-d").'.pdf';                
                $this->pdf_library->create($criteria);
            }
 
             $mydata                     = array();
             $mydata["status"]           = "200";
             $mydata["message"]          = "success";
             $mydata["notification"]     = "Contact Us Listing PDF Downloaded";
             $mydata["filename"]         = 'contact_us_listing_'.date("Y-m-d").'.pdf';
             echo json_encode($mydata);
        }
         else
         {
             $mydata                     = array();
             $mydata["status"]           = "400";
             $mydata["message"]          = "error";
             $mydata["notification"]     = "Nothing to Download";
             echo json_encode($mydata);
         }
    }   
    
    public function download_listing_excel()
    {
        $iDigitalExperienceId                     = $this->input->post("iDigitalExperienceId");  
        $base_path                                = $this->config->item('base_path');

        if(!empty($iDigitalExperienceId))
        {
            foreach ($iDigitalExperienceId as $key => $value) 
            {
                $criteria                          = array();
                $criteria["iDigitalExperienceId"]  = $value;
                $data[$key]                        = $this->digital_experience_model->get_by_id($criteria);
            }
            if(!empty($data))
            {
     
                $write_array[]                     = array("Name","Email","Subject", "Phone","Message","Status");             

                foreach ($data as $key => $value) 
                {
                    $write_array[]                 = array($value->vName,$value->vEmail,$value->vSubject,$value->vPhone,$value->tMessage,$value->eStatus);
                }       

                $criteria                          = array();
                $criteria["data"]                  = $write_array;
                $criteria["title"]                 = "Contact Us"." ".date("Y-m-d");
                $criteria["path"]                  = $base_path.'assets/uploads/listing_excel/contact_us/'.'contact_us_'.date("Y-m-d").'.xlsx';
                $this->excel_library->create($criteria);

                $vFileName                         = 'contact_us_'.date("Y-m-d").'.xlsx';
            }
                $mydata                            = array();
                $mydata["status"]                  = "200";
                $mydata["message"]                 = "success";
                $mydata["notification"]            = "Contact Us Listing Excel Downloaded";
                $mydata["filename"]                = $vFileName;
                echo json_encode($mydata);
        }
        else
        {
            $mydata                                = array();
            $mydata["status"]                      = "400";
            $mydata["message"]                     = "error";
            $mydata["notification"]                = "Nothing to Download";
            echo json_encode($mydata);
        }
    }

    public function send_reply()
    {

        if($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $base_path                  = $this->config->item("base_path");
            $ID                         = $this->input->post('ID');

            $data['vSubject']           = $this->input->post('vSubject');
            $data['tMessage']           = $this->input->post('tMessage');
            $data['vEmail']             = $this->input->post('vEmail');
            $data['iContactId']         = $this->input->post('iContactId');
			$data['dtDateTime'] 	    = date("Y-m-d h:i:s");

            $this->contact_us_email_model->add($data);
           
            $criteria                            = array();
            $criteria["vCode"]                   = "CONTACT_US_EMAIL";
            $email                               = $this->system_email_model->get_by_id($criteria);

            if(!empty($email)){

                $constant                   = array('#tMessage#');
                $value                      = array($data['tMessage'] );
                $message                    = str_replace($constant, $value, $email->tMessage);
                $email_data['to']           = $data['vEmail'];
                $email_data['subject']      =  $data['vSubject'];
                $email_data['message']      = $message;
                $criteria                   = array();
                $criteria['eType']          = 'EMAIL';
                $criteria['vData']          = $email_data;
                $criteria['form_type']      = "Niveshlife";
                $criteria['from_email']     = "info@niveshlife.com";
                $this->general->send_notifiction($criteria);
            }
        }

    }

 


}