<?php
$CI =& get_instance();
$CI->load->library('general');
?>
<input type="hidden" id="iCount" value="<?php echo count($data); ?>">

<?php if(!empty($data)) { ?>
    <?php foreach ($data as $key => $value) { ?>
        <tr style='<?php if($value->eStatus == "Inactive") { echo "background-color: #f5bcc138"; } ?>' class="mob-tr">
                <label class="checkbox checkbox-lg checkbox-inline  mr-0 align-items-center justify-content-center">                    
                    <input type="hidden" name="iRequestId[]" class="checkboxall client_checkbox" value="<?php echo $value->iRequestId;?>" data-id="<?php echo $value->iRequestId; ?>" data-value="<?php echo $value->iRequestId; ?>">
                </label>
            
            <td class="text-left">
                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->vName; ?></span>
            </td>
            <td class="text-left">
                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->eRequest; ?></span>
            </td>
            <td class="text-left">
                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->vPhone; ?></span>
            </td>

            <td class="text-center">
                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $CI->general->date_format2($value->dtUpdatedDate);  ?></span>      
            </td>

            <td class="text-center white-space-nowrap">
                <button  class="btn btn-icon btn-primary btn-sm mr-1 view" data-id="<?php echo $value->iRequestId;?>" data-toggle="tooltip" title="Edit ">
                <span class="fa fa-eye"></span>
                </button>
                <!-- <button class="btn btn-icon btn-danger btn-sm delete" data-id="<?php echo $value->iRequestId;?>" data-toggle="tooltip" title="Delete ">
                <span class="fa fa-trash"></span>
                </button> -->
                
            </td>

        </tr>
    <?php } ?>
        
        <tr class="dis-none">
            <td colspan="9"></td>
        </tr>       
        <tr>
            <td colspan="9"  style="background-color: #eef0f8;" class="pad-0">
                <div class="">
                    <?php echo $paging; ?>    
                </div>   
            </td>
        </tr>
<?php } else { ?>
    <tr align="center">
        <td class="text-dark-75 font-weight-bolder font-size-lg" colspan="9">No Records</td>
    </tr>
<?php } ?>