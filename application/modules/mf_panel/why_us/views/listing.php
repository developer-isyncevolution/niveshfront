<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-lg-flex align-items-center flex-wrap mr-lg-2 d-md-flex d-flex">
                <h5 class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex text-primary">Why Us Listing</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <div class="d-inline-block d-lg-none d-md-none justify-content-end ml-md-auto ml-auto">
                    <a href="<?php echo base_url('mf_panel/why_us/add'); ?>" class="btn btn-light-primary font-weight-bold mx-sm-2"><i class="fa fa-plus pr-0"></i></a>
                </div>
                <div class="d-lg-flex align-items-center d-md-flex d-inline subheader-main" id="kt_subheader_search">
                    <span class="text-dark-50 font-weight-bold  d-inline-block tot dis-none mr-lg-5 mr-md-5" id="kt_subheader_total"><?php echo count($data); ?> Total</span>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-12 mt-lg-0 mt-2">
                            <div class="input-group input-group-md input-group-solid" >
                                <input type="text" class="form-control keyword" id="kt_subheader_search_form" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn btn-light-primary search" type="button">Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-6 mt-lg-0 mt-2">
                            <div class="input-group input-group-md input-group-solid" >
                                <select class="form-control sort">
                                    <option value="">Sort</option>
                                    <option value="vTitle-ASC">Name (A-Z)</option>
                                    <option value="vTitle-DESC">Name (Z-A)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mt-lg-0 mt-2">
                            <div class="input-group input-group-md input-group-solid">
                                <select class="form-control client_status_change">
                                    <option value="">Status</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                </select>
                            </div>
                        </div>
                       
                       
                        <!-- <div class="d-inline-block d-lg-none d-xl-none">
                         <a href="<?php echo base_url('mf_panel/why_us/add'); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus pr-0"></i></a>
                           
                            <div class="ml-2 spinner-border text-primary" role="status" id="download_loader_mob" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="d-lg-flex align-items-center d-xl-flex d-md-flex d-none">
                <a href="<?php echo base_url('mf_panel/why_us/add'); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus pr-0"></i></a>
                <div class="download_option dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
                    <a href="javascript:;" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="btn btn-light-primary font-weight-bold ml-2">
                            <i class="fa fa-download"></i>
                        </span>
                    </a>
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
                        <ul class="navi">
                            <li class="navi-item">
                                <a href="javascript:;" class="navi-link pdf">
                                    <span class="navi-icon">
                                        <i class="fa fa-download"></i>
                                    </span>
                                    <span class="navi-text">PDF</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="javascript:;" class="navi-link excel">
                                    <span class="navi-icon">
                                        <i class="fa fa-download"></i>
                                    </span>
                                    <span class="navi-text">Excel</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                <div class="ml-2 spinner-border text-primary" role="status" id="download_loader" style="display: none;">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card-header border-0 py-5"></div>
    <div class="card card-custom gutter-b shadow-none mob-card" style="border-radius: 0 !important;">
        <div class="card-body py-0 mb-3 pad-0">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-head-custom table-vertical-center mob-tab" id="kt_advance_table_widget_4">
                    <thead class="dis-none">
                        <tr>
                            <th  class="text-center" style="width: 30px" align="center">
                                <label class="checkbox checkbox-lg checkbox-inline mr-0 align-items-center justify-content-center">
                                    <input id="selectall" type="checkbox"  name="selectall" type="checkbox">
                                    <span></span>
                                </label>
                            </th>
                            <th class="text-center" style="width:30px; min-width:30px">Image</th>
                            <th class="text-center" style="width:400px; min-width:400px">Title</th>
                            <th class="text-center" style="width:70px; min-width:70px">Status</th>
                            <th class="text-center" style="width:70px; min-width:70px">Date</th>
                            <th class="text-center" style="width:70px; min-width:70px">Action</th>
                           
                        </tr>
                    </thead>
                    <tbody id="registrar_response">
                        <tr align="center">
                            <td colspan="9">
                                <div class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>   
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
            $.ajax({
                url: "<?php echo base_url('mf_panel/why_us/ajax_listing');?>",
                type: "POST",
                data:  {  }, 
                success: function(response) {
                    $("#registrar_response").html(response);
                }
            });
        }, 1000);
    });

    $(document).on('click','.edit',function(){
        id = $(this).data("id");
        location.href = "why_us/edit/"+id;
    });

    $(document).on('click','.delete',function(){
        if (confirm('Are you sure delete this data?')) {
            iWhyUsId = $(this).data("id");

            $("#ajax-loader").show();

            url = "<?php echo base_url('mf_panel/why_us/why_us/ajax_listing');?>";

            setTimeout(function(){
                $.ajax({
                    url: url,
                    type: "POST",
                    data:  {iWhyUsId:iWhyUsId,vAction:'delete'}, 
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                        location.reload();
                    }
                });
            }, 1000);
        }
    });

    $(document).on('click', '.search', function() {
        var vKeyword = $(".keyword").val();
        var eStatus       = $(".client_status_change").val();
        if(vKeyword.length > 0) {
            $("#registrar_response").html(" ");
            $("#registrar_response").html('<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
            setTimeout(function(){
                $.ajax({
                    url: "<?php echo base_url('mf_panel/why_us/ajax_listing');?>",
                    type: "POST",
                    data:  { vKeyword:vKeyword,eStatus:eStatus , vAction:'search' }, 
                    success: function(response) {
                        $("#registrar_response").html(" ");
                        $("#registrar_response").html(response);
                        $("#kt_subheader_total").html($("#iCount").val()+" Total");

                    }
                });
            }, 1000);
        } else {
            notification_error("Enter Search String");
        }
    });

    $("#selectall").click(function() {
        if(this.checked){
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', true);
            });
        }else{
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', false);
            });
        }
    });




    $(document).on('click', '.client_inactive', function() {
        var vKeyword            = $(".keyword").val();
        var iClientIdArray      = [];
        $('.client_checkbox').each(function(index, value) {
            if(this.checked == true)
            {
                iClientIdArray.push($(this).data("value"));
            }
        });

        if(iClientIdArray.length > 0) {
            $('.client_inactive').hide();
            $('#client_inactive_loader').show();
            $("#registrar_response").html(" ");
            $("#registrar_response").html('<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
            setTimeout(function(){
                $.ajax({
                    url: "<?php echo base_url('mf_panel/why_us/ajax_listing');?>",
                    type: "POST",
                    data:  { iClientIdArray:iClientIdArray, vAction:'client_status',vKeyword:vKeyword }, 
                    success: function(response) {
                        $('.client_inactive').show();
                        $('#client_inactive_loader').hide();
                        $("#registrar_response").html(response);
                        notification_success("why us Status Updated");
                    }
                });
            },1000)
        } else {
            notification_error("Please Select why_us to Mark Inactive");
        }
    });

    $(document).on('change', '.client_status_change', function() {
        var eStatus       = $(this).val();

        var eDeleted    = "<?php echo $eDeleted; ?>";
        var vPage       = $(this).data('pages');
        var vKeyword    = $(".keyword").val();
        var vSort       = $('.sort').val();
        var vPage       = $(this).data('pages');

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
        setTimeout(function(){
            $.ajax({
                url: "<?php echo base_url('mf_panel/why_us/ajax_listing');?>",
                type: "POST",
                data: { vPage:vPage, vKeyword:vKeyword, eDeleted:eDeleted,vPage:vPage,eStatus:eStatus,vSort:vSort}, 
                success: function(response) {
                    $("#registrar_response").html(response);
                }
            });
        },1000);
    })

    $(document).on('click', '.ajax_page', function() {
        
        var eDeleted    = "<?php echo $eDeleted; ?>";
        var vPage       = $(this).data('pages');
        var vKeyword    = $(".keyword").val();
        var vSort       = $('.sort').val();
        var eStatus     = $('.client_status_change').val();
        var vPage       = $(this).data('pages');
        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');

        $.ajax({
            url: "<?php echo base_url('mf_panel/why_us/ajax_listing');?>",
            type: "POST",
            data: { vPage:vPage, vKeyword:vKeyword, eDeleted:eDeleted,vPage:vPage,eStatus:eStatus,vSort:vSort}, 
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    });

    $(document).on('change', '.sort', function() {
        var vKeyword            = $(".keyword").val();
        var eDeleted            = "<?php echo $eDeleted; ?>";
        var vSort               = $(this).val();
        var eStatus             = $('.client_status_change').val();
        var vPage               = $(this).data('pages');


        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
        $.ajax({
            url: "<?php echo base_url('mf_panel/why_us/ajax_listing');?>",
            type: "POST",
            data:  { vKeyword:vKeyword, vAction:'sort', eDeleted:eDeleted, vSort:vSort,eStatus:eStatus,vPage:vPage }, 
            success: function(response) {
                $("#registrar_response").html(" ");
                $("#registrar_response").html(response);
                $("#kt_subheader_total").html($("#iCount").val()+" Total");
            }
        });
    });


    $(document).on('click', '.pdf', function() {
        var iWhyUsId = [];       
        $('.client_checkbox').each(function(index, value) {
            if(this.checked == true)
            {
                iWhyUsId.push($(this).data("value"));
            }
        });

        if(iWhyUsId.length > 0) {

            setTimeout(function(){
                $.ajax({
                    url: "<?php echo base_url('mf_panel/why_us/download_listing_pdf');?>",
                    type: "POST",
                    dataType: "JSON",
                    data:  { iWhyUsId:iWhyUsId }, 
                    success: function(response) {
                        if(response.status == "200") {
                            notification_success(response.notification);
                            var vFile = "<?php echo base_url('assets/uploads/listing_pdf/why_us') ?>"+"/"+response.filename;
                            window.open(vFile);
                        }
                        $('.download_option').show();
                        $('#download_loader').hide();
                    }
                });
            }, 1000);
        } 

               
        else {
            if (confirm('Want to download full listing ?')){
                $('.client_checkbox').each(function(index, value) {
                    iWhyUsId.push($(this).data("value"));
                });
                $('.download_option').hide();
                $('#download_loader').show();
                setTimeout(function(){
                    $.ajax({
                        url: "<?php echo base_url('mf_panel/why_us/download_listing_pdf');?>",
                        type: "POST",
                        dataType: "JSON",
                        data:  { iWhyUsId:iWhyUsId }, 
                        success: function(response) {
                            if(response.status == "200") {
                                notification_success(response.notification);
                                var vFile = "<?php echo base_url('assets/uploads/listing_pdf/why_us') ?>"+"/"+response.filename;
                                window.open(vFile);
                            }
                            $('.download_option').show();
                            $('#download_loader').hide();
                        }
                    });
                }, 1000);
            }
        }

    });

    $(document).on('click', '.excel', function() {
        var iWhyUsId = [];

        $('.client_checkbox').each(function(index, value) {
            if(this.checked == true)
            {
                iWhyUsId.push($(this).data("value"));
            }
        });

        if(iWhyUsId.length > 0) {
            $('.download_option').hide();
            $('#download_loader').show();
            setTimeout(function(){
                $.ajax({
                    url: "<?php echo base_url('mf_panel/why_us/download_listing_excel');?>",
                    type: "POST",
                    dataType: "JSON",
                    data:  { iWhyUsId:iWhyUsId }, 
                    success: function(response) {
                        if(response.status == "200") {
                            notification_success(response.notification);
                            var vFile = "<?php echo base_url('assets/uploads/listing_excel/why_us') ?>"+"/"+response.filename;
                            window.open(vFile);
                        }
                        $('.download_option').show();
                        $('#download_loader').hide();
                    }
                });
            }, 1000);
        } else {
            if (confirm('Want to download full listing ?')){
                $('.client_checkbox').each(function(index, value) {
                    iWhyUsId.push($(this).data("value"));
                });
                $('.download_option').hide();
                $('#download_loader').show();
                setTimeout(function(){
                    $.ajax({
                        url: "<?php echo base_url('mf_panel/why_us/download_listing_excel');?>",
                        type: "POST",
                        dataType: "JSON",
                        data:  { iWhyUsId:iWhyUsId }, 
                        success: function(response) {
                            if(response.status == "200") {
                                notification_success(response.notification);
                                var vFile = "<?php echo base_url('assets/uploads/listing_excel/why_us') ?>"+"/"+response.filename;
                            window.open(vFile);
                            }
                            $('.download_option').show();
                            $('#download_loader').hide();
                        }
                    });
                }, 1000);
            }
        }
    });

</script>