<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary">
                    <?php echo ($data->iServiceId) ? 'Edit ' : 'Add '; ?> Service</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 py-md-6 px-md-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post"
                            action="<?php echo base_url('mf_panel/service/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iServiceId" name="iServiceId" value="<?php echo $iServiceId;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

                                <div class="row">
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label> service Title <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vTitle"
                                                name="vTitle" placeholder=" Enter service Title"
                                                value="<?php echo $data->vTitle;?>">
                                            <div class="text-danger" id="vTitle_error" style="display: none;">
                                                Enter service Title</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6  col-md-6 ">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Image </label>
                                                    <input type="file" class="form-control" name="vImage"
                                                        id="vImage">
                                                </div>
                                                <div class="col-md-6 mt-0 mt-2">
                                                    <?php if(!empty($data->vImage)) { ?>
                                                    <img src="<?php echo base_url('assets/uploads/service/'.$iServiceId.'/'.$data->vImage); ?>"
                                                        width="60px" id="vImage_Preview">
                                                    <?php } else { ?>
                                                    <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>"
                                                        width="60px" id="vImage_Preview">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="text-danger" id="vImage_Error" style="display: none;">Image
                                                Required</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Status <span class="text-danger">*</span></label>
                                            <select name="eStatus" id="eStatus" class="form-control show-tick">
                                                <option value="">Select Status</option>
                                                <option <?php echo ($data->eStatus == 'Active') ? 'selected' : '';?>
                                                    value="Active">Active</option>
                                                <option <?php echo ($data->eStatus == 'Inactive') ? 'selected' : '';?>
                                                    value="Inactive">Inactive</option>
                                            </select>
                                            <div class="text-danger" id="eStatus_error" style="display: none;">Select
                                                Status</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea id="tDescription" class="form-control" name="tDescription"
                                                rows="4" placeholder="Enter Description"
                                                cols="40"><?php echo $data->tDescription;?></textarea>
                                            <div class="text-danger" id="tDescription_error" style="display: none;">
                                                Enter Description</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button"
                                            class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                        <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader"
                                            type="button" disabled style="display: none;">
                                            <span class="spinner-border spinner-border-sm" role="status"
                                                aria-hidden="true"></span>
                                            Loading...
                                        </button>
                                        &nbsp;
                                        <button type="button"
                                            class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<script type="text/javascript">
$(document).on('click', '.cancel', function() {
    window.location = "<?php echo base_url('mf_panel/service/service'); ?>";
});

$(document).on('click', '.submit', function() {
    var iServiceId = $("#iServiceId").val();
    var vTitle    = $("#vTitle").val();
    var eStatus   = $("#eStatus").val();
    var vError    = false;

    if (vTitle.length == 0) {
        $("#vTitle_error").show();
        vError = true;
    } else {
        $("#vTitle_error").hide();
    }

    if (eStatus.length == 0) {
        $("#eStatus_error").show();
        vError = true;
    } else {
        $("#eStatus_error").hide();
    }
    if (vError == false) {
        $(".submit").hide();
        $(".submit_loader").show();
        setTimeout(function() {
            $("#frm").submit();
        }, 1000);
    }
});


$(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#vImage_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
    }
});
</script>