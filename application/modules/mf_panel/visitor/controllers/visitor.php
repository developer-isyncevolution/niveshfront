<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visitor extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('visitor_model');
        $this->load->library('pagination');
        $this->load->library('general');  
        $this->general->authentication();
    }

    public function index()
    {
        $criteria               = array(); 
        $criteria["eDeleted"]   = "No";
        $data["data"]           = $this->visitor_model->get_all_data($criteria);
        $data["eDeleted"]       = "No";

        $this->template->build('listing', $data);
    }

    public function ajax_listing()
    {
        $vAction        = $this->input->post('vAction');
        $iVisitorId     = $this->input->post('iVisitorId');
        $vKeyword       = $this->input->post('vKeyword');
        $eStatus        = $this->input->post('eStatus');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');

        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iVisitorId";
            $order      = "DESC";
        }   

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['vKeyword']   = $vKeyword;

        $data                   = $this->visitor_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->visitor_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

}