<div class="content d-flex flex-column flex-column-fluid" id="kt_content"> 
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex">
                <h5 class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex text-primary">Contact Detail</h5>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom detail-card">
        <div class="card-body p-lg-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
            <input type="hidden" id="iVisitorId" name="iVisitorId" value="<?php echo $iVisitorId;?>">
                <div class="row justify-content-center py-md-5 px-md-10 py-lg-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <label class="contact-info">
                            <span class="name-lable">Name</span>  
                            <span class="name-span">: <?php echo $data->vName; ?></span> 
                        </label>
                        <label class="contact-info">
                            <span class="name-lable">Email</span>
                            <span class="name-span">: <?php echo $data->vEmail; ?></span>
                        </label>
                        <label class="contact-info">
                            <span class="name-lable">Subject</span> 
                            <span class="name-span">: <?php echo $data->vSubject; ?></span>
                        </label>
                        <label class="contact-info">
                            <span class="name-lable">Phone</span> 
                            <span class="name-span">: <?php echo $data->vPhone; ?></span>
                        </label>
                        <label class="contact-info">
                            <span class="name-lable">Message</span> 
                            <span class="name-span message" style="height:auto; max-height:200px; overflow:auto !important;">: <?php echo $data->tMessage; ?></span>
                        </label>
                    </div>                    
                </div>
                <div class="row justify-content-center pb-lg-8 px-lg-8">
                    <div class="col-md-12">                           
                    <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  
    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/visitor/visitor'); ?>";
    });
</script>