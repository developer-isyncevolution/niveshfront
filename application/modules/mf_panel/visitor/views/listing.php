<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-lg-flex align-items-center flex-wrap mr-lg-2 d-md-flex d-flex">
                <h5 class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex text-primary">Visitor Listing
                </h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <div class="d-lg-flex align-items-center d-md-flex d-inline subheader-main" id="kt_subheader_search">
                    <span class="text-dark-50 font-weight-bold  d-inline-block tot dis-none mr-5"
                        id="kt_subheader_total"><?php echo count($data); ?> Total</span>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12 mt-lg-0 mt-2">
                            <div class="input-group input-group-md input-group-solid">
                                <input type="text" class="form-control keyword" id="kt_subheader_search_form"
                                    placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn btn-light-primary search" type="button">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card-header border-0 py-5"></div>
    <div class="card card-custom gutter-b shadow-none mob-card" style="border-radius: 0 !important;">
        <div class="card-body py-0 mb-3 pad-0">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-head-custom table-vertical-center mob-tab"
                    id="kt_advance_table_widget_4">
                    <thead class="dis-none">
                        <tr>
                            <th class="text-center" style="width:400px; min-width:400px">IP Address</th>
                            <th class="text-center" style="width:70px; min-width:7px">Date</th>
                            <!-- <th class="text-center" style="width:70px; min-width:70px">Action</th> -->
                        </tr>
                    </thead>
                    <tbody id="registrar_response">
                        <tr align="center">
                            <td colspan="9">
                                <div class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $.ajax({
            url: "<?php echo base_url('mf_panel/visitor/ajax_listing');?>",
            type: "POST",
            data: {},
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    }, 1000);
});


$(document).on('click', '.search', function() {
    var vKeyword = $(".keyword").val();
    if (vKeyword.length > 0) {
        $("#registrar_response").html(" ");
        $("#registrar_response").html(
            '<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>'
            );
        setTimeout(function() {
            $.ajax({
                url: "<?php echo base_url('mf_panel/visitor/ajax_listing');?>",
                type: "POST",
                data: {
                    vKeyword: vKeyword,
                    vAction: 'search'
                },
                success: function(response) {
                    $("#registrar_response").html(" ");
                    $("#registrar_response").html(response);
                    $("#kt_subheader_total").html($("#iCount").val() + " Total");

                }
            });
        }, 1000);
    } else {
        notification_error("Enter Search String");
    }
});

$(document).on('click', '.ajax_page', function() {

    var vPage = $(this).data('pages');
    var vKeyword = $(".keyword").val();

    $("#registrar_response").html(" ");
    $("#registrar_response").html(
        '<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>'
        );

    $.ajax({
        url: "<?php echo base_url('mf_panel/visitor/ajax_listing');?>",
        type: "POST",
        data: {
            vPage: vPage,
            vKeyword: vKeyword,
        },
        success: function(response) {
            $("#registrar_response").html(response);
        }
    });
});
</script>