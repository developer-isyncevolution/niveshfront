<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Website_design extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('website_design_model');
        $this->load->library('general');

    }

    public function index()
    {
        $criteria               = array();
        $criteria['eStatus']    = "Active";
        $data["data"]           = $this->website_design_model->get_all_data($criteria);
        $this->template->build('listing', $data);
    }

    public function ajax_listing()
    {
        $vAction          = $this->input->post('vAction');
        $iWebSiteDesignId = $this->input->post('iWebSiteDesignId');
        $vKeyword         = $this->input->post('vKeyword');
        $eStatus          = $this->input->post('eStatus');
        $vPage            = $this->input->post('vPage');
        $vSort            = $this->input->post('vSort');
        $iClientIdArray   = $this->input->post('iClientIdArray');


        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iWebSiteDesignId";
            $order      = "DESC";
        }

        if($vAction == "delete" && !empty($iWebSiteDesignId))
        {
            $iWebSiteDesignId = $this->input->post('iWebSiteDesignId');
			$this->website_design_model->delete($iWebSiteDesignId);
        }
        if($vAction == "client_status" & !empty($iClientIdArray))
        {
            foreach ($iClientIdArray as $key => $value) 
            {
                $myArray                   = array();
                $myArray["iWebSiteDesignId"] = $value;
                $check                     = $this->website_design_model->get_by_id($myArray);

                if($check->eStatus == "Active")
                {
                    $where                    = array();
                    $where["iWebSiteDesignId"]  = $value;

                    $mydata              = array();
                    $mydata["eStatus"]   = "Inactive";

                    $this->website_design_model->update($where, $mydata);
                }
                else
                {
                    $where                     = array();
                    $where["iWebSiteDesignId"]   = $value;

                    $mydata             = array();
                    $mydata["eStatus"]  = "Active";

                    $this->website_design_model->update($where, $mydata);   
                }
            }
        } 
    

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(!empty($eStatus)) {
            $criteria['eStatus']  = $eStatus;
        }       
         $criteria['vKeyword']   = $vKeyword;

        $data                   = $this->website_design_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->website_design_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $base_path                  = $this->config->item("base_path");
            $iWebSiteDesignId             = $this->input->post('iWebSiteDesignId');
            
      
            $data['vTitle']             = $this->input->post('vTitle');
            $data['tDescription']       = $this->input->post('tDescription');
            $data['eStatus']            = $this->input->post('eStatus');
        
			if($iWebSiteDesignId){
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}else{
				$data['dtAddedDate'] 	= date("Y-m-d h:i:s");
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}
            if(!empty($iWebSiteDesignId))
            {
                mkdir($base_path.'assets/uploads/website_design/' . $iWebSiteDesignId, 0777, TRUE);

                //image update code
                if($_FILES['vImage']['name'] != "")
                {
                    if($_FILES['vImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }

                    $vImage = time().'_Image'.$ext;
                    move_uploaded_file($_FILES['vImage']['tmp_name'], $base_path.'assets/uploads/website_design/'.$iWebSiteDesignId.'/'.$vImage);

                    $data['vImage'] = $vImage;
                }
            
                sleep(1);
    
                //image update code

                $where = array("iWebSiteDesignId" => $iWebSiteDesignId);
                $this->website_design_model->update($where, $data);

              

                $this->session->set_flashdata('success', "Website Design Updated Successfully." );
                redirect(base_url('mf_panel/website_design/website_design'));
            }
            else
            {
                $ID = $this->website_design_model->add($data);

                mkdir($base_path.'assets/uploads/website_design/' . $ID, 0777, TRUE);

                if($_FILES['vImage']['name'] != "")
                {
                    if($_FILES['vImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vImage = time().str_replace(' ','',$this->input->post('vTitle')).$ext; 
                    move_uploaded_file($_FILES['vImage']['tmp_name'], $base_path.'assets/uploads/website_design/'.$ID.'/'.$vImage);
                    $update_where                 = array();
                    $update_where["iWebSiteDesignId"]  = $ID;
    
                    $update_image                   = array();
                    $update_image["vImage"]          = $vImage;
                    $this->website_design_model->update($update_where, $update_image);
    
                }
                sleep(1);
                //image update    
                $this->session->set_flashdata('success', "Website Design Added Successfully." );
                redirect(base_url('mf_panel/website_design/website_design'));
            }
        }
        else
        {
            $this->template->build('add', NULL);
        }
    }

    public function edit($iWebSiteDesignId ) 
	{

        if(!empty($iWebSiteDesignId))
        {
        $criteria                    = array();
        $criteria["iWebSiteDesignId"]  = $iWebSiteDesignId;

        $data['data']                = $this->website_design_model->get_by_id($criteria);
        $data['iWebSiteDesignId']      = $iWebSiteDesignId;
       $this->template->build('add',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/website_design/add'));
        }
    }

    // public function download_listing_pdf()
    // {
    //     $iWebSiteDesignId   = $this->input->post("iWebSiteDesignId");
    //     $base_path  = $this->config->item('base_path');
 
    //     if(!empty($iWebSiteDesignId))
    //     {
    //         foreach ($iWebSiteDesignId as $key => $value) 
    //         {
    //             $criteria                          = array();
    //             $criteria["iWebSiteDesignId"]        = $value;
    //             $data[$key]                        = $this->why_us_model->get_by_id($criteria);
    //         }

    //         if(!empty($data))
    //         {
    //             $html = "";
    //             $html.= '<table style="border-collapse: collapse;width: 100%;max-width: 1000px;margin: 0 auto;">';
    //             $html.= '<tr>';
    //                 $html.= '<td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">';
    //                     $html.= '<img src="'.base_url("assets/uploads/company_logo/logo.png").'" style="height: 70px;">';
    //                 $html.= '</td>';
    //             $html.= '</tr>';
    //             $html.= '</table>';

    //             $html.= "<table style='width:100%;border-collase:collapse;border-spacing:0px;border-bottom:1px solid #b5b5c3;padding:0;margin:0;font-size:14px;'>";
                
    //             $html.= "<tr>";
    //             $html.= "<td colspan='4' style='padding: 10px;background: #cacaca;color: #000;font-size: 14px;font-weight: bold;'><strong>Why Us Listing</strong></td>";
    //             $html.= "</tr>";
    
    //             $html.= "<tr>";
    //             $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Image</th>";
    //             $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Title</th>";

    //             $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:300px;height:50px;'>Description</th>";                
    //             $html.= "<th style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;border-right:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:70px;height:50px;'>Status</th>";
    //             $html.= "</tr>";
               
    //             foreach ($data as $key => $value) 
    //             {

    //                 if(!empty($value->vImage))
    //                 {
    //                     $image = base_url('assets/uploads/website_design/'.$value->iWhyUsId.'/'.$value->vImage);
    //                 }
    //                 else
    //                 {
    //                     $image = base_url('assets/mf_panel/images/noimage.png');
    //                 }  

    //                 $html.= "<tr>";
    //                 $html.='<td  style="border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;">
    //                 <img src='.$image.' style="height: 45px;width:auto;">';
    //                 $html.='</td>';
    //                 $html.= "<td  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->vTitle."</td>";
    //                 $html.= "<td  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:300px;height:50px;'>".$value->tDescription."</td>";
    //                 $html.= "<td  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;border-right:1px solid #b5b5c3;text-align:center;font-size:14px;width:70px;height:50px;'>".$value->eStatus."</td>";
    //                 $html.= "</tr>";
    //             }
 
    //             $html.= "</table>";

    //             $criteria               = array();
    //             $criteria["html"]       = $html;
    //             $criteria["path"]       = $base_path.'assets/uploads/listing_pdf/why_us/'.'why_us_listing_'.date("Y-m-d").'.pdf';                
    //             $this->pdf_library->create($criteria);
    //         }
 
    //          $mydata                     = array();
    //          $mydata["status"]           = "200";
    //          $mydata["message"]          = "success";
    //          $mydata["notification"]     = "Why Us Listing PDF Downloaded";
    //          $mydata["filename"]         = 'why_us_listing_'.date("Y-m-d").'.pdf';
    //          echo json_encode($mydata);
    //     }
    //      else
    //      {
    //          $mydata                     = array();
    //          $mydata["status"]           = "400";
    //          $mydata["message"]          = "error";
    //          $mydata["notification"]     = "Nothing to Download";
    //          echo json_encode($mydata);
    //      }
    // }   
    
    // public function download_listing_excel()
    // {
    //     $iWhyUsId                   =   $this->input->post("iWhyUsId");  
    //     $base_path                   = $this->config->item('base_path');

    //     if(!empty($iWhyUsId))
    //     {
    //         foreach ($iWhyUsId as $key => $value) 
    //         {
    //             $criteria               = array();
    //             $criteria["iWhyUsId"]  = $value;
    //             $data[$key]             = $this->why_us_model->get_by_id($criteria);
    //         }
    //         if(!empty($data))
    //         {
     
    //             $write_array[]          = array("Title","Description","Status");
 
    //             foreach ($data as $key => $value) 
    //             {
    //                 $write_array[]      = array($value->vTitle,$value->tDescription,$value->eStatus);
    //             }       

    //             $criteria                = array();
    //             $criteria["data"]        = $write_array;
    //             $criteria["title"]       = "Why Us"." ".date("Y-m-d");
    //             $criteria["path"]        = $base_path.'assets/uploads/listing_excel/why_us/'.'why_us_'.date("Y-m-d").'.xlsx';
    //             $this->excel_library->create($criteria);

    //             $vFileName = 'why_us_'.date("Y-m-d").'.xlsx';
    //         }
    //             $mydata                     = array();
    //             $mydata["status"]           = "200";
    //             $mydata["message"]          = "success";
    //             $mydata["notification"]     = "Why Us Listing Excel Downloaded";
    //             $mydata["filename"]         = $vFileName;
    //             echo json_encode($mydata);
    //     }
    //     else
    //     {
    //         $mydata                     = array();
    //         $mydata["status"]           = "400";
    //         $mydata["message"]          = "error";
    //         $mydata["notification"]     = "Nothing to Download";
    //         echo json_encode($mydata);
    //     }
    // }
}