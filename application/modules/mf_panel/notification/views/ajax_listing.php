<?php
$CI =& get_instance();
$CI->load->library('general');
?>
<input type="hidden" id="iCount" value="<?php echo count($data); ?>">

<?php if(!empty($data)) { ?>
    <?php foreach ($data as $key => $value) { ?>
            <tr style='<?php if($value->eStatus == "Inactive") { echo "background-color: #f5bcc138"; } ?>'>
                <td class="py-6">
                    <label class="checkbox checkbox-lg checkbox-inline  mr-0 align-items-center justify-content-center">                    
                        <input id="<?php echo $value->iNotificationId;?>" data-id="<?php echo $value->iNotificationId; ?>" data-value="<?php echo $value->iNotificationId; ?>" type="checkbox" name="iNotificationId[]" class="checkboxall client_checkbox" value="<?php echo $value->iNotificationId;?>">
                        <span></span>
                    </label>
                </td>
                <td class="text-left">
                <?php if(!empty($value->vImage)) { ?>
                    <img src="<?php echo base_url('assets/uploads/notification/'.$value->vImage); ?>" width="60px" id="vImage_Preview">
                <?php } else { ?>  
                    <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" id="vImage_Preview">
                <?php } ?>
                    </td> 
                <td class="text-left">
                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo ucwords($value->vNotification); ?></span>
                   Added By: <span class="card-label font-weight-bolder text-primary"><?php echo $value->eType;?></span>
                </td>   
                <td class="text-center">
                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo ucwords($value->eNotification); ?></span>
                </td> 
                    
                <td class="text-center">
                    <?php if($value->eStatus == "Active") { ?>
                        <span class="label label-lg label-light-success label-inline"><?php echo $value->eStatus; ?></span>
                    <?php } ?>
                    <?php if($value->eStatus == "Inactive") { ?>
                        <span class="label label-lg label-light-danger label-inline"><?php echo $value->eStatus; ?></span>
                    <?php } ?>                
                </td>
                <td class="text-center">
                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $CI->general->date_format2($value->dtUpdatedDate);  ?></span>
                </td>
                <td class="white-space-nowrap text-center">
                    <button class="btn btn-icon btn-primary btn-sm mr-1 edit" data-id="<?php echo $value->iNotificationId;?>">
                    <span class="fa fa-pencil"></span>
                    </button>
                    <button class="btn btn-icon btn-danger btn-sm delete" data-id="<?php echo $value->iNotificationId;?>">
                    <span class="fa fa-trash"></span>
                    </button>
                    
                </td>          
                
    <?php } ?>
         <tr>
            <td class="text-dark-75 font-weight-bolder font-size-lg"  colspan="7">
                <div class="row m-0">
                    <div class="text-left col-md-6 pl-0"><button class="btn btn-primary client_inactive">Active/Inactive</button></div>
                    <div class="spinner-border text-primary" role="status" id="client_inactive_loader" style="display: none;">
                      <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="9">
              
            </td>
        </tr>
        <tr>
            <td colspan="9"  style="background-color: #eef0f8;">
                <div class="">
                    <?php echo $paging; ?>    
                </div>   
            </td>
        </tr>
<?php } else { ?>
    <tr align="center">
        <td class="text-dark-75 font-weight-bolder font-size-lg" colspan="7">No Records</td>
    </tr>
<?php } ?>