<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary">Notification Listing</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <div class="d-flex align-items-center" id="kt_subheader_search">
                    <span class="text-dark-50 font-weight-bold" id="kt_subheader_total"><?php echo count($data); ?> Total</span>
                    <div class="ml-5">
                        <div class="input-group input-group-md input-group-solid" style="max-width:300px">
                            <input type="text" class="form-control keyword" id="kt_subheader_search_form" placeholder="Search...">
                            <div class="input-group-append">
                                <button class="btn btn-light-primary search" type="button">Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="ml-5">
                        <div class="input-group input-group-md input-group-solid" style="max-width:300px">
                                     <select class="form-control notification_change">
                                        <option value="">Select Notification</option>
                                        <option value="Client">Client</option>
                                        <option value="ChannelPartner">ChannelPartner</option>
                                        <option value="RelationshipManager">RelationshipManager</option>
                                        <option value="BDM">BDM</option>
                                        <option value="Employee">Employee</option>
                                        <option value="Branch">Branch</option>
                                    </select>
                        </div>                      
                    </div>
                    <div class="ml-5">
                        <div class="input-group input-group-md input-group-solid" style="max-width:300px">
                            <select class="form-control client_status_change">
                                <option value="">Status</option>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex align-items-center">
                <a href="<?php echo base_url('mf_panel/notification/add'); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus pr-0"></i></a>
                <div class="download_option dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
                                     
                </div>
                <div class="ml-2 spinner-border text-primary" role="status" id="download_loader" style="display: none;">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
        <div class="card-header border-0 py-5"></div>
        <div class="card card-custom gutter-b shadow-none" style="border-radius: 0 !important;">

        <div class="card-body py-0 mb-3">
            <div class="table-responsive">
            <table class="table table-striped table-bordered table-head-custom table-vertical-center" id="kt_advance_table_widget_4">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 30px">
                                <label class="checkbox checkbox-lg checkbox-inline  mr-0 align-items-center justify-content-center">
                                    <input type="checkbox" value="1">
                                    <span></span>
                                </label>
                            </th>
                            <th style="width:50px;min-width:50px;">Image</th>
                            <th style="width:450px;min-width:450px;text-align:left;">Notification</th>
                            <th style="width:180px;min-width:180px;">Notification Type</th>
                            <th style="width:90px;min-width:90px;">Status</th>
                            <th style="width:100px;min-width:100px;">Date</th>
                            <th style="width:60px;min-width:60px;">Action</th>
                        </tr>
                    </thead>
                    <tbody id="registrar_response">
                        <tr align="center">
                            <td colspan="7">
                                <div class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>   
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
            $.ajax({
                url: "<?php echo base_url('mf_panel/notification/notification/ajax_listing');?>",
                type: "POST",
                data:  {  }, 
                success: function(response) {
                    $("#registrar_response").html(response);
                }
            });
        }, 1000);
    });

    $(document).on('click','.edit',function(){
        id = $(this).data("id");

        location.href = "edit/"+id;
    });

    $(document).on('click','.delete',function(){
        if (confirm('Are you sure delete this data?')) {
            iNotificationId = $(this).data("id");

            $("#ajax-loader").show();

            url = "<?php echo base_url('mf_panel/notification/notification/ajax_listing');?>";

            setTimeout(function(){
                $.ajax({
                    url: url,
                    type: "POST",
                    data:  {iNotificationId:iNotificationId,vAction:'delete'}, 
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                        location.reload();
                    }
                });
            }, 1000);
        }
    });

    $(document).on('click', '.search', function() {
        var vKeyword = $(".keyword").val();
        var eStatus       = $(".client_status_change").val();

        if(vKeyword.length > 0) {
            $("#registrar_response").html(" ");
            $("#registrar_response").html('<tr align="center"><td colspan="7"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
            setTimeout(function(){
                $.ajax({
                    url: "<?php echo base_url('mf_panel/notification/ajax_listing');?>",
                    type: "POST",
                    data:  { vKeyword:vKeyword,eStatus:eStatus, vAction:'search' }, 
                    success: function(response) {
                        $("#registrar_response").html(" ");
                        $("#registrar_response").html(response);
                        $("#kt_subheader_total").html($("#iCount").val()+" Total");

                    }
                });
            }, 1000);
        } else {
            notification_error("Enter Search String");
        }
    }); 

    $(document).on('click', '.client_inactive', function() {
        var vKeyword            = $(".keyword").val();
        var iClientIdArray      = [];
        $('.client_checkbox').each(function(index, value) {
            if(this.checked == true)
            {
                iClientIdArray.push($(this).data("value"));
            }
        });

        if(iClientIdArray.length > 0) {
            $('.client_inactive').hide();
            $('#client_inactive_loader').show();
            $("#registrar_response").html(" ");
            $("#registrar_response").html('<tr align="center"><td colspan="7"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
            setTimeout(function(){
                $.ajax({
                    url: "<?php echo base_url('mf_panel/notification/ajax_listing');?>",
                    type: "POST",
                    data:  { iClientIdArray:iClientIdArray, vAction:'client_status',vKeyword:vKeyword }, 
                    success: function(response) {
                        $('.client_inactive').show();
                        $('#client_inactive_loader').hide();
                        $("#registrar_response").html(response);
                        notification_success("Notification Status Updated");
                    }
                });
            },1000)
        } else {
            notification_error("Please Select Notification to Mark Inactive");
        }
    });

    $(document).on('change', '.client_status_change', function() {
        var eStatus       = $(this).val();
        var vKeyword            = $(".keyword").val();
        var eNotification       = $(".notification_change").val();

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<tr align="center"><td colspan="7"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
        setTimeout(function(){
            $.ajax({
                url: "<?php echo base_url('mf_panel/notification/ajax_listing');?>",
                type: "POST",
                data:  { eStatus:eStatus,eNotification:eNotification, vKeyword:vKeyword }, 
                success: function(response) {
                    $("#registrar_response").html(response);
                }
            });
        },1000);
    })

    $(document).on('click', '.ajax_page', function() {
        
        var eDeleted    = "<?php echo $eDeleted; ?>";
        var vPage       = $(this).data('pages');
        var vKeyword    = $(".keyword").val();

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');

        $.ajax({
            url: "<?php echo base_url('mf_panel/notification/ajax_listing');?>",
            type: "POST",
            data: { vPage:vPage, vKeyword:vKeyword, eDeleted:eDeleted }, 
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    });

    $(document).on('change', '.notification_change', function() {
        var eNotification       = $(this).val();
        var eStatus             = $(".client_status_change").val();

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<tr align="center"><td colspan="7"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
        setTimeout(function(){
            $.ajax({
                url: "<?php echo base_url('mf_panel/notification/ajax_listing');?>",
                type: "POST",
                data:  { eNotification:eNotification,eStatus:eStatus,vAction:'notification_status' }, 
                success: function(response) {
                    $("#registrar_response").html(response);
                }
            });
        },1000);
    })

    $(document).on('change', '.sort', function() {
        var vKeyword            = $(".keyword").val();
        var eDeleted            = "<?php echo $eDeleted; ?>";
        var vSort               = $(this).val();


        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
        $.ajax({
            url: "<?php echo base_url('mf_panel/notification/ajax_listing');?>",
            type: "POST",
            data:  { vKeyword:vKeyword, vAction:'sort', eDeleted:eDeleted, vSort:vSort }, 
            success: function(response) {
                $("#registrar_response").html(" ");
                $("#registrar_response").html(response);
                $("#kt_subheader_total").html($("#iCount").val()+" Total");
            }
        });
    });
</script>