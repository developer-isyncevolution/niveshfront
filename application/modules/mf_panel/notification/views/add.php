<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary"><?php echo ($data->iNotificationId) ? 'Edit' : 'Add'; ?> Notification</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-10 px-8 py-lg-12 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post" action="<?php echo base_url('mf_panel/notification/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iNotificationId" name="iNotificationId" value="<?php echo $data->iNotificationId;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <div class="row">
                                    <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Notification <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vNotification" name="vNotification" placeholder="Enter Notification" value="<?php echo $data->vNotification;?>">
                                            <div class="text-danger" id="vNotification_error" style="display: none;">Enter Notification</div>
                                        </div>
                                    </div>                                   
                                     <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Status <span class="text-danger">*</span></label>
                                            <select name="eStatus" id="eStatus" class="form-control show-tick">
                                                <option value="">Select Status</option>
                                                <option <?php echo ($data->eStatus == 'Active') ? 'selected' : '';?> value="Active">Active</option>
                                                <option <?php echo ($data->eStatus == 'Inactive') ? 'selected' : '';?> value="Inactive">Inactive</option>
                                            </select>
                                            <div class="text-danger" id="eStatus_error" style="display: none;">Select Status</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Select Notification <span class="text-danger">*</span></label>
                                            <select name="eNotification" id="eNotification" class="form-control show-tick">
                                                <option value="">Select Notification</option>
                                                <option <?php echo ($data->eNotification == 'Client') ? 'selected' : '';?> value="Client">Client</option>
                                                <option <?php echo ($data->eNotification == 'ChannelPartner') ? 'selected' : '';?> value="ChannelPartner">Channel Partner</option>
                                                <option <?php echo ($data->eNotification == 'RelationshipManager') ? 'selected' : '';?> value="RelationshipManager">Relationship Manager</option>
                                                <option <?php echo ($data->eNotification == 'BDM') ? 'selected' : '';?> value="BDM">BDM</option>
                                                <option <?php echo ($data->eNotification == 'Employee') ? 'selected' : '';?> value="Employee">Employee</option>
                                                <option <?php echo ($data->eNotification == 'Branch') ? 'selected' : '';?> value="Branch">Branch</option>
                                            </select>
                                            <div class="text-danger" id="eNotification_error" style="display: none;">Select Notification</div>
                                        </div>
                                    </div> 
                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <label> Image </label>
                                                    <input type="file" class="form-control" name="vImage" id="vImage">        
                                                </div>
                                                <div class="col-md-3" style="margin-top: 25px;">
                                                  <?php if(!empty($data->vImage)) { ?>
                                                        <img src="<?php echo base_url('assets/uploads/notification/'.$data->vImage); ?>" width="60px" id="vImage_Preview">
                                                    <?php } else { ?>  
                                                        <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" id="vImage_Preview">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Description <span class="text-danger">*</span></label>
                                            <textarea id="tDescription" class="form-control" name="tDescription" rows="4" placeholder = "Enter Description" cols="40"><?php echo $data->tDescription;?></textarea>
                                            <div class="text-danger" id="tDescription_error" style="display: none;">Enter Description</div>
                                        </div>
                                    </div>                  
                                </div>
                            </div>
                            <div class="row">
                                  <div class="col-md-12 col-lg-12">
                                <button class="btn btn-primary font-weight-bolder text-uppercase next_loader" type="button" disabled style="display: none;">
                                      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                      Loading...
                                    </button>
                                <button type="button" class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>&nbsp
                                <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/notification/notification'); ?>";
    });

    $(document).on('change','#vImage',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vImage_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
    
    $(document).on('click', '.submit', function () {
        var iNotificationId       = $("#iNotificationId").val();
        var vNotification         = $("#vNotification").val();
        var tDescription          = $("#tDescription").val();
        var eStatus               = $("#eStatus").val();
        var eNotification         = $("#eNotification").val();
        
        var vError                = false;

        if(vNotification.length == 0){
            $("#vNotification_error").show();
            vError = true;
        } else {
            $("#vNotification_error").hide();
        }
        if(tDescription.length == 0){
            $("#tDescription_error").show();
            vError = true;
        } else {
            $("#tDescription_error").hide();
        }
        if(eStatus.length == 0){
            $("#eStatus_error").show();
            vError = true;
        } else {
            $("#eStatus_error").hide();
        }
        if(eNotification.length == 0){
            $("#eNotification_error").show();
            vError = true;
        } else {
            $("#eNotification_error").hide();
        }
       
        setTimeout(function(){
          if(vError == false){
              $(".submit").hide();
              $(".next_loader").show();
              setTimeout(function(){
                  $("#frm").submit();
              }, 500);
           } 
        }, 1000);

    });

</script>