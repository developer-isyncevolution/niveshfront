<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('notification_model');
        $this->load->library('pagination');
        $this->load->library('general');
        $this->load->library('pdf_library');
        $this->load->library('excel_library');
        $this->general->authentication();
    }

    public function index()
    {
        $criteria               = array();
        $criteria["eDeleted"]   = "No";

        $data                   = array();
        $data["data"]           = $this->notification_model->get_all_data($criteria);
        $data["eDeleted"]       = "No";

        $this->template->build('listing', $data);
    }

    public function ajax_listing()
    {
        $vAction            = $this->input->post('vAction');
        $iNotificationId    = $this->input->post('iNotificationId');
        $vKeyword           = $this->input->post('vKeyword');
        $iClientIdArray     = $this->input->post('iClientIdArray');
        $eNotification     = $this->input->post('eNotification');
        $eStatus            = $this->input->post('eStatus');
        $vPage              = $this->input->post('vPage');
        $vSort              = $this->input->post('vSort');
        
        if($vAction == "sort" && !empty($vSort))
        {
        $res =   list($vColumn, $vOrder) = explode('-', $vSort);
            $column = $vColumn;
            $order = $vOrder;
        } else {
            $column = "iNotificationId";
            $order = "DESC";
        }

        if($vAction == "delete")
        {
             //update eDelete Status
             $where = array();
             $where["iNotificationId"] = $iNotificationId;
 
             $data_update = array();
             $data_update["eDeleted"] = "Yes";
             
             $this->notification_model->update($where, $data_update);
             //update eDelete Status
        }  

        if($vAction == "client_status" & !empty($iClientIdArray))
        {
            foreach ($iClientIdArray as $key => $value) 
            {
                $myArray                        = array();
                $myArray["iNotificationId"]     = $value;
                $check                          = $this->notification_model->get_by_id($myArray);

                if($check->eStatus == "Active")
                {
                    $where                       = array();
                    $where["iNotificationId"]    = $value;

                    $mydata                      = array();
                    $mydata["eStatus"]           = "Inactive";
                    $mydata["eRead"]             = "No";

                    $this->notification_model->update($where, $mydata);
                }
                else
                {
                    $where                       = array();
                    $where["iNotificationId"]    = $value;

                    $mydata                      = array();
                    $mydata["eStatus"]           = "Active";
                    $mydata["eRead"]             = "No";


                    $this->notification_model->update($where, $mydata);   
                }
            }
        }

        if($vAction == "notification_status")
        {                    
                if($eNotification == 'ChannelPartner')
                {
                    $criteria                    = array();
                    $criteria['eNotification']   =    $eNotification;
                    $data                        = $this->notification_model->get_all_data($criteria);                   
                }
                if($eNotification == 'Client')
                {
                    $criteria                    = array();
                    $criteria['eNotification']   = $eNotification;
                    $data                        = $this->notification_model->get_all_data($criteria);                   
                }
                if($eNotification == 'RelationshipManager')
                {
                    $criteria                    = array();
                    $criteria['eNotification']   =    $eNotification;
                    $data                        = $this->notification_model->get_all_data($criteria);                   
                }
                if($eNotification == 'BDM')
                {
                    $criteria                    = array();
                    $criteria['eNotification']   = $eNotification;
                    $data                        = $this->notification_model->get_all_data($criteria);                   
                }
                if($eNotification == 'Employee')
                {
                    $criteria                    = array();
                    $criteria['eNotification']   = $eNotification;
                    $data                        = $this->notification_model->get_all_data($criteria);                   
                }
                if($eNotification == 'Branch')
                {
                    $criteria                    = array();
                    $criteria['eNotification']   = $eNotification;
                    $data                        = $this->notification_model->get_all_data($criteria);                   
                }            
            
        }
        
        $criteria['iAddedBy']       = $this->session->userdata('iUserId');
        $criteria['Keyword']        = $Keyword;
        $criteria['filter']         = $filter;
        $criteria['column']         = $column;
        $criteria['order']          = $order;

        if(!empty($eStatus)) {
            $criteria['eStatus']       = $eStatus;  
            $criteria['eNotification'] = $eNotification;

        }

        $criteria['vKeyword']       = $vKeyword;

        $data                       = $this->notification_model->get_all_data($criteria);
        $data['count']            = count($data);

        $pages                    = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;

        $paginator->is_ajax         = true;
        $paging                     = true;    

        $data['data']               = $this->notification_model->get_all_data($criteria);

        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {
      
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $base_path           = $this->config->item('base_path');

            $iNotificationId            = $this->input->post('iNotificationId');

            $data['vNotification']      = $this->input->post('vNotification');           
            $data['tDescription']       = $this->input->post('tDescription');           
            $data['eStatus']            = $this->input->post('eStatus');
            $data['eNotification']      = $this->input->post('eNotification');
            $data['iAddedBy']           = $this->session->userdata('iUserId');        
           
            if($_FILES['vImage']['name'] != "")
            {
                if($_FILES['vImage']['type'] == 'image/gif'){
                    $ext = ".gif";
                } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                    $ext = ".jpeg";
                } else if($_FILES['vImage']['type'] == 'image/png'){
                    $ext = ".png";
                } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                    $ext = ".jpeg";
                }
                $vImage = time().str_replace(' ','',$this->input->post('vNotification')).$ext;
                move_uploaded_file($_FILES['vImage']['tmp_name'], $base_path.'assets/uploads/notification/'.$vImage);
                $data['vImage'] = $vImage;

            }
            if($iNotificationId){
                $data['dtUpdatedDate'] 	= date("Y-m-d");
            }else{
                $data['dtAddedDate'] 	= date("Y-m-d");
                $data['dtUpdatedDate'] 	= date("Y-m-d");
            }
            if($iNotificationId)
            {
                $data["eRead"] = "No"; 
                $where = array("iNotificationId" => $iNotificationId);                

                $this->notification_model->update($where, $data);

                $this->session->set_flashdata('success', "Notification Updated Successfully." );
                redirect(base_url('mf_panel/notification/notification'));
            }
            else
            {
                
                $this->notification_model->add($data);

                $this->session->set_flashdata('success', "Notification Added Successfully." );
                redirect(base_url('mf_panel/notification/notification'));
            }
        }
        else
        {
            $this->template->build('add',null);
        }
    }

    public function edit($iNotificationId = NULL) 
	{
       if(!empty($iNotificationId))
       {
        $criteria                            = array();
        $criteria['iNotificationId']         =  $iNotificationId;
        $data['data'] 	                     = $this->notification_model->get_by_id($criteria);	
        
        $this->template->build('add',$data);
       }
       else{
        $this->session->set_flashdata('error', "Something Went Wrong." );
        redirect(base_url('mf_panel/notification/add'));
       }
    }   
 
    function notification_read()
    {

        $eType   = $this->session->userdata('eType');

        if($eType == 'SuperPartner'){
            $criteria['eNotification'] = 'BDM';

            $where = array();
            $where["eNotification"] = 'BDM';

            $data_update = array();
            $data_update["eRead"] = "Yes"; 
           
        }
        else 
        {
            $criteria['eNotification'] = $eType;

            $where = array();
            $where["eNotification"] = $eType;

            $data_update = array();
            $data_update["eRead"] = "Yes"; 
        }
      
        $this->notification_model->update($where, $data_update);
        
    }
 
}