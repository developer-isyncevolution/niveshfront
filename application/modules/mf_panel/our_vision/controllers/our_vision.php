<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Our_vision extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('our_vision_model');
        $this->load->model('user/user_model');
        $this->load->model('system_email/system_email_model');
        $this->load->library('pagination');
        $this->load->library('general');
        $this->load->library('pdf_library');
        $this->load->library('excel_library');  
        $this->general->authentication();
    }

    public function index()
    {
  
        $this->template->build('listing', NULL);
    }

    public function ajax_listing()
    {
        $vAction        = $this->input->post('vAction');
        $iVisionId      = $this->input->post('iVisionId');
        $vKeyword       = $this->input->post('vKeyword');
        $eStatus        = $this->input->post('eStatus');
        $eDeleted       = $this->input->post('eDeleted');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');
        $iClientIdArray     = $this->input->post('iClientIdArray');


        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iVisionId";
            $order      = "DESC";
        }

        if($vAction == "delete" && !empty($iVisionId))
        {
            $iVisionId = $this->input->post('iVisionId');
			$this->our_vision_model->delete($iVisionId);
        }
        if($vAction == "client_status" & !empty($iClientIdArray))
        {
            foreach ($iClientIdArray as $key => $value) 
            {
                $myArray                = array();
                $myArray["iVisionId"]     = $value;
                $check                  = $this->our_vision_model->get_by_id($myArray);

                if($check->eStatus == "Active")
                {
                    $where               = array();
                    $where["iVisionId"]    = $value;

                    $mydata              = array();
                    $mydata["eStatus"]   = "Inactive";

                    $this->our_vision_model->update($where, $mydata);
                }
                else
                {
                    $where              = array();
                    $where["iVisionId"]   = $value;

                    $mydata             = array();
                    $mydata["eStatus"]  = "Active";

                    $this->our_vision_model->update($where, $mydata);   
                }
            }
        } 
    

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(!empty($eStatus)) {
            $criteria['eStatus']  = $eStatus;
        }       
         $criteria['vKeyword']   = $vKeyword;

        $data                   = $this->our_vision_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->our_vision_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $base_path                  = $this->config->item("base_path");
            $iVisionId                  = $this->input->post('iVisionId');

            $data['vTitle']             = $this->input->post('vTitle');
            $data['tDescription']       = $this->input->post('tDescription');
            $data['eStatus']            = $this->input->post('eStatus');
        
			if($iVisionId){
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}else{
				$data['dtAddedDate'] 	= date("Y-m-d h:i:s");
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}
            if(!empty($iVisionId))
            {
                mkdir($base_path.'assets/uploads/our_vision/' . $iVisionId, 0777, TRUE);

                //image update code
                if($_FILES['vImage']['name'] != "")
                {
                    if($_FILES['vImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }

                    $vImage = time().'_Image'.$ext;
                    move_uploaded_file($_FILES['vImage']['tmp_name'], $base_path.'assets/uploads/our_vision/'.$iVisionId.'/'.$vImage);

                    $data['vImage'] = $vImage;
                }
            
                sleep(1);
    
                //image update code

                $where = array("iVisionId" => $iVisionId);
                $this->our_vision_model->update($where, $data);

                $this->session->set_flashdata('success', "Vision Updated Successfully." );
                redirect(base_url('mf_panel/our_vision/our_vision'));
            }
            else
            {
                $ID = $this->our_vision_model->add($data);

                mkdir($base_path.'assets/uploads/our_vision/' . $ID, 0777, TRUE);

                if($_FILES['vImage']['name'] != "")
                {
                    if($_FILES['vImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vImage = time().str_replace(' ','',$this->input->post('vTitle')).$ext; 
                    move_uploaded_file($_FILES['vImage']['tmp_name'], $base_path.'assets/uploads/our_vision/'.$ID.'/'.$vImage);
                    $update_where                 = array();
                    $update_where["iVisionId"]    = $ID;
    
                    $update_image                 = array();
                    $update_image["vImage"]       = $vImage;
                    $this->our_vision_model->update($update_where, $update_image);
    
                }
                sleep(1);
                //image update    
                $this->session->set_flashdata('success', "Vision Added Successfully." );
                redirect(base_url('mf_panel/our_vision/our_vision'));
            }
        }
        else
        {
            $this->template->build('add', NULL);
        }
    }

    public function edit($iVisionId ) 
	{

        if(!empty($iVisionId))
        {
        $criteria               = array();
        $criteria["iVisionId"]  = $iVisionId;

        $data['data']           = $this->our_vision_model->get_by_id($criteria);
        $data['iVisionId']      = $iVisionId;
       $this->template->build('add',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/banner/add'));
        }
    }
    
    public function download_listing_pdf()
    {
        $iVisionId                      = $this->input->post("iVisionId");
        $base_path                      = $this->config->item('base_path');
 
        if(!empty($iVisionId))
        {
            foreach ($iVisionId as $key => $value) 
            {
                $criteria               = array();
                $criteria["iVisionId"]  = $value;
                $data[$key]             = $this->our_vision_model->get_by_id($criteria);
            }            

            if(!empty($data))
            {
                $html = "";
                $html.= '<table style="border-collapse: collapse;width: 100%;max-width: 1000px;margin: 0 auto;">';
                $html.= '<tr>';
                    $html.= '<td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">';
                        $html.= '<img src="'.base_url("assets/uploads/company_logo/logo.png").'" style="height: 70px;">';
                    $html.= '</td>';
                $html.= '</tr>';
                $html.= '</table>';
                $html.= "<table style='width:100%;border-collase:collapse;border-spacing:0px;border-bottom:1px solid #b5b5c3;padding:0;margin:0;font-size:14px;'>";
                
                $html.= "<tr>";
                $html.= "<td colspan='4' style='padding: 10px;background: #cacaca;color: #000;font-size: 14px;font-weight: bold;'><strong>Banner Listing</strong></td>";
                $html.= "</tr>";
    
                $html.= "<tr>";
                $html.= "<th  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Image</th>";
                $html.= "<th  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:100px;height:50px;'>Banner Title</th>";

                $html.= "<th  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:300px;height:50px;'>Description</th>";                
                $html.= "<th  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;border-right:1px solid #b5b5c3;padding:5px 5px;text-align:center;width:70px;height:50px;'>Status</th>";
                $html.= "</tr>";
               
                foreach ($data as $key => $value) 
                {
                    if(!empty($value->vImage))
                    {
                        $image = base_url('assets/uploads/our_vision/'.$value->iVisionId.'/'.$value->vImage);
                    }
                    else
                    {
                        $image = base_url('assets/mf_panel/images/noimage.png');
                    }  

                    $html.= "<tr>";
                    $html.='<td  style="border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;">
                    <img src='.$image.' style="width: 100px;">';
                    $html.='</td>';
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:100px;height:50px;'>".$value->vBannerTitle."</td>";
                    $html.= "<td  style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;text-align:center;font-size:14px;width:300px;height:50px;'>".$value->tDescription."</td>";
                    $html.= "<td style='border-top:1px solid #b5b5c3;border-left:1px solid #b5b5c3;padding:5px 5px;border-right:1px solid #b5b5c3;text-align:center;font-size:14px;width:70px;height:50px;'>".$value->eStatus."</td>";
                    $html.= "</tr>";
                }
 
                $html.= "</table>";

                $criteria                = array();
                $criteria["html"]        = $html;
                $criteria["path"]        = $base_path.'assets/uploads/listing_pdf/our_vision/'.'our_vision_'.date("Y-m-d").'.pdf';                
                $this->pdf_library->create($criteria);
            }
 
             $mydata                     = array();
             $mydata["status"]           = "200";
             $mydata["message"]          = "success";
             $mydata["notification"]     = "Our Vision Listing PDF Downloaded";
             $mydata["filename"]         = 'our_vision_'.date("Y-m-d").'.pdf';
             echo json_encode($mydata);
        }
         else
         {
             $mydata                     = array();
             $mydata["status"]           = "400";
             $mydata["message"]          = "error";
             $mydata["notification"]     = "Nothing to Download";
             echo json_encode($mydata);
         }
    }   
    
    public function download_listing_excel()
    {
        $iVisionId                      =  $this->input->post("iVisionId");  
        $base_path                      = $this->config->item('base_path');

        if(!empty($iVisionId))
        {
            foreach ($iVisionId as $key => $value) 
            {
                $criteria               = array();
                $criteria["iVisionId"]  = $value;
                $data[$key]             = $this->our_vision_model->get_by_id($criteria);
            }
            if(!empty($data))
            {     
                $write_array[]          = array("Banner Title","Description","Status");
 
                foreach ($data as $key => $value) 
                {
                    $write_array[]      = array($value->vBannerTitle,$value->tDescription,$value->eStatus);
                }

                $criteria                = array();
                $criteria["data"]        = $write_array;
                $criteria["title"]       = "Banner"." ".date("Y-m-d");
                $criteria["path"]        = $base_path.'assets/uploads/listing_excel/banner/'.'banner_'.date("Y-m-d").'.xlsx';
                $this->excel_library->create($criteria);

                $vFileName               = 'banner_'.date("Y-m-d").'.xlsx';
            }
                $mydata                  = array();
                $mydata["status"]        = "200";
                $mydata["message"]       = "success";
                $mydata["notification"]  = "Banner Listing Excel Downloaded";
                $mydata["filename"]      = $vFileName;
                echo json_encode($mydata);
        }
        else
        {
            $mydata                      = array();
            $mydata["status"]            = "400";
            $mydata["message"]           = "error";
            $mydata["notification"]      = "Nothing to Download";
            echo json_encode($mydata);
        }
    }

  

}