<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary"><?php echo ($data->iAboutUsId) ? 'Edit ' : 'Add '; ?> About Us</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post" action="<?php echo base_url('mf_panel/about_us/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iAboutUsId" name="iAboutUsId" value="<?php echo $iAboutUsId;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                            
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Title <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder=" Enter Title" value="<?php echo $data->vTitle;?>">
                                            <div class="text-danger" id="vTitle_error" style="display: none;">Enter Title</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6  col-md-6 ">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Image </label>
                                                    <input type="file" class="form-control" name="vImage" id="vImage">
                                                </div>
                                                <div class="col-md-6 mt-0 mt-2">
                                                    <?php if(!empty($data->vImage)) { ?>
                                                        <img src="<?php echo base_url('assets/uploads/about_us/'.$iAboutUsId.'/'.$data->vImage); ?>" width="60px" id="vImage_Preview">
                                                    <?php } else { ?>  
                                                        <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" id="vImage_Preview">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="text-danger" id="vImage_Error" style="display: none;">Image Required</div>
                                        </div>
                                    </div>
                                     <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <select name="eStatus" id="eStatus" class="form-control show-tick">
                                        <option value="">Select Status</option>
                                            <option <?php echo ($data->eStatus == 'Active') ? 'selected' : '';?> value="Active">Active</option>
                                            <option <?php echo ($data->eStatus == 'Inactive') ? 'selected' : '';?> value="Inactive">Inactive</option>
                                            
                                        </select>
                                        <div class="text-danger" id="eStatus_error" style="display: none;">Select Status</div>

                                        </div>
                                    </div>  
                                    <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Content</label>
                                            <textarea id="tContent" class="form-control" name="tContent" rows="4" placeholder = "Enter Content" cols="40"><?php echo $data->tContent;?></textarea>
                                            <div class="text-danger" id="tContent_error" style="display: none;">Enter Content</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Long Content</label>
                                            <textarea id="tLongContent" class="form-control" name="tLongContent" rows="4" placeholder = "Enter Long Content" cols="40"><?php echo $data->tLongContent;?></textarea>
                                            <div class="text-danger" id="tLongContent_error" style="display: none;">Enter Long Content</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader" type="button" disabled style="display: none;">
                                  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                  Loading...
                                </button>
                                &nbsp;
                                <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                </div>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.9.2/tinymce.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<script type="text/javascript">
  
    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/about_us/about_us'); ?>";
    });

    $(document).ready(function() {
        tinymce.init({
            selector: '#tLongContent',
            height: 500,
            width: 905,
            file_picker_types: 'file image media',
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount', 'image code',
            ],
            toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help | link image ',
                    image_title: true,
                automatic_uploads: true,
                file_picker_types: 'image',
                file_picker_callback: function (cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {

                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                    reader.readAsDataURL(file);
                };
                input.click();
            },
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    });


    $(document).on('click', '.submit', function () {
        var $iAboutUsId            = $("#iAboutUsId").val();
        var vTitle                 = $("#vTitle").val();
        var eStatus              = $("#eStatus").val();
        // var tContent              = $("#tContent").val();
        var tLongContent              = $("#tLongContent").val();
        var vError               = false;

        if(vTitle.length == 0){
            $("#vTitle_error").show();
            vError = true;
        } else {
            $("#vTitle_error").hide();
        }

        if(eStatus.length == 0){
            $("#eStatus_error").show();
            vError = true;
        } else {
            $("#eStatus_error").hide();
        }
        // if(tContent.length == 0){
        //     $("#tContent_error").show();
        //     vError = true;
        // } else {
        //     $("#tContent_error").hide();
        // }

        if(tLongContent.length == 0){
            $("#tLongContent_error").show();
            vError = true;
        } else {
            $("#tLongContent_error").hide();
        }
        if(vError == false){
            $(".submit").hide();
            $(".submit_loader").show();
            setTimeout(function(){
              $("#frm").submit();
            }, 1000);
        }
    });


    $(document).on('change','#vImage',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vImage_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
   

</script>