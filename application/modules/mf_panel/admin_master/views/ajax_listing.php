<?php
$CI =& get_instance();
$CI->load->library('general');
?>
<input type="hidden" id="iCount" value="<?php echo count($data); ?>">
<?php if(!empty($data)) { ?>
    <?php foreach ($data as $key => $value) { ?>
        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 branch_checkbox" data-id="<?php echo $value->iAdminId; ?>"  data-value="<?php echo $value->iAdminId; ?>">
                    <div class="card card-custom gutter-b card-stretch" style="<?php if($value->eStatus == "Inactive") { echo "background-color: #ffd3d3"; } ?>">
                        <div class="card-body pt-4">
                            <div class="d-flex align-items-center mb-5">
                                <div class="flex-shrink-0 mr-4">
                                    <div class="symbol symbol-circle symbol-lg-75 obg-c">
                                        <?php
                                            if($value->vImage != ""){
                                                $image = base_url('assets/uploads/admin_master/'.$value->iAdminId.'/'.$value->vImage);
                                            } else {
                                                $image = base_url('assets/mf_panel/images/noimage.png');
                                            }
                                        ?>
                                        <img src="<?php echo $image; ?>" alt="<?php echo $value->vFirstName; ?>" />
                                    </div>
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="text-dark font-weight-bold text-hover-primary font-size-h6 mb-0"><?php echo strtoupper($value->vFirstName); ?></span>
                                    <span class="text-muted font-weight-bold"><span class="label label-lg label-light-warning label-inline mr-1 font-weight-bolder"><?php echo $value->vCode; ?></span></span>
                                </div>
                            </div>
                            <div class="mb-5">
                                <div class="d-flex justify-content-between align-items-center my-1 w-100">
                                    <span class="label label-lg label-light-link label-inline w-100"><?php if(!empty($value->vEmail)) { echo $value->vEmail; } else { echo "N/A"; } ?></span>
                                </div>

                                <div class="d-flex justify-content-between align-items-center my-1">
                                    <span class="text-dark-75 font-weight-bolder">Name:</span>
                                    <span class="label label-lg label-light-success label-inline mt-2 mr-1"><?php if(!empty($value->vFirstName)) { echo $value->vFirstName; } else { echo "N/A"; } ?></span>
                                </div>

                                <div class="d-flex justify-content-between align-items-center my-1">
                                    <span class="text-dark-75 font-weight-bolder">Mobile:</span>
                                    <span class="label label-lg label-light-danger label-inline mt-2 mr-1"><?php if(!empty($value->vMobile)) { echo $value->vMobile; } else { echo "N/A"; } ?></span>
                                </div>

                                <div class="d-flex justify-content-between align-items-center my-1">
                                    <span class="text-dark-75 font-weight-bolder">Phone:</span>
                                    <span class="label label-lg label-light-info label-inline mt-2 mr-1"><?php if(!empty($value->vPhone)) { echo $value->vPhone; } else { echo "N/A"; } ?></span>
                                </div>


                            </div>

                            <div class="text-center">
                                <a href="javascript:;" class="btn btn-icon btn-light-success btn-sm edit" data-id="<?php echo $value->iAdminId; ?>" data-toggle="tooltip" title="Edit">
                                    <span class="fa fa-pencil"></span>
                                </a>
                                <a href="javascript:;" class="btn btn-icon btn-light-danger btn-sm delete" data-id="<?php echo $value->iAdminId; ?>" data-toggle="tooltip" title="Delete">
                                    <span class="fa fa-trash"></span>
                                </a>
                                <a href="javascript:;" class="btn btn-icon btn-light-info btn-sm change_password" data-id="<?php echo $value->iAdminId; ?>" data-toggle="tooltip" title="Change Password">
                                    <span class="fa fa-lock"></span>
                                </a>
                                <a href="javascript:;" class="btn btn-icon btn-light-primary btn-sm send_email" data-id="<?php echo $value->iAdminId; ?>" data-toggle="tooltip" title="Email">
                                    <span class="fa fa-envelope"></span>
                                </a>
                                <a href="javascript:;" class="btn btn-icon btn-light-warning btn-sm admin_master_status" data-id="<?php echo $value->iAdminId; ?>" data-toggle="tooltip" title="Make Inactive">
                                    <span class="fa fa-close"></span>
                                </a>
                            </div>
                            
                        </div>
                    </div>
                </div>

    <?php } ?>

    <div class="container ">
        <div class="row">
            <?php echo $paging; ?> 
        </div>   
    </div>
    
<?php } else { ?>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
        <span class="label label-inline label-light-warning font-weight-bolder font-size-h6">No Data Found</span>
    </div>
<?php } ?>