
<div class="container mb-3">
    <div class="card card-custom">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-10 px-8 py-lg-12 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post" action="<?php echo base_url('mf_panel/admin_master/change_password_action');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iAdminId" name="iAdminId" value="<?php echo $iAdminId;?>">
                            <input type="hidden" id="iUserId" name="iUserId" value="<?php echo $data->iUserId;?>">

                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <h4 class="mb-5 font-weight-bold text-primary"><strong><?php echo ($iAdminId) ? '' : ''; ?> </strong>Change Password</h4>        
                                    </div>                                    
                                </div>
                                <hr>                                
                                <div class="row">                                  
                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>New Password <span class="text-danger">*</span></label>
                                            <div class="input-group xl-3">                                   
                                                <input type="password" class="form-control" id="vPassword" name="vPassword" placeholder="Enter New Password" value="">
                                                <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-eye-slash" id="eye"></i></div>
                                                </div>
                                            </div> 
                                            <div class="text-danger" id="vPassword_error" style="display: none;">Enter New Password</div>
                                                               
                                        </div>                                                                
                                    </div>                                                                     
                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Confirm Password <span class="text-danger">*</span></label>
                                            <div class="input-group xl-3">
                                                <input type="password" class="form-control" id="vConfirmPassword" name="vConfirmPassword" placeholder="Enter Confirm Password" value="<?php echo $data->vConfirmPassword;?>">
                                                <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-eye-slash" id="eye2"></i></div>
                                                </div>                                               
                                            </div>
                                            <div class="text-danger" id="vConfirmPassword_error" style="display: none;">Enter Confirm Password</div>
                                                <div class="text-danger" id="vConfirmPassword_same_error" style="display: none;">Password should match</div>

                                        </div>
                                    </div>                                   
                                </div>                      
                                <div class="row">
                                  <div class="col-md-12">
                                  <button class="btn btn-primary font-weight-bolder text-uppercase next_loader" type="button" disabled style="display: none;">
                                      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                      Loading...
                                  </button>
                                  <button type="button" class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>&nbsp
                                  <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/admin_master/admin_master'); ?>";
    });

    $(document).on('click', '.submit', function () {
        var iAdminId                     = $("#iAdminId").val();
        var vPassword                     = $("#vPassword").val();
        var vConfirmPassword              = $("#vConfirmPassword").val();

        var vError              = false;

          if(vPassword.length == 0){
              $("#vPassword_error").show();
              vError = true;
          } else {
              $("#vPassword_error").hide();
          }
          if(vConfirmPassword.length == 0){
              $("#vConfirmPassword_error").show();
              vError = true;
          } else {
              $("#vConfirmPassword_error").hide();
          }

          if(vPassword.length != 0 && vConfirmPassword.length != 0)
          {
            if(vPassword != vConfirmPassword){
            $("#vConfirmPassword_same_error").show();
            return false;
            } else{
              $("#vConfirmPassword_same_error").hide();
            } 
          }
        
        setTimeout(function(){
          if(vError == false){
            $(".submit").hide();
            $(".next_loader").show();
            setTimeout(function(){
              $("#frm").submit();
            }, 500);
          } 
        }, 1000);

    });
    $(document).on('click', '#eye', function () {
			
        if($(this).hasClass('fa-eye-slash')){
        
        $(this).removeClass('fa-eye-slash');
        
        $(this).addClass('fa-eye');
        
        $('#vPassword').attr('type','text');
            
        }else{
        
        $(this).removeClass('fa-eye');
        
        $(this).addClass('fa-eye-slash');  
        
        $('#vPassword').attr('type','password');
        }
	});

    $(document).on('click', '#eye2', function () {
			
            if($(this).hasClass('fa-eye-slash')){
            
            $(this).removeClass('fa-eye-slash');
            
            $(this).addClass('fa-eye');
            
            $('#vConfirmPassword').attr('type','text');
                
            }else{
            
            $(this).removeClass('fa-eye');
            
            $(this).addClass('fa-eye-slash');  
            
            $('#vConfirmPassword').attr('type','password');
            }
        });
</script>
