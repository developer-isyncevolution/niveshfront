<div class="container mb-3">
    <div class="card card-custom">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-10 px-8 py-lg-12 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post" action="<?php echo base_url('mf_panel/admin_master/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iAdminId" name="iAdminId" value="<?php echo $iAdminId;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-9">
                                        <h4 class="mb-5 font-weight-bold text-primary"><strong><?php echo ($iAdminId) ? 'Edit' : 'Add'; ?> Admin Detail</strong></h4>        
                                    </div>
                                                                     
                                </div><hr>                                
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label> First Name <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vFirstName" name="vFirstName" placeholder="First Name" value="<?php echo $data->vFirstName;?>">
                                            <div class="text-danger" id="vFirstName_error" style="display: none;">First Name Required</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label> Last Name <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vLastName" name="vLastName" placeholder="Last Name" value="<?php echo $data->vLastName;?>">
                                            <div class="text-danger" id="vLastName_error" style="display: none;">Last Name Required</div>
                                        </div>
                                    </div>
                                    <?php if(empty($data->iAdminId)) { ?>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Password <span class="text-danger">*</span></label>
                                            <div class="input-group xl-3"> 
                                                <input type="password" class="form-control" id="vPassword" name="vPassword" placeholder="Password" value="<?php echo $data->vPassword;?>">
                                                <div class="input-group-append">
                                                 <div class="input-group-text"><i class="fas fa-eye-slash" id="eye"></i></div>
                                                </div>
                                            </div> 
                                                <div class="text-danger" id="vPassword_error" style="display: none;">Password Required</div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Address <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vAddress" name="vAddress" placeholder="Address" value="<?php echo $data->vAddress;?>">
                                            <div class="text-danger" id="vAddress_error" style="display: none;">Address Requried</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>City <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vCity" name="vCity" placeholder="City" value="<?php echo $data->vCity;?>">
                                            <div class="text-danger" id="vCity_error" style="display: none;">City Required</div>
                                        </div>
                                    </div>
                                  
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>DOB <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="dBirthDate" id="dBirthDate" placeholder="Birth Date" value="<?php if(!empty($data->dBirthDate)) { echo $this->general->show_date_format($data->dBirthDate); } else { echo $this->general->show_date_format(date('Y-m-d')); } ?>">
                                            <div class="text-danger" id="dBirthDate_Error" style="display: none;">BirthDate Required </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Phone <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" maxlength="10" min = "1" id="vPhone" name="vPhone" placeholder="Enter Phone" value="<?php echo $data->vPhone;?>">
                                            <div class="text-danger" id="vPhone_error" style="display: none;">Phone Number Required</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Email <span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" id="vEmail" name="vEmail" placeholder="Enter Email" value="<?php echo $data->vEmail;?>">
                                            <div id="vEmail_error" class="error" style="display: none;  color: #F64E60 !important;"> Enter Email</div>
                                            <div id="vEmail_valid_error" class="error" style="display: none;  color: #F64E60 !important;"> Enter Valid Email</div>
                                            <div id="vEmail_unique_error" class="error" style="display: none;  color: #F64E60 !important;"> Enter Different Email</div>                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Mobile <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control"  id="vMobile" name="vMobile" placeholder="Enter Mobile" value="<?php echo $data->vMobile;?>" maxlength="10">
                                            <div class="text-danger" id="vMobile_error" style="display: none;">Enter Mobile</div>
                                            <div class="text-danger" id="MobileDigit_error" style="display: none;">Enter 10 digit mobile number</div>

                                        </div>
                                    </div>
                                    <div class="col-xl-6  col-md-6 ">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Image </label>
                                                    <input type="file" class="form-control" name="vImage" id="vImage1">        
                                                </div>
                                                <div class="col-md-6">
                                                    <?php if(!empty($data->vImage)) { ?>
                                                        <img src="<?php echo base_url('assets/uploads/admin_master/'.$iAdminId.'/'.$data->vImage); ?>" width="60px" id="vImage1_Preview">
                                                    <?php } else { ?>  
                                                        <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" id="vImage1_Preview">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="text-danger" id="vImage_Error" style="display: none;">Image Required</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Aadhar Card</label>
                                            <input type="text" class="form-control" name="vAadharCard" id="vAadharCard" placeholder="Enter Aadhar Card" value="<?php echo $data->vAadharCard; ?>" placeholder="Aadhar Card">
                                            <div class="text-danger" id="vAadharCard_Error" style="display: none;">Enter Aadhar Card</div>
                                        </div>
                                  </div>
                                  <div class="col-xl-6 col-md-6 ">
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-9 col-lg-6">
                                                  <label>AadharCard Front Image </label>
                                                  <input type="file" class="form-control" name="vAadharCardFrontImage" id="vAadharCardFrontImage">        
                                              </div>
                                              <div class="col-md-3 col-lg-6 ">
                                                <?php if(!empty($data->vAadharCardFrontImage)) { ?>
                                                      <img src="<?php echo base_url('assets/uploads/admin_master/'.$iAdminId.'/'.$data->vAadharCardFrontImage); ?>" width="60px" class="imgi-file " id="vAadharCardFrontImage_Preview">
                                                  <?php } else { ?>  
                                                      <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px"  class="imgi-file" id="vAadharCardFrontImage_Preview">
                                                  <?php } ?>
                                              </div>
                                          </div>
                                          <div class="text-danger" id="vAadharCardFrontImage_Error" style="display: none;">AadharCard Front Image Required</div>
                                      </div>
                                  </div>
                                  <div class="col-xl-6 col-md-6 ">
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-9 col-lg-6">
                                                  <label>AadharCard Back Image </label>
                                                  <input type="file" class="form-control" name="vAadharCardBackImage" id="vAadharCardBackImage">        
                                              </div>
                                              <div class="col-md-3 col-lg-6">
                                                <?php if(!empty($data->vAadharCardBackImage)) { ?>
                                                      <img src="<?php echo base_url('assets/uploads/admin_master/'.$iAdminId.'/'.$data->vAadharCardBackImage); ?>" width="60px" class="imgi-file " id="vAadharCardBackImage_Preview">
                                                  <?php } else { ?>  
                                                      <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" class="imgi-file " id="vAadharCardBackImage_Preview">
                                                  <?php } ?>
                                              </div>
                                          </div>
                                          <div class="text-danger" id="vAadharCardBackImage_Error" style="display: none;">AadharCard Back Image Required</div>
                                      </div>
                                  </div>
                                  <div class="col-xl-6 col-md-6 ">
                                    <div class="form-group">
                                        <label>PanCard</label>
                                        <input type="text" name="vPAN" id="vPAN" placeholder="Enter Pancard " class="form-control" value="<?php echo $data->vPAN; ?>">
                                        <div class="text-danger"  id="vPAN_error" style="display: none;">PanCard Required</div>
                                    </div>
                                  </div>
                                  <div class="col-xl-6 col-md-6 ">
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-9 col-lg-6">
                                                  <label>PAN Image </label>
                                                  <input type="file" class="form-control" name="vPANImage" id="vPANImage">        
                                              </div>
                                              <div class="col-lg-6 col-md-3">
                                                <?php if(!empty($data->vPANImage)) { ?>
                                                      <img src="<?php echo base_url('assets/uploads/admin_master/'.$iAdminId.'/'.$data->vPANImage); ?>" width="60px" id="vPANImage_Preview" class="imgi-file ">
                                                  <?php } else { ?>  
                                                      <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" id="vPANImage_Preview" class="imgi-file ">
                                                  <?php } ?>
                                              </div>
                                          </div>
                                          <div class="text-danger" id="vPANImage_Error" style="display: none;">PAN Image Required</div>
                                      </div>
                                  </div>
                                     <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <select name="eStatus" id="eStatus" class="form-control show-tick">
                                        <option value="">Select Status</option>
                                            <option <?php echo ($data->eStatus == 'Active') ? 'selected' : '';?> value="Active">Active</option>
                                            <option <?php echo ($data->eStatus == 'Inactive') ? 'selected' : '';?> value="Inactive">Inactive</option>
                                            
                                        </select>
                                        <div class="text-danger" id="eStatus_error" style="display: none;">Select Status</div>

                                        </div>
                                    </div>                                  
                                </div>                                                              
                                <div class="row">
                                <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader" type="button" disabled style="display: none;">
                                  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                  Loading...
                                </button>
                                &nbsp;
                                <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                </div>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<script type="text/javascript">
  
    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/admin_master/admin_master'); ?>";
    });

    $(document).on('click', '.submit', function () {
        var eStatus             = $("#eStatus").val();
        var iAdminId            = $("#iAdminId").val();
        var vFirstName          = $("#vFirstName").val();
        var vLastName           = $("#vLastName").val();
        var vAddress            = $("#vAddress").val();
        var vCity               = $("#vCity").val();
        var vPhone              = $("#vPhone").val();
        var vMobile             = $("#vMobile").val();
        var dBirthDate          = $("#dBirthDate").val();
        var vPassword           = $("#vPassword").val();
        var vEmail              = $("#vEmail").val();
        var vError              = false;

        if(dBirthDate.length == 0){
            $("#dBirthDate_Error").show();
            vError = true;
        } else {
            $("#dBirthDate_Error").hide();
        }

        if(vFirstName.length == 0){
            $("#vFirstName_error").show();
            vError = true;
        } else {
            $("#vFirstName_error").hide();
        }
        if(vLastName.length == 0){
            $("#vLastName_error").show();
            vError = true;
        } else {
            $("#vLastName_error").hide();
        }
        if(vAddress.length == 0){
            $("#vAddress_error").show();
            vError = true;
        } else {
            $("#vAddress_error").hide();
        }
        if(eStatus.length == 0){
            $("#eStatus_error").show();
            vError = true;
        } else {
            $("#eStatus_error").hide();
        }
        if(vCity.length == 0){
            $("#vCity_error").show();
            vError = true;
        } else {
            $("#vCity_error").hide();
        }
        if(vPhone.length == 0){
            $("#vPhone_error").show();
            vError = true;
        } else {
            $("#vPhone_error").hide();
        }
        if(vMobile.length == 0){
            $("#vMobile_error").show();
            vError = true;
        } else {
            $("#vMobile_error").hide();
        }
        if(iAdminId == "") {
            if(vPassword.length == 0){
                $("#vPassword_error").show();
                vError = true;
            } else {
                $("#vPassword_error").hide();
            }
        }
      <?php if(!isset($data->iAdminId)){?>
        if(vEmail.length == 0){
          $("#vEmail_error").show();
          $("#vEmail_unique_error").hide();
          $("#vEmail_valid_error").hide();
          vError = true;
        } else {
          if(iAdminId != "")
            data = {iAdminId:iAdminId, vEmail:vEmail};
          else
            data = {vEmail:vEmail};
          if(validateEmail(vEmail))
          {
            $("#vEmail_valid_error").hide();
            $.ajax({
              url: "<?php echo base_url('mf_panel/admin_master/admin_master/check_unique_email');?>",
              type: "POST",
              data:  data, 
              success: function(response) {
                if(response == 1){
                  $("#vEmail_unique_error").show();
                  $("#vEmail_error").hide();
                  vError = true;
                } else {
                  $("#vEmail_unique_error").hide();
                  $("#vEmail_error").hide();
                }
              }
            });
          } else {
            $("#vEmail_valid_error").show();
            $("#vEmail_error").hide();
            $("#vEmail_unique_error").hide();
            vError = true;
          }
        }
        <?php } ?>

        if(vError == false){
            $(".submit").hide();
            $(".submit_loader").show();
            setTimeout(function(){
              $("#frm").submit();
            }, 1000);
        }
    });

    $('#vMobile').bind('input propertychange', function () {
      $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });

    $('#vPhone').bind('input propertychange', function () {
      $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });

    function validateEmail(sEmail) 
    {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
        return true;
        }
        else {
        return false;
        }
    }

    $('#dBirthDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose:true,
    });

    $(document).on('change','#vImage1',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vImage1_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });

    $(document).on('change','#vPANImage',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vPANImage_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
    $(document).on('change','#vAadharCardFrontImage',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vAadharCardFrontImage_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
    $(document).on('change','#vAadharCardBackImage',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vAadharCardBackImage_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });

    $(document).on('click', '#eye', function () {
			
        if($(this).hasClass('fa-eye-slash')){
        
        $(this).removeClass('fa-eye-slash');
        
        $(this).addClass('fa-eye');
        
        $('#vPassword').attr('type','text');
            
        }else{
        
        $(this).removeClass('fa-eye');
        
        $(this).addClass('fa-eye-slash');  
        
        $('#vPassword').attr('type','password');
        }
	});
</script>