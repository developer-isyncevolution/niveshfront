<?php
$CI =& get_instance();
$CI->load->library('general');
?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary">Admin Listing</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <div class="d-lg-flex align-items-center d-md-flex d-inline" id="kt_subheader_search">
                    <span class="text-dark-50 font-weight-bold d-inline-block tot" id="kt_subheader_total"><?php echo count($data); ?> Total</span>
                    <div class="ml-lg-5 d-inline-block sra">
                        <div class="input-group input-group-md input-group-solid">
                            <input type="text" class="form-control keyword" id="kt_subheader_search_form" placeholder="Search...">
                            <div class="input-group-append">
                                <button class="btn btn-light-primary search" type="button">Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="ml-lg-5 d-inline-block pad-t">
                        <div class="input-group input-group-md input-group-solid" >
                            <select class="form-control sort">
                                <option value="">Sort</option>
                                <option value="vCode-ASC">Code (A-Z)</option>
                                <option value="vCode-DESC">Code (Z-A)</option>
                                <option value="vFirstName-ASC">Name (A-Z)</option>
                                <option value="vFirstName-DESC">Name (Z-A)</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-inline-block d-lg-none d-xl-none">
                     <a href="<?php echo base_url('mf_panel/admin_master/add'); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus"></i></a>
                        <div class="download_option dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
                            <a href="javascript:;" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="btn btn-light-primary font-weight-bold ml-2">
                                    <i class="fa fa-download"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right pad-t">
                                <ul class="navi">
                                    <li class="navi-item">
                                        <a href="javascript:;" class="navi-link pdf download">
                                            <span class="navi-icon">
                                                <i class="fa fa-download"></i>
                                            </span>
                                            <span class="navi-text">PDF</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="javascript:;" class="navi-link excel">
                                            <span class="navi-icon">
                                                <i class="fa fa-download"></i>
                                            </span>
                                            <span class="navi-text">Excel</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="ml-2 spinner-border text-primary" role="status" id="download_loader" style="display: none;">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-lg-flex align-items-center d-xl-flex d-md-none d-none">
                <a href="<?php echo base_url('mf_panel/admin_master/add'); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus"></i></a>
                <div class="download_option dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
                    <a href="javascript:;" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="btn btn-light-primary font-weight-bold ml-2">
                            <i class="fa fa-download"></i>
                        </span>
                    </a>
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
                        <ul class="navi">
                            <li class="navi-item">
                                <a href="javascript:;" class="navi-link pdf">
                                    <span class="navi-icon">
                                        <i class="fa fa-download"></i>
                                    </span>
                                    <span class="navi-text">PDF</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="javascript:;" class="navi-link excel">
                                    <span class="navi-icon">
                                        <i class="fa fa-download"></i>
                                    </span>
                                    <span class="navi-text">Excel</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                <div class="ml-2 spinner-border text-primary" role="status" id="download_loader" style="display: none;">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row consd" id="registrar_response">
                <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>    
                </div>
            </div>
            
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        var eDeleted       = "<?php echo $eDeleted; ?>";
        $.ajax({
            url: "<?php echo base_url('mf_panel/admin_master/admin_master/ajax_listing');?>",
            type: "POST",
            data:  { eDeleted:eDeleted }, 
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    });

    $(document).on('click','.edit',function(){
        var iAdminId = $(this).data("id");
        location.href = "admin_master/edit/"+iAdminId;
    });

    $(document).on('click','.delete',function(){
        if (confirm('Are you sure delete this data?')) {
            var iAdminId   = $(this).data("id");
            $("#registrar_response").html(" ");
            $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
            $.ajax({
                url: "<?php echo base_url('mf_panel/admin_master/ajax_listing');?>",
                type: "POST",
                data: { iAdminId:iAdminId, vAction:'delete' }, 
                success: function(response) {
                    $("#registrar_response").html(" ");
                    $("#registrar_response").html(response);
                }
            });
        }
    });

    $(document).on('click', '.search', function() {
        var vKeyword    = $(".keyword").val();
        var eDeleted    = "<?php echo $eDeleted; ?>";

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
        $.ajax({
            url: "<?php echo base_url('mf_panel/admin_master/ajax_listing');?>",
            type: "POST",
            data:  { vKeyword:vKeyword, eDeleted:eDeleted }, 
            success: function(response) {
                $("#registrar_response").html(" ");
                $("#registrar_response").html(response);
                $("#kt_subheader_total").html($("#iCount").val()+" Total");
            }
        });
    });

    $(document).on('click', '.admin_master_status', function() {
        var eDeleted       = "<?php echo $eDeleted; ?>";
        var iAdminId      = $(this).data("id");

        if (confirm('Are you sure for status update?'))
        {
            $.ajax({
                url: "<?php echo base_url('mf_panel/admin_master/ajax_listing');?>",
                type: "POST",
                data:  { iAdminId:iAdminId, vAction:'admin_master_status', eDeleted:eDeleted }, 
                success: function(response) {
                    $("#registrar_response").html(" ");
                    $("#registrar_response").html(response);
                    notification_success("Admin Status Updated Successfully");
                }
            });
        }
    });
    
    $(document).on('click','.change_password',function(){
        var iAdminId = $(this).data("id");
        location.href = "change_password/"+iAdminId;
    });

    $(document).on('click', '.ajax_page', function() {
        
        var eDeleted    = "<?php echo $eDeleted; ?>";
        var vPage       = $(this).data('pages');
        var vKeyword    = $(".keyword").val();

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');

        $.ajax({
            url: "<?php echo base_url('mf_panel/admin_master/ajax_listing');?>",
            type: "POST",
            data: { vPage:vPage, vKeyword:vKeyword, eDeleted:eDeleted }, 
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    });

    $(document).on('click', '.pdf', function() {
        var iAdminId = [];
        

        $('.branch_checkbox').each(function(index, value) {
            iAdminId.push($(this).data("value"));
        });
        $('.download_option').hide();
        $('#download_loader').show();
        $.ajax({
            url: "<?php echo base_url('admin_master/download_listing_pdf');?>",
            type: "POST",
            dataType: "JSON",
            data:  { iAdminId:iAdminId }, 
            success: function(response) {
                if(response.status == "200") {
                    notification_success(response.notification);
                    window.open("<?php echo base_url('assets/uploads/listing_pdf/admin_master') ?>"+"/"+response.filename);
                }
                $('.download_option').show();
                $('#download_loader').hide();
            }
        });
    });

    $(document).on('click', '.excel', function() {
        var iAdminId = [];

        $('.branch_checkbox').each(function(index, value) {
            iAdminId.push($(this).data("value"));
        });
        $('.download_option').hide();
        $('#download_loader').show();
        $.ajax({
            url: "<?php echo base_url('mf_panel/admin_master/download_listing_excel');?>",
            type: "POST",
            dataType: "JSON",
            data:  { iAdminId:iAdminId }, 
            success: function(response) {
                if(response.status == "200") {
                    notification_success(response.notification);
                    window.open("<?php echo base_url('assets/uploads/listing_excel/admin_master') ?>"+"/"+response.filename);
                }
                $('.download_option').show();
                $('#download_loader').hide();
            }
        });
    });

    $(document).on('change', '.sort', function() {
        var vKeyword            = $(".keyword").val();
        var eDeleted            = "<?php echo $eDeleted; ?>";
        var vSort               = $(this).val();

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
        $.ajax({
            url: "<?php echo base_url('mf_panel/admin_master/ajax_listing');?>",
            type: "POST",
            data:  { vKeyword:vKeyword, vAction:'sort', eDeleted:eDeleted, vSort:vSort }, 
            success: function(response) {
                $("#registrar_response").html(" ");
                $("#registrar_response").html(response);
                $("#kt_subheader_total").html($("#iCount").val()+" Total");
            }
        });
    });
</script>