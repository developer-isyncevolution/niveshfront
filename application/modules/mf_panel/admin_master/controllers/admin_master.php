<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class admin_master extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_master_model');
        $this->load->model('user/user_model');

        $this->load->model('system_email/system_email_model');

        $this->load->library('pagination');

        $this->load->library('General');
       

        $this->general->authentication();
    }

    public function index()
    {
        $criteria               = array();
        $criteria["eDeleted"]   = "No";
        $data["data"]           = $this->admin_master_model->get_all_data($criteria);
        $data["eDeleted"]       = "No";

        $this->template->build('listing', $data);
    }

    public function ajax_listing()
    {
        $vAction        = $this->input->post('vAction');
        $iAdminId       = $this->input->post('iAdminId');
        $vKeyword       = $this->input->post('vKeyword');
        $eStatus        = $this->input->post('eStatus');
        $eDeleted       = $this->input->post('eDeleted');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');

        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iAdminId";
            $order      = "DESC";
        }

        if($vAction == "delete" && !empty($iAdminId))
        {
			$this->admin_master_model->delete($iAdminId);
        } 
        if($vAction == "admin_master_status" & !empty($iAdminId))
        {
            $myArray                = array();
            $myArray["iAdminId"]    = $iAdminId;
            $check                  = $this->admin_master_model->get_by_id($myArray);

            if($check->eStatus == "Active")
            {
                $where               = array();
                $where["iAdminId"]   = $iAdminId;
                $mydata              = array();
                $mydata["eStatus"]   = "Inactive";
                $this->admin_master_model->update($where, $mydata);
            }
            else
            {
                $where               = array();
                $where["iAdminId"]   = $iAdminId;
                $mydata              = array();
                $mydata["eStatus"]   = "Active";
                $this->admin_master_model->update($where, $mydata);   
            }
        }      

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eStatus']    = $eStatus;
        $criteria['vKeyword']   = $vKeyword;
        $criteria['eDeleted']   = $eDeleted;

        $data                   = $this->admin_master_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->admin_master_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {     
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $base_path                = $this->config->item("base_path");
            $iAdminId                 = $this->input->post('iAdminId');
            
            $data['vFirstName']       = $this->input->post('vFirstName');
            $data['vLastName']        = $this->input->post('vLastName');
            if(empty($iAdminId))
            {
                $data['vPassword']       = md5($this->input->post('vPassword'));
                $data['vNormalPassword'] = $this->input->post('vPassword');
            }
            $data['vCity']              = $this->input->post('vCity');
            $data['vAddress']           = $this->input->post('vAddress');
            $data['dBirthDate']         = $this->general->save_date_format($this->input->post('dBirthDate'));
            $data['vPhone']             = $this->input->post('vPhone');
            $data['vEmail']             = $this->input->post('vEmail');
            $data['vMobile']            = $this->input->post('vMobile');
            $data['eStatus']            = $this->input->post('eStatus');
            $data['vPAN']               = $this->input->post('vPAN');
            $data['vAadharCard']        = $this->input->post('vAadharCard');
        
			if($iAdminId){
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}else{
				$data['dtAddedDate'] 	= date("Y-m-d h:i:s");
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}
            if(!empty($iAdminId))
            {

                mkdir($base_path.'assets/uploads/admin_master/' . $iAdminId, 0777, TRUE);

                //image update code
                if($_FILES['vImage']['name'] != "")
                {
                    if($_FILES['vImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }

                    $vImage = time().'_Image'.$ext;
                    move_uploaded_file($_FILES['vImage']['tmp_name'], $base_path.'assets/uploads/admin_master/'.$iAdminId.'/'.$vImage);

                    $data['vImage'] = $vImage;
                }
                sleep(1);            
                if($_FILES['vPANImage']['name'] != "")
                {
                    if($_FILES['vPANImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vPANImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vPANImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vPANImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vPANImage = time().str_replace(' ','',$this->input->post('vFirstName')).$ext;
                    move_uploaded_file($_FILES['vPANImage']['tmp_name'], $base_path.'assets/uploads/admin_master/'.$iAdminId.'/'.$vPANImage);
                    $data["vPANImage"]              = $vPANImage;
                    
                }
                sleep(1);    
                if($_FILES['vAadharCardFrontImage']['name'] != "")
                {
                    if($_FILES['vAadharCardFrontImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vAadharCardFrontImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vAadharCardFrontImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vAadharCardFrontImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vAadharCardFrontImage = time().str_replace(' ','',$this->input->post('vFirstName')).'_Front_Aadhar_'.$ext;
                    move_uploaded_file($_FILES['vAadharCardFrontImage']['tmp_name'] ,$base_path.'assets/uploads/admin_master/'.$iAdminId.'/'.$vAadharCardFrontImage);
                    $data["vAadharCardFrontImage"]  = $vAadharCardFrontImage;
                }
                sleep(1);
    
                if($_FILES['vAadharCardBackImage']['name'] != "")
                {
                    if($_FILES['vAadharCardBackImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vAadharCardBackImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vAadharCardBackImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vAadharCardBackImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vAadharCardBackImage = time().str_replace(' ','',$this->input->post('vFirstName')).'_Back_Aaddhr_'.$ext;               
                    move_uploaded_file($_FILES['vAadharCardBackImage']['tmp_name'] ,$base_path.'assets/uploads/admin_master/'.$iAdminId.'/'.$vAadharCardBackImage);
                    $data["vAadharCardBackImage"]   = $vAadharCardBackImage;

                }
                sleep(1);
                //image update code

                $where = array("iAdminId" => $iAdminId);
                $this->admin_master_model->update($where, $data);               

                $this->session->set_flashdata('success', "Admin Updated Successfully." );
                redirect(base_url('mf_panel/admin_master/admin_master'));
            }
            else
            {
                $ID = $this->admin_master_model->add($data);

                //image update

                mkdir($base_path.'assets/uploads/admin_master/' . $ID, 0777, TRUE);

                if($_FILES['vImage']['name'] != "")
                {
                    if($_FILES['vImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vImage = time().str_replace(' ','',$this->input->post('vFirstName')).$ext; 
                    move_uploaded_file($_FILES['vImage']['tmp_name'], $base_path.'assets/uploads/admin_master/'.$ID.'/'.$vImage);   
                    $update_where                 = array();
                    $update_where["iAdminId"]  = $ID;
    
                    $update_image                   = array();
                    $update_image["vImage"]          = $vImage;               
                    $this->admin_master_model->update($update_where, $update_image);    
    
                }
                sleep(1);
                
                if($_FILES['vPANImage']['name'] != "")
                {
                    if($_FILES['vPANImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vPANImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vPANImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vPANImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vPANImage = time().str_replace(' ','',$this->input->post('vFirstName')).$ext;
                    move_uploaded_file($_FILES['vPANImage']['tmp_name'], $base_path.'assets/uploads/admin_master/'.$ID.'/'.$vPANImage);
                    
                    $update_where                 = array();
                    $update_where["iAdminId"]  = $ID;
    
                    $update_image                   = array();
                    $update_image["vPANImage"]          = $vPANImage;               
                    $this->admin_master_model->update($update_where, $update_image);  
                }
                sleep(1);
    
                if($_FILES['vAadharCardFrontImage']['name'] != "")
                {
                    if($_FILES['vAadharCardFrontImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vAadharCardFrontImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vAadharCardFrontImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vAadharCardFrontImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vAadharCardFrontImage = time().str_replace(' ','',$this->input->post('vFirstName')).'_Front_Aadhar_'.$ext;
                    move_uploaded_file($_FILES['vAadharCardFrontImage']['tmp_name'] ,$base_path.'assets/uploads/admin_master/'.$ID.'/'.$vAadharCardFrontImage);
                   
                    $update_where               = array();
                    $update_where["iAdminId"]  = $ID;
    
                    $update_image                             = array();
                    $update_image["vAadharCardFrontImage"]    = $vAadharCardFrontImage;               
                    $this->admin_master_model->update($update_where, $update_image);  
                }
                sleep(1);
    
                if($_FILES['vAadharCardBackImage']['name'] != "")
                {
                    if($_FILES['vAadharCardBackImage']['type'] == 'image/gif'){
                        $ext = ".gif";
                    } else if($_FILES['vAadharCardBackImage']['type'] == 'image/jpg'){
                        $ext = ".jpg";
                    } else if($_FILES['vAadharCardBackImage']['type'] == 'image/png'){
                        $ext = ".png";
                    } else if($_FILES['vAadharCardBackImage']['type'] == 'image/jpeg'){
                        $ext = ".jpeg";
                    }
                    $vAadharCardBackImage = time().str_replace(' ','',$this->input->post('vFirstName')).'_Back_Aaddhr_'.$ext;               
                    move_uploaded_file($_FILES['vAadharCardBackImage']['tmp_name'] ,$base_path.'assets/uploads/admin_master/'.$ID.'/'.$vAadharCardBackImage);
                  
                    $update_where               = array();
                    $update_where["iAdminId"]  = $ID;
    
                    $update_image                             = array();
                    $update_image["vAadharCardBackImage"]    = $vAadharCardBackImage;               
                    $this->admin_master_model->update($update_where, $update_image);  
                }
                sleep(1);


                //image update

                $update_where               = array();
                $update_where["iAdminId"]  = $ID;
                $update_data                = array();
                $update_data["vCode"]       = "ANA".str_pad($ID,5,"0",STR_PAD_LEFT);
                $this->admin_master_model->update($update_where, $update_data);

                //Email Code
                $vFirstName                            = $this->input->post('vFirstName');
                $vEmail                             = $this->input->post('vEmail');
                $vPassword                          = $this->input->post('vPassword');

                $criteria                           = array();
                $criteria["vCode"]                  = "ACCOUNT_INFORMATION";
                $email                              = $this->system_email_model->get_by_id($criteria);

                if(!empty($email))
                {
                    $constant                       = array('#EMPLOYEE#','#CODE#','#EMAIL#','#PASSWORD#');
                    $value                          = array($vFirstName, $update_data["vCode"], $vEmail, $vPassword);
                    $message                        = str_replace($constant, $value, $email->tMessage);

                    $email_data['to']               = $vEmail;
                    $email_data['subject']          = $email->vSubject;
                    $email_data['message']          = $message;

                    $criteria                       = array();
                    $criteria['eType']              = 'EMAIL';
                    $criteria['vData']              = $email_data;    

                    //$this->general->send_notifiction($criteria);
                }
                //Email Code

                //User Table Entry
                $user_data                  = array();
                $user_data["vUserName"]     = $update_data["vCode"];
                $user_data["vEmail"]        = $this->input->post('vEmail');
                $user_data["vPassword"]     = md5($this->input->post('vPassword'));
                $user_data["vAuthCode"]     = md5($this->input->post('vEmail'));
                $user_data["eStatus"]       = $this->input->post('eStatus');
                $user_data["eType"]         = "Admin";

                $iUserId                    = $this->user_model->add($user_data);

                $update_where               = array();
                $update_where["iAdminId"]  = $ID;
                $update_data                = array();
                $update_data["iUserId"]     = $iUserId;
                $this->admin_master_model->update($update_where, $update_data);
                //User Table Entry

                $this->session->set_flashdata('success', "Admin Added Successfully." );
                redirect(base_url('mf_panel/admin_master/admin_master'));
            }
        }
        else
        {
            $this->template->build('add', NULL);
        }
    }

    public function edit($iAdminId  = NULL) 
	{

        if(!empty($iAdminId))
        {
            $criteria              = array();
            $criteria["iAdminId"]  = $iAdminId;

            $data['data']          = $this->admin_master_model->get_by_id($criteria);
            $data['iAdminId']      = $iAdminId;
            
            $this->template->build('add',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/admin_master/add'));    
        }
    }
    
    public function check_unique_email()
    {
        $criteria               = array();
        $criteria['iAdminId']   = $this->input->post('iAdminId');
        $criteria['vEmail']     = $this->input->post('vEmail');
        $result                 = $this->admin_master_model->check_unique_email($criteria);

        if(!empty($result)) {
            echo "1";
        } else {
            echo '0';
        }
    }

    public function download_listing_pdf()
    {
        $iAdminId = $this->input->post("iAdminId");
        $base_path = $this->config->item('base_path');
 
        if(!empty($iAdminId))
        {
            foreach ($iAdminId as $key => $value) 
            {
                $criteria               = array();
                $criteria["iAdminId"]  = $value;
                $data[$key]             = $this->admin_master_model->get_by_id($criteria);
            } 
 
            if(!empty($data))
            {
                $html = "";
                $html.= "<table border='1' width='100%'>";
                
                $html.= "<tr>";
                $html.= "<td colspan='15'>Admin Listing</td>";
                $html.= "</tr>";
 
                $html.= "<tr>";
                $html.= "<th colspan='5'>Code</th>";
                $html.= "<th colspan='5'>FirstName</th>";
                $html.= "<th colspan='5'>LastName</th>";
                $html.= "<th colspan='5'>City</th>";
                $html.= "<th colspan='5'>Address</th>";
                $html.= "<th colspan='5'>BirthDate</th>";
                $html.= "<th colspan='5'>Phone</th>";
                $html.= "<th colspan='5'>Mobile</th>";
                $html.= "<th colspan='5'>Email</th>";
                $html.= "<th colspan='5'>Status</th>";
                $html.= "</tr>";
               
                foreach ($data as $key => $value) 
                {
                    $html.= "<tr>";
                    $html.= "<td colspan='5'>".$value->vCode."</td>";
                    $html.= "<td colspan='5'>".$value->vFirstName."</td>";
                    $html.= "<td colspan='5'>".$value->vLastName."</td>";
                    $html.= "<td colspan='5'>".$value->vCity."</td>";
                    $html.= "<td colspan='5'>".$value->vAddress."</td>";
                    $html.= "<td colspan='5'>".$value->dBirthDate."</td>";
                    $html.= "<td colspan='5'>".$value->vPhone."</td>";
                    $html.= "<td colspan='5'>".$value->vMobile."</td>";
                    $html.= "<td colspan='5'>".$value->vEmail."</td>";
                    $html.= "<td colspan='5'>".$value->eStatus."</td>";
                    $html.= "</tr>";
                }
 
                $html.= "</table>";

                $criteria               = array();
                $criteria["html"]       = $html;
                $criteria["path"]       = $base_path.'assets/uploads/listing_pdf/admin_master/'.'admin_master_listing_'.date("Y-m-d").'.pdf';
                
                echo "<pre>";print_r($criteria);die;
                $this->pdf_library->create($criteria);
            }
 
             $mydata                     = array();
             $mydata["status"]           = "200";
             $mydata["message"]          = "success";
             $mydata["notification"]     = "Admin Listing PDF Downloaded";
             $mydata["filename"]         = 'admin_master_listing_'.date("Y-m-d").'.pdf';
             echo json_encode($mydata);
        }
         else
         {
             $mydata                     = array();
             $mydata["status"]           = "400";
             $mydata["message"]          = "error";
             $mydata["notification"]     = "Nothing to Download";
             echo json_encode($mydata);
         }
    }    
 
    public function download_listing_excel()
    {
        $iAdminId = $this->input->post("iAdminId");
        $base_path = $this->config->item('base_path');

        if(!empty($iAdminId))
        {
            foreach ($iAdminId as $key => $value) 
            {
                $criteria               = array();
                $criteria["iAdminId"]  = $value;
                $data[$key]             = $this->admin_master_model->get_by_id($criteria);
            }

            if(!empty($data))
            {
                $write_array[] = array("Code","FirstName","City","Address","BirthDate","Phone","Email","Mobile","Status");
            
                foreach ($data as $key => $value) 
                {
                    $write_array[] = array($value->vCode,$value->vFirstName,$value->vCity,$value->vAddress,$value->dBirthDate,$value->vPhone,$value->vEmail,$value->vMobile,$value->eStatus);
                }

                $criteria               = array();
                $criteria["data"]       = $write_array;
                $criteria["title"]      = "First Name Listing"." ".date("Y-m-d");
                $criteria["path"]       = $base_path.'assets/uploads/listing_excel/admin_master/'.'admin_master_listing_'.date("Y-m-d").'.xlsx';

                $this->excel_library->create($criteria);
            }
            $mydata                     = array();
            $mydata["status"]           = "200";
            $mydata["message"]          = "success";
            $mydata["notification"]     = "First Name Listing Excel Downloaded";
            $mydata["filename"]         = 'admin_master_listing_'.date("Y-m-d").'.xlsx';
            echo json_encode($mydata);
        }
        else
        {
            $mydata                     = array();
            $mydata["status"]           = "400";
            $mydata["message"]          = "error";
            $mydata["notification"]     = "Nothing to Download";
            echo json_encode($mydata);
        }
    }

    public function change_password($iAdminId = NULL)
    {
       if(!empty($iAdminId))
       {
        $criteria                         = array();
        $criteria["iAdminId"]            = $iAdminId;
        
        $data['data']  	                  = $this->admin_master_model->get_by_id($criteria);
        $data['iAdminId']                = $iAdminId;

        $this->template->build('change_password', $data);
        }
        else{
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/admin_master/admin_master'));
        }

    }

    public function change_password_action() 
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
            $iAdminId                 = $this->input->post('iAdminId');
            $iUserId                  = $this->input->post('iUserId');
            $data['vPassword']        = md5($this->input->post('vPassword'));
            $data['vNormalPassword']  = $this->input->post('vPassword');           
            $data1['vPassword']       = md5($this->input->post('vPassword'));

			$where = array("iAdminId" => $iAdminId);
            $this->admin_master_model->update($where, $data);

			$where = array("iUserId" => $iUserId);            
            $this->user_model->update($where, $data1);
            
            $this->session->set_flashdata('success', "Password Change Successfully." );

			redirect(base_url('mf_panel/admin_master/admin_master'));
		} else {
			$data['iAdminId'] = $id;
			$this->template->build('change_password', $data);
		}
	}
}