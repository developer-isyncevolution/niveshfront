<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_email extends MX_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('general');
        $this->load->model('system_email_model');
    }

	public function index()
	{
        $this->template->build('listing',null);
	}

	public function ajax_listing()
	{
		$action = $this->input->post('action');

		if($action == "sort"){
			$column = $this->input->post('column');
			$order = $this->input->post('order');
		} else{
			$column = "vEmailCode";
			$order = "DESC";
		}
		if($action == "search"){
			$keyword = $this->input->post('keyword');
		} else {
			$keyword = "";
		}

		if($action == "delete"){
			$id = $this->input->post('id');
			$this->system_email_model->delete_by_id($id);
        	$this->system_email_lang_model->delete_by_id($id);

		}

		if($action == "status")
		{
			if($this->input->post('eStatus')){
			
				$iEmailTemplateId = explode(',',$this->input->post('iEmailTemplateId'));
				$datax['eStatus'] = $this->input->post('eStatus');
				$where = $iEmailTemplateId;
				$this->system_email_model->update_pro_status($where, $datax);
			}
		}

		$criteria['keyword'] 	= $keyword;
		$criteria['column'] 	= $column;
		$criteria['order'] 		= $order;

        $data['data'] = $this->system_email_model->get_all_data($criteria);
        
        $this->load->view('ajax_listing', $data);
	}

	public function add() 
	{
		$criteria['Active'] 	= 'Active';
		$criteria['iLangId'] 	= 'iLangId';
		$criteria['ASC'] 		= 'ASC';

		$languages['languages'] = $this->language_model->get_all_data($criteria);


		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$id = $this->input->post('id');

			$primary_lang = $this->language_model->get_primary_language();

			$data['vEmailCode'] 	= $this->input->post('vEmailCode');
			$data['vEmailTitle'] 	= $this->input->post('vEmailTitle');
			$data['vFromName'] 		= $this->input->post('vFromName');
			$data['vFromEmail'] 	= $this->input->post('vFromEmail');
			$data['vCcEmail'] 		= $this->input->post('vCcEmail');
			$data['vBccEmail'] 		= $this->input->post('vBccEmail');
			$data['vEmailSubject'] 	= $this->input->post($primary_lang)['vEmailSubject'];
			$data['tEmailMessage'] 	= $this->input->post($primary_lang)['tEmailMessage'];
			$data['tSmsMessage'] 	= $this->input->post($primary_lang)['tSmsMessage'];
			$data['tInternalMessage'] = $this->input->post($primary_lang)['tInternalMessage'];
			$data['eStatus'] 		= $this->input->post('eStatus');
			

			if($id){
				$data['dtUpdatedDate'] = date("Y-m-d h:i:s");
			}
			else{
				$data['dtAddedDate'] = date("Y-m-d h:i:s");
				$data['dtUpdatedDate'] = date("Y-m-d h:i:s");
			}
			
			if($id){
				$where = array("iEmailTemplateId" => $id);
				$this->system_email_model->update($where, $data);

				foreach ($languages['languages'] as $key => $value) {
					$data = array(
						'vEmailSubject' => $this->input->post($value->vLangCode)['vEmailSubject'],
						'tEmailMessage' => $this->input->post($value->vLangCode)['tEmailMessage'],
						'tSmsMessage' => $this->input->post($value->vLangCode)['tSmsMessage'],
						'tInternalMessage' => $this->input->post($value->vLangCode)['tInternalMessage']
						
					);

					$where = array("iEmailTemplateId" => $id, 'vLangCode' => $value->vLangCode);
					$this->system_email_lang_model->update($where,$data);
				}
				$this->session->set_flashdata('success', "System email update successfully." );
					$where = array("iEmailTemplateId" => $id, 'vVarName' => $key);
			} else {
				$id = $this->system_email_model->add($data);

				foreach ($languages['languages'] as $key => $value) {
					$data = array(
						'iEmailTemplateId' => $id,
						'vLangCode' => $value->vLangCode,
						'vEmailSubject' => $this->input->post($value->vLangCode)['vEmailSubject'],
						'tEmailMessage' => $this->input->post($value->vLangCode)['tEmailMessage'],
						'tSmsMessage' => $this->input->post($value->vLangCode)['tSmsMessage'],
						'tInternalMessage' => $this->input->post($value->vLangCode)['tInternalMessage']
					);
					
					$this->system_email_lang_model->add($data);
				}
				$this->session->set_flashdata('success', "System email add successfully." );
			}
			redirect(base_url('mf_panel/system_email/system_email'));
		} else {
			$this->template->build('add',$languages);
		}
	}

	public function edit($id) 
	{
		$criteria['Active'] 	= 'Active';
		$criteria['iLangId'] 	= 'iLangId';
		$criteria['ASC'] 		= 'ASC';

		$data['languages'] = $this->language_model->get_all_data($criteria);

    	$data['data'] = $this->system_email_model->get_by_id($id);
    	$system_email_lang = $this->system_email_lang_model->get_by_id($id);

    	foreach ($system_email_lang as $key => $value) {
    		$data['lang'][$value->vLangCode]['vEmailSubject'] = $value->vEmailSubject;
    		$data['lang'][$value->vLangCode]['tEmailMessage'] = $value->tEmailMessage;
    		$data['lang'][$value->vLangCode]['tSmsMessage'] = $value->tSmsMessage;
    		$data['lang'][$value->vLangCode]['tInternalMessage'] = $value->tInternalMessage;
    	}

    	$this->template->build('add',$data);
	}
}

