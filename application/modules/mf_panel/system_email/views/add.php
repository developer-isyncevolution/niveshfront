<!-- Page Wrapper -->
            <div class="page-wrapper">
      
        <div class="content container-fluid">

          <!-- Page Header -->
          <div class="page-header">
            <div class="row">
              <div class="col">
                <h3 class="page-title">System Emails</h3>
                <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('mf_panel/dashboard'); ?>">Dashboard</a></li>
                  <li class="breadcrumb-item active">System Emails</li>
                </ul>
              </div>
            </div>
          </div>
          <!-- /Page Header -->

          
          
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"><strong><?php echo ($data->iEmailTemplateId) ? 'Edit' : 'Add'; ?> </strong> System Email </h4>
                </div>
                <div class="card-body">
                 <form name="frm" id="frm" role="form" method="post" action="<?php echo base_url('mf_panel/system_email/add');?>" enctype="multipart/form-data">
                      <input type="hidden" id="id" name="id" value="<?php echo $data->iEmailTemplateId;?>">
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                         <label>Email Code </label>
                                  <input type="text" class="form-control" id="vEmailCode" name="vEmailCode" value="<?php echo $data->vEmailCode;?>" placeholder="Enter Email Code">
                        </div>
                        <div class="form-group">
                          <label>Email Title</label>
                                  <input type="text" class="form-control" id="vEmailTitle" name="vEmailTitle" value="<?php echo $data->vEmailTitle;?>" placeholder="Enter Email Title">
                        </div>
                        <div class="form-group">
                           <label>From Name </label>
                                  <input type="text" class="form-control" id="vFromName" name="vFromName" value="<?php echo $data->vFromName;?>" placeholder="Enter From Name">
                        </div>
                        <div class="form-group">
                                 <?php foreach ($languages as $key => $value) {?>
                                  <label>Sms Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></label>
                                  <input type="text" class="form-control" id="<?php echo $value->vLangCode?>_tSmsMessage" name="<?php echo $value->vLangCode?>[tSmsMessage]" value="<?php echo $lang[$value->vLangCode]['tSmsMessage'];?>" placeholder="Enter Sms Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?>">
                                  <div id="<?php echo $value->vLangCode?>_tSmsMessage_error" class="error" style="display: none;"> Enter Sms Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></div>
                                <?php } ?>
                        </div>
                        <div class="form-group">
                                 <?php foreach ($languages as $key => $value) {?>
                                  <label>Internal Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></label>
                                  <div id="<?php echo $value->vLangCode?>_tInternalMessage_error" class="error" style="display: none;"> Enter Internal Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></div>
                                  <textarea id="<?php echo $value->vLangCode?>_tInternalMessage" name="<?php echo $value->vLangCode?>[tInternalMessage]" class="form-control" placeholder="Enter Internal Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?>" required><?php echo htmlspecialchars($lang[$value->vLangCode]['tInternalMessage']);?></textarea>
                                <?php } ?>
                        </div>
                        <div class="form-group">
                          <label>Status</label>
                                  <select name="eStatus" class="form-control show-tick">
                                    <option <?php echo ($data->eStatus == 'Active') ? 'selected' : '';?> value="active">Active</option>
                                    <option <?php echo ($data->eStatus == 'Inactive') ? 'selected' : '';?> value="inactive">Inactive</option>
                                  </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                                 <label>From Email</label>
                                  <input type="text" class="form-control" id="vFromEmail" name="vFromEmail" value="<?php echo $data->vFromEmail;?>" placeholder="Enter From Email"> 
                        </div>
                        <div class="form-group">
                                 <label>Cc Email </label>
                                  <input type="text" class="form-control" id="vCcEmail" name="vCcEmail" value="<?php echo $data->vCcEmail;?>" placeholder="Enter Cc Email"> 
                        </div>
                        <div class="form-group">
                                 <label>BccEmail </label>
                                  <input type="text" class="form-control" id="vBccEmail" name="vBccEmail" value="<?php echo $data->vBccEmail;?>" placeholder="Enter BccEmail">
                        </div>
                        <div class="form-group">
                                 <?php foreach ($languages as $key => $value) {?>
                                  <label>Email Subject <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></label>
                                  <input type="text" class="form-control" id="<?php echo $value->vLangCode?>_vEmailSubject" name="<?php echo $value->vLangCode?>[vEmailSubject]" value="<?php echo $lang[$value->vLangCode]['vEmailSubject'];?>" placeholder="Enter Email Subject<?php echo (count($languages) > 1) ? $value->vLangCode:"";?>" required>
                                  <div id="<?php echo $value->vLangCode?>_vEmailSubject_error" class="error" style="display: none;"> Enter Email Subject <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></div>
                                <?php } ?>
                        </div>
                        <div class="form-group">
                                 <?php foreach ($languages as $key => $value) {?>
                                  <label>Email Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></label>
                                  
                                  <textarea id="<?php echo $value->vLangCode?>_tEmailMessage" name="<?php echo $value->vLangCode?>[tEmailMessage]" class="form-control" placeholder="Enter Internal Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?>" required><?php echo htmlspecialchars($lang[$value->vLangCode]['tEmailMessage']);?></textarea>
                                  <div id="<?php echo $value->vLangCode?>_tEmailMessage_error" class="error" style="display: none;"> Enter Email Message <?php echo (count($languages) > 1) ? $value->vLangCode:"";?></div>
                                <?php } ?>
                        </div>
                        
                        
                      </div>
                    </div>
                    
                    <div class="text-right">
                      <a href="javascript:;" class="btn btn-primary submit">Submit</a>
                      <a href="javascript:;" class="btn btn-primary cancel">Cancel</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          
         
          
        </div>
      </div>
    </div>
</div>
      <!-- /Page Wrapper -->
<!-- <script type="text/javascript" src="<?php echo base_url('assets/mf_panel/js/tinymce/tinymce.min.js');?>"></script>
 --><script type="text/javascript">
    /*tinymce.init({
      selector: 'textarea',
      height: 300,
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor textcolor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code help wordcount'
      ],
      toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css']
    });*/
    $(document).on('click','.cancel',function(){
        location.href = "<?php echo base_url('mf_panel/system_email/system_email');?>";
        return true;
    });

    $(document).on('click','.submit',function(){
      iEmailTemplateId = $("#id").val();

      <?php foreach ($languages as $key => $value) {?>
        <?php echo $value->vLangCode;?>_vEmailSubject = $("#<?php echo $value->vLangCode;?>_vEmailSubject").val();
        <?php echo $value->vLangCode;?>_tEmailMessage = $("#<?php echo $value->vLangCode;?>_tEmailMessage").val();
        <?php echo $value->vLangCode;?>_tInternalMessage = $("#<?php echo $value->vLangCode;?>_tInternalMessage").val();
      <?php }?>

      var error = false;

      <?php foreach ($languages as $key => $value) {?>
        if(<?php echo $value->vLangCode;?>_vEmailSubject.length == 0){
          $("#<?php echo $value->vLangCode;?>_vEmailSubject_error").show();
          error = true;
        } else{
          $("#<?php echo $value->vLangCode;?>_vEmailSubject_error").hide();
        }
       if(<?php echo $value->vLangCode;?>_tEmailMessage.length == 0){
          $("#<?php echo $value->vLangCode;?>_tEmailMessage_error").show();
          error = true;
        } else{
          $("#<?php echo $value->vLangCode;?>_tEmailMessage_error").hide();
        } 
      <?php }?>

      setTimeout(function(){
        if(error == true){
          return false;
        } else {
          $("#frm").submit();
          return true;
        }
      }, 1000);

    });
</script>