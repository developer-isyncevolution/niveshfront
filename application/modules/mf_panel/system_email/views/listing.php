<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
                
                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-7 col-auto">
                                <h3 class="page-title">System Email</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('mf_panel/dashboard'); ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item active">System Email</li>
                                </ul>
                            </div>
                            <div class="col-sm-5 col">
                               
                                <a href="javascript:;" data-toggle="modal" class="btn btn-primary float-right mt-2 add">Add System Email</a>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="datatable table table-hover table-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th><a id="vEmailTitle" class="sort" data-column="vEmailTitle" data-order="ASC" href="#">Email Title</a></th>
                                        <th><a id="vEmailCode" class="sort" data-column="vEmailCode" data-order="ASC" href="#">Email Code</a></th>
                                        <th><a id="dtUpdatedDate" class="sort" data-column="dtUpdatedDate" data-order="ASC" href="#">Modified Date</a></th>
                                        <th><a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">Status</a></th>
                                                    <th class="text-right">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody class="table-hover" id="table_record"></tbody>
                                        </table>
                                        <div class="text-center" id="ajax-loader">
                                            <img src="<?php echo base_url('assets/mf_panel/img/ajax-loader.gif');?>" width="250px" height="auto"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>          
                    </div>
                </div>          
            </div>
            <!-- /Page Wrapper -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#ajax-loader").show();

        url = "<?php echo base_url('mf_panel/system_email/system_email/ajax_listing');?>";

        setTimeout(function(){
            $.ajax({
                url: url,
                type: "POST",
                data:  {}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });
    $(document).on('click','.add',function(){
        location.href = "add";
    });

    $(document).on('click','.edit',function(){
        id = $(this).data("id");
        location.href = "edit/"+id;
    });

    $(document).on('click','.sort',function(){
        column = $(this).data("column");
        order = $(this).attr('data-order');

        if(order == "ASC"){
            $(this).attr('data-order','DESC');
        } else{
            $(this).attr('data-order','ASC');
        }

        $("#ajax-loader").show();

        url = "<?php echo base_url('mf_panel/system_email/system_email/ajax_listing');?>";

        setTimeout(function(){
            $.ajax({
                url: url,
                type: "POST",
                data:  {column:column,order,order,action:'sort'}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });

    $(document).on('click','.search',function(){
        keyword = $("#keyword").val();

        $("#ajax-loader").show();

        url = "<?php echo base_url('mf_panel/system_email/system_email/ajax_listing');?>";

        setTimeout(function(){
            $.ajax({
                url: url,
                type: "POST",
                data:  {keyword:keyword,action:'search'}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });

    $(document).on('click','.delete',function(){
        if (confirm('Are you sure delete this data?')) {
            id = $(this).data("id");

            $("#ajax-loader").show();

            url = "<?php echo base_url('mf_panel/system_email/system_email/ajax_listing');?>";

            setTimeout(function(){
                $.ajax({
                    url: url,
                    type: "POST",
                    data:  {id:id,action:'delete'}, 
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
            }, 1000);
        }
    });

      $("#selectall").click(function()
    {
        if(this.checked){
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', true);
            });
        }else{
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', false);
            });
        }
    });

    $(document).on('change','#status',function()
    {
        if (confirm('Are you sure changes this status?')) {
            eStatus = $("#status").val();

            iEmailTemplateId = [];
            $("input[name='iEmailTemplateId[]']:checked").each( function () {
                iEmailTemplateId.push($(this).val());
            });
            
            //pages = $(this).data("pages");
            iEmailTemplateId = iEmailTemplateId.join(",");
            EmailTemplate = iEmailTemplateId;
        
            $("#table_record").html('');
            // $("#ajax-loader").show();

            url = "<?php echo base_url('mf_panel/system_email/system_email/ajax_listing');?>";

            setTimeout(function(){
                $.ajax({
                    url: url,
                    type: "POST",
                    data:  {iEmailTemplateId : EmailTemplate,action:'status',eStatus:eStatus}, 
                    success: function(response) {
                        // $("#table_record").html(response);
                        // $("#ajax-loader").hide();
                        location.reload();
                    }
                });
            }, 500);
        }
    });
</script>

