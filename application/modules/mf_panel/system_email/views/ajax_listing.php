<?php
$CI =& get_instance();
$CI->load->library('general');
?>
<?php if(count($data) > 0){?>
    <?php foreach ($data as $value) { ?>
        <tr>
            <td>
                <?php echo $value->vEmailTitle; ?>
            </td>
            <td>
                <?php echo $value->vEmailCode; ?>
            </td>
            <td>
                <?php echo $CI->general->date_format($value->dtUpdatedDate);?>
            </td>
            <td>
                <?php echo $value->eStatus;?>
            </td>
             <td class="text-right">
                <div class="actions">
                    <a class="btn btn-sm bg-success-light edit" href="javascript:;" data-id="<?php echo $value->iEmailTemplateId;?>">
                        <i class="fe fe-pencil"></i> Edit
                    </a>
                    <a href="javascript:;" class="btn btn-sm bg-danger-light delete" data-id="<?php echo $value->iEmailTemplateId;?>">
                        <i class="fe fe-trash"></i> Delete
                    </a>
                </div>
            </td>
        </tr>
    <?php }?>
<?php } else {?>
    <tr>
        <td colspan="7"><center>No record found</center></td>
    </tr>
<?php }?>