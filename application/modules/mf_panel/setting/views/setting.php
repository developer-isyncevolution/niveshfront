<?php
$CI =& get_instance();
$CI->load->library('general');
?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary">Edit <?php echo $mode;?></h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 py-md-6 px-md-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post" action="<?php echo base_url('mf_panel/setting/setting_edit');?>" enctype="multipart/form-data">
                            <input type="hidden" id="mode" name="mode" value="<?php echo $mode;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                               
                                <div class="row">
                              <?php foreach ($settings as $setting) { ?>
                                
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                        <?php if($setting->eDisplayType == 'text'){?>
                                            <label><?php echo $setting->vDesc;?> <span class="text-danger"></span></label>
                                            <input type="text" class="form-control" id="<?php echo $setting->vName;?>" name="<?php echo $setting->vName;?>" placeholder=" Name" value="<?php echo $setting->vValue;?>">
                                            <?php } else if($setting->eDisplayType == 'textarea'){?>
                                              <label><?php echo $setting->vDesc;?> <span class="text-danger"></span></label>
                                                <textarea class="form-control" id="<?php echo $setting->vName;?>" name="<?php echo $setting->vName;?>"><?php echo $setting->vValue;?></textarea>
                                              <!-- <label class="setting_label"> <?php echo $setting->vDesc;?></label> -->
                                              <!-- <label class="setting_label"></label> -->
                                              <?php } else if($setting->eDisplayType == 'selectbox'){?>
                                                <?php
                                                  $select = explode(",", $setting->vSourceValue);
                                                ?>
                                          
                                          <label><?php echo $setting->vDesc;?> <span class="text-danger"></span></label>
                                          <select class="form- controlshow-tick" id="<?php echo $setting->vName;?>" name="<?php echo $setting->vName;?>">
                                            <?php foreach ($select as $key => $value) {?>
                                              <option value="<?php echo $value;?>" <?php echo ($value == $setting->vValue) ? 'selected' : '';?>><?php echo $value;?></option>
                                            <?php }?>
                                          </select>
                                          <?php } else if($setting->eDisplayType == 'file'){?>
                                            <label><?php echo $setting->vDesc;?> <span class="text-danger"></span></label>
                                            <input type="file" class="form-control" id="<?php echo $setting->vName;?>" name="<?php echo $setting->vName;?>">
                                            <?php
                                            if($setting->vValue != ""){
                                              $image = base_url('assets/uploads/company_logo/'.$setting->vValue);
                                            ?>
                                              <img id="img" src="<?php echo $image;?>" width="50px" height="auto"/>
                                            <?php }?>
                                          <?php }?>

                                        </div>
                                    </div>    
                               
                                
                                <?php }?>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <button type="button" class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                    <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader" type="button" disabled style="display: none;">
                                      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                      Loading...
                                    </button>
                                    <!-- &nbsp;
                                    <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button> -->
                                  </div>
                                  
                              </div>

                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>    
</div>
<script type="text/javascript">
  
    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/dashboard'); ?>";
    });

    $(document).on('click', '.submit', function () {
        var mode            = $("#mode").val();
       
        var vError               = false;
       
        if(vError == false){
            $(".submit").hide();
            $(".submit_loader").show();
            setTimeout(function(){
              $("#frm").submit();
              $("#message").show();
            }, 1000);
        }
    });
   

</script>