<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class leading_network extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('leading_network_model');
        $this->load->model('user/user_model');
        $this->load->model('system_email/system_email_model');
        $this->load->library('pagination');
        $this->load->library('general');
  
        $this->general->authentication();
    }

    public function index()
    {
        $criteria               = array();
        $criteria["eDeleted"]   = "No";
        $data["data"]           = $this->leading_network_model->get_all_data($criteria);
        $data["eDeleted"]       = "No";

        $this->template->build('listing', $data);
    }

    public function ajax_listing()
    {
        $vAction           = $this->input->post('vAction');
        $iLeadingNetworkId = $this->input->post('iLeadingNetworkId');
        $iClientIdArray    = $this->input->post('iClientIdArray');
        $vKeyword          = $this->input->post('vKeyword');
        $eStatus           = $this->input->post('eStatus');
        $eDeleted          = $this->input->post('eDeleted');
        $vPage             = $this->input->post('vPage');
        $vSort             = $this->input->post('vSort');

        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iLeadingNetworkId";
            $order      = "DESC";
        }

        if($vAction == "delete" && !empty($iLeadingNetworkId))
        {
            $iLeadingNetworkId = $this->input->post('iLeadingNetworkId');
			$this->leading_network_model->delete($iLeadingNetworkId);

        } 
        if($vAction == "client_status" & !empty($iClientIdArray))
        {
            foreach ($iClientIdArray as $key => $value) 
            {
                $myArray                = array();
                $myArray["iLeadingNetworkId"]     = $value;
                $check                  = $this->leading_network_model->get_by_id($myArray);

                if($check->eStatus == "Active")
                {
                    $where               = array();
                    $where["iLeadingNetworkId"]    = $value;

                    $mydata              = array();
                    $mydata["eStatus"]   = "Inactive";

                    $this->leading_network_model->update($where, $mydata);
                }
                else
                {
                    $where              = array();
                    $where["iLeadingNetworkId"]   = $value;

                    $mydata             = array();
                    $mydata["eStatus"]  = "Active";

                    $this->leading_network_model->update($where, $mydata);   
                }
            }
        } 

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(!empty($eStatus)) {
            $criteria['eStatus']  = $eStatus;
        }  
        $criteria['vKeyword']   = $vKeyword;
        $criteria['eDeleted']   = "No";

        $data                   = $this->leading_network_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->leading_network_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $base_path                  = $this->config->item("base_path");
            $iLeadingNetworkId          = $this->input->post('iLeadingNetworkId');           
      
            $data['vTitle']             = $this->input->post('vTitle');
            $data['vTotalClient']       = $this->input->post('vTotalClient');
            $data['eStatus']            = $this->input->post('eStatus');
        
			if($iLeadingNetworkId){
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}else{
				$data['dtAddedDate'] 	= date("Y-m-d h:i:s");
				$data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");
			}
            if(!empty($iLeadingNetworkId))
            {             

                $where = array("iLeadingNetworkId" => $iLeadingNetworkId);
                $this->leading_network_model->update($where, $data);              

                $this->session->set_flashdata('success', "Leading Network Updated Successfully." );
                redirect(base_url('mf_panel/leading_network/leading_network'));
            }
            else
            {
                $ID = $this->leading_network_model->add($data); 

                $this->session->set_flashdata('success', "Leading Network Added Successfully." );
                redirect(base_url('mf_panel/leading_network'));
            }
        }
        else
        {
            $this->template->build('add', NULL);
        }
    }

    public function edit($iLeadingNetworkId ) 
	{

        if(!empty($iLeadingNetworkId))
        {
        $criteria                       = array();
        $criteria["iLeadingNetworkId"]  = $iLeadingNetworkId;
        $data['data']                   = $this->leading_network_model->get_by_id($criteria);
        $data['iLeadingNetworkId']      = $iLeadingNetworkId;
       $this->template->build('add',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/leading_network/add'));    
        }
    }
    
  

  

}