<?php
$CI =& get_instance();
$CI->load->library('general');
$iRoleId = $CI->session->userdata('iRoleId');
$eType = $CI->session->userdata('eType');
?>
<div class="content d-flex flex-column flex-column-fluid dash" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><strong
                        class="text-primary"><?php if($eType == "SuperPartner") { echo "BDM"; } else { echo $eType; } ?></strong>
                    <strong>Dashboard</strong></h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

            </div>

        </div>
    </div>
    <div class="d-flex flex-column-fluid" id="dashboard_search">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xxl-12">
                    <div class="card card-custom bg-gray-100 card-stretch gutter-b consd">
                        <div class="card-header border-0 bg-danger py-5 d-lg-block d-none">
                            <h3 class="card-title font-weight-bolder text-white"></h3>
                        </div>
                        <div class="card-body p-0 position-relative overflow-hidden">
                            <div id="kt_mixed_widget_1_chart" class="card-rounded-bottom bg-danger"
                                style="height:100px"></div>
                            <div class="card-spacer mt-n35">
                                <div class="row m-0">
                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-warning px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
                                                    <span class="svg-icon svg-icon-warning svg-icon-3x"><svg
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                            height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                                fill-rule="evenodd">
                                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                                <path
                                                                    d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z"
                                                                    fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                <path
                                                                    d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z"
                                                                    fill="#000000" fill-rule="nonzero" />
                                                            </g>
                                                        </svg>
                                                        <!--end::Svg Icon-->
                                                    </span>
                                                </div>
                                                <div>
                                                    <strong class="text-warning">Active :
                                                        <?php echo $total_active_banner; ?></strong> <br>
                                                    <strong class="text-warning">Pending :
                                                        <?php echo $total_inactive_banner; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/banner/banner');?>"
                                                class="text-warning font-weight-bold font-size-h6 d-block">Banner</a>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-primary px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
                                                    <span class="svg-icon svg-icon-3x svg-icon-primary">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                            height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                                fill-rule="evenodd">
                                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                                <path
                                                                    d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z"
                                                                    fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                <path
                                                                    d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z"
                                                                    fill="#000000" fill-rule="nonzero" />
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div>
                                                    <strong class="text-primary">Active :
                                                        <?php echo $total_active_digital_exp; ?></strong> <br>
                                                    <strong class="text-primary">Pending :
                                                        <?php echo $total_inactive_digital_exp; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/digital_experience/digital_experience');?>"
                                                class="text-primary font-weight-bold font-size-h6 mt-2">Digital
                                                Experience</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-danger px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
                                                    <span class="svg-icon svg-icon-3x svg-icon-danger  d-block">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                            height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                                fill-rule="evenodd">
                                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                                <path
                                                                    d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z"
                                                                    fill="#000000" fill-rule="nonzero" />
                                                                <path
                                                                    d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z"
                                                                    fill="#000000" opacity="0.3" />
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div>
                                                    <strong class="text-danger">Active :
                                                        <?php echo $total_active_leading_network; ?></strong> <br>
                                                    <strong class="text-danger">Pending :
                                                        <?php echo $total_inactive_leading_network; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/leading_network/leading_network');?>"
                                                class="text-danger  font-weight-bold font-size-h6 mt-2">Leading
                                                Network</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-warning px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
                                                    <span class="svg-icon svg-icon-warning  svg-icon-3x d-block">
                                                        <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Communication/Adress-book2.svg-->
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                            height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                                fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24" />
                                                                <path
                                                                    d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z"
                                                                    fill="#000000" opacity="0.3" />
                                                                <path
                                                                    d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z"
                                                                    fill="#000000" />
                                                            </g>
                                                        </svg>
                                                        <!--end::Svg Icon-->
                                                    </span>
                                                </div>
                                                <div>
                                                    <strong class="text-warning">Active :
                                                        <?php echo $total_active_app_feature; ?></strong> <br>
                                                    <strong class="text-warning">Pending :
                                                        <?php echo $total_inactive_app_feature; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/app_feature/app_feature');?>"
                                                class="text-warning  font-weight-bold font-size-h6 mt-2">App
                                                Features</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-success  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
                                                    <span class="svg-icon svg-icon-3x svg-icon-success">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                            height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                                fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24" />
                                                                <path
                                                                    d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z"
                                                                    fill="#000000" opacity="0.3" />
                                                                <path
                                                                    d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z"
                                                                    fill="#000000" />
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div>
                                                    <strong class="text-success">Active :
                                                        <?php echo $total_active_testimonials; ?></strong> <br>
                                                    <strong class="text-success">Pending :
                                                        <?php echo $total_inactive_testimonials; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/testimonials/testimonials');?>"
                                                class="text-success font-weight-bold font-size-h6 mt-2">Testimonials</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-info px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
                                                    <span class="svg-icon svg-icon-info svg-icon-3x">
                                                        <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Code/Settings4.svg--><svg
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                            height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none"
                                                                fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24" />
                                                                <path
                                                                    d="M18.6225,9.75 L18.75,9.75 C19.9926407,9.75 21,10.7573593 21,12 C21,13.2426407 19.9926407,14.25 18.75,14.25 L18.6854912,14.249994 C18.4911876,14.250769 18.3158978,14.366855 18.2393549,14.5454486 C18.1556809,14.7351461 18.1942911,14.948087 18.3278301,15.0846699 L18.372535,15.129375 C18.7950334,15.5514036 19.03243,16.1240792 19.03243,16.72125 C19.03243,17.3184208 18.7950334,17.8910964 18.373125,18.312535 C17.9510964,18.7350334 17.3784208,18.97243 16.78125,18.97243 C16.1840792,18.97243 15.6114036,18.7350334 15.1896699,18.3128301 L15.1505513,18.2736469 C15.008087,18.1342911 14.7951461,18.0956809 14.6054486,18.1793549 C14.426855,18.2558978 14.310769,18.4311876 14.31,18.6225 L14.31,18.75 C14.31,19.9926407 13.3026407,21 12.06,21 C10.8173593,21 9.81,19.9926407 9.81,18.75 C9.80552409,18.4999185 9.67898539,18.3229986 9.44717599,18.2361469 C9.26485393,18.1556809 9.05191298,18.1942911 8.91533009,18.3278301 L8.870625,18.372535 C8.44859642,18.7950334 7.87592081,19.03243 7.27875,19.03243 C6.68157919,19.03243 6.10890358,18.7950334 5.68746499,18.373125 C5.26496665,17.9510964 5.02757002,17.3784208 5.02757002,16.78125 C5.02757002,16.1840792 5.26496665,15.6114036 5.68716991,15.1896699 L5.72635306,15.1505513 C5.86570889,15.008087 5.90431906,14.7951461 5.82064513,14.6054486 C5.74410223,14.426855 5.56881236,14.310769 5.3775,14.31 L5.25,14.31 C4.00735931,14.31 3,13.3026407 3,12.06 C3,10.8173593 4.00735931,9.81 5.25,9.81 C5.50008154,9.80552409 5.67700139,9.67898539 5.76385306,9.44717599 C5.84431906,9.26485393 5.80570889,9.05191298 5.67216991,8.91533009 L5.62746499,8.870625 C5.20496665,8.44859642 4.96757002,7.87592081 4.96757002,7.27875 C4.96757002,6.68157919 5.20496665,6.10890358 5.626875,5.68746499 C6.04890358,5.26496665 6.62157919,5.02757002 7.21875,5.02757002 C7.81592081,5.02757002 8.38859642,5.26496665 8.81033009,5.68716991 L8.84944872,5.72635306 C8.99191298,5.86570889 9.20485393,5.90431906 9.38717599,5.82385306 L9.49484664,5.80114977 C9.65041313,5.71688974 9.7492905,5.55401473 9.75,5.3775 L9.75,5.25 C9.75,4.00735931 10.7573593,3 12,3 C13.2426407,3 14.25,4.00735931 14.25,5.25 L14.249994,5.31450877 C14.250769,5.50881236 14.366855,5.68410223 14.552824,5.76385306 C14.7351461,5.84431906 14.948087,5.80570889 15.0846699,5.67216991 L15.129375,5.62746499 C15.5514036,5.20496665 16.1240792,4.96757002 16.72125,4.96757002 C17.3184208,4.96757002 17.8910964,5.20496665 18.312535,5.626875 C18.7350334,6.04890358 18.97243,6.62157919 18.97243,7.21875 C18.97243,7.81592081 18.7350334,8.38859642 18.3128301,8.81033009 L18.2736469,8.84944872 C18.1342911,8.99191298 18.0956809,9.20485393 18.1761469,9.38717599 L18.1988502,9.49484664 C18.2831103,9.65041313 18.4459853,9.7492905 18.6225,9.75 Z"
                                                                    fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                <path
                                                                    d="M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                                    fill="#000000" />
                                                            </g>
                                                        </svg>
                                                        <!--end::Svg Icon-->
                                                    </span>
                                                    </g>
                                                    </svg>
                                                    <!--end::Svg Icon-->
                                                    </span>
                                                </div>
                                                <div>
                                                    <strong class="text-info">Total :
                                                        <?php echo $total_page_setting; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/page_setting/page_setting');?>"
                                                class="text-info font-weight-bold font-size-h6 mt-2">Page Setting</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-danger px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-danger svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Cooking/Dish.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path d="M12,21 C7.02943725,21 3,16.9705627 3,12 C3,7.02943725 7.02943725,3 12,3 C16.9705627,3 21,7.02943725 21,12 C21,16.9705627 16.9705627,21 12,21 Z M12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C8.6862915,6 6,8.6862915 6,12 C6,15.3137085 8.6862915,18 12,18 Z" fill="#000000"/>
														<path d="M12,16 C14.209139,16 16,14.209139 16,12 C16,9.790861 14.209139,8 12,8 C9.790861,8 8,9.790861 8,12 C8,14.209139 9.790861,16 12,16 Z" fill="#000000" opacity="0.3"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-danger">Total :
                                                        <?php echo $total_visitor; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/visitor/visitor');?>"
                                                class="text-danger  font-weight-bold font-size-h6 mt-2">Total Visitor</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-primary px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-primary svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Design/Rectangle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<rect fill="#000000" x="4" y="4" width="16" height="16" rx="2"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-primary">Total :
                                                        <?php echo $total_request; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/request/request');?>"
                                                class="text-primary  font-weight-bold font-size-h6 mt-2">Total
                                                Website/logo Design Request</a>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-warning px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-warning svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Design/Border.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path d="M7,6 C6.44771525,6 6,6.44771525 6,7 L6,17 C6,17.5522847 6.44771525,18 7,18 L17,18 C17.5522847,18 18,17.5522847 18,17 L18,7 C18,6.44771525 17.5522847,6 17,6 L7,6 Z M7,4 L17,4 C18.6568542,4 20,5.34314575 20,7 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 Z" fill="#000000" fill-rule="nonzero"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-warning">Total :
                                                        <?php echo $total_contact_us; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/contact_us/contact_us');?>"
                                                class="text-warning  font-weight-bold font-size-h6 mt-2">Total
                                                Contact</a>
                                        </div>
                                    </div>


                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-danger px-6 py-8 rounded-xl  mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-danger svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Home/Book-open.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path d="M13.6855025,18.7082217 C15.9113859,17.8189707 18.682885,17.2495635 22,17 C22,16.9325178 22,13.1012863 22,5.50630526 L21.9999762,5.50630526 C21.9999762,5.23017604 21.7761292,5.00632908 21.5,5.00632908 C21.4957817,5.00632908 21.4915635,5.00638247 21.4873465,5.00648922 C18.658231,5.07811173 15.8291155,5.74261533 13,7 C13,7.04449645 13,10.79246 13,18.2438906 L12.9999854,18.2438906 C12.9999854,18.520041 13.2238496,18.7439052 13.5,18.7439052 C13.5635398,18.7439052 13.6264972,18.7317946 13.6855025,18.7082217 Z" fill="#000000"/>
														<path d="M10.3144829,18.7082217 C8.08859955,17.8189707 5.31710038,17.2495635 1.99998542,17 C1.99998542,16.9325178 1.99998542,13.1012863 1.99998542,5.50630526 L2.00000925,5.50630526 C2.00000925,5.23017604 2.22385621,5.00632908 2.49998542,5.00632908 C2.50420375,5.00632908 2.5084219,5.00638247 2.51263888,5.00648922 C5.34175439,5.07811173 8.17086991,5.74261533 10.9999854,7 C10.9999854,7.04449645 10.9999854,10.79246 10.9999854,18.2438906 L11,18.2438906 C11,18.520041 10.7761358,18.7439052 10.4999854,18.7439052 C10.4364457,18.7439052 10.3734882,18.7317946 10.3144829,18.7082217 Z" fill="#000000" opacity="0.3"/>
													</g>
												</svg><!--end::Svg Icon--></span>

                                                </div>
                                                <div>
                                                    <strong class="text-danger">Total :
                                                        <?php echo $total_book_demo; ?></strong>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url('mf_panel/book_demo/book_demo');?>"
                                                class="text-danger  font-weight-bold font-size-h6 mt-2">Total
                                                Book Demo</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-warning  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
												<span class="svg-icon svg-icon-warning svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Shopping/Chart-bar2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<rect fill="#000000" opacity="0.3" x="17" y="4" width="3" height="13" rx="1.5"/>
														<rect fill="#000000" opacity="0.3" x="12" y="9" width="3" height="8" rx="1.5"/>
														<path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fill-rule="nonzero"/>
														<rect fill="#000000" opacity="0.3" x="7" y="11" width="3" height="6" rx="1.5"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-warning">Active :
                                                        <?php echo $mutual_fund_active; ?></strong> <br>
                                                    <strong class="text-warning">Pending :
                                                        <?php echo $mutual_fund_inactive; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/mutual_fund/mutual_fund');?>"
                                                class="text-warning font-weight-bold font-size-h6 mt-2">Mutual Fund</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-danger  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
												<span class="svg-icon svg-icon-danger svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Code/Code.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path d="M17.2718029,8.68536757 C16.8932864,8.28319382 16.9124644,7.65031935 17.3146382,7.27180288 C17.7168119,6.89328641 18.3496864,6.91246442 18.7282029,7.31463817 L22.7282029,11.5646382 C23.0906029,11.9496882 23.0906029,12.5503176 22.7282029,12.9353676 L18.7282029,17.1853676 C18.3496864,17.5875413 17.7168119,17.6067193 17.3146382,17.2282029 C16.9124644,16.8496864 16.8932864,16.2168119 17.2718029,15.8146382 L20.6267538,12.2500029 L17.2718029,8.68536757 Z M6.72819712,8.6853647 L3.37324625,12.25 L6.72819712,15.8146353 C7.10671359,16.2168091 7.08753558,16.8496835 6.68536183,17.2282 C6.28318808,17.6067165 5.65031361,17.5875384 5.27179713,17.1853647 L1.27179713,12.9353647 C0.909397125,12.5503147 0.909397125,11.9496853 1.27179713,11.5646353 L5.27179713,7.3146353 C5.65031361,6.91246155 6.28318808,6.89328354 6.68536183,7.27180001 C7.08753558,7.65031648 7.10671359,8.28319095 6.72819712,8.6853647 Z" fill="#000000" fill-rule="nonzero"/>
														<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-345.000000) translate(-12.000000, -12.000000) " x="11" y="4" width="2" height="16" rx="1"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-danger">Active :
                                                        <?php echo $total_active_website_design; ?></strong> <br>
                                                    <strong class="text-danger">Pending :
                                                        <?php echo $total_inactive_website_design; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/website_design/website_design');?>"
                                                class="text-danger font-weight-bold font-size-h6 mt-2">Webiste
                                                Design</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-success  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">

                                                <div>
												<span class="svg-icon svg-icon-success svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Design/Horizontal.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"/>
														<path d="M21,12 C21,12.5522847 20.5522847,13 20,13 L19,13 C18.4477153,13 18,12.5522847 18,12 C18,11.4477153 18.4477153,11 19,11 L20,11 C20.5522847,11 21,11.4477153 21,12 Z M16,12 C16,12.5522847 15.5522847,13 15,13 L14,13 C13.4477153,13 13,12.5522847 13,12 C13,11.4477153 13.4477153,11 14,11 L15,11 C15.5522847,11 16,11.4477153 16,12 Z M11,12 C11,12.5522847 10.5522847,13 10,13 L9,13 C8.44771525,13 8,12.5522847 8,12 C8,11.4477153 8.44771525,11 9,11 L10,11 C10.5522847,11 11,11.4477153 11,12 Z M6,12 C6,12.5522847 5.55228475,13 5,13 L4,13 C3.44771525,13 3,12.5522847 3,12 C3,11.4477153 3.44771525,11 4,11 L5,11 C5.55228475,11 6,11.4477153 6,12 Z" fill="#000000"/>
														<path d="M14.9596876,21 L9.04031242,21 C8.76417005,21 8.54031242,20.7761424 8.54031242,20.5 C8.54031242,20.3864643 8.5789528,20.276309 8.64987802,20.1876525 L11.6095656,16.488043 C11.7820704,16.272412 12.0967166,16.2374514 12.3123475,16.4099561 C12.3411799,16.433022 12.3673685,16.4592107 12.3904344,16.488043 L15.350122,20.1876525 C15.5226268,20.4032834 15.4876661,20.7179296 15.2720351,20.8904344 C15.1833786,20.9613596 15.0732233,21 14.9596876,21 Z M9.04031242,3 L14.9596876,3 C15.23583,3 15.4596876,3.22385763 15.4596876,3.5 C15.4596876,3.61353575 15.4210472,3.723691 15.350122,3.81234752 L12.3904344,7.51195699 C12.2179296,7.72758796 11.9032834,7.76254865 11.6876525,7.59004388 C11.6588201,7.56697799 11.6326315,7.54078935 11.6095656,7.51195699 L8.64987802,3.81234752 C8.47737324,3.59671656 8.51233393,3.28207037 8.7279649,3.1095656 C8.81662142,3.03864038 8.92677668,3 9.04031242,3 Z" fill="#000000" opacity="0.3"/>
													</g>
												</svg><!--end::Svg Icon--></span>

                                                </div>
                                                <div>
                                                    <strong class="text-success">Active :
                                                        <?php echo $total_active_logo_design; ?></strong> <br>
                                                    <strong class="text-success">Pending :
                                                        <?php echo $total_inactive_logo_design; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/logo_design/logo_design');?>"
                                                class="text-success font-weight-bold font-size-h6 mt-2">Logo Design</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-info  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-info svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Shopping/Box3.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path d="M20.4061385,6.73606154 C20.7672665,6.89656288 21,7.25468437 21,7.64987309 L21,16.4115967 C21,16.7747638 20.8031081,17.1093844 20.4856429,17.2857539 L12.4856429,21.7301984 C12.1836204,21.8979887 11.8163796,21.8979887 11.5143571,21.7301984 L3.51435707,17.2857539 C3.19689188,17.1093844 3,16.7747638 3,16.4115967 L3,7.64987309 C3,7.25468437 3.23273352,6.89656288 3.59386153,6.73606154 L11.5938615,3.18050598 C11.8524269,3.06558805 12.1475731,3.06558805 12.4061385,3.18050598 L20.4061385,6.73606154 Z" fill="#000000" opacity="0.3"/>
														<polygon fill="#000000" points="14.9671522 4.22441676 7.5999999 8.31727912 7.5999999 12.9056825 9.5999999 13.9056825 9.5999999 9.49408582 17.25507 5.24126912"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-info">Active :
                                                        <?php echo $total_active_vision; ?></strong> <br>
                                                    <strong class="text-info">Pending :
                                                        <?php echo $total_inactive_vision; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/our_vision/our_vision');?>"
                                                class="text-info font-weight-bold font-size-h6 mt-2">Our Vision</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-primary  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-primary svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Code/Git4.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path d="M6,7 C7.1045695,7 8,6.1045695 8,5 C8,3.8954305 7.1045695,3 6,3 C4.8954305,3 4,3.8954305 4,5 C4,6.1045695 4.8954305,7 6,7 Z M6,9 C3.790861,9 2,7.209139 2,5 C2,2.790861 3.790861,1 6,1 C8.209139,1 10,2.790861 10,5 C10,7.209139 8.209139,9 6,9 Z" fill="#000000" fill-rule="nonzero"/>
														<path d="M7,11.4648712 L7,17 C7,18.1045695 7.8954305,19 9,19 L15,19 L15,21 L9,21 C6.790861,21 5,19.209139 5,17 L5,8 L5,7 L7,7 L7,8 C7,9.1045695 7.8954305,10 9,10 L15,10 L15,12 L9,12 C8.27142571,12 7.58834673,11.8052114 7,11.4648712 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
														<path d="M18,22 C19.1045695,22 20,21.1045695 20,20 C20,18.8954305 19.1045695,18 18,18 C16.8954305,18 16,18.8954305 16,20 C16,21.1045695 16.8954305,22 18,22 Z M18,24 C15.790861,24 14,22.209139 14,20 C14,17.790861 15.790861,16 18,16 C20.209139,16 22,17.790861 22,20 C22,22.209139 20.209139,24 18,24 Z" fill="#000000" fill-rule="nonzero"/>
														<path d="M18,13 C19.1045695,13 20,12.1045695 20,11 C20,9.8954305 19.1045695,9 18,9 C16.8954305,9 16,9.8954305 16,11 C16,12.1045695 16.8954305,13 18,13 Z M18,15 C15.790861,15 14,13.209139 14,11 C14,8.790861 15.790861,7 18,7 C20.209139,7 22,8.790861 22,11 C22,13.209139 20.209139,15 18,15 Z" fill="#000000" fill-rule="nonzero"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-primary">Active :
                                                        <?php echo $total_active_why_us; ?></strong> <br>
                                                    <strong class="text-primary">Pending :
                                                        <?php echo $total_inactive_why_us; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/why_us/why_us');?>"
                                                class="text-primary font-weight-bold font-size-h6 mt-2">Why Us</a>
                                        </div>
                                    </div>


                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-success  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-success svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Shopping/Ticket.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path d="M3,10.0500091 L3,8 C3,7.44771525 3.44771525,7 4,7 L9,7 L9,9 C9,9.55228475 9.44771525,10 10,10 C10.5522847,10 11,9.55228475 11,9 L11,7 L21,7 C21.5522847,7 22,7.44771525 22,8 L22,10.0500091 C20.8588798,10.2816442 20,11.290521 20,12.5 C20,13.709479 20.8588798,14.7183558 22,14.9499909 L22,17 C22,17.5522847 21.5522847,18 21,18 L11,18 L11,16 C11,15.4477153 10.5522847,15 10,15 C9.44771525,15 9,15.4477153 9,16 L9,18 L4,18 C3.44771525,18 3,17.5522847 3,17 L3,14.9499909 C4.14112016,14.7183558 5,13.709479 5,12.5 C5,11.290521 4.14112016,10.2816442 3,10.0500091 Z M10,11 C9.44771525,11 9,11.4477153 9,12 L9,13 C9,13.5522847 9.44771525,14 10,14 C10.5522847,14 11,13.5522847 11,13 L11,12 C11,11.4477153 10.5522847,11 10,11 Z" fill="#000000" opacity="0.3" transform="translate(12.500000, 12.500000) rotate(-45.000000) translate(-12.500000, -12.500000) "/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-success">Active :
                                                        <?php echo $total_active_about_us; ?></strong> <br>
                                                    <strong class="text-success">Pending :
                                                        <?php echo $total_inactive_about_us; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/about_us/about_us');?>"
                                                class="text-success font-weight-bold font-size-h6 mt-2">About Us</a>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3 col-6 mb-7">
                                        <div class="bg-light-warning  px-6 py-8 rounded-xl mb-7 dats-box">
                                            <div class="countrts">
                                                <div>
												<span class="svg-icon svg-icon-warning svg-icon-3x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Communication/Chat-smile.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<polygon fill="#000000" opacity="0.3" points="5 15 3 21.5 9.5 19.5"/>
														<path d="M13,2 C18.5228475,2 23,6.4771525 23,12 C23,17.5228475 18.5228475,22 13,22 C7.4771525,22 3,17.5228475 3,12 C3,6.4771525 7.4771525,2 13,2 Z M7.16794971,13.5547002 C8.67758127,15.8191475 10.6456687,17 13,17 C15.3543313,17 17.3224187,15.8191475 18.8320503,13.5547002 C19.1384028,13.0951715 19.0142289,12.4743022 18.5547002,12.1679497 C18.0951715,11.8615972 17.4743022,11.9857711 17.1679497,12.4452998 C16.0109146,14.1808525 14.6456687,15 13,15 C11.3543313,15 9.9890854,14.1808525 8.83205029,12.4452998 C8.52569784,11.9857711 7.90482849,11.8615972 7.4452998,12.1679497 C6.98577112,12.4743022 6.86159725,13.0951715 7.16794971,13.5547002 Z" fill="#000000"/>
													</g>
												</svg><!--end::Svg Icon--></span>
                                                </div>
                                                <div>
                                                    <strong class="text-warning">Active :
                                                        <?php echo $total_active_service; ?></strong> <br>
                                                    <strong class="text-warning">Pending :
                                                        <?php echo $total_inactive_service; ?></strong>
                                                </div>
                                            </div>

                                            <a href="<?php echo base_url('mf_panel/service/service');?>"
                                                class="text-warning font-weight-bold font-size-h6 mt-2">Service</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>


<script type="text/javascript">
</script>