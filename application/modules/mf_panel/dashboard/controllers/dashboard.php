<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller 
{
    public function __construct() 
    {
    
        $this->load->model('user/user_model');
        $this->load->model('banner/banner_model');
        $this->load->model('digital_experience/digital_experience_model');
        $this->load->model('leading_network/leading_network_model');
        $this->load->model('testimonials/testimonials_model');
        $this->load->model('page_setting/page_setting_model');
        $this->load->model('contact_us/contact_us_model');
        $this->load->model('why_us/why_us_model');
        $this->load->model('request/request_model');
        $this->load->model('app_feature/app_feature_model');
        $this->load->model('book_demo/book_demo_model');
        $this->load->model('visitor/visitor_model');
        $this->load->model('mutual_fund/mutual_fund_model');
        $this->load->model('website_design/website_design_model');
        $this->load->model('logo_design/logo_design_model');
        $this->load->model('our_vision/our_vision_model');
        $this->load->model('about_us/about_us_model');
        $this->load->model('service/service_model');
        $this->load->library('general');
        $this->general->authentication();

        parent::__construct();
    }

	public function index()
    {  
        
        $criteria                           = array();
        $criteria["eDeleted"]               = "No";        
        $criteria['eStatus']                = "Inactive";
        $data["total_inactive_banner"]      = $this->banner_model->total_banner($criteria);  
        $criteria                           = array();
        $criteria["eDeleted"]               = "No";   
        $criteria['eStatus']                = "Active"; 
        $data["total_active_banner"]        = $this->banner_model->total_banner($criteria);

        $criteria                           = array();
        $criteria["eDeleted"]               = "No";        
        $criteria['eStatus']                = "Inactive";
        $data["total_inactive_digital_exp"] = $this->digital_experience_model->total_digital_exp($criteria);   
        $criteria                           = array();
        $criteria["eDeleted"]               = "No";   
        $criteria['eStatus']                = "Active"; 
        $data["total_active_digital_exp"]   = $this->digital_experience_model->total_digital_exp($criteria);

        $criteria                           = array();
        $criteria["eDeleted"]               = "No";        
        $criteria['eStatus']                = "Inactive";
        $data["total_inactive_leading_network"] = $this->leading_network_model->total_leading_network($criteria);   
        $criteria                           = array();
        $criteria["eDeleted"]               = "No";   
        $criteria['eStatus']                = "Active"; 
        $data["total_active_leading_network"] = $this->leading_network_model->total_leading_network($criteria);

        $criteria                           = array();
        $criteria["eDeleted"]               = "No";        
        $criteria['eStatus']                = "Active";
        $data["total_active_testimonials"] = $this->testimonials_model->total_testimonials($criteria);   
        $criteria                           = array();
        $criteria["eDeleted"]               = "No";   
        $criteria['eStatus']                = "Inactive"; 
        $data["total_inactive_testimonials"] = $this->testimonials_model->total_testimonials($criteria);


        $criteria                           = array();
        $data["total_page_setting"]         = $this->page_setting_model->total_page_setting($criteria);   
       
        $criteria                           = array();
        $criteria["eDeleted"]               = "No";        
        $criteria['eStatus']                = "Active";
        $data["total_contact_us"]           = $this->contact_us_model->total_contact_us($criteria);

        $criteria                           = array();
        $criteria["eDeleted"]               = "No";   
        $criteria['eStatus']                = "Active"; 
        $data["total_active_leading_network"] = $this->leading_network_model->total_leading_network($criteria);

        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_active_app_feature"] = $this->app_feature_model->total_app_feature($criteria);

        $criteria                           = array();
        $criteria['eStatus']                = "Inactive"; 
        $data["total_inactive_app_feature"] = $this->app_feature_model->total_app_feature($criteria);

        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_active_why_us"]        = $this->why_us_model->total_why_us($criteria);
        
        $criteria                           = array();
        $criteria['eStatus']                = "Inactive"; 
        $data["total_inactive_why_us"]      = $this->why_us_model->total_why_us($criteria);

        $criteria                           = array();
        $data["total_visitor"]              = $this->visitor_model->total_visitor($criteria);

        $criteria                           = array();
        $data["total_contact_us"]           = $this->contact_us_model->total_contact_us($criteria);

        $criteria                           = array();
        $data["total_request"]              = $this->request_model->total_request($criteria);

        $criteria                           = array();
        $data["total_book_demo"]            = $this->book_demo_model->total_book_demo($criteria);

        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["mutual_fund_active"]        = $this->mutual_fund_model->total_muttual_fund($criteria);
        
        $criteria                           = array();
        $criteria['eStatus']                = "Inactive"; 
        $data["mutual_fund_inactive"]      = $this->mutual_fund_model->total_muttual_fund($criteria);

        
        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_active_website_design"]= $this->website_design_model->total_website_design($criteria);
        
        $criteria                             = array();
        $criteria['eStatus']                  = "Inactive"; 
        $data["total_inactive_website_design"]= $this->website_design_model->total_website_design($criteria);


        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_active_logo_design"]   = $this->logo_design_model->total_logo_design($criteria);
        
        $criteria                             = array();
        $criteria['eStatus']                  = "Inactive"; 
        $data["total_inactive_logo_design"]   = $this->logo_design_model->total_logo_design($criteria);


        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_active_vision"]        = $this->our_vision_model->total_vision($criteria);
        
        $criteria                           = array();
        $criteria['eStatus']                = "Inactive"; 
        $data["total_inactive_vision"]      = $this->our_vision_model->total_vision($criteria);


        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_why_us"]               = $this->why_us_model->total_why_us($criteria);
        
        $criteria                           = array();
        $criteria['eStatus']                = "Inactive"; 
        $data["total_why_us"]               = $this->why_us_model->total_why_us($criteria);

        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_active_about_us"]      = $this->about_us_model->total_about_us($criteria);
        
        $criteria                           = array();
        $criteria['eStatus']                = "Inactive"; 
        $data["total_inactive_about_us"]    = $this->about_us_model->total_about_us($criteria);

        $criteria                           = array();
        $criteria['eStatus']                = "Active";
        $data["total_active_service"]       = $this->service_model->total_service($criteria);
        
        $criteria                           = array();
        $criteria['eStatus']                = "Inactive"; 
        $data["total_inactive_service"]     = $this->service_model->total_service($criteria);

        $this->template->build('dashboard', $data);
	}
    
}