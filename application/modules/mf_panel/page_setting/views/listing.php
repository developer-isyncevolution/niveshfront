<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-lg-flex align-items-center flex-wrap mr-lg-2 d-md-flex d-flex">
                <h5 class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex text-primary">Page Setting Listing</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <div class="d-inline-block d-lg-none d-md-none justify-content-end ml-md-auto ml-auto">
                    <a href="<?php echo base_url('mf_panel/page_setting/add'); ?>" class="btn btn-light-primary font-weight-bold mx-sm-2"><i class="fa fa-plus pr-0"></i></a>
                </div>
                <div class="d-lg-flex align-items-center d-md-flex d-inline subheader-main" id="kt_subheader_search">
                    <span class="text-dark-50 font-weight-bold  d-inline-block tot dis-none mr-lg-5 mr-md-5" id="kt_subheader_total"><?php echo count($data); ?> Total</span>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-6 mt-lg-0 mt-2">
                            <div class="input-group input-group-md input-group-solid" >
                                <input type="text" class="form-control keyword" id="kt_subheader_search_form" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn btn-light-primary search" type="button">Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-6 mt-lg-0 mt-2">
                            <div class="input-group input-group-md input-group-solid" >
                                <select class="form-control sort">
                                    <option value="">Sort</option>
                                    <option value="vPageTitle-ASC">Name (A-Z)</option>
                                    <option value="vPageTitle-DESC">Name (Z-A)</option>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="d-inline-block d-lg-none d-xl-none">
                            <a href="<?php echo base_url('mf_panel/page_setting/add'); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus pr-0"></i></a>
                           
                            <div class="ml-2 spinner-border text-primary" role="status" id="download_loader_mob" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            
            <div class="d-lg-flex align-items-center d-xl-flex d-md-flex d-none">
                <a href="<?php echo base_url('mf_panel/page_setting/add'); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus pr-0"></i></a>
                
                <div class="ml-2 spinner-border text-primary" role="status" id="download_loader" style="display: none;">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card-header border-0 py-5"></div>
    <div class="card card-custom gutter-b shadow-none mob-card" style="border-radius: 0 !important;">
        <div class="card-body py-0 mb-3 pad-0">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-head-custom table-vertical-center mob-tab" id="kt_advance_table_widget_4">
                    <thead class="dis-none">
                        <tr>
                            <th class="text-center" style="width:240px; min-width:240px">Title</th>
                            <th class="text-center" style="width:240px; min-width:240px">Page Code</th>
                            <th class="text-center" style="width:70px; min-width:70px">Date</th>
                            <th class="text-center" style="width:70px; min-width:70px">Action</th>
                        </tr>
                    </thead>
                    <tbody id="registrar_response">
                        <tr align="center">
                            <td colspan="9">
                                <div class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>   
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
            $.ajax({
                url: "<?php echo base_url('mf_panel/page_setting/ajax_listing');?>",
                type: "POST",
                data:  {  }, 
                success: function(response) {
                    $("#registrar_response").html(response);
                }
            });
        }, 1000);
    });

    $(document).on('click','.edit',function(){
        iPageId = $(this).data("id");
        location.href = "page_setting/edit/"+iPageId;
    });


    $(document).on('change', '.sort', function() {
        var vKeyword            = $(".keyword").val();
        var eDeleted            = "<?php echo $eDeleted; ?>";
        var vSort               = $(this).val();

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
        $.ajax({
            url: "<?php echo base_url('mf_panel/page_setting/ajax_listing');?>",
            type: "POST",
            data:  { vKeyword:vKeyword, vAction:'sort', eDeleted:eDeleted, vSort:vSort }, 
            success: function(response) {
                $("#registrar_response").html(" ");
                $("#registrar_response").html(response);
            }
        });
    });

    $(document).on('click', '.search', function() {
        var vKeyword = $(".keyword").val();
        if(vKeyword.length > 0) {
            $("#registrar_response").html(" ");
            $("#registrar_response").html('<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>');
            setTimeout(function(){
                $.ajax({
                    url: "<?php echo base_url('mf_panel/page_setting/ajax_listing');?>",
                    type: "POST",
                    data:  { vKeyword:vKeyword , vAction:'search' }, 
                    success: function(response) {
                        $("#registrar_response").html(" ");
                        $("#registrar_response").html(response);
                        $("#kt_subheader_total").html($("#iCount").val()+" Total");

                    }
                });
            }, 1000);
        } else {
            notification_error("Enter Search String");
        }
    });
    $(document).on('click','.delete',function(){
        if (confirm('Are you sure delete this data?')) {
            iPageId = $(this).data("id");
            $("#ajax-loader").show();

            url = "<?php echo base_url('mf_panel/page_setting/ajax_listing');?>";

            setTimeout(function(){
                $.ajax({
                    url: url,
                    type: "POST",
                    data:  {iPageId:iPageId,vAction:'delete'}, 
                    success: function(response) {
                        $("#registrar_response").html(response);
                        $("#ajax-loader").hide();
                        location.reload();

                    }
                });
            }, 1000);
        }
    });


    $(document).on('click', '.ajax_page', function() {
        
        var eDeleted    = "<?php echo $eDeleted; ?>";
        var vPage       = $(this).data('pages');
        var vKeyword    = $(".keyword").val();
        var vSort               = $('.sort').val();

        $("#registrar_response").html(" ");
        $("#registrar_response").html('<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');

        $.ajax({
            url: "<?php echo base_url('mf_panel/page_setting/ajax_listing');?>",
            type: "POST",
            data: { vPage:vPage, vKeyword:vKeyword, eDeleted:eDeleted,vSort:vSort }, 
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    });

</script>

