<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary">
                    <?php echo ($data->iPageId) ? 'Edit ' : 'Add '; ?> Page Setting</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 px-md-5 py-md-6 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post"
                            action="<?php echo base_url('mf_panel/page_setting/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iPageId" name="iPageId" value="<?php echo $data->iPageId;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

                                <div class="row">
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label> Page Title <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vPageTitle" name="vPageTitle"
                                                placeholder="Enter Page Title" value="<?php echo $data->vPageTitle;?>">
                                            <div class="text-danger" id="vPageTitle_error" style="display: none;">Enter
                                                Page Title</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label> Page Code <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vPageCode" name="vPageCode"
                                                placeholder="Enter Page Code" value="<?php echo $data->vPageCode;?>">
                                            <div class="text-danger" id="vPageCode_error" style="display: none;">Enter
                                                Page Code</div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xl-12 col-md-12">
                                        <div class="form-group">
                                        <textarea id="tDescription" name="tLongContent" class="form-control" placeholder="Enter Description" required><?php echo $data->tLongContent;?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-xl-21  col-md-6 ">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Image </label>
                                                    <input type="file" class="form-control" name="image"
                                                        id="vBannerPicture">
                                                </div>
                                                <div class="col-md-6">
                                                    <?php if(!empty($data->vImage)) { ?>
                                                    <img src="<?php echo base_url('assets/uploads/page_setting/'.$data->vImage); ?>"
                                                        width="60px" id="vBannerPicture_Preview">
                                                    <?php } else { ?>
                                                    <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>"
                                                        width="60px" id="vBannerPicture_Preview">
                                                    <?php } ?>


                                                    <img id="img" src="<?php echo $image;?>" width="100px"
                                                        height="auto" />
                                                </div>
                                            </div>
                                            <div class="text-danger" id="vImage_Error" style="display: none;">Image
                                                Required</div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button"
                                            class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                        <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader"
                                            type="button" disabled style="display: none;">
                                            <span class="spinner-border spinner-border-sm" role="status"
                                                aria-hidden="true"></span>
                                            Loading...
                                        </button>
                                        &nbsp;
                                        <button type="button"
                                            class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<script type="text/javascript" src="<?php echo base_url('assets/mf_panel/js/tinymce/tinymce.min.js');?>"></script>
<script>
$(document).on('click', '.cancel', function() {
    window.location = "<?php echo base_url('mf_panel/page_setting/page_setting'); ?>";
});

tinymce.init({
    selector: 'textarea',
    height: 300,
    menubar: true,
    plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
    ],
    toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});

$(document).on('click', '.submit', function() {
    var iPageId = $("#iPageId").val();
    var vPageTitle = $("#vPageTitle").val();
    var tContent = $("#tContent").val();
    var vPageCode = $("#vPageCode").val();
    var vError = false;

    if (vPageTitle.length == 0) {
        $("#vPageTitle_error").show();
        vError = true;
    } else {
        $("#vPageTitle_error").hide();
    }
    if (vPageCode.length == 0) {
        $("#vPageCode_error").show();
        vError = true;
    } else {
        $("#vPageCode_error").hide();
    }
    // if (tContent.length == 0) {
    //     $("#tContent_error").show();
    //     vError = true;
    // } else {
    //     $("#tContent_error").hide();
    // }

    if (vError == false) {
        $(".submit").hide();
        $(".submit_loader").show();
        setTimeout(function() {
            $("#frm").submit();
        }, 1000);
    }
});


$(document).on('change', '#vBannerPicture', function() {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#vBannerPicture_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
    }
});
</script>