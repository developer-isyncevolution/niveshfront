<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class page_setting extends MX_Controller 
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('page_setting_model');
        $this->load->library('pagination');

    }

	public function index()
	{
		$criteria               = array();
        $data["data"]           = $this->page_setting_model->get_all_data($criteria);
        $this->template->build('listing', $data);
	}

    public function ajax_listing()
    {
        $vAction        = $this->input->post('vAction');
        $iPageId      = $this->input->post('iPageId');
        $vKeyword       = $this->input->post('vKeyword');
        $eStatus        = $this->input->post('eStatus');
        $eDeleted       = $this->input->post('eDeleted');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');

        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iPageId";
            $order      = "DESC";
        }
        if($vAction == "delete" && !empty($iPageId))
        {
			$this->page_setting_model->delete($iPageId);
        }    

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eStatus']    = $eStatus;
        $criteria['vKeyword']   = $vKeyword;
        $criteria['eDeleted']   = "No";
        $data                   = $this->page_setting_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->page_setting_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

	public function add() 
	{
	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$iPageId = $this->input->post('iPageId');

			$data['vPageCode'] 		= $this->input->post('vPageCode');
			$data['vPageTitle'] 	= $this->input->post('vPageTitle');
			$data['tContent'] 		= $this->input->post('tContent');
			$data['tLongContent'] 	= $this->input->post('tLongContent');
			
			if($iPageId){
				$data['dtUpdatedDate'] = date("Y-m-d h:i:s");
			}
			else{
				$data['dtAddedDate'] = date("Y-m-d h:i:s");
				$data['dtUpdatedDate'] = date("Y-m-d h:i:s");
			}

			if($_FILES['image']['name'] != ""){
				if($_FILES['image']['type'] == 'image/gif'){
					$ext = ".gif";
				} else if($_FILES['image']['type'] == 'image/jpeg'){
					$ext = ".jpeg";
				} else if($_FILES['image']['type'] == 'image/png'){
					$ext = ".png";
				}

				$image = time().$ext;

				$config = array(
					'file_name' => $image,
					'upload_path' => "../assets/uploads/page_setting/",
					'allowed_types' => "gif|jpg|png|jpeg",
				);

				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('image'))	{
					echo "<pre>";
					print_r($this->upload->display_errors());
					exit;
				}

				$data['vImage'] = $image;
			}
			
			
			if($iPageId)
			{
				$where = array("iPageId" => $iPageId);
				$this->page_setting_model->update($where, $data);

				$this->session->set_flashdata('success', "Page Setting update successfully." );
			} else {
				$ID = $this->page_setting_model->add($data);

				$this->session->set_flashdata('success', "Page Setting add successfully." );
			}
			redirect(base_url('mf_panel/page_setting/page_setting'));
		} else {
			$this->template->build('add',$data);
		}
	}



	public function edit($iPageId  = NULL) 
	{

        if(!empty($iPageId))
        {
            $criteria              = array();
            $criteria["iPageId"]  = $iPageId;

            $data['data']          = $this->page_setting_model->get_by_id($criteria);
		
            $this->template->build('add',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/page_setting/add'));    
        }
    }
}


