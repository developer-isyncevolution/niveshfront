<?php
$CI =& get_instance();
$CI->load->library('general');
?>

<?php if(!empty($data)) { ?>
<?php foreach ($data as $key => $value) { ?>
<tr style='<?php if($value->eStatus == "Inactive") { echo "background-color: #f5bcc138"; } ?>' class="mob-tr">
    <!-- <td class="py-6 dis-none">
        <label class="checkbox checkbox-lg checkbox-inline  mr-0 align-items-center justify-content-center">
            <input type="checkbox" name="ID[]" class="checkboxall client_checkbox"
                value="<?php echo $value->ID;?>" data-id="<?php echo $value->ID; ?>"
                data-value="<?php echo $value->ID; ?>">
            <span></span>
        </label>
    </td> -->
    <td class="text-center">
        <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->vARNNumber; ?></span>
    </td>
    <td class="text-left">
        <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->vEmail; ?></span>
    </td>
    <td class="text-center">
        <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->vMobile; ?></span>
    </td>
    <td class="text-center dis-none">
        <?php if($value->eStatus == "Complete") { ?>
        <span class="label label-lg label-light-success label-inline"><?php echo $value->eStatus; ?></span>
        <?php } ?>
        <?php if($value->eStatus == "Pending") { ?>
        <span class="label label-lg label-light-danger label-inline"><?php echo $value->eStatus; ?></span>
        <?php } ?>
    </td>

    <td class="text-center white-space-nowrap">
        <button class="btn btn-icon btn-primary btn-sm mr-1 edit" data-id="<?php echo $value->ID;?>"
            data-toggle="tooltip" title="Edit ">
            <span class="fa fa-pencil"></span>
        </button>
        <button class="btn btn-icon btn-light-primary btn-sm mr-1 view" data-id="<?php echo $value->ID;?>">
        <span class="fa fa-eye"></span>
        </button>
        <button class="btn btn-icon btn-light-primary btn-sm mr-1 note" data-id="<?php echo $value->ID;?>">
        <span class="fa fa-book"></span>
        </button>
    </td>

</tr>
<?php } ?>
<!-- <tr class="dis-none">
    <td class="text-dark-75 font-weight-bolder font-size-lg" colspan="4">
        <div class="row m-0">
            <div class="text-left col-md-6 pl-0"><button
                    class="btn btn-primary client_inactive">Active/Inactive</button></div>
            <div class="spinner-border text-primary" role="status" id="client_inactive_loader" style="display: none;">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </td>
</tr> -->

<tr>
    <td colspan="9"></td>
</tr>
<tr>
    <td colspan="9" style="background-color: #eef0f8;" class="pad-0">
        <div class="">
            <?php echo $paging; ?>
        </div>
    </td>
</tr>
<?php } else { ?>
<tr align="center">
    <td class="text-dark-75 font-weight-bolder font-size-lg" colspan="9">No Records</td>
</tr>
<?php } ?>