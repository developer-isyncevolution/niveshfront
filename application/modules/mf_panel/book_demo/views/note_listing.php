<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-lg-flex align-items-center flex-wrap mr-lg-2 d-md-flex d-flex">
                <h5 class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex text-primary">Book Demo Note Listing
                </h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <div class="d-inline-block d-lg-none d-md-none justify-content-end ml-md-auto ml-auto">
                    <a href="<?php echo base_url('mf_panel/book_demo/add_note/'.$ID); ?>"
                        class="btn btn-light-primary font-weight-bold mx-md-2"><i class="fa fa-plus pr-0"></i></a>
                </div>
            </div>
            <div class="d-lg-flex align-items-center d-xl-flex d-md-flex d-none">
                <a href="<?php echo base_url('mf_panel/book_demo/add_note/'.$ID); ?>" class="btn btn-light-primary font-weight-bold ml-2"><i class="fa fa-plus pr-0"></i></a>
                <input type="hidden" id="iBookDemoId" name="iBookDemoId" value="<?php echo $ID;?>">

                <div class="ml-2 spinner-border text-primary" role="status" id="download_loader" style="display: none;">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card-header border-0 py-5"></div>
    <div class="card card-custom gutter-b shadow-none mob-card" style="border-radius: 0 !important;">
        <div class="card-body py-0 mb-3 pad-0">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-head-custom table-vertical-center mob-tab"
                    id="kt_advance_table_widget_4">
                    <thead class="dis-none">
                        <tr>
                        
                            <th class="text-center" style="width:100px; min-width:100px">ARN Number</th>
                            <th class="text-center" style="width:300px; min-width:300px">Note</th>
                            <th class="text-center" style="width:100px; min-width:100px">Date</th>
                            <th class="text-center" style="width:70px; min-width:70px">Action</th>
                        </tr>
                    </thead>
                    <tbody id="registrar_response">
                        <tr align="center">
                            <td colspan="9">
                                <div class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    iBookDemoId     = $("#iBookDemoId").val();

    setTimeout(function() {
        $.ajax({
            url: "<?php echo base_url('mf_panel/book_demo/note_ajax_listing/'.$iBookDemoId);?>",
            type: "POST",
            data: {iBookDemoId:iBookDemoId},
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    }, 1000);
});


$(document).on('click','.view_note',function(){
    iBookDemoNoteId = $(this).data("id");
    iBookDemoId = $(this).data("filter");
    window.location = "<?php echo base_url('mf_panel/book_demo/view_note'); ?>"+"/"+iBookDemoId+"/"+iBookDemoNoteId;
    });

    $(document).on('click','.edit_note',function(){
    iBookDemoNoteId = $(this).data("id");
    iBookDemoId = $(this).data("filter");
    window.location = "<?php echo base_url('mf_panel/book_demo/edit_note'); ?>"+"/"+iBookDemoId+"/"+iBookDemoNoteId;
    });


$(document).on('click', '.delete_note', function() {
    if (confirm('Are you sure delete this data?')) {
        iBookDemoNoteId = $(this).data("id");
        $("#ajax-loader").show();
        url = "<?php echo base_url('mf_panel/book_demo/book_demo/note_ajax_listing');?>";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    iBookDemoNoteId: iBookDemoNoteId,
                    vAction: 'delete'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                    location.reload();
                }
            });
        }, 1000);
    }
});

$(document).on('click', '.search', function() {
    var vKeyword = $(".keyword").val();
    var eStatus = $(".client_status_change").val();
    if (vKeyword.length > 0) {
        $("#registrar_response").html(" ");
        $("#registrar_response").html(
            '<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>'
            );
        setTimeout(function() {
            $.ajax({
                url: "<?php echo base_url('mf_panel/book_demo/note_ajax_listing');?>",
                type: "POST",
                data: {
                    vKeyword: vKeyword,
                    eStatus: eStatus,
                    vAction: 'search'
                },
                success: function(response) {
                    $("#registrar_response").html(" ");
                    $("#registrar_response").html(response);
                    $("#kt_subheader_total").html($("#iCount").val() + " Total");

                }
            });
        }, 1000);
    } else {
        notification_error("Enter Search String");
    }
});

$("#selectall").click(function() {
    if (this.checked) {
        $('.checkboxall').each(function() {
            $(".checkboxall").prop('checked', true);
        });
    } else {
        $('.checkboxall').each(function() {
            $(".checkboxall").prop('checked', false);
        });
    }
});




$(document).on('click', '.client_inactive', function() {
    var vKeyword = $(".keyword").val();
    var iClientIdArray = [];

    $('.client_checkbox').each(function(index, value) {
        if (this.checked == true) {
            iClientIdArray.push($(this).data("value"));
        }
    });

    if (iClientIdArray.length > 0) {
        $('.client_inactive').hide();
        $('#client_inactive_loader').show();
        $("#registrar_response").html(" ");
        $("#registrar_response").html(
            '<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>'
            );
        setTimeout(function() {
            $.ajax({
                url: "<?php echo base_url('mf_panel/book_demo/note_ajax_listing');?>",
                type: "POST",
                data: {
                    iClientIdArray: iClientIdArray,
                    vAction: 'client_status',
                    vKeyword: vKeyword
                },
                success: function(response) {
                    $('.client_inactive').show();
                    $('#client_inactive_loader').hide();
                    $("#registrar_response").html(response);
                    notification_success("vision Status Updated");
                }
            });
        }, 1000)
    } else {
        notification_error("Please Select vision to Mark Inactive");
    }
});

$(document).on('change', '.client_status_change', function() {
    var eStatus = $(this).val();
    var vKeyword = $(".keyword").val();
    var eDeleted = "<?php echo $eDeleted; ?>";
    var vPage = $(this).data('pages');
    var vSort = $('.sort').val();

    $("#registrar_response").html(" ");
    $("#registrar_response").html(
        '<tr align="center"><td colspan="9"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></td></tr>'
        );
    setTimeout(function() {
        $.ajax({
            url: "<?php echo base_url('mf_panel/book_demo/note_ajax_listing');?>",
            type: "POST",
            data: {
                eStatus: eStatus,
                vKeyword: vKeyword,
                vPage: vPage,
                vSort: vSort
            },
            success: function(response) {
                $("#registrar_response").html(response);
            }
        });
    }, 1000);
})

$(document).on('click', '.ajax_page', function() {

    var eDeleted = "<?php echo $eDeleted; ?>";
    var vPage = $(this).data('pages');
    var vKeyword = $(".keyword").val();
    var vSort = $('.sort').val();
    var eStatus = $('client_status_change').val();

    $("#registrar_response").html(" ");
    $("#registrar_response").html(
        '<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>'
        );

    $.ajax({
        url: "<?php echo base_url('mf_panel/book_demo/note_ajax_listing');?>",
        type: "POST",
        data: {
            vPage: vPage,
            vKeyword: vKeyword,
            eDeleted: eDeleted,
            vSort: vSort,
            eStatus: eStatus
        },
        success: function(response) {
            $("#registrar_response").html(response);
        }
    });
});

$(document).on('change', '.sort', function() {
    var vKeyword = $(".keyword").val();
    var eDeleted = "<?php echo $eDeleted; ?>";
    var vSort = $(this).val();
    var eStatus = $('.client_status_change').val();


    $("#registrar_response").html(" ");
    $("#registrar_response").html(
        '<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>'
        );
    $.ajax({
        url: "<?php echo base_url('mf_panel/book_demo/note_ajax_listing');?>",
        type: "POST",
        data: {
            vKeyword: vKeyword,
            vAction: 'sort',
            eDeleted: eDeleted,
            vSort: vSort,
            eStatus: eStatus
        },
        success: function(response) {
            $("#registrar_response").html(" ");
            $("#registrar_response").html(response);
            $("#kt_subheader_total").html($("#iCount").val() + " Total");
        }
    });
});

$(document).on('click', '.pdf', function() {
    var ID = [];
    $('.client_checkbox').each(function(index, value) {
        if (this.checked == true) {
            ID.push($(this).data("value"));
        }
    });

    if (ID.length > 0) {

        setTimeout(function() {
            $.ajax({
                url: "<?php echo base_url('mf_panel/book_demo/download_listing_pdf');?>",
                type: "POST",
                dataType: "JSON",
                data: {
                    ID: ID
                },
                success: function(response) {
                    if (response.status == "200") {
                        notification_success(response.notification);
                        var vFile =
                            "<?php echo base_url('assets/uploads/listing_pdf/vision') ?>" +
                            "/" + response.filename;
                        window.open(vFile);
                    }
                    $('.download_option').show();
                    $('#download_loader').hide();
                }
            });
        }, 1000);
    } else {
        if (confirm('Want to download full listing ?')) {
            $('.client_checkbox').each(function(index, value) {
                ID.push($(this).data("value"));
            });
            $('.download_option').hide();
            $('#download_loader').show();
            setTimeout(function() {
                $.ajax({
                    url: "<?php echo base_url('mf_panel/book_demo/download_listing_pdf');?>",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        ID: ID
                    },
                    success: function(response) {
                        if (response.status == "200") {
                            notification_success(response.notification);
                            var vFile =
                                "<?php echo base_url('assets/uploads/listing_pdf/vision') ?>" +
                                "/" + response.filename;
                            window.open(vFile);
                        }
                        $('.download_option').show();
                        $('#download_loader').hide();
                    }
                });
            }, 1000);
        }
    }

});

$(document).on('click', '.excel', function() {
    var ID = [];

    $('.client_checkbox').each(function(index, value) {
        if (this.checked == true) {
            ID.push($(this).data("value"));
        }
    });

    if (ID.length > 0) {
        $('.download_option').hide();
        $('#download_loader').show();
        setTimeout(function() {
            $.ajax({
                url: "<?php echo base_url('mf_panel/book_demo/download_listing_excel');?>",
                type: "POST",
                dataType: "JSON",
                data: {
                    ID: ID
                },
                success: function(response) {
                    if (response.status == "200") {
                        notification_success(response.notification);
                        var vFile =
                            "<?php echo base_url('assets/uploads/listing_excel/vision') ?>" +
                            "/" + response.filename;
                        window.open(vFile);
                    }
                    $('.download_option').show();
                    $('#download_loader').hide();
                }
            });
        }, 1000);
    } else {
        if (confirm('Want to download full listing ?')) {
            $('.client_checkbox').each(function(index, value) {
                ID.push($(this).data("value"));
            });
            $('.download_option').hide();
            $('#download_loader').show();
            setTimeout(function() {
                $.ajax({
                    url: "<?php echo base_url('mf_panel/book_demo/download_listing_excel');?>",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        ID: ID
                    },
                    success: function(response) {
                        if (response.status == "200") {
                            notification_success(response.notification);
                            var vFile =
                                "<?php echo base_url('assets/uploads/listing_excel/vision') ?>" +
                                "/" + response.filename;
                            window.open(vFile);
                        }
                        $('.download_option').show();
                        $('#download_loader').hide();
                    }
                });
            }, 1000);
        }
    }
});


</script>