<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary"><?php echo ($data->ID) ? 'Edit ' : 'Add '; ?>
                    Book Demo</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 py-md-6 px-md-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post"
                            action="<?php echo base_url('mf_panel/book_demo/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="ID" name="ID" value="<?php echo $ID;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label> ARN Number Title <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vARNNumber" name="vARNNumber"
                                                placeholder=" Enter book_demo Title"
                                                value="<?php echo $data->vARNNumber;?>">
                                            <div class="text-danger" id="vARNNumber_error" style="display: none;">Enter
                                                ARN Number Title</div>
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Status <span class="text-danger">*</span></label>
                                            <select name="eStatus" id="eStatus" class="form-control show-tick">
                                                <option value="">Select Status</option>
                                                <option <?php echo ($data->eStatus == 'Pending') ? 'selected' : '';?>
                                                    value="Pending">Pending</option>
                                                <option <?php echo ($data->eStatus == 'Complete') ? 'selected' : '';?>
                                                    value="Complete">Complete</option>

                                            </select>
                                            <div class="text-danger" id="eStatus_error" style="display: none;">Select
                                                Status</div>

                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Email <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vEmail" name="vEmail"
                                                placeholder=" Enter Email"
                                                value="<?php echo $data->vEmail;?>">
                                            <div class="text-danger" id="vEmail_error" style="display: none;">Enter
                                                Email</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Mobile <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vMobile" name="vMobile"
                                                placeholder=" Enter Mobile "
                                                value="<?php echo $data->vMobile;?>" maxlength="10">
                                            <div class="text-danger" id="vMobile_error" style="display: none;">Enter
                                                Mobile</div>
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>State <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vState" name="vState"
                                                placeholder=" Enter State "
                                                value="<?php echo $data->vState;?>">
                                            <div class="text-danger" id="vState_error" style="display: none;">Enter
                                                State</div>
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>City <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vCity" name="vCity"
                                                placeholder=" Enter City "
                                                value="<?php echo $data->vCity;?>">
                                            <div class="text-danger" id="vCity_error" style="display: none;">Enter
                                                City</div>
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Date <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="dDate" name="dDate"
                                                placeholder=" Enter Date "
                                                value="<?php echo $this->general->date_format2($data->dDate);?>">
                                            <div class="text-danger" id="dDate_error" style="display: none;">Enter
                                            dDate</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Time <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="tTime" name="tTime"
                                                placeholder=" Enter Time "
                                                value="<?php echo  $this->general->show_time($data->tTime);?>">
                                            <div class="text-danger" id="tTime_error" style="display: none;">Enter
                                            tTime</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button"
                                            class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                        <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader"
                                            type="button" disabled style="display: none;">
                                            <span class="spinner-border spinner-border-sm" role="status"
                                                aria-hidden="true"></span>
                                            Loading...
                                        </button>
                                        &nbsp;
                                        <button type="button"
                                            class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<script type="text/javascript">
$(document).on('click', '.cancel', function() {
    window.location = "<?php echo base_url('mf_panel/book_demo/book_demo'); ?>";
});


$('#vMobile').bind('input propertychange', function () {
      $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });

$(document).on('click', '.submit', function() {
    var vARNNumber  = $("#vARNNumber").val();
    var ID          = $("#ID").val();
    var vEmail 	   = $("#vEmail").val();
    var vMobile    = $("#vMobile").val();
    var vState     = $("#vState").val();
    var vCity      = $("#vCity").val();
    var dDate      = $("#dDate").val();
    var tTime     = $("#tTime").val();
    var vError     = false;


	if (vARNNumber.length == 0) {
        $("#vARNNumber_error").show();
        vError = true;
    } else {
        $("#vARNNumber_error").hide();
    }

    if (vEmail.length == 0) {
        $("#vEmail_error").show();
        $("#vEmail_unique_error").hide();
        $("#vEmail_valid_error").hide();
        vError = true;
    } else {
        if (validateEmail(vEmail)) {
            $("#vEmail_valid_error").hide();
        } else {
            $("#vEmail_valid_error").show();
            $("#vEmail_error").hide();
            $("#vEmail_unique_error").hide();
            vError = true;
        }
    }
    if (vMobile.length == 0) {
        $("#vMobile_error").show();
        vError = true;
    } else {
        if (vMobile.length != 10) {
            $("#vMobileDigit_error").show();
            vError = true;
        } else {
            $("#vMobile_error").hide();
            $("#vMobileDigit_error").hide();
        }
    }

    if (vState.length == 0) {
        $("#vState_error").show();
        vError = true;
    } else {
        $("#vState_error").hide();
    }
	if (vCity.length == 0) {
        $("#vCity_error").show();
        vError = true;
    } else {
        $("#vCity_error").hide();
    }

	if (dDate.length == 0) {
        $("#dDate_error").show();
        vError = true;
    } else {
        $("#dDate_error").hide();
    }
    if (tTime.length == 0) {
        $("#tTime_error").show();
        vError = true;
    } else {
        $("#tTime_error").hide();
    }

    if (vError == false) {
        $(".submit").hide();
        $(".submit_loader").show();
        setTimeout(function() {
            $("#frm").submit();
        }, 1000);
    }
});


function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}
$('#dDate').datetimepicker({
        format: 'DD/MM/YYYY',

    });
    $('#tTime').datetimepicker({
        format: 'h:m A',
                
    });
</script>