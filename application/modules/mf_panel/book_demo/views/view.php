<?php
$CI =& get_instance();
$CI->load->library('general');
?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex">
                <h5 class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex text-primary">Book Demo
                </h5>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom detail-card">
        <div class="card-body p-lg-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <input type="hidden" id="ID" name="ID" value="<?php echo $ID;?>">
                <div class="row justify-content-center py-lg-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">ARN Number</span>
                                    <span class="name-span">: <?php echo $data->vARNNumber; ?></span>
                                </label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">Email</span>
                                    <span class="name-span">: <?php echo $data->vEmail; ?></span>
                                </label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">Mobile</span>
                                    <span class="name-span">: <?php echo $data->vMobile; ?></span>
                                </label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">State</span>
                                    <span class="name-span">: <?php echo $data->vState; ?></span>
                                </label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">City</span>
                                    <span class="name-span">: <?php echo $data->vCity; ?></span>
                                </label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">Date</span>
                                    <span class="name-span">:
                                        <?php echo$this->general->date_format2($data->dDate); ?></span>
                                </label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">Time</span>
                                    <span class="name-span">:
                                        <?php echo$this->general->show_time($data->tTime); ?></span>
                                </label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <label class="contact-info">
                                    <span class="name-lable">Status</span>
                                    <span class="name-span">:

                                        <?php if($data->eStatus == "Complete") { ?>
                                        <span class="name-span label label-lg label-light-success label-inline">:
                                            <?php echo $data->eStatus; ?></span>
                                        <?php } ?>
                                        <?php if($data->eStatus == "Pending") { ?>
                                        <span
                                            class="name-span label label-lg label-light-danger label-inline"><?php echo $data->eStatus; ?></span>
                                        <?php } ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center pb-lg-8 px-lg-8">
                    <div class="col-md-12">
                        <button type="button"
                            class="btn btn-danger font-weight-bolder text-uppercase cancel">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(!empty($data_email)) { ?>

<div class="card-spacer mb-3" id="kt_inbox_reply">

    <div class="card card-custom shadow-sm">
        <div class="card-body p-0">
            <div class="col-md-12 col-xl-12 pt-4 px-4 ">
                <h4 class=" font-weight-bold text-primary">
                    <strong>Previous Email Chat</strong>
                </h4>
            </div>
            <input type="hidden" name="iBookDemoId" id="iBookDemoId" value="<?php echo $data->ID;?>">
            <div class="d-block">
                <div class="border-bottom p-4 scroll-block" >
                    <?php foreach ($data_email as $key => $value) { ?>
                    <div class="recent-email">
                        <p><?php echo $value->vComment;?></p>
                        <p class="date_email text-right">
                            <span class=" label label-light-primary label-inline"><?php echo $CI->general->date_format2($value->dtDateTime);?></span></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="card-spacer mb-3" id="kt_inbox_reply">
    <div class="card card-custom shadow-sm">
        <div class="card-body p-0">
            <!--begin::Form-->
            <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm1" id="kt_inbox_reply_form"
                method="post" action="<?php echo base_url('mf_panel/book_demo/send_reply');?>"
                enctype="multipart/form-data">
                <input type="hidden" name="vARNNumber" id="vARNNumber" value="<?php echo $data->vARNNumber;?>">
                <input type="hidden" name="iBookDemoId" id="iBookDemoId" value="<?php echo $data->ID;?>">

                <!--begin::Body-->
                <div class="d-block">
                    <!--begin::To-->
                    <div class="d-flex align-items-center border-bottom inbox-to px-8 min-h-50px">
                        <div class="text-dark-50 w-75px">To:</div>
                        <div class="d-flex align-items-center flex-grow-1">
                            <input type="text" class="form-control border-0" id="vEmail" name="vEmail"
                                value="<?php echo $data->vEmail;?>" style="text-bold">
                        </div>
                    </div>

                    <div class="border-bottom">
                        <input type="text" class="form-control border-0 px-8 min-h-45px" id="vSubject" name="vSubject"
                            placeholder="Subject">
                    </div>
                    <div id="kt_inbox_reply_editor" class="border-0 ql-container ql-snow" style="height: 250px">
                        <div class="ql-clipboard" contenteditable="true" tabindex="-1"></div>
                        <textarea class="form-control border-0 px-8 min-h-45px" id="vComment" rows="11" name="vComment"
                            placeholder="Type a message"></textarea>
                        <div class="text-danger px-8" id="vComment_error" style="display: none;">Enter Message</div>

                    </div>

                </div>
                <div class="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top">
                    <div class="d-flex align-items-center mr-3">
                        <div class="btn-group mr-4">
                            <div class="col-md-12">
                                <button type="button"
                                    class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase chat_send"
                                    data-id="<?php echo $data->ID;?>">Submit</button>
                                <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader"
                                    type="button" disabled style="display: none;">
                                    <span class="spinner-border spinner-border-sm" role="status"
                                        aria-hidden="true"></span>
                                    Loading...
                                </button>
                                &nbsp;
                            </div>

                        </div>

                    </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).on('click', '.cancel', function() {
    window.location = "<?php echo base_url('mf_panel/book_demo/book_demo'); ?>";
});


$(document).on('click', '.chat_send', function() {
    ID = $(this).data("id");
    var vComment = $("#vComment").val();
    var vSubject = $("#vSubject").val();
    var vEmail = $("#vEmail").val();
    var vARNNumber = $("#vARNNumber").val();
    var iBookDemoId = $("#iBookDemoId").val();
    $("#vComment_error").hide();


    if (vComment != '') {
        $("#chat").hide();
        $(".chat_send").hide();
        $(".submit_loader").show();


        url = "<?php echo base_url('mf_panel/book_demo/send_reply');?>";
        setTimeout(function() {
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    ID: ID,
                    vComment: vComment,
                    vSubject: vSubject,
                    vEmail: vEmail,
                    vARNNumber: vARNNumber,
                    iBookDemoId: iBookDemoId
                },
                success: function(response) {
                    // location.reload();
                    notification_success("Message Sent Successfully");
                    setTimeout(function() {
                        location.href =
                            "<?php echo base_url('mf_panel/book_demo/book_demo/view'); ?>" +
                            "/" + ID;
                    }, 1000);
                }
            });
        }, 1000);

    } else {
        $("#vComment_error").show();
    }

});


// $('textarea').keyup(function() {
//         var maxLength = 2;
//         var length = $(this).val().length;
//         var length = maxLength-length;
//         $('#chars').text(length);

//     });
</script>