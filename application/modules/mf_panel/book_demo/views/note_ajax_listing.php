<?php
$CI =& get_instance();
$CI->load->library('general');
?>

<?php if(!empty($data)) { ?>
<?php foreach ($data as $key => $value) { ?>
<tr style='<?php if($value->eStatus == "Inactive") { echo "background-color: #f5bcc138"; } ?>' class="mob-tr">

    <td class="text-center">
        <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->vARNNumber; ?></span>
    </td>
    <td class="text-left">
        <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $value->tNote; ?></span>
    </td>

    <td class="text-center">
                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo $CI->general->date_format2($value->dtDateTime);  ?></span>
            </td>
    <td class="text-center white-space-nowrap">
        <button class="btn btn-icon btn-primary btn-sm mr-1 edit_note" data-filter= "<?php echo $value->iBookDemoId;?>" data-id="<?php echo $value->iBookDemoNoteId;?>"
            data-toggle="tooltip" title="Edit ">
            <span class="fa fa-pencil"></span>
        </button>
        <button class="btn btn-icon btn-light-primary btn-sm mr-1 view_note"  data-filter = "<?php echo $value->iBookDemoId;?>" data-id="<?php echo $value->iBookDemoNoteId;?>">
        <span class="fa fa-eye"></span>
        </button>
        <button class="btn btn-icon btn-light-danger btn-sm mr-1 delete_note"  data-filter = "<?php echo $value->iBookDemoId;?>" data-id="<?php echo $value->iBookDemoNoteId;?>">
        <span class="fa fa-trash"></span>
        </button>
    </td>

</tr>
<?php } ?>

<tr>
    <td colspan="9"></td>
</tr>
<tr>
    <td colspan="9" style="background-color: #eef0f8;" class="pad-0">
        <div class="">
            <?php echo $paging; ?>
        </div>
    </td>
</tr>
<?php } else { ?>
<tr align="center">
    <td class="text-dark-75 font-weight-bolder font-size-lg" colspan="9">No Records</td>
</tr>
<?php } ?>