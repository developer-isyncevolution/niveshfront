<div class="content d-flex flex-column flex-column-fluid" id="kt_content"> 
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex">
                <h5 class="d-lg-flex align-items-center flex-wrap mr-2 d-md-flex d-flex text-primary">Book Demo Note</h5>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom detail-card">
        <div class="card-body p-lg-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <label class="contact-info">
                            <span class="name-lable">Name</span>  
                            <span class="name-span">: <?php echo $data->vARNNumber; ?></span> 
                        </label>
                        <label class="contact-info">
                            <span class="name-lable">Note</span>
                            <span class="name-span">: <?php echo $data->tNote; ?></span>
                        </label>
                    </div>
                </div>
                <div class="row justify-content-center pb-lg-8 px-lg-8">
                    <div class="col-md-12">
                    <button type="button" class="btn btn-danger font-weight-bolder text-uppercase iBookDemoId back_event" data-id="<?php echo $data->iBookDemoId;?>">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  
  $(document).on("click", ".back_event", function() {
    iBookDemoId = $(this).data("id");
    window.location = "<?php echo base_url('mf_panel/book_demo/note_listing'); ?>"+"/"+iBookDemoId;
});
</script>