<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                
            <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary">
            <?php if(!empty($ID)) {  echo "<h5 class='font-weight-bold mt-2 mb-2 mr-5 text-primary'>Add Book Demo Note</h5>" ;} 
            else
            {
                echo "<h5 class='font-weight-bold mt-2 mb-2 mr-5 text-primary'>Edit  Book Demo Note</h5>" ;
            } ?>
                
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 py-md-6 px-md-5 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post"
                            action="<?php echo base_url('mf_panel/book_demo/add_note');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iBookDemoId" name="iBookDemoId" value="<?php if(!empty($ID)) { echo $ID; } else { echo  $data->iBookDemoId; }?>">
                            <input type="hidden" id="iBookDemoNoteId" name="iBookDemoNoteId" value="<?php echo $data->iBookDemoNoteId;?>">

                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <div class="row">
                        
                                <div class="col-xl-12 col-md-12">
                                        <div class="form-group">
                                            <label>Note</label>
                                            <textarea id="tNote" class="form-control" name="tNote" rows="4" placeholder = "Enter Note" cols="40"><?php echo $data->tNote;?></textarea>
                                            <div class="text-danger" id="tNote_error" style="display: none;">Enter Note</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button"
                                            class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                        <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader"
                                            type="button" disabled style="display: none;">
                                            <span class="spinner-border spinner-border-sm" role="status"
                                                aria-hidden="true"></span>
                                            Loading...
                                        </button>
                                        &nbsp;
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<script type="text/javascript">
$(document).on('click', '.cancel', function() {
    window.location = "<?php echo base_url('mf_panel/book_demo/book_demo'); ?>";
});

$(document).on('click', '.submit', function() {
    var iBookDemoNoteId      = $("#iBookDemoNoteId").val();
    var iBookDemoId          = $("#iBookDemoId").val();
    var tNote                = $("#tNote").val();
    var vError                = false;


	if (tNote.length == 0) {
        $("#tNote_error").show();
        vError = true;
    } else {
        $("#tNote_error").hide();
    }
    if (vError == false) {
        $(".submit").hide();
        $(".submit_loader").show();
        setTimeout(function() {
            $("#frm").submit();
        }, 1000);
    }
});

$('#dDate').datetimepicker({
        format: 'DD/MM/YYYY',

});

$('#tTime').datetimepicker({
    format: 'h:m A',
            
});
</script>