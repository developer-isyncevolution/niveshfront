<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_demo extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('book_demo_model');
        $this->load->model('book_demo_email/book_demo_email_model');
        $this->load->library('general');
        $this->load->model('setting/setting_model');
        $this->load->model('book_demo_note/book_demo_note_model');
        $this->load->model('arn_email_credential/arn_email_credentials_model');
        $this->load->model('system_email/system_email_model');
    }

    public function index()
    {
        $this->template->build('listing', Null);
    }

    public function ajax_listing()
    {
        $vAction        = $this->input->post('vAction');
        $ID      = $this->input->post('ID');
        $vKeyword       = $this->input->post('vKeyword');
        $eStatus        = $this->input->post('eStatus');
        $eDeleted       = $this->input->post('eDeleted');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');
        $iClientIdArray     = $this->input->post('iClientIdArray');


        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "ID";
            $order      = "DESC";
        }

        if($vAction == "delete" && !empty($ID))
        {
            $ID = $this->input->post('ID');
			$this->book_demo_model->delete($ID);
        }
        if($vAction == "client_status" & !empty($iClientIdArray))
        {
            foreach ($iClientIdArray as $key => $value) 
            {
                $myArray                = array();
                $myArray["ID"]     = $value;
                $check                  = $this->book_demo_model->get_by_id($myArray);

                if($check->eStatus == "Active")
                {
                    $where               = array();
                    $where["ID"]    = $value;

                    $mydata              = array();
                    $mydata["eStatus"]   = "Inactive";

                    $this->book_demo_model->update($where, $mydata);
                }
                else
                {
                    $where              = array();
                    $where["ID"]   = $value;

                    $mydata             = array();
                    $mydata["eStatus"]  = "Active";

                    $this->book_demo_model->update($where, $mydata);   
                }
            }
        } 
    

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(!empty($eStatus)) {
            $criteria['eStatus']  = $eStatus;
        }       
         $criteria['vKeyword']   = $vKeyword;

        $data                   = $this->book_demo_model->get_all_data($criteria);

        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->book_demo_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('ajax_listing', $data);
    }

    public function add()
    {

        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $ID                  = $this->input->post('ID');

            $data                   = array();
            $data['vEmail']         = strtolower($this->input->post('vEmail'));
            $data['vARNNumber']     = "ARN-".$this->input->post('vARNNumber');
            $data['vMobile']        = $this->input->post('vMobile');
            $data['vState']         = ucwords($this->input->post('vState'));
            $data['vCity']          = ucwords($this->input->post('vCity'));
            $data['dDate']          = $this->general->save_date_format($this->input->post('dDate'));
            $data['tTime']          = date("H:i:s",strtotime($this->input->post('tTime')));
            $data['eStatus']          = $this->input->post('eStatus');
            
            // $data['dtDateTime'] 	= date("Y-m-d h:i:s");
            // $data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");

            if(!empty($ID))
            {
                $where = array("ID" => $ID);
                $this->book_demo_model->update($where, $data);

                $this->session->set_flashdata('success', "Booking Demo Updated Successfully." );
                redirect(base_url('mf_panel/book_demo/book_demo'));
            }
            else {
                $this->book_demo_model->add($data);
                $this->session->set_flashdata('success', "Your Booking Demo Request Send Successfully." );
                redirect(base_url());
            }
            
        }
    }


    public function edit($ID ) 
	{
        if(!empty($ID))
        {
        $criteria                   = array();
        $criteria["ID"]  = $ID;

        $data['data']               = $this->book_demo_model->get_by_id($criteria);
        $data['ID']      = $ID;
       $this->template->build('add',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/book_demo/add'));
        }
    }

    public function view($ID) 
	{

        if(!empty($ID))
        {
            $criteria               = array();
            $criteria["ID"]         = $ID;
            $data['data']           = $this->book_demo_model->get_by_id($criteria);
            $data['ID']             = $ID;

            $criteria               = array();
            $criteria["iBookDemoId"]= $ID;
            $data['data_email']     = $this->book_demo_email_model->get_all_data($criteria);
            $this->template->build('view',$data);
        }
        else
        {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/notification/add'));    
        }
    }


    public function send_reply()
    {

        if($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $base_path                  = $this->config->item("base_path");
            $ID                         = $this->input->post('ID');
            $vARNNumber                 = $this->input->post('vARNNumber');

            $data['vSubject']           = $this->input->post('vSubject');
            $data['vComment']           = $this->input->post('vComment');
            $data['vEmail']             = $this->input->post('vEmail');
            $data['iBookDemoId']             = $this->input->post('iBookDemoId');
			$data['dtDateTime'] 	    = date("Y-m-d h:i:s");

            $ID = $this->book_demo_email_model->add($data);
           
            $criteria                            = array();
            $criteria["vCode"]                   = "DEMO_BOOKING";
            $email                                  = $this->system_email_model->get_by_id($criteria);

            if(!empty($email)){

                $constant                   = array('#vARNNumber#','#tMessage#');
                $value                      = array($vARNNumber, $data['vComment']);
                $message                    = str_replace($constant, $value, $email->tMessage);
                $email_data['to']           = $data['vEmail'];
                $email_data['subject']      =  $data['vSubject'];
                $email_data['message']      = $message;
                $criteria                   = array();
                $criteria['eType']          = 'EMAIL';
                $criteria['vData']          = $email_data;
                $criteria['form_type']      = "Niveshlife";
                $criteria['from_email']     = "info@niveshlife.com";
                $this->general->send_notifiction($criteria);
            }
        }

    }


    public function note_listing($ID) 
	{

        if(!empty($ID))
        {
        $criteria               = array();
        $criteria["ID"]         = $ID;
        $data['data']           = $this->book_demo_model->get_by_id($criteria);
        $data['ID']             = $ID;
        $this->template->build('note_listing',$data);
        }
        else {
            $this->session->set_flashdata('error', "Something Went Wrong." );
            redirect(base_url('mf_panel/notification/add'));
        }
    }

    public function note_ajax_listing($iBookDemoId = NULL)
    {
        $vAction          = $this->input->post('vAction');
        $iBookDemoNoteId  = $this->input->post('iBookDemoNoteId');
        $iBookDemoId      = $this->input->post('iBookDemoId');
        $vKeyword       = $this->input->post('vKeyword');
        $vPage          = $this->input->post('vPage');
        $vSort          = $this->input->post('vSort');
        $iClientIdArray     = $this->input->post('iClientIdArray');


        if($vAction == "sort" && !empty($vSort)) 
        {
            list($vColumn, $vOrder) = explode('-', $vSort);
            $column     = $vColumn;
            $order      = $vOrder;
        } else {
            $column     = "iBookDemoNoteId";
            $order      = "DESC";
        }

        if($vAction == "delete" && !empty($iBookDemoNoteId))
        {
            $iBookDemoNoteId = $this->input->post('iBookDemoNoteId');
			$this->book_demo_note_model->delete($iBookDemoNoteId);
        }
     
    

        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(!empty($eStatus)) {
            $criteria['eStatus']  = $eStatus;
        }       
         $criteria['vKeyword']   = $vKeyword;
         $criteria['iBookDemoId']   = $iBookDemoId;
        $data                   = $this->book_demo_note_model->get_all_data($criteria);
        $data['count']          = count($data);
        $pages                  = 1;
        if(!empty($vPage)) 
        {
            $pages = $vPage;
        }
        $paginator                  = new Pagination($pages);
        $paginator->currentPage;
        $paginator->total           = count($data);
        $criteria['start']          = ($paginator->currentPage -1) * $paginator->itemsPerPage;
        $criteria['limit']          = $paginator->itemsPerPage;
        $paginator->is_ajax         = true;
        $criteria['paging']         = true;
        $data['data']               = $this->book_demo_note_model->get_all_data($criteria);
        $data['paging']             = $paginator->paginate();
        $data['pages']              = $pages;

        if($criteria['start'] == 0) {
            $data['to']             = 1;
        } else {
            $data['to']             = $criteria['start']+1;
        }

        if($criteria['start'] + $criteria['limit'] >= $data['count']) {
            $data['from']           = $data['count'];
        } else {
            $data['from']           = $criteria['start'] + $criteria['limit'];
        }
        
        $this->load->view('note_ajax_listing', $data);
    }

    public function add_note($iBookDemoId = NULL)
    {
        
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $base_path                  = $this->config->item("base_path");
            $iBookDemoNoteId            = $this->input->post('iBookDemoNoteId');

            $data['iBookDemoId']        = $this->input->post('iBookDemoId');
            $data['tNote']              = $this->input->post('tNote');
			$data['dtDateTime'] 	    = date("Y-m-d h:i:s");

            if(!empty($iBookDemoNoteId))
            {

                $where = array("iBookDemoNoteId" => $iBookDemoNoteId);
                $this->book_demo_note_model->update($where, $data);

                $this->session->set_flashdata('success', "Book Demo Note Updated Successfully." );
                redirect(base_url('mf_panel/book_demo/note_listing/'.$data['iBookDemoId']));
            }
            else
            {
                 $this->book_demo_note_model->add($data);

                $this->session->set_flashdata('success', "Book Demo Note Added Successfully." );
                redirect(base_url('mf_panel/book_demo/note_listing/'.$data['iBookDemoId']));
            }
        }
        else
        {
            $criteria               = array();
            $criteria["ID"]         = $iBookDemoId;
            $datas['data']           = $this->book_demo_model->get_by_id($criteria);
            $datas['ID']             = $iBookDemoId;
            $this->template->build('add_note', $datas);
        }
    }


    public function edit_note($iBookDemoId = NULL , $iBookDemoNoteId = NULL) 
	{

        if(!empty($iBookDemoNoteId))
        {
            $criteria                            = array();
            $criteria["iBookDemoNoteId"]         = $iBookDemoNoteId;
            $criteria["iBookDemoId"]             = $iBookDemoId;
            $data['data']                        = $this->book_demo_note_model->get_by_id($criteria);	
            $this->template->build('add_note',$data);
        }
        else 
        {   
            $this->session->set_flashdata('error','Something Went Wrong');
            redirect(base_url('mf_panel/add_note/add'));
        }
	}

    public function view_note($iBookDemoId = NULL,$iBookDemoNoteId = NULL ) 
	{
        $criteria                            = array();
        $criteria["iBookDemoNoteId"]         = $iBookDemoNoteId;
        $criteria["iBookDemoId"]             = $iBookDemoId;
        $data['data']                        = $this->book_demo_note_model->get_by_id($criteria);
        $this->template->build('view_note',$data);

    }

    
}