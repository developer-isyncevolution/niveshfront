<form class="form" name="frm_password" id="frm_password" method="post" action="<?php echo base_url('mf_panel/profile/password_update'); ?>" enctype="multipart/form-data">
    <input type="hidden" id="iUserId" name="iUserId" value="<?php echo $data_admin->iUserId; ?>">
    <div class="mt-2">
        <div class="card card-custom card-stretch">
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Change Password</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">Change your Login Password</span>
            </div>
            <div class="card-toolbar">
                <button type="button" class="btn btn-primary password_submit">Save</button>
                <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader" type="button" disabled style="display: none;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                </button>

            </div>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                <div class="col-lg-9 col-xl-9">
                    <div class="input-group input-group-lg input-group-solid">                        
                        <input class="form-control form-control-lg form-control-solid" type="password"
                        name="vPassword" id="vPassword" placeholder="Enter Password" autocomplete="off" >
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="la la-eye-slash" id="eye"></i>
                            </span>
                        </div>
                    </div> 
                    <div class="text-danger" id="vPassword_error" style="display: none; floa">Password Required</div>
                       
                </div>
            </div>
                  
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label"> Confirm Password</label>
                <div class="col-lg-9 col-xl-9">
                    <div class="input-group input-group-lg input-group-solid">                       
                        <input class="form-control form-control-lg form-control-solid" type="password"
                        name="vConfirmPassword" id="vConfirmPassword" placeholder="Enter Confirm Password" autocomplete="off" >
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="la la-eye-slash" id="eye2"></i>
                            </span>
                        </div>
                    </div>  
                        <div class="text-danger" id="vConfirmPassword_error" style="display: none;">Enter Confirm Password</div>
                        <div class="text-danger" id="vConfirmPassword_same_error" style="display: none;">Password should match</div> 
                                                    
                    </div>
                </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).on('click', '.password_submit', function() {
        var vPassword = $("#vPassword").val();
        var vConfirmPassword = $("#vConfirmPassword").val();

        var vError = false;

        if (vPassword.length == 0) {
            $("#vPassword_error").show();
            vError = true;
        } else {
            $("#vPassword_error").hide();
        }
        if (vConfirmPassword.length == 0) {
            $("#vConfirmPassword_error").show();
            vError = true;
        } else {
            $("#vConfirmPassword_error").hide();
        }

        if (vPassword.length != 0 && vConfirmPassword.length != 0) {
            if (vPassword != vConfirmPassword) {
                $("#vConfirmPassword_same_error").show();
                return false;
            } else {
                $("#vConfirmPassword_same_error").hide();
            }
        }

        setTimeout(function() {
            if (vError == false) {
                $(".password_submit").hide();
                $(".submit_loader").show();
                setTimeout(function() {
                    $("#frm_password").submit();
                }, 500);
            }
        }, 1000);

    });


    $(document).on('click', '#eye', function() {

        if ($(this).hasClass('fa-eye-slash')) {

            $(this).removeClass('fa-eye-slash');

            $(this).addClass('fa-eye');

            $('#vPassword').attr('type', 'text');

        } else {

            $(this).removeClass('fa-eye');

            $(this).addClass('fa-eye-slash');

            $('#vPassword').attr('type', 'password');
        }
    });

    $(document).on('click', '#eye2', function() {

        if ($(this).hasClass('fa-eye-slash')) {

            $(this).removeClass('fa-eye-slash');

            $(this).addClass('fa-eye');

            $('#vConfirmPassword').attr('type', 'text');

        } else {

            $(this).removeClass('fa-eye');

            $(this).addClass('fa-eye-slash');

            $('#vConfirmPassword').attr('type', 'password');
        }
    });
</script>