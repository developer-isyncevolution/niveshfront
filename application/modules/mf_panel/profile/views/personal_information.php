<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<div class="d-flex flex-column-fluid">
    <div class="container-fluid">
        <div class="row" id = "personal_info">   
            <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5 mb-5">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>    
            </div>        
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        var eDeleted       = "<?php echo $eDeleted; ?>";
        $.ajax({
            url: "<?php echo base_url('mf_panel/profile/load_profile');?>",
            type: "POST",
            data:  { eDeleted:eDeleted }, 
            success: function(response) {
                $("#personal_info").html(response);
            }
        });
    });


</script>