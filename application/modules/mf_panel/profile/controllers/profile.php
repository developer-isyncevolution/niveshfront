<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    
        $this->load->model('user/user_model');
       
        $this->load->library('pagination');
        $this->load->library('general');
        $this->load->library('pdf_library');
        $this->load->library('excel_library');
        $this->general->authentication();
    }

    public function index()
    { 

    }

    public function check_unique_email()
    {
        $criteria                       = array();
        $criteria['vEmail']             = $this->input->post('vEmail');
        $result                         = $this->user_model->check_unique_email($criteria);

        if(empty($result)) {
            echo "1";
        } else {
            echo '0';
        }
    }

    public function personal_information()
    {  
        
        $this->template->build('personal_information', NULL);
    }

    public function load_profile()
    {
        $eType                  = $this->session->userdata('eType');

      
        if($eType == 'Admin'){
            $criteria                         = array();        
            $iUserId                         = $this->session->userdata('iUserId');
            $criteria["iUserId"]             = $iUserId;
            $data['data']                   = $this->user_model->get_by_id($criteria);
            $this->load->view('niveshlife_profile', $data);
        }
       

    }

    public function admin_master_update()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $base_path                    = $this->config->item("base_path");
            $iUserId                      = $this->input->post('iUserId'); 
          
            $data['vFirstName']           = $this->input->post('vFirstName');
            $data['vLastName']            = $this->input->post('vLastName');
            $data['vEmail']               = $this->input->post('vEmail'); 
            $data["vAuthCode"]            = md5($this->input->post('vEmail'));
            // $data['vPhone']               = $this->input->post('vPhone');

            $where = array("iUserId" => $iUserId);
            $this->user_model->update($where, $data);
          
            $this->session->set_flashdata('success', "Profile Updated Successfully." );
            redirect(base_url('mf_panel/profile/personal_information'));
            
         
        }
    }

       
    public function load_change_password()
    {  
        $eType          = $this->session->userdata('eType');

        if($eType == 'SuperPartner'){
            $criteria                            = array();        
            $iSuperPartnerId                     = $this->session->userdata('iSuperPartnerId');
            $criteria["iSuperPartnerId"]         = $iSuperPartnerId;
            $data['data_super_partner']          = $this->super_partner_model->get_by_id($criteria);
        }  
        if($eType == 'ChannelPartner'){
            $criteria                           = array();       
            $iChannelPartnerId                  = $this->session->userdata('iChannelPartnerId'); 
            $criteria["iChannelPartnerId"]      = $iChannelPartnerId;
            $data['data_channel_partner']       = $this->channel_partner_model->get_by_id($criteria); 
        } 
        if($eType == 'Branch'){
            $criteria                      = array();       
            $iBranchId                     = $this->session->userdata('iBranchId'); 
            $criteria["iBranchId"]         = $iBranchId;
            $data['data_branch']           = $this->branch_model->get_by_id($criteria); 
        } 
        if($eType == 'Employee'){
            $criteria                        = array();       
            $iEmployeeId                     = $this->session->userdata('iEmployeeId'); 
            $criteria["iEmployeeId"]         = $iEmployeeId;
            $data['data_employee']           = $this->employee_model->get_by_id($criteria); 
        } 
        if($eType == 'Client'){
            $criteria                          = array();        
            $iClientId                         = $this->session->userdata('iClientId');
            $criteria["iClientId"]             = $iClientId;
            $data['data_client']               = $this->client_model->get_by_id($criteria);
        }
        if($eType == 'RelationshipManager'){
            $criteria                          = array(); 
            $iRelationshipManagerId            = $this->session->userdata('iRelationshipManagerId');
            $criteria["iRelationshipManagerId"]= $iRelationshipManagerId;
            $data['data_rm']                   = $this->relationship_manager_model->get_by_id($criteria);
        }

        if($eType == 'Admin'){
            $criteria                          = array(); 
            $iUserId                          = $this->session->userdata('iUserId');
            $criteria["iUserId"]              = $iUserId;
            $data['data_admin']               = $this->user_model->get_by_id($criteria);
        }

        $this->load->view('change_password', $data);  
  
    }

    public function password_update()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {          
            $iUserId                          = $this->input->post('iUserId');           
            if(!empty($iUserId))
            {      
                $user_where                   = array();
                $user_where["eType"]          = "Admin";
                $user_where["iUserId"]        = $iUserId;

                $user_data                    = array();
                $user_data['vPassword']       = md5($this->input->post('vPassword'));
                $user_data['vCommonPassword'] = $this->input->post('vPassword');
                
                $this->user_model->update($user_where, $user_data);
                
                $this->session->set_flashdata('success', "Password Updated Successfully." );
                redirect(base_url('mf_panel/profile/personal_information'));
            }
        }   
    }
    
}