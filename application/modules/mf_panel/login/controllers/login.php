<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MX_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('user/user_model');
        
        $this->load->model('admin_master/admin_master_model');
       
        $this->load->model('system_email/system_email_model');
        $this->template->set_layout('mf_panel/login.php');
        $this->load->library('General');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function index()
    {
        if(!empty($this->session->userdata('iUserId'))) 
        {
            redirect(base_url('mf_panel/dashboard'));
        }
        else
        {
            $this->template->build('login', NULL);
        }
    }

    public function login_action()
    {
        $vUserName      = $this->input->post('vUserName');
        $vPassword      = md5($this->input->post('vPassword'));

        if(!empty($vUserName) && !empty($vPassword))
        {
            $criteria               = array();
            $criteria["vUserName"]  = $vUserName;
            $criteria["vPassword"]  = $vPassword;

            $data                   = $this->user_model->get_by_id($criteria);

            if(!empty($data))
            {
                if($data->eStatus == "Active")
                {
                    //Login Log
                    $login_where                = array();
                    $login_where["iUserId"]     = $data->iUserId;
                    $login_data                 = array();
                    $login_data["dtLastLogin"]  = date("Y-m-d h:i:s");
                    $login_data["vLoginCount"]  = $data->vLoginCount + 1;
                    $this->user_model->update($login_where, $login_data);
                    //Login Log

                    $session_data['iUserId']    = $data->iUserId;
                    $session_data['vUserName']  = $data->vUserName;
                    $session_data['vFirstName'] = $data->vFirstName;
                    $session_data['vLastName']  = $data->vLastName;
                    $session_data['vEmail']     = $data->vEmail;
                    $session_data['eType']      = $data->eType;
                    $session_data['iRoleId']    = $data->iRoleId;

                    if($data->eType == "Admin")
                    {
                        $criteria               = array();
                        $criteria["iUserId"]    = $data->iUserId;
                        $admin_data             = $this->user_model->get_by_id($criteria);                        

                        if(!empty($admin_data))
                        {
                            $session_data['vCode']      = $admin_data->vCode;
                            $session_data['vFullName']  = $admin_data->vFirstName.' '.$admin_data->vLastName;
                            $session_data['iUserId']   = $admin_data->iUserId;
                        }
                    }
                   
                    $this->session->set_userdata($session_data);
                    $this->session->set_flashdata('success', "Login Successfully." );
                    redirect(base_url('mf_panel/dashboard'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Account is '.$data->eStatus);
                    redirect(base_url('mf_panel/login'));
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'No Account Found.');
                redirect(base_url('mf_panel/login'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Missing Credentials.');
            redirect(base_url('mf_panel/login'));
        }
    }

    // public function logout()
    // {
    //     $this->session->sess_destroy();
    //     $this->session->set_flashdata('success', "Logout Successfully.");
    //     redirect(base_url('mf_panel/login'));
    // }

    public function logout()
    {
        $this->session->set_flashdata('success', "Logout Successfully.");
        //$this->session->unset_userdata('loginaction');
        $this->session->sess_destroy();
         redirect(base_url('mf_panel/login')); 
    }

    public function forgot_password()
    {
        $this->template->build('forgot_password', NULL);
    }
    
    public function forgot_password_action()
    {
        $criteria                                   = array();
        $criteria["vCode"]                          = "FORGOT_PASSWORD";
        $email                                      = $this->system_email_model->get_by_id($criteria);

        
            if(!empty($email)){
                        $vEmail                     = $this->input->post('vEmail');
                        $vLink                      = base_url('mf_panel/login/reset_password/'.base64_encode($vEmail));;
                        $constant                   = array('#CODE#','#EMAIL#','#vLink#');
                        $value                      = array($criteria["vCode"], $vEmail, $vLink);
                        $message                    = str_replace($constant, $value, $email->tMessage);
                        $email_data['to']           = $vEmail;
                        $email_data['subject']      = $email->vSubject;
                        $email_data['message']      = $message;
                        $criteria                   = array();
                        $criteria['eType']          = 'EMAIL';
                        $criteria['vData']          = $email_data;

                        $this->general->send_notifiction($criteria);
                        $this->session->set_flashdata('success', 'Please Check Your Mail.');
                        redirect(base_url('mf_panel/login'));
            
            }

            
    }

    public function check_email()
    {
        $vEmail         = $this->input->post('vEmail');
        $data           = $this->user_model->get_by_email($vEmail);
            if($data == true) {
                echo "1";
            } else {
                echo '0';
            }
    }

    public function reset_password($vEmail = NULL)
    {
       if(!empty($vEmail))
       {
        $criteria                         = array();
        $criteria["vEmail"]               = base64_decode($vEmail);
        
        $data_authcode  	               = $this->user_model->get_by_id($criteria);
        $data['vAuthCode']                 = $data_authcode->vAuthCode;

        $this->template->build('reset_password', $data);  
       } 
       else {
        $this->session->set_flashdata('error', "Something Went Wrong." );
        redirect(base_url('mf_panel/login/forgot_password'));
       } 
    }

    public function reset_password_action()
    {       
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $vAuthCode                = $this->input->post('vAuthCode');

            $data                     = array();
            $data['vPassword']        = md5($this->input->post('vPassword'));
            $data['vCommonPassword']  = $this->input->post('vPassword');

            if(!empty($vAuthCode))
            {
                $criteria                   = array();
                $criteria['vAuthCode']      = $vAuthCode;
                $data_authcode['vAuthCode'] = $this->user_model->get_by_id($criteria);

                $where = array("vAuthCode" => $data_authcode['vAuthCode']->vAuthCode);                
                $this->user_model->update($where, $data);

             
                
                $this->session->set_flashdata('success', "Password Reset Successfully." );
                redirect(base_url('mf_panel/login/login'));
            }
            
            else
            {            
                $this->session->set_flashdata('error', "Error While Reseting Password Please try again." );
                redirect(base_url('mf_panel/login/reset_password'));
            }            

        } 
        
    }

}