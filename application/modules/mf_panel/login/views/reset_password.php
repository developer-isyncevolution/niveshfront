<!-- <div class="login login-3 wizard d-flex flex-column flex-lg-row flex-column-fluid">
	<div class="login-aside d-flex flex-column flex-row-auto">
      <div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
        <a href="#" class="login-logo text-center pt-lg-25 pb-10">
          <img src="<?php echo base_url('assets/mf_panel/images/login-logo.png')?>" class="max-h-70px" alt="" />
        </a>
        <h3 class="font-weight-bolder text-center font-size-h4 text-dark-50 line-height-xl">User Experience &amp; Interface Design
        <br />Strategy SaaS Solutions</h3>
      </div>
      <div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-x-center" style="background-position-y: calc(100% + 5rem); background-image: url(<?php echo base_url('assets/mf_panel/media/svg/illustrations/login-visual-5.svg')?>)">
      </div>
	</div>
	<div class="login-content flex-row-fluid d-flex flex-column p-10">
		<div class="text-right d-flex justify-content-center">
			<div class="top-signin text-right d-flex justify-content-end pt-5 pb-lg-0 pb-10">
				<span class="font-weight-bold text-muted font-size-h4">Having issues?</span>
				<a href="javascript:;" class="font-weight-bold text-primary font-size-h4 ml-2" id="kt_login_signup">Get Help</a>
			</div>
		</div>
		<div class="d-flex flex-row-fluid flex-center">
			<div class="login-form">
				<form role="form" id="reset_form" method="post" action="<?php echo base_url('mf_panel/login/reset_password_action/');?>">
			  	<input type="hidden" id="vAuthCode" name="vAuthCode" value="<?php echo $vAuthCode;?>">
          <div class="pb-5 pb-lg-15">
						<h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Reset Password</h3>
					</div>
          <div class="form-group">
						<div class="d-flex justify-content-between mt-n5">
							<label class="font-size-h6 font-weight-bolder text-dark pt-3">Password</label>
						</div>
						<div class="input-group xl-3">
							<input class="form-control h-auto py-7 px-6  border-0" type="password" name="vPassword" id="vPassword" autocomplete="off" />
							<div class="input-group-append">
									<div class="input-group-text" style="background: #afaaaa1f !important;border: none !important;border-radius: 0px 11px 11px 1px; !important; padding: 0px 40px 0px 40px;"><i class="fas fa-eye-slash" id="eye"></i></div>
						  </div> 				
        	  </div>
            <div class="text-danger" id="vPassword_error" style="display: none;">Password Required</div>
            <br>              
            <div class="form-group">
              <div class="d-flex justify-content-between mt-n5">
                <label class="font-size-h6 font-weight-bolder text-dark pt-3">Confirm Password</label>
              </div>
              <div class="input-group xl-3">
                <input class="form-control h-auto py-7 px-6  border-0" type="password" name="vConfirmPassword" id="vConfirmPassword" autocomplete="off" />
                <div class="input-group-append">
                    <div class="input-group-text" style="background: #afaaaa1f !important;border: none !important;border-radius: 0px 11px 11px 1px; !important; padding: 0px 40px 0px 40px;"><i class="fas fa-eye-slash" id="eye2"></i></div>
                </div>                 
            </div>
            <div class="text-danger" id="vConfirmPassword_error" style="display: none;">Enter Confirm Password</div>
            <div class="text-danger" id="vConfirmPassword_same_error" style="display: none;">Password should match</div>
            <a href="<?php echo base_url('mf_panel/login/login'); ?>" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" style="float:right;">Login ?</a>
            <div class="pb-lg-0 pb-5">
              <button class="btn btn-primary font-weight-bolder text-uppercase next_loader" type="button" disabled style="display: none;">
                  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                  Loading...
              </button>
              <button type="button" id="kt_login_singin_form_submit_button" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3 submit">Submit</button>
            </div>
          </div>
				</form>
			</div>
		</div>
	</div>
</div> -->
<div class="nivesh-bg-img">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-12 login-nivesh nivesh-login-logo">
				<img src="<?php echo base_url('assets/mf_panel/images/login-logo.png')?>" class="img-fluid" alt="" />
				<p class="nivesh-login-title">Rich experience in Mutual Fund domain & provides complete IT solution for financial Industry</p>
			</div>
			<div class="col-lg-4 col-md-4 col-12 login-nivesh sing-side">
				<form role="form" id="reset_form" method="post" action="<?php echo base_url('mf_panel/login/reset_password_action/');?>">
			  	<input type="hidden" id="vAuthCode" name="vAuthCode" value="<?php echo $vAuthCode;?>">
					<div class="pb-3 pb-lg-15">
						<h3 class="nivesh-login">Reset Password</h3>
					</div>
					<div class="form-group">
						<input class="form-control h-auto py-5 px-5" type="password" name="vPassword" id="vPassword" autocomplete="off" placeholder="Password" />
            <div class="text-danger" id="vPassword_error" style="display: none;">Password Required</div>

					</div>
					<div class="form-group">
							<input class="form-control h-auto py-5 px-5" type="password" name="vConfirmPassword" id="vConfirmPassword" autocomplete="off" placeholder="Confirm Password" />
							<div class="text-danger" id="vConfirmPassword_error" style="display: none;">Enter Confirm Password</div>
            	<div class="text-danger" id="vConfirmPassword_same_error" style="display: none;">Password should match</div>
					</div>
					<div class="form-group">
						<div class="pb-lg-5 pb-3 pt-lg-5">
						<button class="btn nivesh-login-btn w-100 px-5 py-4 my-2 mr-3 next_loader" type="button" disabled style="display: none;">
							<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
							Loading...
						</button>
							<a id="kt_login_singin_form_submit_button" class="btn nivesh-login-btn w-100 px-5 py-4 my-2 mr-3 submit">Submit</a>
						</div>
					 	
						<div class="pb-lg-0 pb-5">
							<div class="pb-lg-5 pb-2 pt-lg-3">
									<a href="<?php echo base_url('mf_panel/login/login'); ?>" class="font-weight-bolder nivesh-forget-pass">Login ?</a>
								</div>
							</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

$(document).on('click', '.submit', function () {
        var vPassword                     = $("#vPassword").val();
        var vConfirmPassword              = $("#vConfirmPassword").val();

        var vError              = false;

          if(vPassword.length == 0){
              $("#vPassword_error").show();
              vError = true;
          } else {
              $("#vPassword_error").hide();
          }
          if(vConfirmPassword.length == 0){
              $("#vConfirmPassword_error").show();
              vError = true;
          } else {
              $("#vConfirmPassword_error").hide();
          }

          if(vPassword.length != 0 && vConfirmPassword.length != 0)
          {
            if(vPassword != vConfirmPassword){
            $("#vConfirmPassword_same_error").show();
            return false;
            } else{
              $("#vConfirmPassword_same_error").hide();
            } 
          }
        
        setTimeout(function(){
          if(vError == false){
            $(".submit").hide();
            $(".next_loader").show();
            setTimeout(function(){
              $("#reset_form").submit();
            }, 500);
          } 
        }, 1000);

    });


    $(document).on('click', '#eye', function () {
			
			if($(this).hasClass('fa-eye-slash')){
			
			$(this).removeClass('fa-eye-slash');
			
			$(this).addClass('fa-eye');
			
			$('#vPassword').attr('type','text');
				
			}else{
			
			$(this).removeClass('fa-eye');
			
			$(this).addClass('fa-eye-slash');  
			
			$('#vPassword').attr('type','password');
			}
	});

    $(document).on('click', '#eye2', function () {
			
			if($(this).hasClass('fa-eye-slash')){
			
			$(this).removeClass('fa-eye-slash');
			
			$(this).addClass('fa-eye');
			
			$('#vConfirmPassword').attr('type','text');
				
			}else{
			
			$(this).removeClass('fa-eye');
			
			$(this).addClass('fa-eye-slash');  
			
			$('#vConfirmPassword').attr('type','password');
			}
	});


</script>
