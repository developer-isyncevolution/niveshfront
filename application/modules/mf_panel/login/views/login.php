<!-- <div class="login login-3 wizard d-flex flex-column flex-lg-row flex-column-fluid">
	<div class="login-aside d-flex flex-column flex-row-auto">
		<div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
			<a href="#" class="login-logo text-center pt-lg-25 pb-10">
				<img src="<?php echo base_url('assets/mf_panel/images/login-logo.png')?>" class="max-h-70px" alt="" />
			</a>
			<h3 class="font-weight-bolder text-center font-size-h4 text-dark-50 line-height-xl">User Experience &amp; Interface Design
			<br />Strategy SaaS Solutions</h3>
		</div>
		<div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-x-center" style="background-position-y: calc(100% + 5rem); background-image: url(<?php echo base_url('assets/mf_panel/media/svg/illustrations/login-visual-5.svg')?>)"></div>
	</div>
	<div class="login-content flex-row-fluid d-flex flex-column p-10">
		<div class="text-right d-flex justify-content-center">
			<div class="top-signin text-right d-flex justify-content-end pt-5 pb-lg-0 pb-10"> -->
				<!-- <span class="font-weight-bold text-muted font-size-h4">Having issues?</span>
				<a href="javascript:;" class="font-weight-bold text-primary font-size-h4 ml-2" id="kt_login_signup">Get Help</a> -->
			<!-- </div>
		</div>
		<div class="d-flex flex-row-fluid flex-center">
			<div class="login-form">
				<form role="form" id="login_form" method="post" action="<?php echo base_url('mf_panel/login/login_action/');?>">
					<div class="pb-5 pb-lg-15">
						<h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Sign In</h3>
					</div>
					<div class="form-group">
						<label class="font-size-h6 font-weight-bolder text-dark">Username</label>
						<input class="form-control h-auto py-7 px-6 rounded-lg border-0" type="text" name="vUserName" id="vUserName" autocomplete="off" />
						<div class="text-danger" id="vUserName_Error" style="display: none;">Username Required</div>
					</div>
					<div class="form-group">
						<div class="d-flex justify-content-between mt-n5">
							<label class="font-size-h6 font-weight-bolder text-dark pt-3">Password</label>
						</div>
						<div class="input-group xl-3">
							<input class="form-control h-auto py-7 px-6  border-0" type="password" name="vPassword" id="vPassword" autocomplete="off" />
							<div class="input-group-append">
									<div class="input-group-text" style="background: #afaaaa1f !important;border: none !important;border-radius: 0px 11px 11px 1px; !important; padding: 0px 40px 0px 40px;"><i class="fas fa-eye-slash" id="eye"></i></div>
						     </div> 
							<div class="text-danger" id="vPassword_Error" style="display: none;">Password Required</div>
					</div>
					<div class="form-group">
						<div class="d-flex justify-content-between mt-n5">
							<label class="font-size-h6 font-weight-bolder text-dark pt-6 "></label>
							<a href="<?php echo base_url('mf_panel/login/forgot_password'); ?>" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-7">Forgot Password ?</a>
						</div>
					<div class="pb-lg-0 pb-5">
						<button type="button" id="kt_login_singin_form_submit_button" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Sign In</button> -->
						<!-- <button type="button" class="btn btn-light-primary font-weight-bolder px-8 py-4 my-3 font-size-lg">
						<span class="svg-icon svg-icon-md">
							<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
								<path d="M19.9895 10.1871C19.9895 9.36767 19.9214 8.76973 19.7742 8.14966H10.1992V11.848H15.8195C15.7062 12.7671 15.0943 14.1512 13.7346 15.0813L13.7155 15.2051L16.7429 17.4969L16.9527 17.5174C18.879 15.7789 19.9895 13.221 19.9895 10.1871Z" fill="#4285F4" />
								<path d="M10.1993 19.9313C12.9527 19.9313 15.2643 19.0454 16.9527 17.5174L13.7346 15.0813C12.8734 15.6682 11.7176 16.0779 10.1993 16.0779C7.50243 16.0779 5.21352 14.3395 4.39759 11.9366L4.27799 11.9466L1.13003 14.3273L1.08887 14.4391C2.76588 17.6945 6.21061 19.9313 10.1993 19.9313Z" fill="#34A853" />
								<path d="M4.39748 11.9366C4.18219 11.3166 4.05759 10.6521 4.05759 9.96565C4.05759 9.27909 4.18219 8.61473 4.38615 7.99466L4.38045 7.8626L1.19304 5.44366L1.08875 5.49214C0.397576 6.84305 0.000976562 8.36008 0.000976562 9.96565C0.000976562 11.5712 0.397576 13.0882 1.08875 14.4391L4.39748 11.9366Z" fill="#FBBC05" />
								<path d="M10.1993 3.85336C12.1142 3.85336 13.406 4.66168 14.1425 5.33717L17.0207 2.59107C15.253 0.985496 12.9527 0 10.1993 0C6.2106 0 2.76588 2.23672 1.08887 5.49214L4.38626 7.99466C5.21352 5.59183 7.50242 3.85336 10.1993 3.85336Z" fill="#EB4335" />
							</svg>
						</span>Sign in with Google</button> -->
					<!-- </div>
				</form>
			</div>
		</div>
	</div>
</div>
 -->

<div class="nivesh-bg-img">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-12 login-nivesh nivesh-login-logo">
				<img src="<?php echo base_url('assets/mf_panel/images/login-logo.png')?>" class="img-fluid" alt="" />
				<p class="nivesh-login-title">Rich experience in Mutual Fund domain & provides complete IT solution for financial Industry</p>
			</div>
			<div class="col-lg-4 col-md-4 col-12 login-nivesh sing-side">
				<form role="form" id="login_form" method="post" action="<?php echo base_url('mf_panel/login/login_action/');?>" class="form-place">
					<div class="pb-3 pb-lg-15">
						<h3 class="nivesh-login">Sign In</h3>
					</div>
					<div class="form-group">
						<input class="form-control h-auto py-5 px-5" type="text" name="vUserName" id="vUserName" autocomplete="off" placeholder="Username" />
						<div class="text-danger" id="vUserName_Error" style="display: none;">Username Required</div>
					</div>
					<div class="form-group">
							<input class="form-control h-auto py-5 px-5" type="password" name="vPassword" id="vPassword" autocomplete="off" placeholder="Password" />
							
							<div class="text-danger" id="vPassword_Error" style="display: none;">Password Required</div>
					</div>
					<div class="form-group">
						<div class="pb-lg-5 pb-3 pt-lg-5">
							<a id="kt_login_singin_form_submit_button" class="btn nivesh-login-btn w-100 px-5 py-4 my-2 mr-3 signin">Sign in</a>
						</div>
					 	
						<div class="pb-lg-0 pb-5">
			                <div class="pb-lg-5 pb-2 pt-lg-3">
				                <a href="<?php echo base_url('mf_panel/login/forgot_password'); ?>" class="font-weight-bolder nivesh-forget-pass">Forget password ?</a>
				            </div>
		                </div>
					</div>
				</form>
			</div>
			
		</div>
	</div>
</div>

<script type="text/javascript">

    $(document).on('click', '#kt_login_singin_form_submit_button', function () {
        var vUserName       = $("#vUserName").val();
        var vPassword       = $("#vPassword").val();
        var vError          = false;

        if(vUserName.length == 0){
            $("#vUserName_Error").show();
            vError = true;
        } else {
            $("#vUserName_Error").hide();
        }

        if(vPassword.length == 0){
            $("#vPassword_Error").show();
            vError = true;
        } else {
            $("#vPassword_Error").hide();
        }

      if(vError == false){
        setTimeout(function(){
          $("#login_form").submit();
        }, 500);
      }

    });

	
    $(document).on('click', '#eye', function () {
			
			if($(this).hasClass('fa-eye-slash')){
			
			$(this).removeClass('fa-eye-slash');
			
			$(this).addClass('fa-eye');
			
			$('#vPassword').attr('type','text');
				
			}else{
			
			$(this).removeClass('fa-eye');
			
			$(this).addClass('fa-eye-slash');  
			
			$('#vPassword').attr('type','password');
			}
	});

</script>
