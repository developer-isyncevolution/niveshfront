<!-- <div class="login login-3 wizard d-flex flex-column flex-lg-row flex-column-fluid">
	<div class="login-aside d-flex flex-column flex-row-auto">
		<div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
			<a href="#" class="login-logo text-center pt-lg-25 pb-10">
				<img src="<?php echo base_url('assets/mf_panel/images/login-logo.png')?>" class="max-h-70px" alt="" />
			</a>
			<h3 class="font-weight-bolder text-center font-size-h4 text-dark-50 line-height-xl">User Experience &amp; Interface Design
			<br />Strategy SaaS Solutions</h3>
		</div>
		<div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-x-center" style="background-position-y: calc(100% + 5rem); background-image: url(<?php echo base_url('assets/mf_panel/media/svg/illustrations/login-visual-5.svg')?>)"></div>
	</div>
	<div class="login-content flex-row-fluid d-flex flex-column p-10">
		<div class="text-center d-flex justify-content-center">
			<div class="top-signin text-center d-flex justify-content-center pt-5 pb-lg-0 pb-10">
				<span class="font-weight-bold text-muted font-size-h4">Having issues?</span>
				<a href="javascript:;" class="font-weight-bold text-primary font-size-h4 ml-2" id="kt_login_signup">Get Help</a>
			</div>
		</div>
		<div class="d-flex flex-row-fluid flex-center">
			<div class="login-form">
				<form role="form" id="login_form" method="post" action="<?php echo base_url('mf_panel/login/forgot_password_action/');?>">
				<input type="hidden" id="iUserId" name="iUserId" value="<?php echo $iUserId;?>">

					<div class="pb-5 pb-lg-15">
						<h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Forgot Password</h3>
					</div>
					<div class="form-group">
						<label class="font-size-h6 font-weight-bolder text-dark">Email</label>
						<input class="form-control h-auto py-7 px-6 rounded-lg border-0" type="email" name="vEmail" id="vEmail" autocomplete="off" />
					
						<div id="vEmail_error" class="error" style="display: none;  color: #F64E60 !important;">Please Enter Email</div>
						<div id="vEmail_valid_error" class="error" style="display: none;  color: #F64E60 !important;"> Enter Valid Email</div>
						<div id="vEmail_unique_error" class="error" style="display: none;  color: #F64E60 !important;"> Email  not  exist</div>
					
					</div>
          <a href="<?php echo base_url('mf_panel/login/login'); ?>" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" style="float:right;">Login ?</a>

					
					<div class="pb-lg-0 pb-5">
						<button type="button" id="kt_login_singin_form_submit_button" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Submit</button>

					</div>
				</form>
			</div>
		</div>
	</div>
</div> -->
<div class="nivesh-bg-img">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-7 col-12 login-nivesh">
        <img src="<?php echo base_url('assets/mf_panel/images/login-logo.png')?>" class="img-fluid" alt="" />
        <p class="nivesh-login-title">Rich experience in Mutual Fund domain & provides complete IT solution for financial Industry</p>
      </div>
      <div class="col-lg-4 col-md-4 col-12 login-nivesh sing-side">
        <form role="form" id="login_form" method="post" action="<?php echo base_url('mf_panel/login/forgot_password_action/');?>" class="form-place">
          <input type="hidden" id="iUserId" name="iUserId" value="<?php echo $iUserId;?>">
          <div class="pb-3 pb-lg-15">
            <h3 class="nivesh-login-signup">Forgot Password</h3>
          </div>
          <div class="form-group">
              <input class="form-control h-auto py-5 px-5" type="email" name="vEmail" id="vEmail" autocomplete="off" placeholder="Email I'd" />
               <div id="vEmail_error" class="error" style="display: none;  color: #F64E60 !important;">Please Enter Email</div>
							<div id="vEmail_valid_error" class="error" style="display: none;  color: #F64E60 !important;"> Enter Valid Email</div>
							<div id="vEmail_unique_error" class="error" style="display: none;  color: #F64E60 !important;"> Email  not  exist</div>
          </div>
          <div class="pb-lg-0 pb-5">
                <div class="pb-lg-5 pb-2 pt-lg-3">

                  <a href="<?php echo base_url('mf_panel/login/login'); ?>" class=" font-size-h6 font-weight-bolder pt-5 nivesh-forget-pass" style="float:right;">Login ?</a>
                  <a id="kt_login_singin_form_submit_button" class="btn forget-btn  px-8 py-4 signin">Submit</a>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

    $(document).on('click', '#kt_login_singin_form_submit_button', function () {
		vEmail            = $("#vEmail").val();
        var vError          = false;

        if(vEmail.length == 0){
          $("#vEmail_error").show();
          $("#vEmail_unique_error").hide();
          $("#vEmail_valid_error").hide();
          vError = true;
        } else
        {
              data = {vEmail:vEmail};

          if(validateEmail(vEmail))
          {
            $("#vEmail_valid_error").hide();

            $.ajax({
              url: "<?php echo base_url('mf_panel/login/login/check_email');?>",
              type: "POST",
              data:  data, 
              success: function(response) {
                if(response != 1){
                  $("#vEmail_unique_error").show();
                  $("#vEmail_error").hide();
                  vError = true;
                } else {
                  $("#vEmail_unique_error").hide();
                  $("#vEmail_error").hide();
                }
              }
            });
          } else {
            $("#vEmail_valid_error").show();
            $("#vEmail_error").hide();
            $("#vEmail_unique_error").hide();
            vError = true;
          }
        }
  

        setTimeout(function(){
          if(vError == true){
            return false;
          } else {
            $("#login_form").submit();
            return true;
          }
        }, 1000);

    });
	function validateEmail(sEmail) 
  {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
      return true;
    }
    else {
      return false;
    }
  }

</script>
