<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="font-weight-bold mt-2 mb-2 mr-5 text-primary"><?php echo ($data->iTestimonialsId) ? 'Edit ' : 'Add '; ?>Testimonials</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="card card-custom mob-card">
        <div class="card-body p-0">
            <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="first" data-wizard-clickable="true">
                <div class="row justify-content-center py-lg-12 px-md-5 py-md-6 px-lg-10">
                    <div class="col-xl-12 col-xxl-12">
                        <form class="form fv-plugins-bootstrap fv-plugins-framework" name="frm" id="frm" method="post" action="<?php echo base_url('mf_panel/testimonials/add');?>" enctype="multipart/form-data">
                            <input type="hidden" id="iTestimonialsId" name="iTestimonialsId" value="<?php echo $iTestimonialsId;?>">
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                     
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Name <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vName" name="vName" placeholder=" Enter Name" value="<?php echo $data->vName;?>">
                                            <div class="text-danger" id="vName_error" style="display: none;">Enter Name</div>
                                        </div>
                                    </div>                                 
                                  
                                    <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                            <label>Designation  <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="vDesignation" name="vDesignation" placeholder="Enter Designation" value="<?php echo $data->vDesignation;?>">
                                            <div class="text-danger" id="vDesignation_error" style="display: none;">Enter Designation</div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6  col-md-6 ">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Image </label>
                                                    <input type="file" class="form-control" name="vImage" id="vBannerPicture">        
                                                </div>
                                                <div class="col-md-6">
                                                    <?php if(!empty($data->vImage)) { ?>
                                                        <img src="<?php echo base_url('assets/uploads/testimonials/'.$iTestimonialsId.'/'.$data->vImage); ?>" width="60px" id="vBannerPicture_Preview">
                                                    <?php } else { ?>  
                                                        <img src="<?php echo base_url('assets/mf_panel/images/noimage.png'); ?>" width="60px" id="vBannerPicture_Preview">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="text-danger" id="vImage_Error" style="display: none;">Image Required</div>
                                        </div>
                                    </div>
                                     <div class="col-xl-6 col-md-6 ">
                                        <div class="form-group">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <select name="eStatus" id="eStatus" class="form-control show-tick">
                                        <option value="">Select Status</option>
                                            <option <?php echo ($data->eStatus == 'Active') ? 'selected' : '';?> value="Active">Active</option>
                                            <option <?php echo ($data->eStatus == 'Inactive') ? 'selected' : '';?> value="Inactive">Inactive</option>
                                            
                                        </select>
                                        <div class="text-danger" id="eStatus_error" style="display: none;">Select Status</div>

                                        </div>
                                    </div>  
                                    <div class="col-xl-6 col-md-6">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea id="tDescription" class="form-control" name="tDescription" rows="4" placeholder = "Enter Description" cols="40"><?php echo $data->tDescription;?></textarea>
                                            <div class="text-danger" id="tDescription_error" style="display: none;">Enter Description</div>
                                        </div>
                                    </div>                                  
                                </div>                                                              
                                <div class="row">
                                <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-shadow-hover font-weight-bolder text-uppercase submit">Submit</button>
                                <button class="btn btn-primary font-weight-bolder text-uppercase submit_loader" type="button" disabled style="display: none;">
                                  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                  Loading...
                                </button>
                                &nbsp;
                                <button type="button" class="btn btn-danger font-weight-bolder text-uppercase cancel">Cancel</button>
                                </div>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<script type="text/javascript">
  
    $(document).on('click', '.cancel', function () {
        window.location = "<?php echo base_url('mf_panel/testimonials/testimonials'); ?>";
    });

    $(document).on('click', '.submit', function () {
        var iTestimonialsId  = $("#iTestimonialsId").val();
        var vName            = $("#vName").val();
        var vDesignation     = $("#vDesignation").val();
        var eStatus          = $("#eStatus").val();
        var vError           = false;

        if(vName.length == 0){
            $("#vName_error").show();
            vError = true;
        } else {
            $("#vName_error").hide();
        }
        if(vDesignation.length == 0){
            $("#vDesignation_error").show();
            vError = true;
        } else {
            $("#vDesignation_error").hide();
        }
        if(eStatus.length == 0){
            $("#eStatus_error").show();
            vError = true;
        } else {
            $("#eStatus_error").hide();
        }       
        if(vError == false){
            $(".submit").hide();
            $(".submit_loader").show();
            setTimeout(function(){
              $("#frm").submit();
            }, 1000);
        }
    });


    $(document).on('change','#vBannerPicture',function(){
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#vBannerPicture_Preview').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
   

</script>