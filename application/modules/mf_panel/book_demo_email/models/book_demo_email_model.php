<?php
defined('BASEPATH') || exit('No direct script access allowed');

class Book_demo_email_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    public $listing_data;
    public $rec_per_page;
    public $message;

    var $table = 'book_demo_email';
    var $table_book_demo = 'book_demo';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($criteria = array())
    {
        $vARNNumber = "(SELECT j.`vARNNumber` FROM `".$this->table_book_demo."` j WHERE j.`ID` = t.`iBookDemoId` LIMIT 1) as vARNNumber";

        $this->db->select("t.*, $vARNNumber");

        $this->db->from($this->table. ' t');
       
        if(!empty($criteria['vKeyword']))
        {
            $this->db->where('t.tNote like "%'.$criteria['vKeyword'].'%"');
        }
        if(!empty($criteria['column']) || !empty($criteria['order']))
        {
            $this->db->order_by($criteria['column'],$criteria['order']);
        }
        if(!empty($criteria['iBookDemoId']))
        {
            $this->db->where('t.iBookDemoId', $criteria['iBookDemoId']);
        }
        if(!empty($criteria['limit']) || !empty($criteria['start']))
        {
            $this->db->limit($criteria['limit'], $criteria['start']);
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_id($criteria = array())
    {
        $vARNNumber = "(SELECT j.`vARNNumber` FROM `".$this->table_book_demo."` j WHERE j.`ID` = t.`iBookDemoId` LIMIT 1) as vARNNumber";

        $this->db->select("t.*, $vARNNumber");

        $this->db->from($this->table. ' t');

        if(!empty($criteria['iBookDemoNoteId']))
        {
            $this->db->where('t.iBookDemoNoteId', $criteria['iBookDemoNoteId']);
        }
         if(!empty($criteria['iBookDemoId']))
        {
            $this->db->where('t.iBookDemoId', $criteria['iBookDemoId']);
        }
        
        $query = $this->db->get();
        return $query->row();
    }

    public function add($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        
        return $this->db->affected_rows();
    }

    public function delete($iBannerId)
    {
        $this->db->where('iBannerId', $iBannerId);
        $this->db->delete($this->table);
    }

    public function check_unique_email($criteria = array())
    {
        $this->db->from($this->table);
        if(!empty($criteria['vEmail']))
        {
            $this->db->where('vEmail', $criteria['vEmail']);
        }
        if(!empty($criteria['iBannerId']))
        {
            $this->db->where("iBannerId !=",$criteria['iBannerId']);
        }
        $query=$this->db->get();
        return $query->row();
    }

    public function total_banner($criteria = array())
    {
        if(!empty($criteria['eStatus']))
        {
            $this->db->where('eStatus', $criteria['eStatus']);
        }
        if(!empty($criteria['eDeleted']))
        {
            $this->db->where('eDeleted', $criteria['eDeleted']);
        }
        $this->db->from($this->table);
        $query=$this->db->get();
        return $query->num_rows();
    }
}
