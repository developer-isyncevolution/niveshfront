<section class="page-wrap">
    <!-- main-page banner -->
    <section class="home-banner">
        <div class="container">
            <div class="home-banner-content">
                <div class="left-content">
                    <div class="banner-wrap">
                        <p class="sub-mutual-text">Mutual fund</p>
                        <h1 class="banner-title"> We are a leading Mutual fund software distributor in India</h1>
                    </div>
                    <p class="span-text">With the help of this feature-rich and user-friendly Mutual fund software,
                        Nivesh Life
                        is simplifying the responsibilities of controlling clients and distributors.</p>
                    <a rel="dofollow" href="javascript:;" class="main btn">Book a Free Demo</a>
                </div>
                <div class="right-content">
                    <div class="banner-img">
                        <img src="<?php echo base_url('assets/front/images/banner-img.png');?>" alt="banner"
                            class="img-contain" title="banner" width="600" height="673">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- management description -->
    <section class="managmet-sec-wrap  pt-130">
        <div class="container">
            <p class="sub-mutual-text">Mutual fund</p>
            <div class="row g-4">
                <div class="col-md-6 managmet-sec-left" >
                    <h2 class="common-title">

                        Commenced in 2018, today Nivesh Life is India’s fastest-growing one-stop solution for all your
                        financial
                        management service needs.
                    </h2>
                </div>
                <div class="col-md-6">
                    <div class="managmet-sec-right">
                        <p class="sub-text">We sensed the potential to develop a high-end technology platform for the
                            next-generation financial advisors and Mutual Fund distributors whose soul-focus is to add
                            value to
                            their clients’ relationships.</p>
                        <p class="sub-text"> Our motto “#YourBrand #YourPlatform” addresses all the needs of financial
                            advisors
                            and provides a platform that is completely white-labelled. With no co-branding, Nivesh Life
                            facilitates
                            financial advisors to seamlessly integrate their data into the platform hassle-free.</p>
                        <p class="sub-text"> Nivesh Life provides B2B Mutual fund Software</p>
                    </div>
                </div>
            </div>
            <div class="row mt-70 img-gallary g-2">
                <div class="col-md-3 col-6">
                    <div class="mange-sec-img">
                        <img src="<?php echo base_url('assets/front/images/sub-banne-1.png');?>" alt="manage-sec-img" title="mutual-fund"
                            height="389" width="332" title="mutual-fund" class="img-cover">
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="mange-sec-img">
                        <img src="<?php echo base_url('assets/front/images/sub-banne-2.png');?>" alt="manage-sec-img" title="mutual-fund"
                            height="389" width="332" title="mutual-fund" class="img-cover">
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="mange-sec-img">
                        <img src="<?php echo base_url('assets/front/images/sub-banne-3.png');?>" alt="manage-sec-img" title="mutual-fund"
                            height="389" width="332" title="mutual-fund" class="img-cover">
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="mange-sec-img">
                        <img src="<?php echo base_url('assets/front/images/sub-banne-4.png');?>" alt="manage-sec-img" title="mutual-fund"
                            height="389" width="332" title="mutual-fund" class="img-cover">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why us section start-->
    <section class="why-us pt-130">
        <div class="container">
          
                <p class="sub-mutual-text text-center">MUTUAL FUND</p>
                <h2 class="common-title">
                   Why Choose Us?
               </h2>
            <div class="why-us-content mt-60">
                <ul class="why-us-content-list">
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/on-cloud.png');?>" alt="on cloud"
                                title="on cloud" class="img-contain" height="46" width="55">
                        </div>
                        <h3 class="sub-title">On Cloud</h3>
                        <p class="sub-text">Systematically View Your Mutual Funds, Stock, And Other Investments Backed
                            Up Every
                            Day And Can Be Viewed Everywhere</p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/team.png');?>" alt="Dedicated Team"
                                title="Dedicated Team" class="img-contain" height="46" width="55">
                        </div>
                        <h3 class="sub-title">Dedicated Team</h3>
                        <p class="sub-text"> Specialized Team of IT Coordinators and Trainers Available 24/7 To Help And
                            Guide You
                            With Nivesh Life Software Application</p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/integration.png');?>"
                                alt="Seamless Integration" title="Seamless Integration" class="img-contain" height="46"
                                width="55">
                        </div>
                        <h3 class="sub-title">Seamless Integration</h3>
                        <p class="sub-text">Take Your Historical Business Onboard And Carry It Forward Digitally </p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/technology.png');?>"
                                alt="echnology First" title="echnology First" class="img-contain" height="46"
                                width="55">
                        </div>
                        <h3 class="sub-title">Technology First</h3>
                        <p class="sub-text"> Providing Digitally Empowering Investment Services That Can Handle Big Data
                            For
                            Thousands Of Financial Advisors</p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/reporting.png');?>"
                                alt="Extensive Reporting" title="Extensive Reporting" class="img-contain" height="46"
                                width="55">
                        </div>
                        <h3 class="sub-title">Extensive Reporting</h3>
                        <p class="sub-text">Comprehensive Data Management System With Diverse Reporting </p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/advisory.png');?>" alt="Advisory Support"
                                title="Advisory Support" class="img-contain" height="46" width="55">
                        </div>
                        <h3 class="sub-title">Advisory Support</h3>
                        <p class="sub-text"> Round The Clock After-Sales Services And 24/7 Customer Service For Queries
                            Related To
                            Real-Time Portfolio Management</p>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- why us section end-->
    <!-- feature section section start -->
    <section class="featue-sec-wrap  pt-130">
        <div class="feature-sec">
            <div class="container">
            <p class="sub-mutual-text text-center">MUTUAL FUND</p>
                <h2 class="common-title">                    
                    Our Core Features
                </h2>
                <div class="feature-list mt-70">
                    <div class="feature-content">
                        <div class="feature-content-img">
                            <img src="<?php echo base_url('assets/front/images/feature-1.png');?>" class="img-cover"
                                alt="feature" title="feature" height="225" width="445">
                        </div>
                        <div class="feature-content-des">
                            <h3 class="sub-title">Comprehensive & Diverse Reports</h3>
                            <ul class="list-inform">
                                <li>MF Portfolio Valuation Report</li>
                                <li>Capital Gain Report</li>
                                <li> Transaction History </li>
                                <li> SIP/STP Reports </li>
                            </ul>
                            <a rel="dofollow" href="javascript:;" class="common-btn btn">Know More</a>
                        </div>

                    </div>
                    <div class="feature-content">
                        <div class="feature-content-img">
                            <img src="<?php echo base_url('assets/front/images/feature-2.png');?>" class="img-cover"
                                alt="feature" title="feature" height="225" width="445">
                        </div>
                        <div class="feature-content-des">
                            <h3 class="sub-title">Client Engagement Tools</h3>
                            <ul class="list-inform">
                                <li>Self-Branding (through brand image and video upload)</li>
                                <li>Mass Emailing</li>
                                <li> Auto Birthday Reminders </li>
                            </ul>
                            <a rel="dofollow" href="javascript:;" class="common-btn btn">Know More</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature section section end -->
    <section class="feature-sec-2 pt-130">
        <div class="container">
            <div class="feature-list-2">
                <div class="feature-content-2">
                    <div class="feature-content-img">
                        <img src="<?php echo base_url('assets/front/images/advantage-1.png');?>" class="img-cover"
                            alt="feature" title="feature" height="225" width="445">
                    </div>
                    <div class="content-2-sub">
                        <h3 class="sub-title">Analytics and Insights</h3>
                        <ul class="list-inform">
                            <li>Consolidated Statement Report</li>
                            <li> Net Asset Value (NAV) Report</li>
                            <li> AUM Report</li>
                            <li> Performance Report</li>
                            <li> MIS Reports (available daily, monthly, or brokerage-wise)</li>
                        </ul>
                        <a rel="dofollow" href="javascript:;" class="common-btn btn">Know More</a>
                    </div>
                </div>
                <div class="feature-content-2">
                    <div class="feature-content-img">
                        <img src="<?php echo base_url('assets/front/images/advantage-2.png');?>" alt="feature"
                            title="feature" class="img-cover" height="225" width="445">
                    </div>
                    <div class="content-2-sub">
                        <h3 class="sub-title">Analytics and Insights</h3>
                        <ul class="list-inform">
                            <li>Complete White-Labelled Platform (Only for Android App)</li>
                            <li> Integration with Star MF</li>
                            <li>Switch folios from Other ARN</li>
                            <li> One-click Overview</li>
                            <li> Multi-Asset Management (for Mutual Funds, FDs, Posts, bonds, LPS, Real Estate, and
                                Insurance)
                                <br>Onboarding for Clients
                            </li>
                            <li> Brokerage Setup</li>
                        </ul>
                        <a rel="dofollow" href="javascript:;" class="common-btn btn">Know More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Our Services -->
    <section class="our-services-wrap mt-130 pb-130" style="background-image:url(./images/our-service-bg.png);">
        <div class="our-services">
            <div class="container">
                <div class="service-inner">
                    <p class="sub-mutual-text">MUTUAL FUND</p>
                    <h2 class="common-title">Our Services</h2>
                    <ul class="service-list mt-60">
                        <li class="service-list-card">
                            <div class="sevice-icon">
                                <img src="<?php echo base_url('assets/front/images/white-labelled-solution.png');?>"
                                    alt=" White-Labelled Solution" title=" White-Labelled Solution" height="34"
                                    width="44" class="img-contain">
                            </div>
                            <h3 class="sub-title">Complete White-Labelled Solution</h3>
                            <p class="sub-text">Our motto “Your Brand, Your Platform” is for a complete white-labelled
                                solution that
                                offers financial advisors with the best financial management platform. We offer a
                                URL-based solution that
                                operates on the client’s domain name and is specifically developed to manage and grow
                                your mutual fund
                                business effectively.</p>
                        </li>
                        <li class="service-list-card">
                            <div class="sevice-icon">
                                <img src="<?php echo base_url('assets/front/images/goal-tracker.png');?>"
                                    alt="Goal Tracker" title="Goal Tracker" height="34" width="44" class="img-contain">
                            </div>
                            <h3 class="sub-title">Goal Tracker</h3>
                            <p class="sub-text">The Goal Tracker functionality helps Mutual fund distributors customize
                                their reports
                                and set more realistic goals. Through this, the distributors can map multiple assets to
                                the investors’
                                goals, and in the event of a shortage, the distributor can make purchases to make up for
                                the difference.
                            </p>
                        </li>
                    </ul>

                </div>
                <div class="service-img-gallary mt-80">
                    <div class="service-img">
                        <img src="<?php echo base_url('assets/front/images/services-1.png');?>" alt="manage-sec-img"
                            class="img-cover" width="311" height="395">
                    </div>
                    <div class="service-img">
                        <img src="<?php echo base_url('assets/front/images/services-2.png');?>" alt="manage-sec-img"
                            class="img-cover" width="311" height="395">
                    </div>
                    <div class="service-img">
                        <img src="<?php echo base_url('assets/front/images/services-3.png');?>" alt="manage-sec-img"
                            class="img-cover" width="311" height="395">
                    </div>
                    <div class="service-img">
                        <img src="<?php echo base_url('assets/front/images/services-4.png');?>" alt="manage-sec-img"
                            class="img-cover" width="311" height="395">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- mutual funds -->
    <section class="mutual-funds pt-130 ">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="mutual-fund-img">
                        <img src="<?php echo base_url('assets/front/images/mutual-funds.png');?>" alt="img" height="700"
                            width="663" class="img-contain">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mutual-right">
                        <p class="sub-mutual-text">lorem ipsum</p>
                        <h2 class="common-title">Mutual Funds</h2>
                        <div class="managmet-sec-right mt-50">
                            <p class="sub-text">Nivesh Life addresses all the needs of financial advisors and provides a
                                platform
                                that helps to simplify investing and financing. Mutual Funds are viewed as one of the
                                most popular
                                investment options, along with bank fixed deposits and stocks. Understanding important
                                performance
                                variables before investing can help you choose the finest Mutual Funds for you, and our
                                feature-rich
                                platform will make it happen. </p>
                            <p class="sub-text">Nivesh Life's Mutual fund software is developed to provide financial
                                requirements
                                and risk tolerance which can be beneficial to Mutual fund Distributors, corporations, as
                                well as
                                individuals from different income levels to scale up their financing & investing game.
                            </p>
                            <p class="sub-text"> High-quality technology and metrics power this feature-rich Mutual fund
                                software,
                                which will significantly advance the mutual funds sector and is made available on a
                                subscription
                                basis. You can quickly sign up or cancel the membership whenever you want, from
                                anywhere.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="dow-app-section">
        <div class="container">
            <div class="dow-sec-inner">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dow-app-text" >
                            <h2 class="common-title sub">Download the <br> app & ride now!</h2>
                            <p class="mt-15 sub-text">Lorem ipsum dolor sit amet</p>
                            <div class="dow-btn-grup mt-40">
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/playstore-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/apple-store-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="banner-right-img">
                            <div class="right-img-1">
                                <img src="<?php echo base_url('assets/front/images/screen-1.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="163" height="328" loading="lazy">
                            </div>
                            <div class="right-img-2">
                                <img src="<?php echo base_url('assets/front/images/screen-2.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="210" height="422" loading="lazy">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</section>