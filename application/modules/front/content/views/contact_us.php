<?php
$CI =& get_instance();
$CI->load->library('session');
$company_info = $CI->general->setting_info('Company');
$config_info = $CI->general->setting_info('Config');
?>
<script src="https://www.google.com/recaptcha/api.js?render=<?php echo $config_info['GOOGLE_CAPTCHA_SITE_KEY']['vValue'];?>"></script> 
<section class="page-wrap terms">
    <!-- main-page banner -->
    <section class="terms-condition py-130">
        <div class="container">
            <div class="terms-condition-wrap">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb top contact">
                        <li class="breadcrumb-item span-title"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active span-title" aria-current="page"> Contact</li>
                    </ol>
                </nav>
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="common-title text-start"> Get In Touch..</h1>
                        <div class="addres-portion-wrap mt-50">
                            <div class="address-portion">
                                <div class="address-icon">
                                    <img src="<?php echo base_url('/assets/front/images/location-icon.png');?>"
                                        title="location" alt="location" class="img-contain" width="61" height="61">
                                </div>
                                <div class="address-text">
                                    <p class="add-text"><?php echo $company_info['COMPANY_ADDRESS']['vValue'];?></p>
                                </div>
                            </div>
                            <div class="address-portion">
                                <div class="address-icon">
                                    <img src="<?php echo base_url('/assets/front/images/email-icon.png');?>"
                                        title="email" alt="email" class="img-contain" width="61" height="61">
                                </div>
                                <div class="address-text">
                                    <p class="add-text"><?php echo $company_info['COMPANY_EMAIL']['vValue'];?></p>
                                </div>
                            </div>
                            <div class="address-portion">
                                <div class="address-icon">
                                    <img src="<?php echo base_url('/assets/front/images/call-icon.png');?>"
                                        title="contact" alt="contact" class="img-contain" width="61" height="61">
                                </div>
                                <div class="address-text">
                                    <p class="add-text"><?php echo $company_info['COMPANY_NUMBER']['vValue'];?></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <form name="frm" id="frm" method="post" action="<?php echo base_url('content/add_contact_us');?>" enctype="multipart/form-data">
                            <div class="contact-form">
                                <div class="input-wrap">
                                    <label class="sub-text">Full Name</label>
                                    <input type="text" name="vName" class="form-control" id="vName">
                                    <div class="text-danger" id="vName_error" style="display: none;">Enter Name</div>
                                </div>
                                <div class="input-wrap">
                                    <label class="sub-text">Contact Number</label>
                                    <input type="number" name="vPhone" maxlength="10" class="form-control" id="vPhone">
                                    <div class="text-danger" id="vPhone_error" style="display: none;">Enter Contact Number</div>
                                    <div class="text-danger" id="vPhoneDigit_error" style="display: none;">Enter 10 digit mobile number</div>
                                </div>
                                <div class="input-wrap">
                                    <label class="sub-text">Email</label>
                                    <input type="text" name="vEmail" class="form-control" id="vEmail">
                                    <div id="vEmail_error" class="text-danger" style="display: none;"> Enter Email</div>
                                    <div id="vEmail_valid_error" class="text-danger" style="display: none;"> Enter Valid Email</div>
                                </div>
                                <div class="input-wrap">
                                    <label class="sub-text">Select Feedback</label>
                                    <select class="form-select" aria-label="Default select example" id="eFeedback" name="eFeedback">
                                        <option value="">Select Feedback</option>
                                        <option value="Feedback">Feedback</option>
                                        <option value="Suggestion">Suggestion</option>
                                        <option value="Query">Query</option>
                                    </select>
                                    <div class="text-danger" id="eFeedback_error" style="display: none;">Select Feedback</div>
                                </div>
                                <div class="input-wrap">
                                    <label class="sub-text">Your Message</label>
                                    <textarea class="form-control" aria-label="With textarea" id="tMessage" name="tMessage"></textarea>
                                    <div class="text-danger" id="tMessage_error" style="display: none;">Enter Message</div>
                                </div>
                                <div class="input-wrap">
                                    <a href="javascript:;" class="common-btn btn submit" id="submit">Submit</a>
                                    <button class="common-btn btn submit_loader" type="button" disabled style="display: none;">
                                          <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                          Loading...
                                    </button>
                                </div>

                                <div class="form-group">
                                    <script>
                                        function onClick(e) {
                                            e.preventDefault();
                                            grecaptcha.ready(function() {
                                              grecaptcha.execute('<?php echo $config_info['GOOGLE_CAPTCHA_SITE_KEY']['vValue'];?>', {action: 'submit'}).then(function(token) {
                                                    $("#g-recaptcha-response").val(token);
                                                });
                                            });
                                        }
                                    </script>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dow-app-section">
        <div class="container">
            <div class="dow-sec-inner">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dow-app-text" data-aos="fade-left" data-aos-duration="500">
                            <h2 class="common-title sub">Download the <br> app & ride now!</h2>
                            <p class="mt-15 sub-text">Lorem ipsum dolor sit amet</p>
                            <div class="dow-btn-grup mt-40">
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/playstore-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/apple-store-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="banner-right-img" data-aos="fade-right" data-aos-duration="500">
                            <div class="right-img-1">
                                <img src="<?php echo base_url('assets/front/images/screen-1.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="163" height="328" loading="lazy">
                            </div>
                            <div class="right-img-2">
                                <img src="<?php echo base_url('assets/front/images/screen-2.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="210" height="422" loading="lazy">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why us section end-->
</section>
<script>
    $(document).on('click', '.submit', function() {
        var vName = $("#vName").val();
        var vEmail = $("#vEmail").val();
        var vPhone = $("#vPhone").val();
        var eFeedback = $("#eFeedback").val();
        var tMessage = $("#tMessage").val();
        var vError = false;

        if (vName.length == 0) {
            $("#vName_error").show();
            vError = true;
        } else {
            $("#vName_error").hide();
        }

        if (vEmail.length == 0) {
            $("#vEmail_error").show();
            $("#vEmail_unique_error").hide();
            $("#vEmail_valid_error").hide();
            vError = true;
        } else {
            if (validateEmail(vEmail)) {
                $("#vEmail_valid_error").hide();
            } else {
                $("#vEmail_valid_error").show();
                $("#vEmail_error").hide();
                $("#vEmail_unique_error").hide();
                vError = true;
            }
        }
        if (vPhone.length == 0) {
            $("#vPhone_error").show();
            vError = true;
        } else {
            $("#vPhone_error").hide();
            if (vPhone.length != 10 && vPhone.length <= 10) {
                $("#vPhoneDigit_error").show();
                vError = true;
            } else {
                $("#vPhoneDigit_error").hide();
            }
        }

        if (tMessage.length == 0) {
            $("#tMessage_error").show();
            vError = true;
        } else {
            $("#tMessage_error").hide();
        }

        if (eFeedback.length == 0) {
            $("#eFeedback_error").show();
            vError = true;
        } else {
            $("#eFeedback_error").hide();
        }

        if (vError == false) {
            $(".submit").hide();
            $(".submit_loader").show();
            setTimeout(function() {
                $("#frm").submit();
            }, 1000);
        }
    });

$('#vPhone').bind('input propertychange', function() {
    $(this).val($(this).val().replace(/[^0-9]/g, ''));
});


function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}
</script>