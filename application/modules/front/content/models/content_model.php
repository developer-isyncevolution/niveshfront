<?php
defined('BASEPATH') || exit('No direct script access allowed');

class content_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    
    public $listing_data;
    public $rec_per_page;
    public $message;

    var $table = 'user';
    var $table_specialization = 'specialization';
    var $table_specialization_lang = 'specialization_lang';

    public function __construct()
    {
        parent::__construct();
    }

    public function search_result($vLangCode, $keyword)
    {
        $this->db->from($this->table);
        $this->db->where("(vFirstName LIKE '%$keyword%' OR vLastName LIKE '%$keyword%')");
        $this->db->where('eAccountType','Doctor');
        $this->db->where('eStatus', 'Active' );

        $this->db->order_by('vFirstName', 'ASC');
        $query=$this->db->get();
        $data['name'] = $query->result();

        $this->db->from($this->table);
        $this->db->like('vClinicName',$keyword);
        $this->db->where('eAccountType','Doctor');
        $this->db->where('eStatus', 'Active' );

        $this->db->order_by('vClinicName', 'ASC');
        $query=$this->db->get();
        $data['clinic'] = $query->result();

        $this->db->from($this->table_specialization." t");
        $this->db->join($this->table_specialization_lang." tl", 't.iSpecializationId = tl.iSpecializationId');
        $this->db->where('tl.vLangCode', $vLangCode );
        $this->db->where('t.eStatus', 'Active' );
        $this->db->like('tl.vSpecialization', $keyword );

        $this->db->order_by('t.iSpecializationOrder', 'ASC');
        $query=$this->db->get();
        $data['specialization'] = $query->result();

        // print_r($data);exit;

        return $data;
    }

    public function get_count()
    {
        $this->db->from($this->table);

        $this->db->where('eStatus <> ', 'Pending' );

        $query=$this->db->get();
        return $query->num_rows();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
}
