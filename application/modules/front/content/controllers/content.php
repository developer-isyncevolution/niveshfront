<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class content extends MX_Controller 
{
    public function __construct() 
    {
        parent::__construct();

        $this->load->model('setting/setting_model');
        $this->load->model('contact_us/contact_us_model');
        $this->load->model('arn_email_credential/arn_email_credentials_model');
        $this->load->model('system_email/system_email_model');
        $this->load->library('general');    
    }

	public function index()
	{      
        
        $this->template->build('home',null);
	}

    public function contact_us()
    {      
        
        $this->template->build('contact_us',null);
    }

    public function add_contact_us()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $config_setting = $this->setting_model->get_setting('Config');

            $secret_key = $config_setting['GOOGLE_CAPTCHA_SECRET_KEY']['vValue'];
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']);

            $responseData = json_decode($verifyResponse);

            if($responseData->success == 1)
            {
                $data                   = array();
                $data['vName']          = ucwords($this->input->post('vName'));
                $data['vEmail']         = strtolower($this->input->post('vEmail'));
                $data['vSubject']       = ucwords($this->input->post('eFeedback'));
                $data['vPhone']         = $this->input->post('vPhone');
                $data['eFeedback']      = $this->input->post('eFeedback');
                $data['tMessage']       = ucwords($this->input->post('tMessage'));
                // $data['vAddress']       = ucwords($this->input->post('vAddress'));
                $data['dtAddedDate']    = date("Y-m-d h:i:s");
                $data['dtUpdatedDate']  = date("Y-m-d h:i:s");

                // $this->contact_us_model->add($data);

                $criteria                             = array();
                $criteria['iARNId']                   = '8943';
                $arn_email_credential                 = $this->arn_email_credentials_model->get_by_id($criteria);
                
                //Super Admin Send Email Code  
                $criteria                           = array();
                $criteria["vCode"]                  = "CONTACT_US";
                $data_email                         = $this->system_email_model->get_by_id($criteria);

                $constant                           = array('#NAME#','#ContactNumber#','#EMAIL#','#SUBJECT#','#MESSAGE#','#ADDRESS#');
                $value                              = array($data['vName'],$data['vPhone'],$data['vEmail'],$data['eFeedback'],$data['tMessage'],"N/A");
                $message                            = str_replace($constant, $value, $data_email->tMessage);
                $email_data['to']                  = "info@niveshlife.com";
                $email_data['subject']             = $data_email->vSubject;
                $email_data['message']             = $message;
                $criteria                           = array();
                $criteria['eType']              = 'EMAIL';
                $criteria['vData']              = $email_data;
                $criteria['form_type']          = "Niveshlife";
                $criteria['host']               = $arn_email_credential->support_host;
                $criteria['port']               = $arn_email_credential->support_port;
                $criteria['from_email']         = $arn_email_credential->support_email;
                $criteria['from_password']      = $arn_email_credential->support_password;
                $criteria['status']             = $arn_email_credential->support_estatus;

                $this->general->send_notifiction($criteria);
                //Super Admin Send Email Code  

                //Thanx  Email Code  
                $criteria                           = array();
                $criteria["vCode"]                  = "THANKYOU_MAIL";
                $data_email                         = $this->system_email_model->get_by_id($criteria);
            
                if(!empty($data_email))
                {
                    $constant                           = array('#NAME#');
                    $value                              = array($data['vName']);
                    $message                            = str_replace($constant, $value, $data_email->tMessage);
                    $email_data['to']                  = $data['vEmail'];
                    $email_data['subject']             = $data_email->vSubject;
                    $email_data['message']             = $message;
                    $criteria                           = array();
                    $criteria['eType']              = 'EMAIL';
                    $criteria['vData']              = $email_data;
                    $criteria['form_type']          = "Niveshlife";
                    $criteria['host']               = $arn_email_credential->support_host;
                    $criteria['port']               = $arn_email_credential->support_port;
                    $criteria['from_email']         = $arn_email_credential->support_email;
                    $criteria['from_password']      = $arn_email_credential->support_password;
                    $criteria['status']             = $arn_email_credential->support_estatus;
        
                    $this->general->send_notifiction($criteria);
                }   
                //Thanx  Email Code  

                $this->session->set_flashdata('success', "Contact Deatil Send Successfully." );
                redirect(base_url());
            }
            else {
                $this->session->set_flashdata('error', "You are Robot." );
                redirect(base_url('')); 
            }
        }    
    }
}