<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class package extends MX_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('general');
        $this->load->library('niveshlife_api');
        $this->load->model('system_email/system_email_model');
        $this->load->model('contact_us/contact_us_model');
        $this->load->model('arn_email_credential/arn_email_credentials_model');
        
    }

	public function index()
	{
        $criteria                             = array();
        $criteria['eStatus']                  = "Active";
        $criteria['eDeleted']                 = "No";
        $response                             = $this->niveshlife_api->package($criteria);

        $result                               = json_decode($response,true);

        if($result['code'] == "200")
        {
            $data['data_package']  = $result['data'];
        }
        else 
        {
            $data['data_package'] = NULL;
        }

        $this->template->build('package', $data);
    }

    public function package_feature($iPackageId)
	{

        $criteria                             = array();
        $criteria['eStatus']                  = "Active";
        $criteria['eDeleted']                 = "No";
        $response                             = $this->niveshlife_api->package_feature($criteria);
        $result                               = json_decode($response,true);

        if($result['code'] == "200")
        {
            $data['data_package']  = $result['data'];
            $data['iPackage_Id']   = $iPackageId;
            // $data['iPackage_Id']              = base64_decode(urldecode($iPackageId));


        }

        else 
        {
            $data['data_package'] = NULL;
        }


        $this->template->build('package_feature', $data);
    }

    public function contact_us_popup()
    {
        $data['iPackageId']         = $this->input->post('iPackageId');
        $data['vPackage']              = $this->input->post('vPackage');
        $this->load->view('ajax_contact_us', $data);
    }

    public function add_contact_us()
    {

        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $vPackage                    = $this->input->post('vPackage');
            $Package_Id                 = $this->input->post('Package_Id');
                $data                   = array();
                $data['vName']          = ucwords($this->input->post('vName'));
                $data['vPackage']       = $vPackage;
                 $data['iPackageId']      = $Package_Id;
                $data['vEmail']         = strtolower($this->input->post('vEmail'));
                $data['vSubject']       = ucwords($this->input->post('vSubject'));
                $data['vPhone']         = $this->input->post('vPhone');
                $data['tMessage']       = ucwords($this->input->post('tMessage'));
                $data['vAddress']       = ucwords($this->input->post('vAddress'));
                $data['dtAddedDate'] 	= date("Y-m-d h:i:s");
                $data['dtUpdatedDate'] 	= date("Y-m-d h:i:s");

                $this->contact_us_model->add($data);

                $criteria                             = array();
                $criteria['iARNId']                   = '8943';
                $arn_email_credential                 = $this->arn_email_credentials_model->get_by_id($criteria);
               
                    $criteria                           = array();
                    $criteria["vCode"]                  = "PACKAGE_CONTACT_US";
                    $data_email                         = $this->system_email_model->get_by_id($criteria);


                    $constant                           = array('#NAME#','#ContactNumber#','#EMAIL#','#PACKAGE#');
                    $value                              = array($data['vName'],$data['vPhone'],$data['vEmail'],$data['vPackage']);
                    $message                            = str_replace($constant, $value, $data_email->tMessage);
                    $email_data['to']                  = "info@niveshlife.com";
                    $email_data['subject']             = $data_email->vSubject;
                    $email_data['message']             = $message;
                    $criteria                           = array();
                    $criteria['eType']              = 'EMAIL';
                    $criteria['vData']              = $email_data;
                    $criteria['form_type']          = "Niveshlife";
                    $criteria['host']               = $arn_email_credential->support_host;
                    $criteria['port']               = $arn_email_credential->support_port;
                    $criteria['from_email']         = $arn_email_credential->support_email;
                    $criteria['from_password']      = $arn_email_credential->support_password;
                    $criteria['status']             = $arn_email_credential->support_estatus;

                    $this->general->send_notifiction($criteria);
                      
                //Super Admin Send Email Code  

                //Thanx  Email Code  
                    $criteria                           = array();
                    $criteria["vCode"]                  = "THANKYOU_MAIL";
                    $data_email                         = $this->system_email_model->get_by_id($criteria);
                
                    if(!empty($data_email))
                    {

                    $constant                           = array('#NAME#');
                    $value                              = array($data['vName']);
                    $message                            = str_replace($constant, $value, $data_email->tMessage);
                    $email_data['to']                  = $data['vEmail'];
                    $email_data['subject']             = $data_email->vSubject;
                    $email_data['message']             = $message;
                    $criteria                           = array();
                    $criteria['eType']              = 'EMAIL';
                    $criteria['vData']              = $email_data;
                    $criteria['form_type']          = "Niveshlife";
                    $criteria['host']               = $arn_email_credential->support_host;
                    $criteria['port']               = $arn_email_credential->support_port;
                    $criteria['from_email']         = $arn_email_credential->support_email;
                    $criteria['from_password']      = $arn_email_credential->support_password;
                    $criteria['status']             = $arn_email_credential->support_estatus;

        
                    $this->general->send_notifiction($criteria);
                    }   
                //Thanx  Email Code  

                $this->session->set_flashdata('success', "Contact Deatil Send Successfully." );
                redirect(base_url());
           
        }  
    }
}