<section class="half-banner"
    style="background-image:url('<?php echo base_url('assets/front/images/sub-banner.jpg')?>'); ">
    <div class="sub-title">
        <h1>Package & Features</h1>
    </div>
</section>

<section class="vision-main package-main">
    <div class="container text-center">
        <div class="row">

            <?php if(!empty($data_package)) { ?>
                <?php foreach ($data_package as $key => $value) { ?>
                
                    <div class="col-lg-3 mb-5">
                        <div class="package-box">
                            <ul>
                                <div class="package-bg">
                                    <li class="">
                                        <h2><?php echo $value['vPackage']; ?></h2>
                                    </li>
                                    <a href="javascript:;" id="iPackageId" class="package contact_popup"  data-id="<?php echo $value['iPackageId']; ?>"  data-filter="<?php echo $value['vPackage']; ?>">Contact for Price</a>
                                </div>
                                <?php foreach ($value['package_feature'] as $val) { ?>
                                    <li ><?php echo $val['tDescription']; ?></li>
                                <?php } ?>
                                <li><a href="<?php echo base_url('book-demo'); ?>" class="all-btn package_feature " data-id="<?php echo $value['iPackageId']; ?>">Book Demo</a></li>
                            </ul>
                        </div>
                    </div>

                <?php } ?>
            <?php } ?>
           
        </div>
    </div>
</section>

<!-- Send Email Ppoup Start-->
<div class="modal fade custom-modal" id="email_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- Send Email Ppoup-->

<script>
$(document).on('click', '.contact_popup', function() {
    var iPackageId = $(this).data('id');
    var vPackage = $(this).data('filter');
   
    url = "<?php echo base_url('package/contact_us_popup'); ?>";
    setTimeout(function() {
        $.ajax({
            url: url,
            type: "POST",
            data: {
                iPackageId: iPackageId,
                vPackage: vPackage,
            },
            success: function(response) {
                $("#email_modal .modal-body").html(response);
                $('#email_modal').modal('show');
            }
        });
    });
});

$(document).on('click', '.package_feature', function() {
    var iPackageId = $(this).data('id');
    location.href = "package/package_feature/"+iPackageId;
});

$(document).on('click', '.send', function() {
    var Package_Id = $('#Package_Id').val();
    var vName = $("#vName").val();
    var vEmail = $("#vEmail").val();
    var vPhone = $("#vPhone").val();
    var vPackage = $("#vPackage").val();
    $('#reset').hide();
	$('#show_loader1').show();
    $("#vEmail_valid_error").hide();
    $("#vName_error").hide();
    $("#vPhoneDigit_error").hide();
       if (vEmail.length == 0) {
        $("#vEmail_error").show();
        $("#vEmail_unique_error").hide();
        $("#vEmail_valid_error").hide();
        vError = true;
    } else {
        if (validateEmail(vEmail)) {
            $("#vEmail_valid_error").hide();
        } else {
            $("#vEmail_valid_error").show();
            $("#vEmail_error").hide();
            $("#vEmail_unique_error").hide();
            vError = true;
        }
    }
    if (vPhone.length > 0) {
        setTimeout(function() {
            $.ajax({
                url: "<?php echo base_url('package/add_contact_us');?>",
                type: "POST",
                data: {
                    Package_Id: Package_Id,
                    vName: vName,
                    vEmail: vEmail,
                    vPhone: vPhone,
                    vPackage:vPackage
                },
                success: function(response) {
                    $('#Package_Id').val('');
                    $("#vName").val('');
                    $("#vEmail").val('');
                    $("#vPhone").val('');
                    $("#vPackage").val('');
                    $('#email_modal').modal('hide');
                    notification_success("Package Request Send Successfully.");
                }
            });
        }, 1000)
    } else {
        // $("#vEmail_valid_error").show();
        $("#vName_error").show();
        $("#vPhoneDigit_error").show();
        $('#reset').show();
		$('#show_loader1').hide();
        vError = true;
    }
});
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}
</script>