<?php
defined('BASEPATH') || exit('No direct script access allowed');

class System_email_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    public $listing_data;
    public $rec_per_page;
    public $message;

    var $table = 'system_email';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($criteria = array())
    {
        $this->db->from($this->table);
        if($criteria['column'] || $criteria['order'] != "")
        {
            $this->db->order_by($criteria['column'],$criteria['order']);
        }   
        $query=$this->db->get();
        return $query->result();
    }

    public function get_by_id($criteria = array())
    {
        $this->db->from($this->table);
        if(!empty($criteria['iSystemEmailId']))
        {
            $this->db->where('iSystemEmailId',$criteria['iSystemEmailId']);
        }
        if(!empty($criteria['vCode']))
        {
            $this->db->where('vCode',$criteria['vCode']);
        }
        $query = $this->db->get();
        return $query->row();
    }
    public function add($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
        $this->db->where('iEmailTemplateId', $id);
        $this->db->delete($this->table);
    }

    public function get_email_by_code($vLangCode, $vCode = '')
    {
        $this->db->from($this->table." t");
         $this->db->join($this->table_lang." tl", 't.iEmailTemplateId = tl.iEmailTemplateId');

        $this->db->where('tl.vLangCode', $vLangCode );
        $this->db->where('vEmailCode', $vCode );

        $query=$this->db->get();
        return $query->row();
    }

    // public function get_email_by_code($vCode = '')
    // {
    //     $this->db->from($this->table);
    //     $this->db->where('vEmailCode', $vCode );
    //     $query=$this->db->get();
    //     return $query->row();
    // }

    public function update_pro_status($where, $data)
    {
        $this->db->where_in('iEmailTemplateId',$where);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }
}
