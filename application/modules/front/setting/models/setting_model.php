<?php
defined('BASEPATH') || exit('No direct script access allowed');

class Setting_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    
    public $listing_data;
    public $rec_per_page;
    public $message;

    var $table = 'setting';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_setting($eConfigType = '', $vName = '')
    {
        $this->db->from($this->table);
        
        if($eConfigType != "")
            $this->db->where('eConfigType', $eConfigType );

        if($vName != "")
            $this->db->where('vName', $vName );

        $query=$this->db->get();
        $data = $query->result();

        $array = array();
        foreach ($data as $key => $value) {
            $array[$value->vName]['vValue'] = $value->vValue;
        }

        return $array;
    }


    public function get_all_data($criteria = array())
    {   
        $this->db->from($this->table);
        if(!empty($criteria["eStatus"]))
        {
            $this->db->where('eStatus', $criteria["eStatus"]);    
        }
        if(!empty($criteria["eConfigType"]))
        {
            $this->db->where('eConfigType', $criteria["eConfigType"]);    
        }
        $query=$this->db->get();
        $data = $query->result();

        $array = array();
        foreach ($data as $key => $value) 
        {
            $array[$value->vName]['vValue'] = $value->vValue;
        }

        return $array;
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('vValue',$id);
        $query = $this->db->get();
 
        return $query->row();
    }

    public function setting_update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function update($data = array(), $where = '', $alias = "No", $join = "No")
    {
        if ($alias == "Yes")
        {
            if ($join == "Yes")
            {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '')
            {
                $set_cond = array();
                foreach ($data as $key => $val)
                {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where))
                {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $extra_cond = " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        }
        else
        {
            if (is_numeric($where))
            {
                $this->db->where($this->primary_key, $where);
            }
            elseif ($where)
            {
                $this->db->where($where, FALSE, FALSE);
            }
            else
            {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }
}
