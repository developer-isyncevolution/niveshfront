<section class="page-wrap terms">
    <!-- main-page banner -->
    <section class="terms-condition py-130">
        <div class="container">
            <div class="terms-condition-wrap">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb top">
                        <li class="breadcrumb-item span-title"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active span-title" aria-current="page">Terms and condition</li>
                    </ol>
                </nav>
                <h1 class="common-title text-start"> TERMS AND CONDITIONS</h1>
                <h2 class="sub-mutual-text">Thank you for showing interest in Nivesh Life- A one-stop solution for all
                    financial needs.
                </h2>
                <div class="terms-dec mt-35">
                    <p class="sub-text main">To avail of the services offered by Nivesh Life, you must agree to the
                        terms and
                        conditions set by Nivesh Life. Please read the terms and conditions carefully. While using any
                        current or
                        future services offered by <a href="<?php echo base_url('/'); ?>" class="link">www.niveshlife.in</a> whether or not
                        included in
                        the
                        website, you will abide by the
                        guideline and terms & conditions applicable to such service or business. Nivesh Life holds the
                        full
                        rights
                        to issue, cancel, and renew the usage subscription to the users. Clients of this subscription
                        are
                        bound by
                        the following terms and conditions:</p>
                    </p>
                </div>
                <div class="list-of-terms-wrp mt-40">
                    <ul class="list-of-terms">
                        <li class="tems">
                            <span class="count">1</span>
                            <p class="sub-text">The sale of a subscription of Nivesh Life gives no ownership right to
                                the
                                user, it
                                only gives the right to use the software through its different log-in panels.
                        </li>
                        <li class="tems">
                            <span class="count">2</span>
                            <p class="sub-text">The software application is sold with monthly and yearly fees and the
                                charges will be
                                applicable according to your custom requirements. The company has the right to pay the
                                subscription
                                amount as and when the company wishes and the clients will be intimated of the change as
                                and
                                when it
                                occurs.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">3</span>
                            <p class="sub-text">In case of any error in the software erupting due to the web hosting
                                environment,
                                Nivesh Life cannot be held responsible. We do not claim any responsibility that occurred
                                due
                                to
                                technical disruptions that the client may face due to hardware and software failures of
                                the
                                web hosting.
                                Any changes or rectifications can lead the company to charge you as per the pricing
                                model or
                                the
                                decision made by the company.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">4</span>
                            <p class="sub-text"> Nivesh Life is a web-based software application and all its components
                                are
                                installed
                                on the Cloud Server. The client will be held solely for their data safety, password,
                                etc. of
                                their
                                portal.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">5</span>
                            <p class="sub-text"> Nivesh Life isn’t liable to refund your fees after the subscription is
                                taken whether
                                it is monthly or yearly. It is advisable to take prior training before the subscription
                                is
                                purchased.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">6</span>
                            <p class="sub-text"> Under these Terms of Usage, you are bound by the court of Law that you
                                will
                                not copy,
                                resale, or modify this software under any circumstances. Failing to follow these rules
                                and
                                terms will
                                result in criminal action against the Subscriber under the Copyright Infringement and
                                the
                                Intellectual
                                Property Laws of India.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">7</span>
                            <p class="sub-text"> Nivesh Life is readily accepting requests for customizations,
                                additions,
                                and
                                modifications required in the software to suit the User’s business model. The company
                                has
                                the right to
                                charge for the customizations requested by the client.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">8</span>
                            <p class="sub-text">Nivesh Life isn’t responsible for any errors or problems that arise in
                                the
                                software
                                due to ARNs’ clients or customization sources like a web host, ISP providers, Registrar
                                and
                                Transfer
                                agents, or data providers like CAMS, Kfintech, or BSE.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">9</span>
                            <p class="sub-text">These terms and conditions are governed by and to be interpreted in
                                accordance with
                                relevant Gujarat laws, both substantive and procedural. In the event of any dispute
                                arising
                                in relation
                                to these terms and conditions or any dispute arising in relation to the website whether
                                in
                                contract or
                                tort or otherwise the Indian courts at Ahmedabad(Gujarat) will have exclusive
                                jurisdiction
                                over such
                                dispute.
                            </p>
                        </li>
                        <li class="tems">
                            <span class="count">10</span>
                            <p class="sub-text">By buying this software, you are accepting that you are bound by the
                                current
                                terms and
                                conditions and disclaimer and so you should check it properly at the time of purchase of
                                your license.
                            </p>
                            </p>
                        </li>
                    </ul>

                </div>
                <hr class="terms">
                <div class="payment-wrp-main">
                    <h2 class="common-title text-start">
                        TERMS AND CONDITIONS OF PAYMENTS
                    </h2>
                    <div class="terms-dec mt-35">
                        <p class="sub-text">We, at Nivesh Life, respect our investors’/customers’/clients’ privacy</p>
                        </p>
                    </div>
                    <div class="terms-dec mt-35">
                        <p class="sub-text main">We recognize that the lawful and correct treatment of personal data is
                            very
                            important to
                            maintain investors’ confidence in us.
                            It is our endeavor to ensure that any personal data that we collect, process, record, or use
                            in
                            any
                            way
                            whether it is held on paper, on a computer, or other electronic media will have appropriate
                            safeguards in
                            place to comply with our obligations of confidentiality and privacy.
                            However, there are various terms and conditions regarding the payments that need to be
                            followed:
                        </p>
                        </p>
                    </div>
                    <div class="payment-terms-wrp mt-40">
                        <ul class="list-of-payment-term">
                            <li class="tems">
                                <p class="sub-text">Monthly and Yearly subscription options are available. Your
                                    subscription
                                    will be
                                    counted from the time you pay.
                            </li>
                            <li class="tems">
                                <p class="sub-text">Charges are applicable if you wish to instill add-on features
                                    presented
                                    to
                                    you
                                    during demonstration and training.
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">LThe theme of the application is based on the logo. Charges will be
                                    applicable if
                                    you wish to change the theme.
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">We take up to 15 days to bring your data from CAMS, Kfintech, and
                                    BSE to
                                    our
                                    system.
                                    If there is any delay from the client’s end, then time can be extended. Weekends and
                                    Public
                                    Holidays
                                    are excluded from the 15-day time frame.
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">Nivesh Life holds the right to deactivate the subscription under
                                    foreseeable
                                    circumstances.
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">If timely dues aren’t paid, Nivesh Life holds the right to
                                    close/deactivate
                                    your
                                    account. To re-activate will require you to open a new account and go through all
                                    the
                                    procedures
                                    again.
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text"> At any given point in time, Nivesh Life has the right to change the
                                    payment’s terms
                                    and conditions.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dow-app-section">
        <div class="container">
            <div class="dow-sec-inner">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dow-app-text" data-aos="fade-left" data-aos-duration="500">
                            <h2 class="common-title sub">Download the <br> app & ride now!</h2>
                            <p class="mt-15 sub-text">Lorem ipsum dolor sit amet</p>
                            <div class="dow-btn-grup mt-40">
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/playstore-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/apple-store-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="banner-right-img" data-aos="fade-right" data-aos-duration="500">
                            <div class="right-img-1">
                                <img src="<?php echo base_url('assets/front/images/screen-1.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="163" height="328" loading="lazy">
                            </div>
                            <div class="right-img-2">
                                <img src="<?php echo base_url('assets/front/images/screen-2.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="210" height="422" loading="lazy">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why us section end-->
</section>