<section class="page-wrap terms">
    <!-- main-page banner -->
    <section class="terms-condition py-130">
        <div class="container">
            <div class="terms-condition-wrap privecy">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb top">
                        <li class="breadcrumb-item span-title"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active span-title" aria-current="page">Privacy Policy</li>
                    </ol>
                </nav>
                <h1 class="common-title text-start"> Privacy Policy</h1>
                <h2 class="sub-mutual-text">Build your Company's Credibilitythrough Strong Brand Awareness of your
                    Business.
                </h2>
                <div class="terms-dec main mt-35">
                    <p class="sub-text main">Privacy Policy
                        This privacy policy sets out how Nivesh Life Pvt. Ltd. (also known as Nivesh Life) uses and
                        protects any information that you share when you use this website. Nivesh Life is committed to
                        ensuring that your privacy is protected at all times. Should we ask you to provide certain
                        information by which you can be identified when using this website, you can be assured that it
                        will only be used in accordance with this privacy statement.
                    </p>
                    <p class="sub-text main">
                        Nivesh Life may change this policy from time to time by updating this page. This policy is
                        effective from OCT/ 6/ 2021
                    </p>
                    <p class="sub-text main">
                        Nivesh Life understands that our relationship is strongly built on trust and faith. In Course of
                        using information on this website or availing the services, Nivesh Life may become privy to the
                        personal information of its customer including information that is of confidential nature.
                        Nivesh Life is strictly committed to protecting the privacy of its Customer and has taken
                        reasonable measures to protect the confidentiality of the customer information and its
                        transmission through World Wide Web. However it shall not be liable in any manner for disclosure
                        of the confidential information in accordance with this Privacy Commitment or in terms of the
                        agreement if any with the Customer or by reasons beyond its control. We may however be required
                        to disclose your personal information to Government, Judicial bodies, and our Regulators or to
                        any person to whom the Firm is under an obligation to make disclosure under the requirements of
                        any law binding on the Firm or any of its branches, if required. Hyperlink Policy for user any
                        hyperlink to other Internet sites is at customer’s own risk. The contents of which and the
                        accuracy of opinions expressed are not verified, monitored or endorsed by Nivesh Life, in any
                        way or manner. Nivesh Life is not responsible for the setup of any hyperlink from a third party
                        website to Nivesh Life.

                    </p>
                </div>
                <div class="payment-wrp-main">
                    <h3 class="sub-title text-start">
                        What we collect
                    </h3>
                    <div class="terms-dec ">
                        <p class="sub-text">We may collect the following information</p>
                    </div>
                    <div class="payment-terms-wrp mt-40">
                        <ul class="list-of-payment-term">
                            <li class="tems">
                                <p class="sub-text">Name and contact details
                            </li>
                            <li class="tems">
                                <p class="sub-text">Personal information, including date of birth, Aadhaar Number, and
                                    Permanent Account Number (PAN)
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">Demographic information such as gender and income
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">Other information that can help us improve our services
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr class="terms">
                <div class="payment-wrp-main">
                    <h3 class="sub-title text-start">
                    What we do with the information we gather
                    </h3>
                    <div class="terms-dec ">
                        <p class="sub-text">To use the information to improve our products and services</p>
                    </div>
                    <div class="payment-terms-wrp mt-40">
                        <ul class="list-of-payment-term">
                            <li class="tems">
                                <p class="sub-text">To periodically send emails to your registered email address about your investments, or other information which we think you may find interesting.</p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">You will be free to unsubscribe from our mailing list at any time if you do not wish to receive such emails from us. From time-to-time, we may also use your information to contact you via phone or email for market research purposes.
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">We will not sell, distribute or lease your personal information to third parties unless we are required to share such information under the terms and conditions of the products and services you avail, or we are required to do so by law.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr class="terms">
                <div class="payment-wrp-main">
                    <h3 class="sub-title text-start">
                        Security
                    </h3>
                    <div class="terms-dec ">
                        <p class="sub-text">We are committed to ensuring that your information is secure. In order to
                            prevent unauthorised access or disclosure, we have put in place suitable physical,
                            electronic and managerial procedures to safeguard and secure the information we collect
                            online.</p>
                    </div>
                </div>
                <hr class="terms">
                <div class="payment-wrp-main">
                    <h3 class="sub-title text-start">
                    Links to other websites
                    </h3>
                    <div class="terms-dec ">
                        <p class="sub-text">Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over such third-party websites. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites. You should exercise caution and look at the privacy statement applicable to the website in question.</p>
                    </div>
                </div>
                <hr class="terms">
                <div class="payment-wrp-main">
                    <h3 class="sub-title text-start">
                    Security certificates
                    </h3>
                    <div class="terms-dec ">
                        <p class="sub-text">If you believe that any of your information with us is incorrect or incomplete, please email us as soon as possible at nivesh@mailinator.com. We will promptly correct any information found to be incorrect.</p>
                    </div>
                </div>
                <hr class="terms">
                <div class="payment-wrp-main">
                    <h3 class="sub-title text-start">
                    Controlling your personal information
                    </h3>
                    <div class="terms-dec ">
                        <p class="sub-text">Nivesh Life is an information technology company. We fully recognise and understand the security implications of being a service provider with whom people trust their money. There are many safeguards we adopt in this regard – some of these are technical, and some are structural</p>
                    </div>
                </div>
                <hr class="terms">
                <div class="payment-wrp-main">
                    <h3 class="sub-title text-start">
                    When it comes to data security, our goal is to ensure that:
                    </h3>
                    <div class="payment-terms-wrp mt-40">
                        <ul class="list-of-payment-term">
                            <li class="tems">
                                <p class="sub-text">Your data is stored safely and securely – passwords are one-way encrypted before being stored in the database for high security.
                            </li>
                            <li class="tems">
                                <p class="sub-text">All communication with you, or with mutual fund companies and other service providers – are encrypted using the highest standards

                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">Your data is not shared with anyone, unless you have explicitly requested us to do so to fulfill a transaction request.
                                </p>
                            </li>
                            <li class="tems">
                                <p class="sub-text">To ensure that we achieve these goals, we have a variety of certifications/trust verifications in place for our firm, both from technical and legal/operational perspectives. All our communications are encrypted by 256-bit encryption, and our data is hosted with top-tier hosting service providers. Also, our data is continuously backed up to ensure continuity of operations.

                                </p>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </section>

    <section class="dow-app-section">
        <div class="container">
            <div class="dow-sec-inner">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dow-app-text" data-aos="fade-left" data-aos-duration="500">
                            <h2 class="common-title sub">Download the <br> app & ride now!</h2>
                            <p class="mt-15 sub-text">Lorem ipsum dolor sit amet</p>
                            <div class="dow-btn-grup mt-40">
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/playstore-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/apple-store-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="banner-right-img" data-aos="fade-right" data-aos-duration="500">
                            <div class="right-img-1">
                                <img src="<?php echo base_url('assets/front/images/screen-1.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="163" height="328" loading="lazy">
                            </div>
                            <div class="right-img-2">
                                <img src="<?php echo base_url('assets/front/images/screen-2.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="210" height="422" loading="lazy">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why us section end-->
</section>