<section class="page-wrap">
    <!-- main-page banner -->
    <section class="home-banner about">
        <div class="container">
            <div class="home-banner-content about-us">
                <div class="left-content">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb top">
                            <li class="breadcrumb-item span-title"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active span-title" aria-current="page">About</li>
                        </ol>
                    </nav>
                    <div class="banner-wrap">
                        <p class="sub-mutual-text"></p>
                        <h1 class="common-title"> Build your Company's Credibility through strong Brand Awareness of
                            your Business.</h1>
                    </div>
                </div>
                <div class="right-content">
                    <div class="banner-img">
                        <img src="<?php echo base_url('assets/front/images/about-us-main.png');?>" alt="banner"
                            class="img-contain" title="banner" width="800" height="400">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- management description -->
    <section class="managmet-sec-wrap about-us ">
        <div class="container">
        <p class="sub-mutual-text">About us</p>
            <div class="row g-4">
                <div class="col-lg-6 managmet-sec-left">                   
                    <h2 class="common-title">
                        Nivesh Life is a one-stop solution developed to solve all your financial service needs.
                    </h2>
                </div>
                <div class="col-lg-6">
                    <div class="managmet-sec-right">
                        <p class="sub-text">A brilliant platform that comes in mobile and web applications is a high-end
                            technology platform created for next-gen advisors to build and improve their client
                            relationships and
                            grow their businesses. Our customer-centric approach and well-defined execution processes
                            help us
                            deliver excellent service, deep expertise, and a better environment for financial advisors
                            and end
                            clients.</p>
                        <p class="sub-text"> Nivesh Life is a high-end technology platform created for next-generation
                            Mutual Fund
                            Distributors who wish to add value to their relationship with their clients and focus on
                            growing their
                            business to a new level. Nivesh Life is developed with the aim to empower Mutual Fund
                            Distributors,
                            ARNs, and end-clients to increase their assets under management, identify product
                            opportunities, and win
                            their clients’ loyalty and confidence.</p>
                        <p class="sub-text"> A platform developed to ease the use, affordable, and trusted by millions;
                            Nivesh
                            Life offers a wide range of Mutual Fund investing ways, at your convenience.</p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- feature section section -->
    <section class="feature-sec   mt-130">
        <div class="container">
            <p class="sub-mutual-text text-center">Mutual fund</p>
            <h2 class="common-title">We also serve you with other investment products like:</h2>
            <ul class="product-listing mt-60">
                <li>
                    <a href="javascript:;" class="product-card">
                        <div class="product-list-icon">
                            <img src="<?php echo base_url('assets/front/images/product-1.png');?>" alt="product-list"
                                class="img-contain" width="65" height="65"">
                        </div>
                        <h3 class=" product-list-name">
                            Fixed Deposits
                            </h3>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="product-card">
                        <div class="product-list-icon">
                            <img src="<?php echo base_url('assets/front/images/product-2.png');?>" alt="product-list"
                                class="img-contain" width="65" height="65">
                        </div>
                        <h3 class="product-list-name">
                            Bond
                        </h3>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="product-card">
                        <div class="product-list-icon">
                            <img src="<?php echo base_url('assets/front/images/product-3.png');?>" alt="product-list"
                                class="img-contain" width="65" height="65">
                        </div>
                        <h3 class="product-list-name">
                            National Pension Scheme (NPS)
                        </h3>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="product-card">
                        <div class="product-list-icon">
                            <img src="<?php echo base_url('assets/front/images/product-4.png');?>" alt="product-list"
                                class="img-contain" width="65" height="65">
                        </div>
                        <h3 class="product-list-name">
                            Post
                        </h3>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="product-card">
                        <div class="product-list-icon">
                            <img src="<?php echo base_url('assets/front/images/product-5.png');?>" alt="product-list"
                                class="img-contain" width="65" height="65">
                        </div>
                        <h3 class="product-list-name">
                            Real Estate
                        </h3>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <!-- why us section -->
    <!-- why us section start-->
    <section class="why-us py-130">
    <div class="container">
            <h2 class="common-title">
                <p class="sub-mutual-text">MUTUAL FUND</p>
                Why Choose Us?
            </h2>
            <div class="why-us-content mt-60">
                <ul class="why-us-content-list">
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/on-cloud.png');?>" alt="on cloud"
                                title="on cloud" class="img-contain" height="46" width="55">
                        </div>
                        <h3 class="sub-title">On Cloud</h3>
                        <p class="sub-text">Systematically View Your Mutual Funds, Stock, And Other Investments Backed
                            Up Every
                            Day And Can Be Viewed Everywhere</p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/team.png');?>" alt="Dedicated Team"
                                title="Dedicated Team" class="img-contain" height="46" width="55">
                        </div>
                        <h3 class="sub-title">Dedicated Team</h3>
                        <p class="sub-text"> Specialized Team of IT Coordinators and Trainers Available 24/7 To Help And
                            Guide You
                            With Nivesh Life Software Application</p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/integration.png');?>"
                                alt="Seamless Integration" title="Seamless Integration" class="img-contain" height="46"
                                width="55">
                        </div>
                        <h3 class="sub-title">Seamless Integration</h3>
                        <p class="sub-text">Take Your Historical Business Onboard And Carry It Forward Digitally </p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/technology.png');?>"
                                alt="echnology First" title="echnology First" class="img-contain" height="46"
                                width="55">
                        </div>
                        <h3 class="sub-title">Technology First</h3>
                        <p class="sub-text"> Providing Digitally Empowering Investment Services That Can Handle Big Data
                            For
                            Thousands Of Financial Advisors</p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/reporting.png');?>"
                                alt="Extensive Reporting" title="Extensive Reporting" class="img-contain" height="46"
                                width="55">
                        </div>
                        <h3 class="sub-title">Extensive Reporting</h3>
                        <p class="sub-text">Comprehensive Data Management System With Diverse Reporting </p>
                    </li>
                    <li class="list-card">
                        <div class="icon-wrap">
                            <img src="<?php echo base_url('assets/front/images/advisory.png');?>" alt="Advisory Support"
                                title="Advisory Support" class="img-contain" height="46" width="55">
                        </div>
                        <h3 class="sub-title">Advisory Support</h3>
                        <p class="sub-text"> Round The Clock After-Sales Services And 24/7 Customer Service For Queries
                            Related To
                            Real-Time Portfolio Management</p>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="dow-app-section">
    <div class="container">
            <div class="dow-sec-inner">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dow-app-text" >
                        <h2 class="common-title sub">Download the <br> app & ride now!</h3>
                            <p class="mt-15 sub-text">Lorem ipsum dolor sit amet</p>
                            <div class="dow-btn-grup mt-40">
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/playstore-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                                <a rel="dofollow" href="javascript:;" target="_blank" class="dow-btn"><img
                                        src="<?php echo base_url('assets/front/images/apple-store-btn.png');?>"
                                        alt="dow-btn" title="dow-btn" class="img-contain" width="165" height="45"
                                        loading="lazy"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="banner-right-img" >
                            <div class="right-img-1">
                                <img src="<?php echo base_url('assets/front/images/screen-1.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="163" height="328" loading="lazy">
                            </div>
                            <div class="right-img-2">
                                <img src="<?php echo base_url('assets/front/images/screen-2.png');?>" alt="banner-left"
                                    title="banner-left" class="img-contain" width="210" height="422" loading="lazy">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why us section end-->
</section>