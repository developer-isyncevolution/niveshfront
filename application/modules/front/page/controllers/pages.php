<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller 
{
    public function __construct() 
    {
        parent::__construct();

        $this->load->library('general');    
    }

	public function privacy_policy()
	{      
        
        $this->template->build('privacy_policy',null);
	}

    public function terms_and_conditions()
    {      
        
        $this->template->build('terms_and_conditions',null);
    }

    public function about()
    {      
        
        $this->template->build('about_us',null);
    }
}