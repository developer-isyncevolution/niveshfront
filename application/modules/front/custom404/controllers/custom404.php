<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom404 extends CI_Controller {

  public function __construct() {

    parent::__construct();

    // load base_url
    $this->load->helper('url');
  }

  public function index(){
    
    if($this->uri->segment(1) == "website-design")
    {
      redirect(base_url('finance-website-design'));
    }elseif ($this->uri->segment(1) == "mutual-fund") {
      redirect(base_url('mutual-fund-software'));
    }elseif ($this->uri->segment(1) == "logo-design") {
      redirect(base_url('finance-logo-design'));
    }else{
      $this->output->set_status_header('404'); 
      $this->template->build('custom404');
    }
 
  }

}