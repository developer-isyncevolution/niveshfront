<!doctype html>
<html>
 <head>
   <title>404 Page Not Found</title>
   <style>
        html,
        body {
            overflow-x: hidden;
            box-sizing: border-box;
            margin: 0px;
            padding: 0px;
        }
        
        a,
        a:hover {
            color: unset;
            text-decoration: unset;
        }
        
        .notfound-page-wrapper {
            height: 100%;
            width: 100%;
            position: relative;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            background-image: linear-gradient(220deg, rgb(36 193 162), rgb(6 55 142));
        }
        
        .not-page-inner {
            background-color: #fff;
            box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.075);
            padding: 50px;
            z-index: 99;
            width: 65%;
            height: auto;
            border-radius: 10px;
            margin: 20px auto;
        }
        
        .not-page-inner .title-404 {
            
            font-size: 170px;
            font-weight: bold;
            /* color: #444444; */
            text-align: center;
            letter-spacing: 1px;
            margin:0;
            background: linear-gradient(to right, #24bfa2, #00367e);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
        
        .not-page-inner .error-msg {
            background: linear-gradient(to right, #24bfa2, #00367e);
            color: transparent;
            -webkit-background-clip: text;
            background-clip: text;
            text-align: center;
            font-size: 64px;
            margin: 20px auto;
        }
        
        .not-page-inner .error-msg-2 {
            text-align: center;
            font-size: 32px;
            margin: 0px auto;
            text-transform: capitalize;
            font-weight: 400;
        }
        
        .error-page-btn-wrapper {
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 20px auto;
        }
        
        .error-page-btn-wrapper .error-button {
            text-align: center;
            display: inline-block;
            border: 3px solid;
            margin: 0px 10px;
            padding: 10px;
            width: 240px;
            min-width: 240px;
            border-image: linear-gradient(to right, rgb(36 193 162), rgb(6 55 142)) 1;
            color: #ffffff !important;
            position: relative;
            transition: 0.5s;
            overflow: hidden;
            z-index: 9;
            font-size: 25px;
        }
        .error-page-btn-wrapper .error-button:before {
            content: "";
            background-image: linear-gradient(to right, rgb(36 193 162), rgb(6 55 142));
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0px;
            top: 0;
            opacity: 1;
            z-index: -1;
        }
        .error-page-btn-wrapper .error-button:after {
            content: "";
            position: absolute;
            border: 0px solid #fff;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            opacity: 0;
            /* transition: 0.5s; */
        }
        .error-page-btn-wrapper .error-button:hover:before {
            opacity: 1;
            left: 0;
        }
        .error-page-btn-wrapper .error-button:hover:after {
            opacity: 1;
            border: 2px solid;
            /* top: 0; */
            /* left: 0; */
            /* bottom: 0; */
            /* right: 0; */
        }
    </style>
 </head>


   <body>

    <div class="notfound-page-wrapper">
        <div class="not-page-inner">
            <h1 class="title-404">
                404
            </h1>
            <h2 class="error-msg">
                Oops! Page not found
            </h2>
            <h3 class="error-msg-2">
                Oops! the page you are looking for does not exit.it might have been moved or deleted.
            </h3>
            <div class="error-page-btn-wrapper">
                <a href="<?= base_url(); ?>" class="error-button">Back To Home</a>
                <!-- <a href="#" class="error-button">Contact Us</a> -->
            </div>
        </div>
    </div>
 </body>
</html>