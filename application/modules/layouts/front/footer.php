<?php
$CI =& get_instance();
$CI->load->library('session');
$company_info = $CI->general->setting_info('Company');
$social_info = $CI->general->setting_info('social');
?>
<!-- footer sectin -->
<footer class=" py-50">
  <div class="container">
    <div class="footer-content">
      <div class="row g-4">
        <div class="col-lg-4">
          <div class="footer-logo">
            <?php $image = base_url('assets/uploads/company_logo/'.$company_info['COMPANY_LOGO']['vValue']);
            ?>
            <img src=" <?php echo $image; ?>"  class="img-contain"title="nivesh life" alt="nivesh life" width="234" height="69">
          </div>
          <div class="footer-info">
            <p class="span-text">503, Venus Benecia, <br>
              Above Nexa Showroom, Opp Rajpath <br> Club, SG Road, Ahmedabad-54.</p>
          
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <p class="footer-title">Quick Links</p>
          <ul class="site-map">
            <li><a rel="dofollow" href="javascript:;" class="footer-iteam-text">Mutual Fund</a></li>
            <li><a rel="dofollow" href="javascript:;" class="footer-iteam-text">Website Design</a></li>
            <li><a rel="dofollow" href="javascript:;" class="footer-iteam-text"> Logo Design</a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-6">
          <p class="footer-title">Help</p>
          <ul class="site-map">
            <li><a rel="dofollow" href="<?php echo base_url('about');?>" class="footer-iteam-text"> About Us</a></li>
            <li><a rel="dofollow" href="<?php echo base_url('contact');?>" class="footer-iteam-text"> Contact Us</a></li>
            <li><a rel="dofollow" href="javascript:;" class="footer-iteam-text"> Blog</a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-6">
          <p class="footer-title">Info Links</p>
          <ul class="site-map">
            <li><a rel="dofollow" href="<?php echo base_url('privacy-policy'); ?>" class="footer-iteam-text">Privacy Policy</a></li>
            <li><a rel="dofollow" href="javascript:;" class="footer-iteam-text">Disclaimer</a></li>
            <li><a rel="dofollow" href="<?php echo base_url('terms-and-conditions'); ?>" class="footer-iteam-text"> Terms & Condition</a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-6">
          <p class="footer-title">Get in touch</p>
          <ul class="site-map">
            <li><a rel="dofollow" href="tel:<?php echo $company_info['COMPANY_NUMBER']['vValue'];?>" class="footer-iteam-text"> <?php echo $company_info['COMPANY_NUMBER']['vValue'];?></a></li>
            <li><a rel="dofollow" href="mailto:<?php echo $company_info['COMPANY_EMAIL']['vValue'];?>" class="footer-iteam-text"> <?php echo $company_info['COMPANY_EMAIL']['vValue'];?></a></li>
          </ul>
          <ul class="get-in-touch">
            <li>
              <a rel="dofollow" href="<?php echo $social_info['SOCIAL_FACEBOOK']['vValue'];?>" class="follow-icon" target="_blank">
                <img src="<?php echo base_url('assets/front/images/facebook.png');?>" alt="follow-icon" height="34" width="34" title="social-media" class="img-contain">
              </a>
            </li>
            <li>
              <a rel="dofollow" href="<?php echo $social_info['SOCIAL_LINKEDIN']['vValue'];?>" class="follow-icon" target="_blank">
                <img src="<?php echo base_url('assets/front/images/linkdin.png');?>" alt="follow-icon" height="34" width="34" title="social-media" class="img-contain">
              </a>
            </li>
            <li>
              <a rel="dofollow" href="<?php echo $social_info['SOCIAL_TWITTER']['vValue'];?>" class="follow-icon" target="_blank">
                <img src="<?php echo base_url('assets/front/images/twitter.png');?>" alt="follow-icon" height="34" width="34" title="social-media" class="img-contain">
              </a>
            </li>
            <li>
              <a rel="dofollow" href="<?php echo $social_info['SOCIAL_YOUTUBE']['vValue'];?>" class="follow-icon" target="_blank">
                <img src="<?php echo base_url('assets/front/images/youtube.png');?>" alt="follow-icon" height="34" width="34" title="social-media" class="img-contain">
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <span class="span-text copy">
            <?php echo str_replace("#CURRENT_YEAR#", date('Y'), $company_info['COPYRIGHTED_TEXT']['vValue']);?>
          </span>
        </div>
      </div>
    </div>
  </div>
  </div>
</footer>