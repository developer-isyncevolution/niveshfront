<?php
$CI =& get_instance();
$CI->load->library('general');
$meta_info = $CI->general->setting_info('Meta');
$company_info = $CI->general->setting_info('Company');
$all_meta_info = $CI->general->meta_info();

$controller         = $this->router->fetch_class();
$method             = $this->router->fetch_method();

//redirect(base_url('maintenance'));
?>

<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if($all_meta_info[0]->vController == $controller)	{ ?>
    <title><?php echo $all_meta_info[0]->vMetaTitle; ?></title>
    <meta name="description" content="<?php echo $all_meta_info[0]->tMetaDescription; ?>">
    <?php } ?>
    <?php if($all_meta_info[1]->vController == $controller){ ?>
    <title><?php echo $all_meta_info[1]->vMetaTitle; ?></title>
    <meta name="description" content="<?php echo $all_meta_info[1]->tMetaDescription; ?>">
    <?php } ?>
    <?php if($all_meta_info[2]->vController == $controller){ ?>
    <title><?php echo $all_meta_info[2]->vMetaTitle; ?></title>
    <meta name="description" content="<?php echo $all_meta_info[2]->tMetaDescription; ?>">
    <?php } ?>
    <?php if($all_meta_info[3]->vController == $controller){ ?>
    <title><?php echo $all_meta_info[3]->vMetaTitle; ?></title>
    <meta name="description" content="<?php echo $all_meta_info[3]->tMetaDescription; ?>">
    <?php } ?>
    <?php if($all_meta_info[4]->vController == $controller){ ?>
    <title><?php echo $all_meta_info[4]->vMetaTitle; ?></title>
    <meta name="description" content="<?php echo $all_meta_info[4]->tMetaDescription; ?>">
    <?php } ?>
    <?php if($controller != "mutual_fund" && $controller != "website_design" && $controller != "logo_design" && $controller != "book_demo" && $controller != "about_us"){ ?>
    <title><?php echo $meta_info['META_TITLE']['vValue'];?></title>
    <meta name="description" content="<?php echo $meta_info['META_DESCRIPTION']['vValue'];?>">
    <?php } ?>


    <meta name="keywords" content="<?php echo $meta_info['META_KEYWORD']['vValue'];?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <?php include_once 'css.php';?>
    <script src="<?php echo base_url('assets/front/js/jquery-3.3.1.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/front/js/datepicker.js'); ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/front/css/datepicker.css');?>" type="text/css" />
    <link type="image/x-icon" href="<?php echo base_url('assets/front/img/favicon.png'); ?>" rel="icon">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Nivesh Life">
    <meta name="twitter:site" content="@niveshlife">
    <meta name="twitter:description"
        content="Nivesh Life has provided B2B & B2C application services to assist organizations and IFA in exceeding their technical requirements in India.">
    <meta name="twitter:image" content="<?php echo base_url('assets/uploads/company_logo/twitter_logo.png');?>">
    <meta name="twitter:image:alt" content="Nivesh Life - Mutual Fund Software">

    <meta property="og:type" content="business.business">
    <meta property="og:title" content="Nivesh Life">
    <meta property="og:url" content="<?php echo base_url();?>">
    <meta property="og:image" content="<?php echo base_url('assets/uploads/company_logo/og_tag.png');?>">
    <meta property="og:description"
        content="Nivesh Life has provided B2B & B2C application services to assist organizations and IFA in exceeding their technical requirements in India.">
    <meta property="business:contact_data:street_address"
        content="503, Venus Benecia, Above Nexa Showroom, Opp Rajpath Club, SG Road">
    <meta property="business:contact_data:locality" content="Ahmedabad">
    <meta property="business:contact_data:region" content="Gujrat">
    <meta property="business:contact_data:postal_code" content="380054">
    <meta property="business:contact_data:country_name" content="India">

    <meta name="google-site-verification" content="aZPgEva6y0kVS7_ax5zJkQdETvJWNvitJ5kBEGsto4I" />

    <?php if($controller == "mutual_fund")	{ ?>
    <link rel="canonical" href="<?php echo base_url('mutual-fund-software');?>" />
    <?php } ?>
    <?php if($controller == "website_design"){ ?>
    <link rel="canonical" href="<?php echo base_url('finance-website-design');?>" />
    <?php } ?>
    <?php if($controller == "logo_design"){ ?>
    <link rel="canonical" href="<?php echo base_url('finance-logo-design');?>" />
    <?php } ?>
    <?php if($controller == "book_demo"){ ?>
    <link rel="canonical" href="<?php echo base_url('book-demo');?>" />
    <?php } ?>
    <?php if($controller == "about_us"){ ?>
    <link rel="canonical" href="<?php echo base_url('about');?>" />
    <?php } ?>
    <?php if($controller != "mutual_fund" && $controller != "website_design" && $controller != "logo_design" && $controller != "book_demo" && $controller != "about_us"){ ?>
    <link rel="canonical" href="<?php echo base_url();?>" />
    <?php } ?>

</head>

<body class="">
    <?php include_once 'header.php';?>
    <main>
        <?php echo $template['body']; ?>
    </main>
    <?php include_once 'footer.php';?>

    <?php include_once 'js.php';?>
    <?php if($this->session->flashdata('success')){?>
    <script type="text/javascript">
    $.Toast("<?php echo $this->session->flashdata('success');?>", "", "success", {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    </script>
    <?php }?>
    <?php if($this->session->flashdata('error')){?>
    <script type="text/javascript">
    $.Toast("<?php echo $this->session->flashdata('error');?>", "", "error", {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    </script>
    <?php }?>
    <script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Website",
        "name": "Nivesh Life",
        "url": "https://www.niveshlife.in/",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "{search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
    </script>

    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "Nivesh Life",
        "url": "https://www.niveshlife.in/",
        "address": "503, Venus Benecia, Above Nexa Showroom, Opp Rajpath Club, SG Road, Ahmedabad",
        "sameAs": [
            "https://www.facebook.com/niveshlife21",
            "https://twitter.com/niveshlife",
            "https://www.youtube.com/channel/UC5quw747YG3scoYvXf9jilw",
            "https://www.instagram.com/niveshlife/",
            "https://www.linkedin.com/company/nivesh-life-pvt-ltd"
        ]
    }
    </script>

</body>

</html>