<?php
$CI =& get_instance();
$CI->load->library('session');
$company_info = $CI->general->setting_info('Company');
$controller = $this->router->fetch_class();
$method = $this->router->fetch_method();
?>
<!-- Navbar Start -->
<header>
  <div class="navbar-wrap">
    <div class="container">
      <div class="navbar">
        <div class="navbar-brand">
          <a rel="dofollow" href="<?php echo base_url('/'); ?>" class="brand-logo">
            <?php $image = base_url('assets/uploads/company_logo/'.$company_info['COMPANY_LOGO']['vValue']);?>
            <img src="<?php echo $image;?>"  class="img-contain" alt="nivesh life" title="nivesh life" width="196" height="58">
          </a>
        </div>
        <div class="nav-mid">
          <ul class="navbar-menu">
            <li class="nav-iteam">
              <a rel="dofollow" href="<?php echo base_url('about');?>" class="nav-link <?php if($controller == "pages" && $method == "about") { echo 'active'; } ?> ">About</a>
            </li>
            <li class="nav-iteam">
              <a rel="dofollow" href="javascript:;" class="nav-link">Package</a>
            </li>
            <li class="nav-iteam">
              <a rel="dofollow" href="javascript:;" class="nav-link">Services</a>
            </li>
            <li class="nav-iteam">
              <a rel="dofollow" href="<?php echo base_url('contact');?>" class="nav-link <?php if($controller == "content" && $method == "contact_us") { echo 'active'; } ?>">Contact</a>
            </li>
          </ul>
          <div class="navbar-btn-grup">
            <a rel="dofollow" href="javascript:;" class="book-btn btn">Book Now</a>
          </div>
        </div>
        <div class="mobile-toggle">
          <button class="btn navbar-toggle" type="button" data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
            <span class="line-toggle"></span>
            <span class="line-toggle"></span>
            <span class="line-toggle"></span></button>
          <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight"
            aria-labelledby="offcanvasRightLabel">
            <div class="cancel-btn">
              <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
              <div class="offcanvash-list">
                <ul class="navbar-menu">
                  <li class="nav-iteam">
                    <a rel="dofollow" href="<?php echo base_url('about'); ?>" class="nav-link active">About</a>
                  </li>
                  <li class="nav-iteam">
                    <a rel="dofollow" href="javascript:;" class="nav-link">Package</a>
                  </li>
                  <li class="nav-iteam">
                    <a rel="dofollow" href="javascript:;" class="nav-link">Services</a>
                  </li>
                  <li class="nav-iteam">
                    <a rel="dofollow" href="<?php echo base_url('contact');?>" class="nav-link">Contact</a>
                  </li>
                </ul>
                <div class="navbar-btn-grup">
                  <a rel="dofollow" href="javascript:;" class="join-btn btn">Join Now</a>
                  <a rel="dofollow" href="javascript:;" class="book-btn btn">Book Now</a>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- Navbar END -->