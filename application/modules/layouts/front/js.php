<script src="<?php echo base_url('/assets/front/plugins/jquery/jquery-3.6.0.min.js');?>"></script> 
<script src="<?php echo base_url('/assets/front/plugins/aos/aos.js');?>"></script>
<script src="<?php echo base_url('/assets/front/plugins/fontawesome/js/all.min.js');?>"></script>
<script src="<?php echo base_url('/assets/front/plugins/bootstrap/bootstrap.bundle.min.js');?>"></script>
<script  src="<?php echo base_url('/assets/front/plugins/fancybox/js/fancybox.umd.js');?>"></script>
<script  src="<?php echo base_url('/assets/front/plugins/owlcarousel/js/owl.carousel.min.js');?>"></script>
<script src="<?php echo base_url('assets/front/js/custom.js');?>"></script> 