<link rel="canonical" href="https://keenthemes.com/metronic" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<link href="<?php echo base_url('assets/mf_panel/plugins/custom/fullcalendar/fullcalendar.bundle.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/mf_panel/plugins/global/plugins.bundle.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/mf_panel/plugins/custom/prismjs/prismjs.bundle.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/mf_panel/css/style.bundle.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/mf_panel/css/themes/layout/header/base/light.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="<?php echo base_url('assets/mf_panel/css/themes/layout/header/menu/light.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/mf_panel/css/themes/layout/brand/dark.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/mf_panel/css/themes/layout/aside/dark.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/mf_panel/css/toast.style.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('assets/mf_panel/css/select2.css');?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>" type="text/css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/mf_panel/css/ecovital_style.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>"> <!-- Ecovital business card -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/mf_panel/css/media_style.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>"> <!-- Media business card -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/mf_panel/css/business_style.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>"> <!-- business card -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/mf_panel/css/logodesign_style.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>"> <!-- Logo Design business card -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/mf_panel/css/klsbuilder_style.css'); ?>?nocache=<?php echo md5(uniqid(rand(), true)); ?>"> <!-- Logo Design business card -->