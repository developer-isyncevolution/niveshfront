<?php
$CI =& get_instance();
$CI->load->library('session');
$CI->load->library('general');
$sub_menu = $this->router->fetch_class();
$sub_menu2 = $this->router->fetch_method();
$arn_logo_header = $CI->general->arn_logo_header();
$eType = $this->session->userdata('eType');
?>
<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <div class="brand flex-column-auto" id="kt_brand">
        <a href="<?php echo base_url('mf_panel/dashboard'); ?>" class="brand-logo">
                <img alt="Logo" src="<?php echo base_url('assets/mf_panel/images/logo_header.png')?>"  class="logos" />
        </a>
        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
            <span class="svg-icon svg-icon svg-icon-xl">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                        <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                    </g>
                </svg>
            </span>
        </button>
    </div>

    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <ul class="menu-nav">
                <li class="menu-item menu-item-active" aria-haspopup="true">
                    <a href="<?php echo base_url('mf_panel/dashboard')?>" class="menu-link">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-home"></i>
                        </span>
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>

                <?php $office_array = array('arn_group', 'arn_communication' ,'customer_communication','calendar'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $office_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-info-circle"></i>
                        </span>
                        <span class="menu-text">Office</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">

                            <li class="menu-item <?php if($sub_menu == "arn_group") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                               <a href="<?php echo base_url('mf_panel/arn_group/arn_group'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">ARN Group</span>
                                </a>
                            </li>

                            <li class="menu-item <?php if($sub_menu == "arn_communication") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                               <a href="<?php echo base_url('mf_panel/arn_communication/arn_communication'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">ARN Communication</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "customer_communication" && $sub_menu2 == "index") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                               <a href="<?php echo base_url('mf_panel/customer_communication/customer_communication'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Customer Communication</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "calendar" || $sub_menu2 == "calendar") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                               <a href="<?php echo base_url('mf_panel/customer_communication/calendar'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Calendar</span>
                                </a>
                            </li>
                                                                                          
                        </ul>
                    </div>
                </li>

                <?php $general_array = array('amc', 'income_slab', 'module_master', 'occupation', 'registrar', 'role','role_module', 'scheme','tax_status', 'transaction', 'instrument', 'investment','redemption','stp_scheme', 'sip_scheme','taxation','pan_exempt','dividend_paymode','milestone_type','token_key', 'quarter_year'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $general_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-info-circle"></i>
                        </span>
                        <span class="menu-text">General</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item <?php if($sub_menu == "token_key") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                               <a href="<?php echo base_url('mf_panel/token_key/token_key'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Token Key</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "amc") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                               <a href="<?php echo base_url('mf_panel/amc/amc'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">AMC</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "income_slab") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/income_slab/income_slab'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Income Slab</span>
                                </a>
                            </li>   
                            <li class="menu-item <?php if($sub_menu == "module_master") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/module_master/module_master'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Module Master</span>
                                </a>
                            </li>                           
                            <li class="menu-item <?php if($sub_menu == "occupation") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/occupation/occupation'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Occupation</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "milestone_type") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/milestone_type/milestone_type');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Milestone Type</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "registrar") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/registrar/registrar'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Registrar</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "role") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/role/role'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Role</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "role_module") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/role_module/add'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Role Module</span>
                                </a>
                            </li>
                           
                            <li class="menu-item <?php if($sub_menu == "scheme") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/scheme/scheme'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Scheme</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "sip_scheme") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/sip_scheme/sip_scheme'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> SIP Scheme</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "stp_scheme") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/stp_scheme/stp_scheme'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> STP Scheme</span>
                                </a>
                            </li>  
                            <li class="menu-item <?php if($sub_menu == "tax_status") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tax_status/tax_status'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Tax Status</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "taxation") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/taxation/taxation'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Indexation</span>
                                </a>
                            </li>   
                            <li class="menu-item <?php if($sub_menu == "pan_exempt") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/pan_exempt/pan_exempt'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Pan Exempt</span>
                                </a>
                            </li>      
                            <li class="menu-item <?php if($sub_menu == "dividend_paymode") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/dividend_paymode/dividend_paymode'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Dividend Paymode</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "quarter_year") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                               <a href="<?php echo base_url('mf_panel/quarter_year/quarter_year'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Quarter Year</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <?php $master_array = array('partner', 'employee','arn', 'package', 'subscription', 'partner_subscription', 'system_scheduler','file_code', 'content_management','invite','promo_code','designation','package_feature','milestone','nfo', 'service', 'sub_service', 'partner_setup', 'scheme_rule'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $master_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fas fa-user-secret"></i>
                        </span>
                        <span class="menu-text">Masters</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item menu-item-parent" aria-haspopup="true">
                                <span class="menu-link">
                                    <span class="menu-text">Masters</span>
                                </span>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "arn") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/arn'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">ARN Master</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "subscription") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/subscription'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">ARN Subscription</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "partner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/partner'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Partner Master</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "partner_subscription") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/partner_subscription'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Partner Subscription</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "partner_setup") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/partner_setup'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Partner Setup</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "employee") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/employee/employee'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Employee Master</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "package") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/package'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Package</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "package_feature") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/package_feature/package_feature');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Package Feature</span>
                                </a>
                            </li>
                           
                            <li class="menu-item <?php if($sub_menu == "system_scheduler") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/system_scheduler/system_scheduler');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">System Scheduler</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "file_code") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/file_code/file_code');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">File Code</span>
                                </a>
                            </li>
                            <?php 
                            $url = 'https://local.niveshlife.com';
                            $current_url = 'https://'.$_SERVER['HTTP_HOST'];
                            if($url == $current_url) { ?> 
                            <li class="menu-item <?php if($sub_menu == "content_management") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/content_management/listing');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">CMS</span>
                                </a>
                            </li> 
                            <?php } ?>                      
                            <li class="menu-item <?php if($sub_menu == "invite") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/invite/invite');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Invite</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "promo_code") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/promo_code/promo_code');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Promo Code</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "designation") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/designation/designation');?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Designation</span>
                                </a>
                            </li>
                            
                            <li class="menu-item <?php if($sub_menu == "nfo") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/nfo'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">NFO</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "service") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/service/service'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Service</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "sub_service") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/sub_service/sub_service'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Sub Service</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "scheme_rule") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/scheme_rule/scheme_rule'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Scheme Rule</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <?php $tools_array = array('scheme','sip_scheme','stp_scheme'); ?>

                <li class="menu-item menu-item-submenu  <?php if(in_array($sub_menu2, $tools_array)) { echo 'menu-item-open'; }  ?>" aria-haspopup="true" data-menu-toggle="hover"> 

                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-tools"></i>
                        </span>
                        <span class="menu-text">Tools</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">                     
                          
                            
                            <li class="menu-item <?php if($sub_menu2 == "sip_scheme") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/sip_scheme'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">1. Upload SIP Scheme</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu2 == "stp_scheme") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/stp_scheme'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">2. Upload STP Scheme</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu2 == "scheme") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/scheme'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">3. Upload Scheme</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </li>
            </ul>
             
        </div>
    </div>
</div>


