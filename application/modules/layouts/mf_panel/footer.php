<?php
$CI = & get_instance();
$CI->load->library('general');
$CI->load->library('session');
$company_info = $CI->general->company_info();
?>
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
   <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-center">
      <div class="text-dark order-2 order-md-1 text-center">
         <a href="javascript:;" target="_blank" class="text-dark-75 text-hover-primary"><?php echo str_replace("#CURRENT_YEAR#", date("Y"), $company_info['COPYRIGHTED_TEXT']['vValue']);?></a>
      </div>
   </div>
</div>