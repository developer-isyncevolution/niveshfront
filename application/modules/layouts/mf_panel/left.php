<?php
$CI =& get_instance();
$CI->load->library('session');
$CI->load->library('general');
$sub_menu = $this->router->fetch_class();
$sub_menu2 = $this->router->fetch_method();
$company_info = $CI->general->setting_info('Company');
$setting = $this->uri->segment(3);

?>
<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <div class="brand flex-column-auto" id="kt_brand">
        <a class="brand-logo" href="<?php echo 'https://www.niveshlife.in/'; ?>">
            <?php $image = base_url('assets/uploads/company_logo/'.$company_info['COMPANY_LOGO']['vValue']);
            ?>
            <img src="<?php echo $image;?>" class="logos"/>	
        </a>	

        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
            <span class="svg-icon svg-icon svg-icon-xl">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                        <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                    </g>
                </svg>
            </span>
        </button>
    </div>

    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <ul class="menu-nav">
                <li class="menu-item menu-item-active" aria-haspopup="true">
                    <a href="<?php echo base_url('mf_panel/dashboard')?>" class="menu-link">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-home"></i>
                        </span>
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>
                <?php $general_array = array('visitor','book_demo','mutual_fund','logo_design','request','contact_us','website_design'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $general_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-info-circle"></i>
                        </span>
                        <span class="menu-text">General</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item <?php if($sub_menu == "visitor") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/visitor/visitor'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> Visitor</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "contact_us") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/contact_us'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Contact Us</span>
                                </a>
                            </li>   
                            <li class="menu-item <?php if($sub_menu == "request") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/request'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Request</span>
                                </a>
                            </li>  
                            <li class="menu-item <?php if($sub_menu == "book_demo") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/book_demo/book_demo'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> Book Demo</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "mutual_fund") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/mutual_fund/mutual_fund'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> Mutual Fund</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "website_design") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/website_design/website_design'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> Website Design</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "logo_design") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/logo_design/logo_design'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> Logo Design</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <?php $master_array = array('banner','about_us','blog','testimonials','page_setting','why_us','app_feature','our_vision','service','meta'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $master_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fas fa-user-secret"></i>
                            <!--end::Svg Icon-->
                        </span>
                        <span class="menu-text">Masters</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item menu-item-parent" aria-haspopup="true">
                                <span class="menu-link">
                                    <span class="menu-text">Masters</span>
                                </span>
                            </li>
                                 
                            <li class="menu-item <?php if($sub_menu == "banner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/banner'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Banner</span>
                                </a>
                            </li> 
                            <li class="menu-item <?php if($sub_menu == "blog") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/blog'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Blog</span>
                                </a>
                            </li> 
                            <li class="menu-item <?php if($sub_menu == "about_us") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/about_us/about_us'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">About Us</span>
                                </a>
                            </li> 
                            <li class="menu-item <?php if($sub_menu == "our_vision") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/our_vision/our_vision'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Vision</span>
                                </a>
                            </li> 
                            <li class="menu-item <?php if($sub_menu == "why_us") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/why_us'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Why Us</span>
                                </a>
                            </li>  
                            <li class="menu-item <?php if($sub_menu == "app_feature") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/app_feature'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">App Feature</span>
                                </a>
                            </li>     
                            <li class="menu-item <?php if($sub_menu == "testimonials") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/testimonials'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Testimonials</span>
                                </a>
                            </li>     
                          
                            <li class="menu-item <?php if($sub_menu == "page_setting") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/page_setting'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Page Setting</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "service") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/service/service'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Service</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "meta") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/meta/meta'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Meta</span>
                                </a>
                            </li> 
                             
                        </ul>
                        
                    </div>
                </li>

                <?php $settings_array = array('setting'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $settings_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fas fa-cog"></i>
                        </span>
                        <span class="menu-text">Setting</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item menu-item-parent" aria-haspopup="true">
                                <span class="menu-link">
                                    <span class="menu-text">Setting</span>
                                </span>
                            </li>
                            <li class="menu-item <?php if($setting == "Appearance") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/setting/index/Appearance'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Appearance</span>
                                </a>                                
                            </li> 
                            <li class="menu-item <?php if($setting == "Email") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a  class="menu-link" href="<?php echo base_url('mf_panel/setting/index/Email');?>">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Email Settings</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($setting == "Company") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a  class="menu-link" href="<?php echo base_url('mf_panel/setting/index/Company');?>">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Company Info</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($setting == "Meta") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a  class="menu-link" href="<?php echo base_url('mf_panel/setting/index/Meta');?>">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Meta Info</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($setting == "Social") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a  class="menu-link" href="<?php echo base_url('mf_panel/setting/index/Social');?>">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Social Info</span>
                                </a>
                            <!-- </li>
                            <li class="menu-item" aria-haspopup="true">
                                <a  class="menu-link" href="<?php echo base_url('mf_panel/setting/index/Payment');?>">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Payment Info</span>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </li>
    
                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="<?php echo base_url('mf_panel/login/logout'); ?>" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/General/Settings-1.svg-->
                            <i class="fas fa-sign-out-alt"></i>
                            <!--end::Svg Icon-->
                        </span>
                        <span class="menu-text">Logout</span>
                    </a>
                </li>
            </ul>
            <!--end::Menu Nav-->
        </div>
        <!--end::Menu Container-->
    </div>
    <!--end::Aside Menu-->
</div>


