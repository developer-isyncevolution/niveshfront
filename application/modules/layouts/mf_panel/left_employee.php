<?php
$CI =& get_instance();
$CI->load->library('session');
$CI->load->library('general');
$general_info = $CI->general->general_info();
$arn_logo_header = $CI->general->arn_logo_header();
$sub_menu = $this->router->fetch_class();
$sub_menu2 = $this->router->fetch_method();
$eType  = $this->session->userdata('eType');
?>
<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <div class="brand flex-column-auto" id="kt_brand">
        <a href="<?php echo base_url('mf_panel/dashboard'); ?>" class="brand-logo">
            <?php if(!empty($arn_logo_header)) { ?>
                <img alt="Logo" src="<?php echo base_cdn.'assets/uploads/arn/'.$arn_logo_header->iARNId.'/'.$arn_logo_header->vImage?>"  class="logos" />
            <?php }else{ ?>
                <img alt="Logo" src="<?php echo base_url('assets/mf_panel/images/logo_header.png')?>"  class="logos" />
            <?php } ?>
        </a>
        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
            <span class="svg-icon svg-icon svg-icon-xl">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                        <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                    </g>
                </svg>
            </span>
        </button>
    </div>
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <ul class="menu-nav">
                <li class="menu-item menu-item-active" aria-haspopup="true">
                    <a href="<?php echo base_url('mf_panel/dashboard')?>" class="menu-link">
                        <span class="svg-icon menu-icon">
                            <i class="fa fa-home"></i>
                        </span>
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>
                <?php $general_array = array('media_image','media_video', 'kyc', 'invite', 'file_code','courier','bulk_challan', 'bulk_email','milestone','top_client'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $general_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-info-circle"></i>
                        </span>
                        <span class="menu-text" style="text-align: left;">General</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item <?php if($sub_menu == "media_image") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/media_image/media_image'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> Media Image</span>
                                </a>
                            </li>  
                            <li class="menu-item <?php if($sub_menu == "media_video") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/media_video/media_video'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"> Media Video</span>
                                </a>
                            </li>
                            <?php if($arn_logo_header->vARN == "142096") { ?>
                            <li class="menu-item <?php if($sub_menu == "kyc") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/kyc/kyc'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">KYC</span>
                                </a>
                            </li>
                            <?php } ?> 
                            <?php if($data_subscription->vPackage == "Diamond") { ?>
                                <li class="menu-item <?php if($sub_menu == "invite") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                    <a href="<?php echo base_url('mf_panel/invite/invite'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                        <span class="menu-text"> Invite</span>
                                    </a>
                                </li>
                            <?php } ?>  
                            <li class="menu-item <?php if($sub_menu == "file_code") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/file_code/listing'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">File Code</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "courier") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/courier/courier'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Courier</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "bulk_challan") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/bulk_challan/bulk_challan'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Bulk Challan</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "bulk_email") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/bulk_email/bulk_email');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Bulk Email</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "milestone") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/milestone/milestone'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Goal</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "top_client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/top_client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Top Client</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <?php $master_array = array('client', 'demat_client', 'channel_partner', 'branch', 'relationship_manager', 'family', 'super_partner', 'employee','video_kyc','physical_client','map_client','un_map_client'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $master_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fas fa-user-secret"></i>
                        </span>
                        <span class="menu-text" style="text-align: left;">Masters</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item menu-item-parent" aria-haspopup="true">
                                <span class="menu-link">
                                    <span class="menu-text">Masters</span>
                                </span>
                            </li>

                            <?php if(!empty($data_subscription->package_detail)) { ?>
                                <?php foreach ($data_subscription->package_detail as $key => $value) { ?>
                                    <?php if($value->vRole == "Client") { ?>
                                        <li class="menu-item <?php if($sub_menu == "client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Client</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($value->vRole == "ChannelPartner") { ?>
                                        <li class="menu-item <?php if($sub_menu == "channel_partner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/channel_partner/channel_partner'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Channel Partner</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($value->vRole == "SuperPartner") { ?>
                                        <li class="menu-item <?php if($sub_menu == "super_partner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/super_partner/super_partner'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">BDM</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($value->vRole == "RelationshipManager") { ?>
                                        <li class="menu-item <?php if($sub_menu == "relationship_manager") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/relationship_manager/relationship_manager'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Relationship Manager</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($value->vRole == "Branch") { ?>
                                        
                                    <?php } ?>
                                    <?php if($value->vRole == "Employee") { ?>
                                        
                                    <?php } ?>
                                <?php } ?>
                                <!-- <li class="menu-item <?php if($sub_menu == "demat_client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                        <a href="<?php echo base_url('mf_panel/demat_client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">Demat Client</span>
                                        </a>
                                    </li> -->
                                    <li class="menu-item <?php if($sub_menu == "family") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                        <a href="<?php echo base_url('mf_panel/family/family'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">Client Family</span>
                                        </a>
                                    </li>
                            <?php } ?>
                            <li class="menu-item <?php if($sub_menu == "task") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/task/task'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Task</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "physical_client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/physical_client/physical_client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Physical Client</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "map_client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/map_client/map_client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Map Client</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "un_map_client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/un_map_client/un_map_client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Un Map Client</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                
                <?php $manual_array = array('manual_mutualfund', 'manual_fixed_deposit', 'manual_postal', 'manual_stock', 'manual_realestate', 'manual_bond', 'manual_insurance_policy'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $manual_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fa fa-edit"></i>
                        </span>
                        <span class="menu-text" style="text-align: left;">Manual Entry</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item <?php if($sub_menu == "manual_mutualfund") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/manual_mutualfund/manual_mutualfund'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text" >Mutual Fund</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "manual_fixed_deposit") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/manual_fixed_deposit/manual_fixed_deposit'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">FD / Post/ Bond</span>
                                </a>
                            </li>
                            <!-- <li class="menu-item <?php if($sub_menu == "manual_postal") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url("admin/manual_postal/manual_postal") ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Postal</span>
                                </a>
                            </li> -->
                            <!-- <li class="menu-item <?php if($sub_menu == "manual_stock") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/manual_stock/manual_stock');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Stock</span>
                                </a>
                            </li> -->
                            <li class="menu-item <?php if($sub_menu == "manual_realestate") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/manual_realestate/manual_realestate') ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Real Estate</span>
                                </a>
                            </li>
                            <!-- <li class="menu-item <?php if($sub_menu == "manual_bond") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/manual_bond/manual_bond');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Bond</span>
                                </a>
                            </li> -->
                            <li class="menu-item <?php if($sub_menu == "manual_insurance_policy") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/manual_insurance_policy/manual_insurance_policy');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Insurance Policy</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <?php $client_array = array('realized', 'un_realized', 'order_report', 'mandate_report', 'sip_report','transaction_report'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu2, $client_array) || in_array($sub_menu, $client_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-chart-pie"></i>
                        </span>
                        <span class="menu-text" style="text-align: left;">Client Base Reports</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item <?php if($sub_menu == "order_report") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/order_report'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Order Report</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "mandate_report") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/mandate_report'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Mandate Report</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "valuation_report") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/valuation_report'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Valuation Report</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu2 == "capital_gain_realized") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/capital_gain_report/capital_gain_realized'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Capital Gain Realized</span>
                                </a>
                            </li>
                            <!-- <li class="menu-item <?php if($sub_menu2 == "realized") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/capital_gain_report/realized'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Capital Gain Realized</span>
                                </a>
                            </li> -->
                            <!-- <li class="menu-item <?php if($sub_menu2 == "un_realized") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/capital_gain_report/un_realized'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Capital Gain Un Realized</span>
                                </a>
                            </li> -->
                            <li class="menu-item <?php if($sub_menu == "transaction_report") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/transaction_report/transaction_report'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Transaction Report</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "sip_report") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/sip_report'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">SIP Report</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <?php $arn_base_report = array('brokerage_package', 'brokerage_report', 'brokerage_report_amc', 'aum', 'aum_scheme_wise', 'aum_amc_wise','nav_report'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $arn_base_report)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">                   
                 <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fas fa-folder"></i>
                        </span>
                        <span class="menu-text" style="text-align: left;">ARN Base Reports</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item <?php if($sub_menu2 == "channel_partner_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/brokerage_report/channel_partner_brokerage');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">CP Brokerage</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu2 == "super_partner_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/brokerage_report/super_partner_brokerage');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">BDM Brokerage</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu2 == "relationship_manager_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/brokerage_report/relationship_manager_brokerage');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">RM Brokerage</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "brokerage_report_amc") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/brokerage_report_amc/brokerage_report_amc');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">AMC Wise Brokerage</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "aum" && $sub_menu2 == "index") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/aum'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">AUM Report</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "aum_scheme_wise" && $sub_menu2 == "index") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/aum_scheme_wise'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">AUM Scheme Wise</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "aum_amc_wise" && $sub_menu2 == "index" || $sub_menu2 == "amc_wise_folio") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/aum_amc_wise'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">AUM AMC Wise</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "nav_report" && $sub_menu2 == "index") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/nav_report'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">NAV Report</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <?php $stock_base_report = array('stock_deficit', 'stock_brokerage_brief','stock_brokerage_report','stock_brokerage_summary'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $stock_base_report)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">                   
                 <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fas fa-folder"></i>
                        </span>
                        <span class="menu-text pl-2" style="text-align: left;">Stock Base Reports</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item <?php if($sub_menu == "stock_brokerage_brief") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/stock_brokerage_brief/stock_brokerage_brief');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Brokerage Brief</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "stock_deficit") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/stock_deficit/stock_deficit');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Stock Deficit</span>
                                </a>
                            </li>
                            <?php if(!empty($data_subscription->package_detail)) { ?>
                                <?php foreach ($data_subscription->package_detail as $key => $value) { ?>
                                    <?php if($value->vRole == "ChannelPartner") { ?>
                                        <li class="menu-item <?php if($sub_menu2 == "channel_partner_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_report/channel_partner_brokerage');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">CP Brokerage</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "stock_brokerage_summary" && $sub_menu2 == "stock_brokerage_summary_channel_partner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_summary/stock_brokerage_summary_channel_partner');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">CP Summary</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "stock_brokerage_summary" && $sub_menu2 == "stock_brokerage_summary_channel_partner_final") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_summary/stock_brokerage_summary_channel_partner_final');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">CP Summary Final</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($value->vRole == "SuperPartner" || $value->vRole == "BusinessDevelopmentManager") { ?>
                                        <li class="menu-item <?php if($sub_menu2 == "super_partner_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_report/super_partner_brokerage');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">BDM Brokerage</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "stock_brokerage_summary" && $sub_menu2 == "stock_brokerage_summary_super_partner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_summary/stock_brokerage_summary_super_partner'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">BDM Summary</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($value->vRole == "RelationshipManager") { ?>
                                        <li class="menu-item <?php if($sub_menu2 == "relationship_manager_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_report/relationship_manager_brokerage');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">RM Brokerage</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "stock_brokerage_summary" && $sub_menu2 == "stock_brokerage_summary_relationship_manager") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_summary/stock_brokerage_summary_relationship_manager');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">RM Summary</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($value->vRole == "Branch") { ?>
                                        <li class="menu-item <?php if($sub_menu2 == "branch_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_report/branch_brokerage');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">BM Brokerage</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "stock_brokerage_summary" && $sub_menu2 == "stock_brokerage_summary_branch") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/stock_brokerage_summary/stock_brokerage_summary_branch');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">BM Summary</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </li>

                <?php if($this->session->userdata('eUserType') == "Live") { ?>
                <?php $tools_array = array('client','mandate','sip','swp','stp','brokerage', 'brokerage_clawback','demat_client','transaction','nav','ticob','isin','ticob_folio', 'aum','user_hierarchy', 'channel_partner', 'super_partner', 'relationship_manager', 'branch', 'employee'); ?>
                <li class="menu-item menu-item-submenu  <?php if(in_array($sub_menu2, $tools_array)) { 
                    if($sub_menu =='tools') { echo 'menu-item-open'; } }  ?>" aria-haspopup="true" data-menu-toggle="hover"> 
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-tools"></i>
                        </span>
                        <span class="menu-text pl-2" style="text-align: left;">Tools</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">

                        <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                            <a href="<?php echo base_url('mf_panel/tools/client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                <span class="menu-text">Upload BSE Client</span>
                            </a>
                        </li>

                        <?php if(!empty($data_subscription->package_detail)) { ?>
                            <?php foreach ($data_subscription->package_detail as $key => $value) { ?>
                                
                                <?php if($value->vRole == "ChannelPartner") { ?>
                                    <li class="menu-item <?php if($sub_menu == "tools" && $sub_menu2 == "channel_partner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                        <a href="<?php echo base_url('mf_panel/tools/channel_partner'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                            <span class="menu-text">Upload CP</span>
                                        </a>
                                    </li>
                                <?php } ?>

                                <?php if($value->vRole == "SuperPartner") { ?>
                                    <li class="menu-item <?php if($sub_menu == "tools" && $sub_menu2 == "super_partner") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                        <a href="<?php echo base_url('mf_panel/tools/super_partner'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                            <span class="menu-text">Upload BDE</span>
                                        </a>
                                    </li>
                                <?php } ?>

                                <?php if($value->vRole == "RelationshipManager") { ?>
                                    <li class="menu-item <?php if($sub_menu == "tools" && $sub_menu2 == "relationship_manager") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                        <a href="<?php echo base_url('mf_panel/tools/relationship_manager'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                            <span class="menu-text">Upload RM</span>
                                        </a>
                                    </li>
                                <?php } ?>

                                <?php if($value->vRole == "Branch") { ?>
                                    <li class="menu-item <?php if($sub_menu == "tools" && $sub_menu2 == "branch") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                        <a href="<?php echo base_url('mf_panel/tools/branch'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                            <span class="menu-text">Upload Branch</span>
                                        </a>
                                    </li>
                                <?php } ?>

                                <?php if($value->vRole == "Employee") { ?>
                                    <li class="menu-item <?php if($sub_menu == "tools" && $sub_menu2 == "employee") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                        <a href="<?php echo base_url('mf_panel/tools/employee'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                            <span class="menu-text">Upload Employee</span>
                                        </a>
                                    </li>
                                <?php } ?>

                            <?php } ?>
                        <?php } ?>

                        <li class="menu-item <?php if($sub_menu == "tools" && $sub_menu2 == "mandate") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                            <a href="<?php echo base_url('mf_panel/tools/mandate'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                <span class="menu-text">Upload BSE Mandate</span>
                            </a>
                        </li>
                        <li class="menu-item <?php if($sub_menu == "tools" && $sub_menu2 == 'sip') { 
                            echo 'menu-item-active'; } ?>" aria-haspopup="true">
                            <a href="<?php echo base_url('mf_panel/tools/sip'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                <span class="menu-text">Upload BSE SIP</span>
                            </a>
                        </li>
                            <!-- <li class="menu-item <?php if($sub_menu == "tools" & $sub_menu2 == "swp") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/swp'); ?>" class="menu-link">
                                    <span class="menu-text">Upload BSE SWP</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "stp") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/stp'); ?>" class="menu-link">
                                    <span class="menu-text">Upload BSE STP</span>
                                </a>
                            </li> -->
                            <?php if(!empty($data_subscription->package_detail)) { ?>
                                <?php foreach ($data_subscription->package_detail as $key => $value) { ?>
                                    <?php if($value->vRole == "ChannelPartner") { ?>
                                        <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/tools/brokerage'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">Upload MF Brokerage</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "stock_brokerage") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/tools/stock_brokerage'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">Upload Stock Brokerage</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "stock_deficit") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/tools/stock_deficit'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">Upload Stock Deficit</span>
                                            </a>
                                        </li>
                                        <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "deficit_excel") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/tools/deficit_excel'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                                <span class="menu-text">Upload Deficit Excel</span>
                                            </a>
                                        </li>
                                        <!-- <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "brokerage_clawback") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                            <a href="<?php echo base_url('mf_panel/tools/brokerage_clawback'); ?>" class="menu-link">
                                                <span class="menu-text">Upload Brokerage ClawBack</span>
                                            </a>
                                        </li> -->
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            
                            <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "demat_client") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/demat_client'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Upload Demat Client</span>
                                </a>
                            </li>

                            <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "transaction") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/transaction'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Upload Transaction</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "nav") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/nav'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Upload NAV</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "ticob") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/ticob'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Upload TICOB Brief</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "tools" &&  $sub_menu2 == "isin") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/isin'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Missing ISIN List</span>
                                </a>
                            </li>
                            <li class="menu-item" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/investor_master'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Upload InvestorMaster</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu2 == "ticob_folio") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/ticob_folio'); ?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">TICOB Folio List</span>
                                </a>
                            </li>
                            <!-- <li class="menu-item <?php if($sub_menu2 == "aum") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/aum'); ?>" class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Upload AUM</span>
                                </a>
                            </li> -->
                            <!-- <li class="menu-item <?php if($sub_menu2 == "sip_report") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/tools/sip_report'); ?>" class="menu-link">
                                    <span class="menu-text">Upload SIP</span>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </li>
                <?php } ?>

                <?php $general_array = array('sms_message', 'email_message', 'email_scheduler','notification'); ?>
                <li class="menu-item menu-item-submenu <?php if(in_array($sub_menu, $general_array)) { echo 'menu-item-open'; } ?>" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                           <i class="fas fa-cog"></i>
                        </span>
                        <span class="menu-text" style="text-align: left;">Setting</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">                                                    
                            <li class="menu-item <?php if($sub_menu == "notification") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/notification/notification');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Notification</span>
                                </a>
                            </li> 
                            <li class="menu-item <?php if($sub_menu == "email_scheduler") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/email_scheduler/email_scheduler');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Email Schedular</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "sms_message") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/sms_message/sms_message');?>" class="menu-link" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">SMS Message</span>
                                </a>
                            </li>
                            <li class="menu-item <?php if($sub_menu == "email_message") { echo 'menu-item-active'; } ?>" aria-haspopup="true">
                                <a href="<?php echo base_url('mf_panel/email_message/email_message')?>" class="menu-link menu-toggle" style="text-align: left; padding-left: 20px !important;">
                                    <span class="menu-text">Email Message</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <?php
                $useragent=$_SERVER['HTTP_USER_AGENT'];
                if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) { 
                    //echo "mobile";
                } else { ?>
                <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="<?php echo base_url('mf_panel/login/logout'); ?>" class="menu-link menu-toggle">
                        <span class="svg-icon menu-icon">
                            <i class="fas fa-sign-out-alt"></i>
                        </span>
                        <span class="menu-text" style="text-align: left;">Logout</span>
                    </a>
                </li>
                <?php } ?>
               
            </ul>
        </div>
    </div>
</div>


