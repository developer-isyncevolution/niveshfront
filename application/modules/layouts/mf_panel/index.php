<?php
$CI =& get_instance();
$CI->load->library('general');
$CI->load->library('session');
$general_info = $CI->general->general_info();
$iRoleId = $CI->session->userdata('iRoleId');
$eType = $CI->session->userdata('eType');
$company_info = $CI->general->setting_info('Company');
$appearance_info = $CI->general->setting_info('Appearance');

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $appearance_info['CPANEL_TITLE']['vValue'];?></title>
		<?php include_once 'css.php';?>
		<script src="<?php echo base_url('assets/mf_panel/js/jquery-3.3.1.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/mf_panel/js/datepicker.js'); ?>"></script>
		<link rel="stylesheet" href="<?php echo base_url('assets/mf_panel/css/datepicker.css');?>" type="text/css" />
  		<!-- <link rel="shortcut icon" href="<?php echo base_url('assets/front/img/favicon.png');?>"> -->
  		<link rel="shortcut icon" href="<?php echo base_url('assets/uploads/company_logo/'.$company_info['COMPANY_FAVICON']['vValue']);?>">
  	</head>
		<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
			<div class="main">
				<div id="kt_header_mobile" class="header-mobile align-items-center header-mobile-fixed">
					<div>
						<button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
							<span></span>
						</button>
					</div>
					<a class="brand-logo" href="<?php echo 'https://www.niveshlife.in/'; ?>">
						<?php $image = base_url('assets/uploads/company_logo/'.$company_info['COMPANY_LOGO']['vValue']);
						?>
						<img src="<?php echo $image;?>" class="logos"/>	
					</a>	
					<div class="d-flex align-items-center">
						
						<button class="btn p-0 burger-icon ml-4" id="kt_header_mobile_toggle">
							<span></span>
						</button>
						<!-- <button class="btn btn-hover-text-primary p-0 ml-2" id="kt_header_mobile_topbar_toggle">
							<span class="svg-icon svg-icon-xl">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<polygon points="0 0 24 0 24 24 0 24" />
										<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
										<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
									</g>
								</svg>
							</span>
						</button> -->
						<div class="dropdown">
				<div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="false">
					<div class="topbar-item">
						<div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
							<span class="svg-icon svg-icon-lg d-block d-md-none d-lg-none">
								<!--begin::Svg Icon | path:/metronic/theme/html/demo8/dist/assets/media/svg/icons/General/User.svg-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<polygon points="0 0 24 0 24 24 0 24"></polygon>
										<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
										<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
									</g>
								</svg>
								<!--end::Svg Icon-->
							</span>
							<span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
							<span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">Ashwamegh Venture</span>
							<span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
							</span>
						</div>
					</div>
				</div>		
				<div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="width: 337px;">
					<div class="tab-content">
						<div class="tab-pane active show p-8" id="topbar_notifications_notifications" role="tabpanel" style="padding: 0px !important;">
							<div class="scroll pr-7 mr-n7" data-scroll="true" data-height="300" data-mobile-height="200" style="padding:26px 0px 4px 26px !important;">			
								<div class="d-flex align-items-center mb-6">
									<div class="symbol symbol-40 symbol-light-warning mr-5">
										<span class="symbol-label">
											<span class="fas fa-user">	
											</span>
										</span>
									</div>
									<div class="d-flex flex-column font-weight-bold">
											<a href="<?php echo base_url('mf_panel/profile/personal_information'); ?>" class="menu-link Profjelmenu-y">
												<span class="text-dark text-hover-warning mb-1 font-size-lg">Profile</span>
											</a>
									</div>											
								</div>
								<div class="d-flex align-items-center mb-6">
									<div class="symbol symbol-40 symbol-light-warning mr-5">
										<span class="symbol-label">
											<span class="fas fa-sign-out-alt">	
											</span>
										</span>
									</div>
									<div class="d-flex flex-column font-weight-bold">
											<a href="<?php echo base_url('mf_panel/login/logout'); ?>" class="menu-link menu-toggle">
												<span class="text-dark text-hover-warning mb-1 font-size-lg">Logout</span>
											</a>
									</div>											
								</div>		
							</div>							
						</div>					
					</div>
				</div>
			</div>
					</div>
				</div>
				<div class="d-flex flex-column flex-root">
					<div class="d-flex flex-row flex-column-fluid page">
						<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
							<?php include_once 'header.php';?>
							<?php if($eType == 'Admin') { ?>
								<?php include_once 'left.php';?>
							<?php } ?>
							<?php if($eType == 'Client') { ?>
								<?php include_once 'left_client.php';?>
							<?php } ?>
							<?php if($eType == 'ChannelPartner') { ?>
								<?php include_once 'left_channel_partner.php';?>
							<?php } ?>
							<?php if($eType == 'SuperPartner') {?>
								<?php include_once 'left_super_partner.php';?>
							<?php } ?>
							<?php if($eType == 'Branch') {?>
								<?php include_once 'left_branch_manager.php';?>
							<?php } ?>
							<?php if($eType == 'RelationshipManager') {?>
								<?php include_once 'left_relationship_manager.php';?>
							<?php } ?>
							<?php if($eType == 'Employee') {?>
								<?php include_once 'left_employee.php';?>
							<?php } ?>
							<div id="content" class="app-content" role="main">
								<div class="app-content-body ">
									<?php echo $template['body'];?>
							  	</div>
							</div>
		  					<?php include_once 'footer.php';?>
							<?php include_once 'js.php';?>
							<?php if($this->session->flashdata('success')){?>
								<script type="text/javascript">
									$.Toast("<?php echo $this->session->flashdata('success');?>", "", "success", {
								        has_icon:true,
								        has_close_btn:true,
										stack: true,
								        fullscreen:true,
								        timeout:8000,
								        sticky:false,
								        has_progress:true,
								        rtl:false,
								    });
								</script>
							<?php }?>
							<?php if($this->session->flashdata('error')){?>
								<script type="text/javascript">
									$.Toast("<?php echo $this->session->flashdata('error');?>", "", "error", {
								        has_icon:true,
								        has_close_btn:true,
										stack: true,
								        fullscreen:true,
								        timeout:8000,
								        sticky:false,
								        has_progress:true,
								        rtl:false,
								    });
								</script>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
		</body>
</html>