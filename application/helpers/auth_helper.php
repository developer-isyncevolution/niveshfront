<?php
$CI = & get_instance();
$CI->load->library('session');
$CI->load->helper('cookie');
$panel = $CI->config->item('panel');
$controller_name = $this->router->fetch_class();

if($controller_name != "scheduler")
{
	if($panel == "mf_panel"){
		$iUserId = $CI->session->userdata('iUserId');
		$controller = $CI->router->class;

		$action = array("login", "logout", "non_client_kyc");

		if(empty($iUserId)) {
		    if(!in_array($controller, $action)) {
		        redirect(base_url('mf_panel/login'));
		    }
		}
	}

	if(empty(get_cookie('cart_session'))) {
		$uniqid = uniqid();
		setcookie("cart_session", $uniqid,"0","/");
	}

	$CI->lang->load('message','EN');
}
?>